package com.amis.company;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: COMPANY 
  Generation date: Mon Apr 25 16:16:46 CEST 2016 

******************************************************************************/

import com.ldx.util.Printer;

import java.util.List;
import java.util.ArrayList;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * TEmployer data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class TEmployer extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for TEmployer.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public TEmployer(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type TEmployer.
	 */
	static class Allocator implements TypeAllocator<TEmployer> {
		/**
		 * method for getting a new instance of type TEmployer.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public TEmployer newInstance(String elementName, ComplexDataType parent) {
			return new TEmployer(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for Name element. 
	 *  @serial
	 */	
	private String m_name = null;
	
	/** element item for EmployerNr element. 
	 *  @serial
	 */	
	private String m_employerNr = null;
	
	/** element item for Address element. 
	 *  @serial
	 */	
	private AddressType_d0e16_a21 m_address = null;
	
	/** list of Employee element. 
	 *  @serial
	 */	
	private List<TEmployee> m_employeeList = new ArrayList<TEmployee>();
	
	/**
	 * Get the embedded Name element.
	 * @return the item.
	 */
	public String getName() {
		return m_name;
	}
		
	/**
	 * This method sets (overwrites) the element Name.
	 * @param data the item that needs to be added.
	 */
	void setName(String data) {
		m_name = data;
	}
		
	/**
	 * Get the embedded EmployerNr element.
	 * @return the item.
	 */
	public String getEmployerNr() {
		return m_employerNr;
	}
		
	/**
	 * This method sets (overwrites) the element EmployerNr.
	 * @param data the item that needs to be added.
	 */
	void setEmployerNr(String data) {
		m_employerNr = data;
	}
		
	/**
	 * Get the embedded Address element.
	 * @return the item.
	 */
	public AddressType_d0e16_a21 getAddress() {
		return m_address;
	}
		
	/**
	 * This method sets (overwrites) the element Address.
	 * @param data the item that needs to be added.
	 */
	void setAddress(AddressType_d0e16_a21 data) {
		m_address = data;
	}
		
	/**
	 * Get the embedded list of Employee elements.
	 * @return list of items.
	 */
	public List<TEmployee> getEmployees() {
		return m_employeeList;
	}
		
	/**
	 * This method adds data to the list of Employee.
	 * @param data the item that needs to be added.
	 */
	void setEmployee(TEmployee data) {
		m_employeeList.add(data);
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_name, ((TEmployer)that).m_name))
			return false;
		
		if (!Compare.equals(m_employerNr, ((TEmployer)that).m_employerNr))
			return false;
		
		if (!Compare.equals(m_address, ((TEmployer)that).m_address))
			return false;
		
		if (!Compare.equals(m_employeeList, ((TEmployer)that).m_employeeList))
			return false;
		
		return true;
	}	

  
  
  
	/**
	 *	Printing method, prints the XML element to a Printer.
	 *  This method prints an XML fragment starting from tEmployer.
	 *
	 *  @param out the Printer that the element is printed to
	 *  @see com.ldx.util.Printer
	 */
	protected void printElements(Printer out) {
		super.printElements(out);
  
		if (m_name != null) {
			out.print("\n<Name>");
			out.print(m_name);
			out.print("</Name>\n");
		}
		
		if (m_employerNr != null) {
			out.print("\n<EmployerNr>");
			out.print(m_employerNr);
			out.print("</EmployerNr>\n");
		}
		
		if (m_address != null)
			m_address.print(out);
		else {
			// out.print("<Address>null</Address>");
		}
		
		if (m_employeeList != null)
			for(TEmployee l_Employee : m_employeeList) {
				l_Employee.print(out);
			}
		else {
			// out.print("<Employee>null</Employee>");
		}
		
	}
}
