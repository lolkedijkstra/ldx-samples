package com.amis.company;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: COMPANY 
  Generation date: Mon Apr 25 16:16:46 CEST 2016 

******************************************************************************/

import com.ldx.util.Printer;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * TEmployee data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class TEmployee extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for TEmployee.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public TEmployee(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type TEmployee.
	 */
	static class Allocator implements TypeAllocator<TEmployee> {
		/**
		 * method for getting a new instance of type TEmployee.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public TEmployee newInstance(String elementName, ComplexDataType parent) {
			return new TEmployee(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for EmployeeNr element. 
	 *  @serial
	 */	
	private String m_employeeNr = null;
	
	/** element item for DOB element. 
	 *  @serial
	 */	
	private String m_dOB = null;
	
	/** element item for FamilyName element. 
	 *  @serial
	 */	
	private String m_familyName = null;
	
	/** element item for Initials element. 
	 *  @serial
	 */	
	private String m_initials = null;
	
	/** element item for Gender element. 
	 *  @serial
	 */	
	private String m_gender = null;
	
	/** element item for Nat element. 
	 *  @serial
	 */	
	private String m_nat = null;
	
	/** element item for EmploymentDate element. 
	 *  @serial
	 */	
	private String m_employmentDate = null;
	
	/**
	 * Get the embedded EmployeeNr element.
	 * @return the item.
	 */
	public String getEmployeeNr() {
		return m_employeeNr;
	}
		
	/**
	 * This method sets (overwrites) the element EmployeeNr.
	 * @param data the item that needs to be added.
	 */
	void setEmployeeNr(String data) {
		m_employeeNr = data;
	}
		
	/**
	 * Get the embedded DOB element.
	 * @return the item.
	 */
	public String getDOB() {
		return m_dOB;
	}
		
	/**
	 * This method sets (overwrites) the element DOB.
	 * @param data the item that needs to be added.
	 */
	void setDOB(String data) {
		m_dOB = data;
	}
		
	/**
	 * Get the embedded FamilyName element.
	 * @return the item.
	 */
	public String getFamilyName() {
		return m_familyName;
	}
		
	/**
	 * This method sets (overwrites) the element FamilyName.
	 * @param data the item that needs to be added.
	 */
	void setFamilyName(String data) {
		m_familyName = data;
	}
		
	/**
	 * Get the embedded Initials element.
	 * @return the item.
	 */
	public String getInitials() {
		return m_initials;
	}
		
	/**
	 * This method sets (overwrites) the element Initials.
	 * @param data the item that needs to be added.
	 */
	void setInitials(String data) {
		m_initials = data;
	}
		
	/**
	 * Get the embedded Gender element.
	 * @return the item.
	 */
	public String getGender() {
		return m_gender;
	}
		
	/**
	 * This method sets (overwrites) the element Gender.
	 * @param data the item that needs to be added.
	 */
	void setGender(String data) {
		m_gender = data;
	}
		
	/**
	 * Get the embedded Nat element.
	 * @return the item.
	 */
	public String getNat() {
		return m_nat;
	}
		
	/**
	 * This method sets (overwrites) the element Nat.
	 * @param data the item that needs to be added.
	 */
	void setNat(String data) {
		m_nat = data;
	}
		
	/**
	 * Get the embedded EmploymentDate element.
	 * @return the item.
	 */
	public String getEmploymentDate() {
		return m_employmentDate;
	}
		
	/**
	 * This method sets (overwrites) the element EmploymentDate.
	 * @param data the item that needs to be added.
	 */
	void setEmploymentDate(String data) {
		m_employmentDate = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_employeeNr, ((TEmployee)that).m_employeeNr))
			return false;
		
		if (!Compare.equals(m_dOB, ((TEmployee)that).m_dOB))
			return false;
		
		if (!Compare.equals(m_familyName, ((TEmployee)that).m_familyName))
			return false;
		
		if (!Compare.equals(m_initials, ((TEmployee)that).m_initials))
			return false;
		
		if (!Compare.equals(m_gender, ((TEmployee)that).m_gender))
			return false;
		
		if (!Compare.equals(m_nat, ((TEmployee)that).m_nat))
			return false;
		
		if (!Compare.equals(m_employmentDate, ((TEmployee)that).m_employmentDate))
			return false;
		
		return true;
	}	

  
  
  
	/**
	 *	Printing method, prints the XML element to a Printer.
	 *  This method prints an XML fragment starting from tEmployee.
	 *
	 *  @param out the Printer that the element is printed to
	 *  @see com.ldx.util.Printer
	 */
	protected void printElements(Printer out) {
		super.printElements(out);
  
		if (m_employeeNr != null) {
			out.print("\n<EmployeeNr>");
			out.print(m_employeeNr);
			out.print("</EmployeeNr>\n");
		}
		
		if (m_dOB != null) {
			out.print("\n<DOB>");
			out.print(m_dOB);
			out.print("</DOB>\n");
		}
		
		if (m_familyName != null) {
			out.print("\n<FamilyName>");
			out.print(m_familyName);
			out.print("</FamilyName>\n");
		}
		
		if (m_initials != null) {
			out.print("\n<Initials>");
			out.print(m_initials);
			out.print("</Initials>\n");
		}
		
		if (m_gender != null) {
			out.print("\n<Gender>");
			out.print(m_gender);
			out.print("</Gender>\n");
		}
		
		if (m_nat != null) {
			out.print("\n<Nat>");
			out.print(m_nat);
			out.print("</Nat>\n");
		}
		
		if (m_employmentDate != null) {
			out.print("\n<EmploymentDate>");
			out.print(m_employmentDate);
			out.print("</EmploymentDate>\n");
		}
		
	}
}
