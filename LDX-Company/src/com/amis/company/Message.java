package com.amis.company;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: COMPANY 
  Generation date: Mon Apr 25 16:16:46 CEST 2016 

******************************************************************************/

import com.ldx.util.Printer;

import java.util.List;
import java.util.ArrayList;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * Message data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class Message extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for Message.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public Message(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type Message.
	 */
	static class Allocator implements TypeAllocator<Message> {
		/**
		 * method for getting a new instance of type Message.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public Message newInstance(String elementName, ComplexDataType parent) {
			return new Message(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for MsgProperties element. 
	 *  @serial
	 */	
	private TMsgProperties m_msgProperties = null;
	
	/** list of Employer element. 
	 *  @serial
	 */	
	private List<TEmployer> m_employerList = new ArrayList<TEmployer>();
	
	/**
	 * Get the embedded MsgProperties element.
	 * @return the item.
	 */
	public TMsgProperties getMsgProperties() {
		return m_msgProperties;
	}
		
	/**
	 * This method sets (overwrites) the element MsgProperties.
	 * @param data the item that needs to be added.
	 */
	void setMsgProperties(TMsgProperties data) {
		m_msgProperties = data;
	}
		
	/**
	 * Get the embedded list of Employer elements.
	 * @return list of items.
	 */
	public List<TEmployer> getEmployers() {
		return m_employerList;
	}
		
	/**
	 * This method adds data to the list of Employer.
	 * @param data the item that needs to be added.
	 */
	void setEmployer(TEmployer data) {
		m_employerList.add(data);
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_msgProperties, ((Message)that).m_msgProperties))
			return false;
		
		if (!Compare.equals(m_employerList, ((Message)that).m_employerList))
			return false;
		
		return true;
	}	

  
  

	/**
	 * Get 'test' attribute.
	 * @return the item.
	 */
	public String getTest() {
		return getAttr("test");
	}

	/**
	 * Set 'test' attribute.
	 * 
	 * Set (overwrite) the attribute data.
	 * @param data the item that needs to be added.
	 */
	public void setTest(String data) {
		setAttr("test", data);
	}
  
  
	/**
	 *	Printing method, prints the XML element to a Printer.
	 *  This method prints an XML fragment starting from Message.
	 *
	 *  @param out the Printer that the element is printed to
	 *  @see com.ldx.util.Printer
	 */
	protected void printElements(Printer out) {
		super.printElements(out);
  
		if (m_msgProperties != null)
			m_msgProperties.print(out);
		else {
			// out.print("<MsgProperties>null</MsgProperties>");
		}
		
		if (m_employerList != null)
			for(TEmployer l_Employer : m_employerList) {
				l_Employer.print(out);
			}
		else {
			// out.print("<Employer>null</Employer>");
		}
		
	}
}
