package com.amis.company.processor;


import com.amis.company.TEmployee;
import com.amis.company.TEmployer;
import com.amis.company.TMsgProperties;

/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: COMPANY 
  Generation date: Mon Apr 25 16:16:46 CEST 2016 

******************************************************************************/

import com.ldx.util.ConsoleWriter;
	
import com.ldx.xml.core.ComplexDataType;
import com.ldx.xml.core.MessageProcessor;
import com.ldx.xml.core.ProcessorException;
import com.ldx.xml.core.XMLEvent;

/**
 *	This class processes events that are sent by the LDX+ framework.
 *  You can change the runtime properties file to change the runtime behaviour of the application.
 *  E.g. 
 *  - Message/Employer/Employee/@process=true
 *  - Message/Employer/Employee/@detach=true
 *  
 *  Causes the framework to generate an event on Employee and detaches it from its parent (Employer).
 *  This allows the number of employees to be arbitrarily large. If it is small it may be changed to
 *  the default (which is false).
 */
public class CompanyProcessor implements MessageProcessor {

	@Override
	public void process(XMLEvent evt, ComplexDataType data)
			throws ProcessorException {

		/*
		 *	TODO Auto-generated method stub	implement your own handling here.
		 * 	Use the runtime configuration file to determine which events are sent to the processor.
		 */	
			
		if (evt == XMLEvent.END) {
			if (data instanceof TEmployee) {
				process((TEmployee)data);
			} else if (data instanceof TEmployer) {
				process((TEmployer)data);
			} else if (data instanceof TMsgProperties) {
				process((TMsgProperties)data);
			}
		}
	}

	private void process(TEmployee data) {
		// TODO Auto-generated method stub
//		data.print(ConsoleWriter.out);
	}

	private void process(TEmployer data) {
		// TODO Auto-generated method stub
		data.print(ConsoleWriter.out);
	}

	private void process(TMsgProperties data) {
		// TODO Auto-generated method stub
//		data.print(ConsoleWriter.out);
	}
}
