package com.amis.company;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: COMPANY 
  Generation date: Mon Apr 25 16:16:46 CEST 2016 

******************************************************************************/

import com.ldx.util.Printer;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * AddressType_d0e16_a21 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class AddressType_d0e16_a21 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for AddressType_d0e16_a21.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public AddressType_d0e16_a21(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type AddressType_d0e16_a21.
	 */
	static class Allocator implements TypeAllocator<AddressType_d0e16_a21> {
		/**
		 * method for getting a new instance of type AddressType_d0e16_a21.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public AddressType_d0e16_a21 newInstance(String elementName, ComplexDataType parent) {
			return new AddressType_d0e16_a21(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for Street element. 
	 *  @serial
	 */	
	private String m_street = null;
	
	/** element item for PostalCode element. 
	 *  @serial
	 */	
	private String m_postalCode = null;
	
	/** element item for City element. 
	 *  @serial
	 */	
	private String m_city = null;
	
	/** element item for CountryCode element. 
	 *  @serial
	 */	
	private String m_countryCode = null;
	
	/**
	 * Get the embedded Street element.
	 * @return the item.
	 */
	public String getStreet() {
		return m_street;
	}
		
	/**
	 * This method sets (overwrites) the element Street.
	 * @param data the item that needs to be added.
	 */
	void setStreet(String data) {
		m_street = data;
	}
		
	/**
	 * Get the embedded PostalCode element.
	 * @return the item.
	 */
	public String getPostalCode() {
		return m_postalCode;
	}
		
	/**
	 * This method sets (overwrites) the element PostalCode.
	 * @param data the item that needs to be added.
	 */
	void setPostalCode(String data) {
		m_postalCode = data;
	}
		
	/**
	 * Get the embedded City element.
	 * @return the item.
	 */
	public String getCity() {
		return m_city;
	}
		
	/**
	 * This method sets (overwrites) the element City.
	 * @param data the item that needs to be added.
	 */
	void setCity(String data) {
		m_city = data;
	}
		
	/**
	 * Get the embedded CountryCode element.
	 * @return the item.
	 */
	public String getCountryCode() {
		return m_countryCode;
	}
		
	/**
	 * This method sets (overwrites) the element CountryCode.
	 * @param data the item that needs to be added.
	 */
	void setCountryCode(String data) {
		m_countryCode = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_street, ((AddressType_d0e16_a21)that).m_street))
			return false;
		
		if (!Compare.equals(m_postalCode, ((AddressType_d0e16_a21)that).m_postalCode))
			return false;
		
		if (!Compare.equals(m_city, ((AddressType_d0e16_a21)that).m_city))
			return false;
		
		if (!Compare.equals(m_countryCode, ((AddressType_d0e16_a21)that).m_countryCode))
			return false;
		
		return true;
	}	

  
  
  
	/**
	 *	Printing method, prints the XML element to a Printer.
	 *  This method prints an XML fragment starting from AddressType_d0e16_a21.
	 *
	 *  @param out the Printer that the element is printed to
	 *  @see com.ldx.util.Printer
	 */
	protected void printElements(Printer out) {
		super.printElements(out);
  
		if (m_street != null) {
			out.print("\n<Street>");
			out.print(m_street);
			out.print("</Street>\n");
		}
		
		if (m_postalCode != null) {
			out.print("\n<PostalCode>");
			out.print(m_postalCode);
			out.print("</PostalCode>\n");
		}
		
		if (m_city != null) {
			out.print("\n<City>");
			out.print(m_city);
			out.print("</City>\n");
		}
		
		if (m_countryCode != null) {
			out.print("\n<CountryCode>");
			out.print(m_countryCode);
			out.print("</CountryCode>\n");
		}
		
	}
}
