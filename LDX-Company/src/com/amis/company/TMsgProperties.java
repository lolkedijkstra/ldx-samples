package com.amis.company;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: COMPANY 
  Generation date: Mon Apr 25 16:16:46 CEST 2016 

******************************************************************************/

import com.ldx.util.Printer;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * TMsgProperties data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class TMsgProperties extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for TMsgProperties.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public TMsgProperties(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type TMsgProperties.
	 */
	static class Allocator implements TypeAllocator<TMsgProperties> {
		/**
		 * method for getting a new instance of type TMsgProperties.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public TMsgProperties newInstance(String elementName, ComplexDataType parent) {
			return new TMsgProperties(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for MsgId element. 
	 *  @serial
	 */	
	private String m_msgId = null;
	
	/** element item for SenderId element. 
	 *  @serial
	 */	
	private String m_senderId = null;
	
	/**
	 * Get the embedded MsgId element.
	 * @return the item.
	 */
	public String getMsgId() {
		return m_msgId;
	}
		
	/**
	 * This method sets (overwrites) the element MsgId.
	 * @param data the item that needs to be added.
	 */
	void setMsgId(String data) {
		m_msgId = data;
	}
		
	/**
	 * Get the embedded SenderId element.
	 * @return the item.
	 */
	public String getSenderId() {
		return m_senderId;
	}
		
	/**
	 * This method sets (overwrites) the element SenderId.
	 * @param data the item that needs to be added.
	 */
	void setSenderId(String data) {
		m_senderId = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_msgId, ((TMsgProperties)that).m_msgId))
			return false;
		
		if (!Compare.equals(m_senderId, ((TMsgProperties)that).m_senderId))
			return false;
		
		return true;
	}	

  
  
  
	/**
	 *	Printing method, prints the XML element to a Printer.
	 *  This method prints an XML fragment starting from tMsgProperties.
	 *
	 *  @param out the Printer that the element is printed to
	 *  @see com.ldx.util.Printer
	 */
	protected void printElements(Printer out) {
		super.printElements(out);
  
		if (m_msgId != null) {
			out.print("\n<MsgId>");
			out.print(m_msgId);
			out.print("</MsgId>\n");
		}
		
		if (m_senderId != null) {
			out.print("\n<SenderId>");
			out.print(m_senderId);
			out.print("</SenderId>\n");
		}
		
	}
}
