package com.ldx.sepa2016.demo;

/******************************************************************************

******************************************************************************/

//----------------------- 		IO		-----------------------//
import java.io.FileInputStream;
import java.io.IOException;
//-----------------------    LOGGING	-----------------------//
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//-----------------------    	SAX		-----------------------//
import org.xml.sax.SAXException;
//-----------------------    	LDX		-----------------------//
import com.ldx.xml.core.ParserConfiguration;
import com.ldx.xml.core.ProcessorException;

import com.ldx.xml.parser.ParserRunnable;
import com.ldx.util.DefaultThreadManager;

import com.ldx.sepa2016.pain.runnable.CustomerCreditTransferInitiationV07Task;
import com.ldx.sepa2016.pain.processor.CustomerCreditTransferInitiationV07Processor;

/**
 * An example implementation of a parser application. You will need to adapt
 * this to meet your specific requirements. This example demonstrates: - the
 * glue code that connects reader and processor - how you can customize error
 * handling
 *
 * The application uses arguments passed on the command line, however you can
 * connect any class derived from java.io.InputStream.
 */
public class CstmrCrdtTrfIntnDemo {

	static final int NTHREADS = 3;
	static final String LDX_HOME = System.getenv("LDX_HOME");
	static final String XML = LDX_HOME + "/samples/sepa/input/pain.001.001.03_%d.xml";
	static final String PROPERTIES = LDX_HOME + "/samples/sepa/prop/pain001a2.properties";
	
	/**
	 * Entry point of the application.
	 */
	public static void main(String[] args) {


		// initialize logger component
		Logger log = LoggerFactory.getLogger(CstmrCrdtTrfIntnDemo.class);

		try {
			log.info("Starting application.");

			// load runtime configuration
			log.info("Loading runtime configuration");
			ParserConfiguration configuration = new ParserConfiguration(PROPERTIES);

			// create thread manager
			DefaultThreadManager manager = new DefaultThreadManager();

			for (int i = 0; i != NTHREADS; i++) {
				// create a parser task
				ParserRunnable task = new CustomerCreditTransferInitiationV07Task(configuration);

				// prepare the XML parser
				String xmlDocument = String.format(XML, i+1); 
				task.prepareStart(new FileInputStream(xmlDocument), new CustomerCreditTransferInitiationV07Processor());

				// add the worker to list of workers
				manager.addTask(task.getClass().getName() + (i+1), task);
			}
			// when all workers have been added start threads
			manager.startThreads();

			// wait for completion
			manager.monitorThreads();

			log.info("Processing complete.");

		} catch (ProcessorException e) {
			log.error("Execution aborted due to PROCESSING error (\n\tmessage: {}\n\tcause: {})", e.getMessage(),
					e.getCause());
		} catch (SAXException | IOException e) {
			log.error(e.getMessage());
		}
	}
}
