package com.ldx.sepa2016.pacs;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.com
		email: dijkstra@xml2java.com
		support: support@xml2java.com
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.3
  Generated code is compatible with ldx-framework v. 2.3
  License: Dijkstra ICT Consulting 
  Module: PACS 
  Generation date: Sun May 22 21:52:45 CEST 2016 

******************************************************************************/

import java.util.List;
import java.util.ArrayList;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * PaymentTransaction65 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class PaymentTransaction65 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for PaymentTransaction65.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public PaymentTransaction65(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type PaymentTransaction65.
	 */
	static class Allocator implements TypeAllocator<PaymentTransaction65> {
		/**
		 * method for getting a new instance of type PaymentTransaction65.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public PaymentTransaction65 newInstance(String elementName, ComplexDataType parent) {
			return new PaymentTransaction65(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for RtrId element. 
	 *  @serial
	 */	
	private String m_rtrId = null;
	
	/** element item for OrgnlGrpInf element. 
	 *  @serial
	 */	
	private OriginalGroupInformation3 m_orgnlGrpInf = null;
	
	/** element item for OrgnlInstrId element. 
	 *  @serial
	 */	
	private String m_orgnlInstrId = null;
	
	/** element item for OrgnlEndToEndId element. 
	 *  @serial
	 */	
	private String m_orgnlEndToEndId = null;
	
	/** element item for OrgnlTxId element. 
	 *  @serial
	 */	
	private String m_orgnlTxId = null;
	
	/** element item for OrgnlClrSysRef element. 
	 *  @serial
	 */	
	private String m_orgnlClrSysRef = null;
	
	/** element item for OrgnlIntrBkSttlmAmt element. 
	 *  @serial
	 */	
	private ActiveOrHistoricCurrencyAndAmount m_orgnlIntrBkSttlmAmt = null;
	
	/** element item for RtrdIntrBkSttlmAmt element. 
	 *  @serial
	 */	
	private ActiveCurrencyAndAmount m_rtrdIntrBkSttlmAmt = null;
	
	/** element item for IntrBkSttlmDt element. 
	 *  @serial
	 */	
	private String m_intrBkSttlmDt = null;
	
	/** element item for SttlmPrty element. 
	 *  @serial
	 */	
	private String m_sttlmPrty = null;
	
	/** element item for RtrdInstdAmt element. 
	 *  @serial
	 */	
	private ActiveOrHistoricCurrencyAndAmount m_rtrdInstdAmt = null;
	
	/** element item for XchgRate element. 
	 *  @serial
	 */	
	private String m_xchgRate = null;
	
	/** element item for CompstnAmt element. 
	 *  @serial
	 */	
	private ActiveOrHistoricCurrencyAndAmount m_compstnAmt = null;
	
	/** element item for ChrgBr element. 
	 *  @serial
	 */	
	private String m_chrgBr = null;
	
	/** list of ChrgsInf element. 
	 *  @serial
	 */	
	private List<Charges2> m_chrgsInfList = new ArrayList<Charges2>();
	
	/** element item for InstgAgt element. 
	 *  @serial
	 */	
	private BranchAndFinancialInstitutionIdentification5 m_instgAgt = null;
	
	/** element item for InstdAgt element. 
	 *  @serial
	 */	
	private BranchAndFinancialInstitutionIdentification5 m_instdAgt = null;
	
	/** list of RtrRsnInf element. 
	 *  @serial
	 */	
	private List<PaymentReturnReason1> m_rtrRsnInfList = new ArrayList<PaymentReturnReason1>();
	
	/** element item for OrgnlTxRef element. 
	 *  @serial
	 */	
	private OriginalTransactionReference22 m_orgnlTxRef = null;
	
	/** list of SplmtryData element. 
	 *  @serial
	 */	
	private List<SupplementaryData1> m_splmtryDataList = new ArrayList<SupplementaryData1>();
	
	/**
	 * Get the embedded RtrId element.
	 * @return the item.
	 */
	public String getRtrId() {
		return m_rtrId;
	}
		
	/**
	 * This method sets (overwrites) the element RtrId.
	 * @param data the item that needs to be added.
	 */
	void setRtrId(String data) {
		m_rtrId = data;
	}
		
	/**
	 * Get the embedded OrgnlGrpInf element.
	 * @return the item.
	 */
	public OriginalGroupInformation3 getOrgnlGrpInf() {
		return m_orgnlGrpInf;
	}
		
	/**
	 * This method sets (overwrites) the element OrgnlGrpInf.
	 * @param data the item that needs to be added.
	 */
	void setOrgnlGrpInf(OriginalGroupInformation3 data) {
		m_orgnlGrpInf = data;
	}
		
	/**
	 * Get the embedded OrgnlInstrId element.
	 * @return the item.
	 */
	public String getOrgnlInstrId() {
		return m_orgnlInstrId;
	}
		
	/**
	 * This method sets (overwrites) the element OrgnlInstrId.
	 * @param data the item that needs to be added.
	 */
	void setOrgnlInstrId(String data) {
		m_orgnlInstrId = data;
	}
		
	/**
	 * Get the embedded OrgnlEndToEndId element.
	 * @return the item.
	 */
	public String getOrgnlEndToEndId() {
		return m_orgnlEndToEndId;
	}
		
	/**
	 * This method sets (overwrites) the element OrgnlEndToEndId.
	 * @param data the item that needs to be added.
	 */
	void setOrgnlEndToEndId(String data) {
		m_orgnlEndToEndId = data;
	}
		
	/**
	 * Get the embedded OrgnlTxId element.
	 * @return the item.
	 */
	public String getOrgnlTxId() {
		return m_orgnlTxId;
	}
		
	/**
	 * This method sets (overwrites) the element OrgnlTxId.
	 * @param data the item that needs to be added.
	 */
	void setOrgnlTxId(String data) {
		m_orgnlTxId = data;
	}
		
	/**
	 * Get the embedded OrgnlClrSysRef element.
	 * @return the item.
	 */
	public String getOrgnlClrSysRef() {
		return m_orgnlClrSysRef;
	}
		
	/**
	 * This method sets (overwrites) the element OrgnlClrSysRef.
	 * @param data the item that needs to be added.
	 */
	void setOrgnlClrSysRef(String data) {
		m_orgnlClrSysRef = data;
	}
		
	/**
	 * Get the embedded OrgnlIntrBkSttlmAmt element.
	 * @return the item.
	 */
	public ActiveOrHistoricCurrencyAndAmount getOrgnlIntrBkSttlmAmt() {
		return m_orgnlIntrBkSttlmAmt;
	}
		
	/**
	 * This method sets (overwrites) the element OrgnlIntrBkSttlmAmt.
	 * @param data the item that needs to be added.
	 */
	void setOrgnlIntrBkSttlmAmt(ActiveOrHistoricCurrencyAndAmount data) {
		m_orgnlIntrBkSttlmAmt = data;
	}
		
	/**
	 * Get the embedded RtrdIntrBkSttlmAmt element.
	 * @return the item.
	 */
	public ActiveCurrencyAndAmount getRtrdIntrBkSttlmAmt() {
		return m_rtrdIntrBkSttlmAmt;
	}
		
	/**
	 * This method sets (overwrites) the element RtrdIntrBkSttlmAmt.
	 * @param data the item that needs to be added.
	 */
	void setRtrdIntrBkSttlmAmt(ActiveCurrencyAndAmount data) {
		m_rtrdIntrBkSttlmAmt = data;
	}
		
	/**
	 * Get the embedded IntrBkSttlmDt element.
	 * @return the item.
	 */
	public String getIntrBkSttlmDt() {
		return m_intrBkSttlmDt;
	}
		
	/**
	 * This method sets (overwrites) the element IntrBkSttlmDt.
	 * @param data the item that needs to be added.
	 */
	void setIntrBkSttlmDt(String data) {
		m_intrBkSttlmDt = data;
	}
		
	/**
	 * Get the embedded SttlmPrty element.
	 * @return the item.
	 */
	public String getSttlmPrty() {
		return m_sttlmPrty;
	}
		
	/**
	 * This method sets (overwrites) the element SttlmPrty.
	 * @param data the item that needs to be added.
	 */
	void setSttlmPrty(String data) {
		m_sttlmPrty = data;
	}
		
	/**
	 * Get the embedded RtrdInstdAmt element.
	 * @return the item.
	 */
	public ActiveOrHistoricCurrencyAndAmount getRtrdInstdAmt() {
		return m_rtrdInstdAmt;
	}
		
	/**
	 * This method sets (overwrites) the element RtrdInstdAmt.
	 * @param data the item that needs to be added.
	 */
	void setRtrdInstdAmt(ActiveOrHistoricCurrencyAndAmount data) {
		m_rtrdInstdAmt = data;
	}
		
	/**
	 * Get the embedded XchgRate element.
	 * @return the item.
	 */
	public String getXchgRate() {
		return m_xchgRate;
	}
		
	/**
	 * This method sets (overwrites) the element XchgRate.
	 * @param data the item that needs to be added.
	 */
	void setXchgRate(String data) {
		m_xchgRate = data;
	}
		
	/**
	 * Get the embedded CompstnAmt element.
	 * @return the item.
	 */
	public ActiveOrHistoricCurrencyAndAmount getCompstnAmt() {
		return m_compstnAmt;
	}
		
	/**
	 * This method sets (overwrites) the element CompstnAmt.
	 * @param data the item that needs to be added.
	 */
	void setCompstnAmt(ActiveOrHistoricCurrencyAndAmount data) {
		m_compstnAmt = data;
	}
		
	/**
	 * Get the embedded ChrgBr element.
	 * @return the item.
	 */
	public String getChrgBr() {
		return m_chrgBr;
	}
		
	/**
	 * This method sets (overwrites) the element ChrgBr.
	 * @param data the item that needs to be added.
	 */
	void setChrgBr(String data) {
		m_chrgBr = data;
	}
		
	/**
	 * Get the embedded list of ChrgsInf elements.
	 * @return list of items.
	 */
	public List<Charges2> getChrgsInfs() {
		return m_chrgsInfList;
	}
		
	/**
	 * This method adds data to the list of ChrgsInf.
	 * @param data the item that needs to be added.
	 */
	void setChrgsInf(Charges2 data) {
		m_chrgsInfList.add(data);
	}
		
	/**
	 * Get the embedded InstgAgt element.
	 * @return the item.
	 */
	public BranchAndFinancialInstitutionIdentification5 getInstgAgt() {
		return m_instgAgt;
	}
		
	/**
	 * This method sets (overwrites) the element InstgAgt.
	 * @param data the item that needs to be added.
	 */
	void setInstgAgt(BranchAndFinancialInstitutionIdentification5 data) {
		m_instgAgt = data;
	}
		
	/**
	 * Get the embedded InstdAgt element.
	 * @return the item.
	 */
	public BranchAndFinancialInstitutionIdentification5 getInstdAgt() {
		return m_instdAgt;
	}
		
	/**
	 * This method sets (overwrites) the element InstdAgt.
	 * @param data the item that needs to be added.
	 */
	void setInstdAgt(BranchAndFinancialInstitutionIdentification5 data) {
		m_instdAgt = data;
	}
		
	/**
	 * Get the embedded list of RtrRsnInf elements.
	 * @return list of items.
	 */
	public List<PaymentReturnReason1> getRtrRsnInfs() {
		return m_rtrRsnInfList;
	}
		
	/**
	 * This method adds data to the list of RtrRsnInf.
	 * @param data the item that needs to be added.
	 */
	void setRtrRsnInf(PaymentReturnReason1 data) {
		m_rtrRsnInfList.add(data);
	}
		
	/**
	 * Get the embedded OrgnlTxRef element.
	 * @return the item.
	 */
	public OriginalTransactionReference22 getOrgnlTxRef() {
		return m_orgnlTxRef;
	}
		
	/**
	 * This method sets (overwrites) the element OrgnlTxRef.
	 * @param data the item that needs to be added.
	 */
	void setOrgnlTxRef(OriginalTransactionReference22 data) {
		m_orgnlTxRef = data;
	}
		
	/**
	 * Get the embedded list of SplmtryData elements.
	 * @return list of items.
	 */
	public List<SupplementaryData1> getSplmtryDatas() {
		return m_splmtryDataList;
	}
		
	/**
	 * This method adds data to the list of SplmtryData.
	 * @param data the item that needs to be added.
	 */
	void setSplmtryData(SupplementaryData1 data) {
		m_splmtryDataList.add(data);
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_rtrId, ((PaymentTransaction65)that).m_rtrId))
			return false;
		
		if (!Compare.equals(m_orgnlGrpInf, ((PaymentTransaction65)that).m_orgnlGrpInf))
			return false;
		
		if (!Compare.equals(m_orgnlInstrId, ((PaymentTransaction65)that).m_orgnlInstrId))
			return false;
		
		if (!Compare.equals(m_orgnlEndToEndId, ((PaymentTransaction65)that).m_orgnlEndToEndId))
			return false;
		
		if (!Compare.equals(m_orgnlTxId, ((PaymentTransaction65)that).m_orgnlTxId))
			return false;
		
		if (!Compare.equals(m_orgnlClrSysRef, ((PaymentTransaction65)that).m_orgnlClrSysRef))
			return false;
		
		if (!Compare.equals(m_orgnlIntrBkSttlmAmt, ((PaymentTransaction65)that).m_orgnlIntrBkSttlmAmt))
			return false;
		
		if (!Compare.equals(m_rtrdIntrBkSttlmAmt, ((PaymentTransaction65)that).m_rtrdIntrBkSttlmAmt))
			return false;
		
		if (!Compare.equals(m_intrBkSttlmDt, ((PaymentTransaction65)that).m_intrBkSttlmDt))
			return false;
		
		if (!Compare.equals(m_sttlmPrty, ((PaymentTransaction65)that).m_sttlmPrty))
			return false;
		
		if (!Compare.equals(m_rtrdInstdAmt, ((PaymentTransaction65)that).m_rtrdInstdAmt))
			return false;
		
		if (!Compare.equals(m_xchgRate, ((PaymentTransaction65)that).m_xchgRate))
			return false;
		
		if (!Compare.equals(m_compstnAmt, ((PaymentTransaction65)that).m_compstnAmt))
			return false;
		
		if (!Compare.equals(m_chrgBr, ((PaymentTransaction65)that).m_chrgBr))
			return false;
		
		if (!Compare.equals(m_chrgsInfList, ((PaymentTransaction65)that).m_chrgsInfList))
			return false;
		
		if (!Compare.equals(m_instgAgt, ((PaymentTransaction65)that).m_instgAgt))
			return false;
		
		if (!Compare.equals(m_instdAgt, ((PaymentTransaction65)that).m_instdAgt))
			return false;
		
		if (!Compare.equals(m_rtrRsnInfList, ((PaymentTransaction65)that).m_rtrRsnInfList))
			return false;
		
		if (!Compare.equals(m_orgnlTxRef, ((PaymentTransaction65)that).m_orgnlTxRef))
			return false;
		
		if (!Compare.equals(m_splmtryDataList, ((PaymentTransaction65)that).m_splmtryDataList))
			return false;
		
		return true;
	}	

  
  
}
