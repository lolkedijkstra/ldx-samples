package com.ldx.sepa2016.pacs;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.com
		email: dijkstra@xml2java.com
		support: support@xml2java.com
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.3
  Generated code is compatible with ldx-framework v. 2.3
  License: Dijkstra ICT Consulting 
  Module: PACS 
  Generation date: Sun May 22 21:52:45 CEST 2016 

******************************************************************************/

/* SAX 2.0 dependencies */
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/* Framework dependencies */
import com.ldx.xml.core.DataSetter;
import com.ldx.xml.core.ComplexDataType;
import com.ldx.xml.core.XMLEvent;
import com.ldx.xml.core.XMLFragmentHandler;
import com.ldx.xml.parser.ParserTask;


/**
 * FIToFIPaymentReversalV06Document handler class.
 *
 * @see FIToFIPaymentReversalV06Document
 * 
 */
public class FIToFIPaymentReversalV06DocumentHandler extends XMLFragmentHandler<FIToFIPaymentReversalV06Document> {
	/**
	 * Proxy for FIToFIPaymentReversalV06DocumentHandler.
	 */
	static class Proxy extends HandlerProxy<FIToFIPaymentReversalV06Document> {
		/**
		 * Allocator for FIToFIPaymentReversalV06DocumentHandler.
		 */
		private static class Allocator 
				extends HandlerProxy.Allocator<FIToFIPaymentReversalV06Document> {			
			public XMLFragmentHandler<FIToFIPaymentReversalV06Document> create(
					ParserTask task
					, XMLReader reader
					, XMLFragmentHandler<?> handler
					, String elementName
					, DataSetter setter
					, boolean doProcess) {
				return new FIToFIPaymentReversalV06DocumentHandler(
					task
					, reader
					, handler
					, elementName
					, FIToFIPaymentReversalV06Document.getAllocator()
					, setter
					, doProcess);
			}
		}
		
		/**
		 * Constructor for Proxy.
		 *
		 * @param task the XML parser task
		 * @param reader the XML reader
		 * @param parentH the parent XML handler class
		 * @param elementName the name of the XML Element (tag).
		 * @param pSetter the setter for the parent data
		 * @param doProcess indicates whether processing is active for this handler instance
		 */
		public Proxy(ParserTask task, XMLReader reader, XMLFragmentHandler<?> parentH, String elementName,
				DataSetter pSetter, boolean doProcess) {
			super(task, reader, parentH, elementName, pSetter, new Allocator(), doProcess);
		}
	}
	
	/** Data setter class for FIToFIPmtRvsl element. */
	private class FIToFIPmtRvslSetter implements DataSetter {
		/** data target. */
		private FIToFIPaymentReversalV06DocumentHandler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public FIToFIPmtRvslSetter(FIToFIPaymentReversalV06DocumentHandler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setFIToFIPmtRvsl((FIToFIPaymentReversalV06) data);	
		}
	}


	/**
	 * Constructor for handler class.
	 *
	 * @param reader the XML reader
	 * @param parentH the parent XML handler class
	 * @param elementName the name of the XML Element (tag)
	 * @param allocator the allocator for the data type
	 * @param pSetter the setter for the parent data
	 * @param doProcess indicates whether processing is active for this handler instance
	 */
	public FIToFIPaymentReversalV06DocumentHandler(
			ParserTask application
			, XMLReader reader
			, XMLFragmentHandler<?> parentH
			, String elementName
			, FIToFIPaymentReversalV06Document.Allocator allocator
			, DataSetter pSetter
			, boolean doProcess) {
		super(application, reader, parentH, elementName, allocator, pSetter, doProcess);

		// code for linking children..
		registerHandler(
			new FIToFIPaymentReversalV06Handler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of FIToFIPaymentReversalV06Handler
				, "FIToFIPmtRvsl" // XML element name
				, doLink("FIToFIPmtRvsl") // linking to parent
					? new FIToFIPmtRvslSetter(this) // ON
					: null // OFF
				, doProcess("FIToFIPmtRvsl")) // processing active or not
				);
  	}


	/**
	 * This method overrides the getData() of the super type, returning the more
	 * specific type.
	 * 
	 */
	@Override 
	public FIToFIPaymentReversalV06Document getData() {
		return (FIToFIPaymentReversalV06Document)super.getData();
	}




	/**
	 * This method is called by the XML parser on an end of element event.
	 *
	 * If the localName == 'the name of the element that this handler handles' control is
	 * passed back to the parent handler (future events go there) and the data of
	 * the children of this handler is attached to the data associated to this handler.	
	 * 
	 * The data that is collected is connected to the parent data only if a parentDataSetter
	 * if found (getParentDataSetter()).
	 *
	 * If processing is enabled for a handler, the processor is called.
	 */
	@Override
	public void endElement(String uri, String localName, String name)
		throws SAXException {
		
		if (localName.equals(getXMLElementName())) {
				
			// return control to parent handler..
			this.deactivate();
			
			// get content of this item..
			getData().setContent(this.getValue());
			
			// attach data to parent (if parent data setter is found)..
			DataSetter setter = getParentDataSetter();
			if (setter != null) {
				setter.set(getData());
			} 
			
			// process data if required..
			if (doProcess()) {
				process(XMLEvent.END);
			}
		}
		
	}	
}
