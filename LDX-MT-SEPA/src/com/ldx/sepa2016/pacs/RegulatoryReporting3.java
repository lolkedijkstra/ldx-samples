package com.ldx.sepa2016.pacs;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.com
		email: dijkstra@xml2java.com
		support: support@xml2java.com
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.3
  Generated code is compatible with ldx-framework v. 2.3
  License: Dijkstra ICT Consulting 
  Module: PACS 
  Generation date: Sun May 22 21:52:46 CEST 2016 

******************************************************************************/

import java.util.List;
import java.util.ArrayList;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * RegulatoryReporting3 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class RegulatoryReporting3 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for RegulatoryReporting3.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public RegulatoryReporting3(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type RegulatoryReporting3.
	 */
	static class Allocator implements TypeAllocator<RegulatoryReporting3> {
		/**
		 * method for getting a new instance of type RegulatoryReporting3.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public RegulatoryReporting3 newInstance(String elementName, ComplexDataType parent) {
			return new RegulatoryReporting3(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for DbtCdtRptgInd element. 
	 *  @serial
	 */	
	private String m_dbtCdtRptgInd = null;
	
	/** element item for Authrty element. 
	 *  @serial
	 */	
	private RegulatoryAuthority2 m_authrty = null;
	
	/** list of Dtls element. 
	 *  @serial
	 */	
	private List<StructuredRegulatoryReporting3> m_dtlsList = new ArrayList<StructuredRegulatoryReporting3>();
	
	/**
	 * Get the embedded DbtCdtRptgInd element.
	 * @return the item.
	 */
	public String getDbtCdtRptgInd() {
		return m_dbtCdtRptgInd;
	}
		
	/**
	 * This method sets (overwrites) the element DbtCdtRptgInd.
	 * @param data the item that needs to be added.
	 */
	void setDbtCdtRptgInd(String data) {
		m_dbtCdtRptgInd = data;
	}
		
	/**
	 * Get the embedded Authrty element.
	 * @return the item.
	 */
	public RegulatoryAuthority2 getAuthrty() {
		return m_authrty;
	}
		
	/**
	 * This method sets (overwrites) the element Authrty.
	 * @param data the item that needs to be added.
	 */
	void setAuthrty(RegulatoryAuthority2 data) {
		m_authrty = data;
	}
		
	/**
	 * Get the embedded list of Dtls elements.
	 * @return list of items.
	 */
	public List<StructuredRegulatoryReporting3> getDtlss() {
		return m_dtlsList;
	}
		
	/**
	 * This method adds data to the list of Dtls.
	 * @param data the item that needs to be added.
	 */
	void setDtls(StructuredRegulatoryReporting3 data) {
		m_dtlsList.add(data);
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_dbtCdtRptgInd, ((RegulatoryReporting3)that).m_dbtCdtRptgInd))
			return false;
		
		if (!Compare.equals(m_authrty, ((RegulatoryReporting3)that).m_authrty))
			return false;
		
		if (!Compare.equals(m_dtlsList, ((RegulatoryReporting3)that).m_dtlsList))
			return false;
		
		return true;
	}	

  
  
}
