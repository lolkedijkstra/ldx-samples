package com.ldx.sepa2016.pacs.handlers;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.com
		email: dijkstra@xml2java.com
		support: support@xml2java.com
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.3
  Generated code is compatible with ldx-framework v. 2.3
  License: Dijkstra ICT Consulting 
  Module: PACS 
  Generation date: Sun May 22 21:52:44 CEST 2016 

******************************************************************************/

import org.xml.sax.XMLReader;

import com.ldx.sepa2016.pacs.FIToFIPaymentStatusReportV07Document;
import com.ldx.sepa2016.pacs.FIToFIPaymentStatusReportV07DocumentHandler;

import com.ldx.xml.core.XMLMessageHandler;
import com.ldx.xml.parser.ParserTask;

/**
 * This class reads the XML document from an XML inputsource.
 *
 * This class is the entry point for the client application.
 */
public class FIToFIPaymentStatusReportV07MessageHandler extends
		XMLMessageHandler<FIToFIPaymentStatusReportV07Document> {
	
	/** root element. */	
	static final String ELEMENT_NAME = "Document";	
	
	/**
	 * Constructor.
	 *
	 * @see XMLMessageHandler XMLMessageHandler
	 * @param task 
	 *            The parser task
	 * @param reader
	 *            The (SAX) XML Reader object
	 */
	public FIToFIPaymentStatusReportV07MessageHandler(ParserTask task, XMLReader reader) {
		super(reader
		, new FIToFIPaymentStatusReportV07DocumentHandler(
			task
			, reader
			, null	// root has no parent
			, ELEMENT_NAME
			, FIToFIPaymentStatusReportV07Document.getAllocator()
			, null	// not applicable for root
			, doProcess(ELEMENT_NAME, task))
		);
	}
}

