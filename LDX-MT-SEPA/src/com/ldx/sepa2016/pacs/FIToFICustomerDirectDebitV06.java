package com.ldx.sepa2016.pacs;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.com
		email: dijkstra@xml2java.com
		support: support@xml2java.com
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.3
  Generated code is compatible with ldx-framework v. 2.3
  License: Dijkstra ICT Consulting 
  Module: PACS 
  Generation date: Sun May 22 21:52:44 CEST 2016 

******************************************************************************/

import java.util.List;
import java.util.ArrayList;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * FIToFICustomerDirectDebitV06 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class FIToFICustomerDirectDebitV06 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for FIToFICustomerDirectDebitV06.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public FIToFICustomerDirectDebitV06(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type FIToFICustomerDirectDebitV06.
	 */
	static class Allocator implements TypeAllocator<FIToFICustomerDirectDebitV06> {
		/**
		 * method for getting a new instance of type FIToFICustomerDirectDebitV06.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public FIToFICustomerDirectDebitV06 newInstance(String elementName, ComplexDataType parent) {
			return new FIToFICustomerDirectDebitV06(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for GrpHdr element. 
	 *  @serial
	 */	
	private GroupHeader50 m_grpHdr = null;
	
	/** list of DrctDbtTxInf element. 
	 *  @serial
	 */	
	private List<DirectDebitTransactionInformation20> m_drctDbtTxInfList = new ArrayList<DirectDebitTransactionInformation20>();
	
	/** list of SplmtryData element. 
	 *  @serial
	 */	
	private List<SupplementaryData1> m_splmtryDataList = new ArrayList<SupplementaryData1>();
	
	/**
	 * Get the embedded GrpHdr element.
	 * @return the item.
	 */
	public GroupHeader50 getGrpHdr() {
		return m_grpHdr;
	}
		
	/**
	 * This method sets (overwrites) the element GrpHdr.
	 * @param data the item that needs to be added.
	 */
	void setGrpHdr(GroupHeader50 data) {
		m_grpHdr = data;
	}
		
	/**
	 * Get the embedded list of DrctDbtTxInf elements.
	 * @return list of items.
	 */
	public List<DirectDebitTransactionInformation20> getDrctDbtTxInfs() {
		return m_drctDbtTxInfList;
	}
		
	/**
	 * This method adds data to the list of DrctDbtTxInf.
	 * @param data the item that needs to be added.
	 */
	void setDrctDbtTxInf(DirectDebitTransactionInformation20 data) {
		m_drctDbtTxInfList.add(data);
	}
		
	/**
	 * Get the embedded list of SplmtryData elements.
	 * @return list of items.
	 */
	public List<SupplementaryData1> getSplmtryDatas() {
		return m_splmtryDataList;
	}
		
	/**
	 * This method adds data to the list of SplmtryData.
	 * @param data the item that needs to be added.
	 */
	void setSplmtryData(SupplementaryData1 data) {
		m_splmtryDataList.add(data);
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_grpHdr, ((FIToFICustomerDirectDebitV06)that).m_grpHdr))
			return false;
		
		if (!Compare.equals(m_drctDbtTxInfList, ((FIToFICustomerDirectDebitV06)that).m_drctDbtTxInfList))
			return false;
		
		if (!Compare.equals(m_splmtryDataList, ((FIToFICustomerDirectDebitV06)that).m_splmtryDataList))
			return false;
		
		return true;
	}	

  
  
}
