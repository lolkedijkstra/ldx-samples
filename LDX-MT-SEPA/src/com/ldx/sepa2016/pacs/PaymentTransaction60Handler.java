package com.ldx.sepa2016.pacs;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.com
		email: dijkstra@xml2java.com
		support: support@xml2java.com
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.3
  Generated code is compatible with ldx-framework v. 2.3
  License: Dijkstra ICT Consulting 
  Module: PACS 
  Generation date: Sun May 22 21:52:45 CEST 2016 

******************************************************************************/

/* SAX 2.0 dependencies */
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/* Framework dependencies */
import com.ldx.xml.core.DataSetter;
import com.ldx.xml.core.ComplexDataType;
import com.ldx.xml.core.XMLEvent;
import com.ldx.xml.core.XMLFragmentHandler;
import com.ldx.xml.parser.ParserTask;


/**
 * PaymentTransaction60 handler class.
 *
 * @see PaymentTransaction60
 * 
 */
public class PaymentTransaction60Handler extends XMLFragmentHandler<PaymentTransaction60> {
	/**
	 * Proxy for PaymentTransaction60Handler.
	 */
	static class Proxy extends HandlerProxy<PaymentTransaction60> {
		/**
		 * Allocator for PaymentTransaction60Handler.
		 */
		private static class Allocator 
				extends HandlerProxy.Allocator<PaymentTransaction60> {			
			public XMLFragmentHandler<PaymentTransaction60> create(
					ParserTask task
					, XMLReader reader
					, XMLFragmentHandler<?> handler
					, String elementName
					, DataSetter setter
					, boolean doProcess) {
				return new PaymentTransaction60Handler(
					task
					, reader
					, handler
					, elementName
					, PaymentTransaction60.getAllocator()
					, setter
					, doProcess);
			}
		}
		
		/**
		 * Constructor for Proxy.
		 *
		 * @param task the XML parser task
		 * @param reader the XML reader
		 * @param parentH the parent XML handler class
		 * @param elementName the name of the XML Element (tag).
		 * @param pSetter the setter for the parent data
		 * @param doProcess indicates whether processing is active for this handler instance
		 */
		public Proxy(ParserTask task, XMLReader reader, XMLFragmentHandler<?> parentH, String elementName,
				DataSetter pSetter, boolean doProcess) {
			super(task, reader, parentH, elementName, pSetter, new Allocator(), doProcess);
		}
	}
	
	/** Data setter class for ChrgsInf element. */
	private class ChrgsInfSetter implements DataSetter {
		/** data target. */
		private PaymentTransaction60Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public ChrgsInfSetter(PaymentTransaction60Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setChrgsInf((Charges2) data);	
		}
	}	
	/** Data setter class for CompstnAmt element. */
	private class CompstnAmtSetter implements DataSetter {
		/** data target. */
		private PaymentTransaction60Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public CompstnAmtSetter(PaymentTransaction60Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setCompstnAmt((ActiveOrHistoricCurrencyAndAmount) data);	
		}
	}	
	/** Data setter class for InstdAgt element. */
	private class InstdAgtSetter implements DataSetter {
		/** data target. */
		private PaymentTransaction60Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public InstdAgtSetter(PaymentTransaction60Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setInstdAgt((BranchAndFinancialInstitutionIdentification5) data);	
		}
	}	
	/** Data setter class for InstgAgt element. */
	private class InstgAgtSetter implements DataSetter {
		/** data target. */
		private PaymentTransaction60Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public InstgAgtSetter(PaymentTransaction60Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setInstgAgt((BranchAndFinancialInstitutionIdentification5) data);	
		}
	}	
	/** Data setter class for OrgnlGrpInf element. */
	private class OrgnlGrpInfSetter implements DataSetter {
		/** data target. */
		private PaymentTransaction60Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public OrgnlGrpInfSetter(PaymentTransaction60Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setOrgnlGrpInf((OriginalGroupInformation3) data);	
		}
	}	
	/** Data setter class for OrgnlIntrBkSttlmAmt element. */
	private class OrgnlIntrBkSttlmAmtSetter implements DataSetter {
		/** data target. */
		private PaymentTransaction60Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public OrgnlIntrBkSttlmAmtSetter(PaymentTransaction60Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setOrgnlIntrBkSttlmAmt((ActiveOrHistoricCurrencyAndAmount) data);	
		}
	}	
	/** Data setter class for OrgnlTxRef element. */
	private class OrgnlTxRefSetter implements DataSetter {
		/** data target. */
		private PaymentTransaction60Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public OrgnlTxRefSetter(PaymentTransaction60Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setOrgnlTxRef((OriginalTransactionReference22) data);	
		}
	}	
	/** Data setter class for RvsdInstdAmt element. */
	private class RvsdInstdAmtSetter implements DataSetter {
		/** data target. */
		private PaymentTransaction60Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public RvsdInstdAmtSetter(PaymentTransaction60Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setRvsdInstdAmt((ActiveOrHistoricCurrencyAndAmount) data);	
		}
	}	
	/** Data setter class for RvsdIntrBkSttlmAmt element. */
	private class RvsdIntrBkSttlmAmtSetter implements DataSetter {
		/** data target. */
		private PaymentTransaction60Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public RvsdIntrBkSttlmAmtSetter(PaymentTransaction60Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setRvsdIntrBkSttlmAmt((ActiveCurrencyAndAmount) data);	
		}
	}	
	/** Data setter class for RvslRsnInf element. */
	private class RvslRsnInfSetter implements DataSetter {
		/** data target. */
		private PaymentTransaction60Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public RvslRsnInfSetter(PaymentTransaction60Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setRvslRsnInf((PaymentReversalReason7) data);	
		}
	}	
	/** Data setter class for SplmtryData element. */
	private class SplmtryDataSetter implements DataSetter {
		/** data target. */
		private PaymentTransaction60Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public SplmtryDataSetter(PaymentTransaction60Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setSplmtryData((SupplementaryData1) data);	
		}
	}


	/**
	 * Constructor for handler class.
	 *
	 * @param reader the XML reader
	 * @param parentH the parent XML handler class
	 * @param elementName the name of the XML Element (tag)
	 * @param allocator the allocator for the data type
	 * @param pSetter the setter for the parent data
	 * @param doProcess indicates whether processing is active for this handler instance
	 */
	public PaymentTransaction60Handler(
			ParserTask application
			, XMLReader reader
			, XMLFragmentHandler<?> parentH
			, String elementName
			, PaymentTransaction60.Allocator allocator
			, DataSetter pSetter
			, boolean doProcess) {
		super(application, reader, parentH, elementName, allocator, pSetter, doProcess);

		// code for linking children..
		registerHandler(
			new Charges2Handler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of Charges2Handler
				, "ChrgsInf" // XML element name
				, doLink("ChrgsInf") // linking to parent
					? new ChrgsInfSetter(this) // ON
					: null // OFF
				, doProcess("ChrgsInf")) // processing active or not
				);
  
		registerHandler(
			new ActiveOrHistoricCurrencyAndAmountHandler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of ActiveOrHistoricCurrencyAndAmountHandler
				, "CompstnAmt" // XML element name
				, doLink("CompstnAmt") // linking to parent
					? new CompstnAmtSetter(this) // ON
					: null // OFF
				, doProcess("CompstnAmt")) // processing active or not
				);
  
		registerHandler(
			new BranchAndFinancialInstitutionIdentification5Handler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of BranchAndFinancialInstitutionIdentification5Handler
				, "InstdAgt" // XML element name
				, doLink("InstdAgt") // linking to parent
					? new InstdAgtSetter(this) // ON
					: null // OFF
				, doProcess("InstdAgt")) // processing active or not
				);
  
		registerHandler(
			new BranchAndFinancialInstitutionIdentification5Handler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of BranchAndFinancialInstitutionIdentification5Handler
				, "InstgAgt" // XML element name
				, doLink("InstgAgt") // linking to parent
					? new InstgAgtSetter(this) // ON
					: null // OFF
				, doProcess("InstgAgt")) // processing active or not
				);
  
		registerHandler(
			new OriginalGroupInformation3Handler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of OriginalGroupInformation3Handler
				, "OrgnlGrpInf" // XML element name
				, doLink("OrgnlGrpInf") // linking to parent
					? new OrgnlGrpInfSetter(this) // ON
					: null // OFF
				, doProcess("OrgnlGrpInf")) // processing active or not
				);
  
		registerHandler(
			new ActiveOrHistoricCurrencyAndAmountHandler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of ActiveOrHistoricCurrencyAndAmountHandler
				, "OrgnlIntrBkSttlmAmt" // XML element name
				, doLink("OrgnlIntrBkSttlmAmt") // linking to parent
					? new OrgnlIntrBkSttlmAmtSetter(this) // ON
					: null // OFF
				, doProcess("OrgnlIntrBkSttlmAmt")) // processing active or not
				);
  
		registerHandler(
			new OriginalTransactionReference22Handler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of OriginalTransactionReference22Handler
				, "OrgnlTxRef" // XML element name
				, doLink("OrgnlTxRef") // linking to parent
					? new OrgnlTxRefSetter(this) // ON
					: null // OFF
				, doProcess("OrgnlTxRef")) // processing active or not
				);
  
		registerHandler(
			new ActiveOrHistoricCurrencyAndAmountHandler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of ActiveOrHistoricCurrencyAndAmountHandler
				, "RvsdInstdAmt" // XML element name
				, doLink("RvsdInstdAmt") // linking to parent
					? new RvsdInstdAmtSetter(this) // ON
					: null // OFF
				, doProcess("RvsdInstdAmt")) // processing active or not
				);
  
		registerHandler(
			new ActiveCurrencyAndAmountHandler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of ActiveCurrencyAndAmountHandler
				, "RvsdIntrBkSttlmAmt" // XML element name
				, doLink("RvsdIntrBkSttlmAmt") // linking to parent
					? new RvsdIntrBkSttlmAmtSetter(this) // ON
					: null // OFF
				, doProcess("RvsdIntrBkSttlmAmt")) // processing active or not
				);
  
		registerHandler(
			new PaymentReversalReason7Handler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of PaymentReversalReason7Handler
				, "RvslRsnInf" // XML element name
				, doLink("RvslRsnInf") // linking to parent
					? new RvslRsnInfSetter(this) // ON
					: null // OFF
				, doProcess("RvslRsnInf")) // processing active or not
				);
  
		registerHandler(
			new SupplementaryData1Handler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of SupplementaryData1Handler
				, "SplmtryData" // XML element name
				, doLink("SplmtryData") // linking to parent
					? new SplmtryDataSetter(this) // ON
					: null // OFF
				, doProcess("SplmtryData")) // processing active or not
				);
  	}


	/**
	 * This method overrides the getData() of the super type, returning the more
	 * specific type.
	 * 
	 */
	@Override 
	public PaymentTransaction60 getData() {
		return (PaymentTransaction60)super.getData();
	}




	/**
	 * This method is called by the XML parser on an end of element event.
	 *
	 * If the localName == 'the name of the element that this handler handles' control is
	 * passed back to the parent handler (future events go there) and the data of
	 * the children of this handler is attached to the data associated to this handler.	
	 * 
	 * The data that is collected is connected to the parent data only if a parentDataSetter
	 * if found (getParentDataSetter()).
	 *
	 * If processing is enabled for a handler, the processor is called.
	 */
	@Override
	public void endElement(String uri, String localName, String name)
		throws SAXException {
		
		// collect data from children..
		if (localName.equals("ChrgBr")) {
			getData().setChrgBr(getValue());
			getContents().reset();
		} else if (localName.equals("IntrBkSttlmDt")) {
			getData().setIntrBkSttlmDt(getValue());
			getContents().reset();
		} else if (localName.equals("OrgnlClrSysRef")) {
			getData().setOrgnlClrSysRef(getValue());
			getContents().reset();
		} else if (localName.equals("OrgnlEndToEndId")) {
			getData().setOrgnlEndToEndId(getValue());
			getContents().reset();
		} else if (localName.equals("OrgnlInstrId")) {
			getData().setOrgnlInstrId(getValue());
			getContents().reset();
		} else if (localName.equals("OrgnlTxId")) {
			getData().setOrgnlTxId(getValue());
			getContents().reset();
		} else if (localName.equals("RvslId")) {
			getData().setRvslId(getValue());
			getContents().reset();
		} else if (localName.equals("SttlmPrty")) {
			getData().setSttlmPrty(getValue());
			getContents().reset();
		} else if (localName.equals("XchgRate")) {
			getData().setXchgRate(getValue());
			getContents().reset();
		} else if (localName.equals(getXMLElementName())) {
				
			// return control to parent handler..
			this.deactivate();
			
			// get content of this item..
			getData().setContent(this.getValue());
			
			// attach data to parent (if parent data setter is found)..
			DataSetter setter = getParentDataSetter();
			if (setter != null) {
				setter.set(getData());
			} 
			
			// process data if required..
			if (doProcess()) {
				process(XMLEvent.END);
			}
		}
		
	}	
}
