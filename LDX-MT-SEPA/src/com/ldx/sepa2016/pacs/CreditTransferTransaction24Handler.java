package com.ldx.sepa2016.pacs;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.com
		email: dijkstra@xml2java.com
		support: support@xml2java.com
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.3
  Generated code is compatible with ldx-framework v. 2.3
  License: Dijkstra ICT Consulting 
  Module: PACS 
  Generation date: Sun May 22 21:52:46 CEST 2016 

******************************************************************************/

/* SAX 2.0 dependencies */
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/* Framework dependencies */
import com.ldx.xml.core.DataSetter;
import com.ldx.xml.core.ComplexDataType;
import com.ldx.xml.core.XMLEvent;
import com.ldx.xml.core.XMLFragmentHandler;
import com.ldx.xml.parser.ParserTask;


/**
 * CreditTransferTransaction24 handler class.
 *
 * @see CreditTransferTransaction24
 * 
 */
public class CreditTransferTransaction24Handler extends XMLFragmentHandler<CreditTransferTransaction24> {
	/**
	 * Proxy for CreditTransferTransaction24Handler.
	 */
	static class Proxy extends HandlerProxy<CreditTransferTransaction24> {
		/**
		 * Allocator for CreditTransferTransaction24Handler.
		 */
		private static class Allocator 
				extends HandlerProxy.Allocator<CreditTransferTransaction24> {			
			public XMLFragmentHandler<CreditTransferTransaction24> create(
					ParserTask task
					, XMLReader reader
					, XMLFragmentHandler<?> handler
					, String elementName
					, DataSetter setter
					, boolean doProcess) {
				return new CreditTransferTransaction24Handler(
					task
					, reader
					, handler
					, elementName
					, CreditTransferTransaction24.getAllocator()
					, setter
					, doProcess);
			}
		}
		
		/**
		 * Constructor for Proxy.
		 *
		 * @param task the XML parser task
		 * @param reader the XML reader
		 * @param parentH the parent XML handler class
		 * @param elementName the name of the XML Element (tag).
		 * @param pSetter the setter for the parent data
		 * @param doProcess indicates whether processing is active for this handler instance
		 */
		public Proxy(ParserTask task, XMLReader reader, XMLFragmentHandler<?> parentH, String elementName,
				DataSetter pSetter, boolean doProcess) {
			super(task, reader, parentH, elementName, pSetter, new Allocator(), doProcess);
		}
	}
	
	/** Data setter class for Cdtr element. */
	private class CdtrSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction24Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public CdtrSetter(CreditTransferTransaction24Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setCdtr((PartyIdentification43) data);	
		}
	}	
	/** Data setter class for CdtrAcct element. */
	private class CdtrAcctSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction24Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public CdtrAcctSetter(CreditTransferTransaction24Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setCdtrAcct((CashAccount24) data);	
		}
	}	
	/** Data setter class for CdtrAgt element. */
	private class CdtrAgtSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction24Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public CdtrAgtSetter(CreditTransferTransaction24Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setCdtrAgt((BranchAndFinancialInstitutionIdentification5) data);	
		}
	}	
	/** Data setter class for CdtrAgtAcct element. */
	private class CdtrAgtAcctSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction24Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public CdtrAgtAcctSetter(CreditTransferTransaction24Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setCdtrAgtAcct((CashAccount24) data);	
		}
	}	
	/** Data setter class for Dbtr element. */
	private class DbtrSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction24Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public DbtrSetter(CreditTransferTransaction24Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setDbtr((PartyIdentification43) data);	
		}
	}	
	/** Data setter class for DbtrAcct element. */
	private class DbtrAcctSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction24Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public DbtrAcctSetter(CreditTransferTransaction24Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setDbtrAcct((CashAccount24) data);	
		}
	}	
	/** Data setter class for DbtrAgt element. */
	private class DbtrAgtSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction24Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public DbtrAgtSetter(CreditTransferTransaction24Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setDbtrAgt((BranchAndFinancialInstitutionIdentification5) data);	
		}
	}	
	/** Data setter class for DbtrAgtAcct element. */
	private class DbtrAgtAcctSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction24Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public DbtrAgtAcctSetter(CreditTransferTransaction24Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setDbtrAgtAcct((CashAccount24) data);	
		}
	}	
	/** Data setter class for InitgPty element. */
	private class InitgPtySetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction24Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public InitgPtySetter(CreditTransferTransaction24Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setInitgPty((PartyIdentification43) data);	
		}
	}	
	/** Data setter class for InstdAmt element. */
	private class InstdAmtSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction24Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public InstdAmtSetter(CreditTransferTransaction24Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setInstdAmt((ActiveOrHistoricCurrencyAndAmount) data);	
		}
	}	
	/** Data setter class for InstrForCdtrAgt element. */
	private class InstrForCdtrAgtSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction24Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public InstrForCdtrAgtSetter(CreditTransferTransaction24Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setInstrForCdtrAgt((InstructionForCreditorAgent1) data);	
		}
	}	
	/** Data setter class for InstrForNxtAgt element. */
	private class InstrForNxtAgtSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction24Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public InstrForNxtAgtSetter(CreditTransferTransaction24Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setInstrForNxtAgt((InstructionForNextAgent1) data);	
		}
	}	
	/** Data setter class for IntrmyAgt1 element. */
	private class IntrmyAgt1Setter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction24Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public IntrmyAgt1Setter(CreditTransferTransaction24Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setIntrmyAgt1((BranchAndFinancialInstitutionIdentification5) data);	
		}
	}	
	/** Data setter class for IntrmyAgt1Acct element. */
	private class IntrmyAgt1AcctSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction24Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public IntrmyAgt1AcctSetter(CreditTransferTransaction24Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setIntrmyAgt1Acct((CashAccount24) data);	
		}
	}	
	/** Data setter class for IntrmyAgt2 element. */
	private class IntrmyAgt2Setter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction24Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public IntrmyAgt2Setter(CreditTransferTransaction24Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setIntrmyAgt2((BranchAndFinancialInstitutionIdentification5) data);	
		}
	}	
	/** Data setter class for IntrmyAgt2Acct element. */
	private class IntrmyAgt2AcctSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction24Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public IntrmyAgt2AcctSetter(CreditTransferTransaction24Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setIntrmyAgt2Acct((CashAccount24) data);	
		}
	}	
	/** Data setter class for IntrmyAgt3 element. */
	private class IntrmyAgt3Setter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction24Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public IntrmyAgt3Setter(CreditTransferTransaction24Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setIntrmyAgt3((BranchAndFinancialInstitutionIdentification5) data);	
		}
	}	
	/** Data setter class for IntrmyAgt3Acct element. */
	private class IntrmyAgt3AcctSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction24Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public IntrmyAgt3AcctSetter(CreditTransferTransaction24Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setIntrmyAgt3Acct((CashAccount24) data);	
		}
	}	
	/** Data setter class for PrvsInstgAgt element. */
	private class PrvsInstgAgtSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction24Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public PrvsInstgAgtSetter(CreditTransferTransaction24Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setPrvsInstgAgt((BranchAndFinancialInstitutionIdentification5) data);	
		}
	}	
	/** Data setter class for PrvsInstgAgtAcct element. */
	private class PrvsInstgAgtAcctSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction24Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public PrvsInstgAgtAcctSetter(CreditTransferTransaction24Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setPrvsInstgAgtAcct((CashAccount24) data);	
		}
	}	
	/** Data setter class for RmtInf element. */
	private class RmtInfSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction24Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public RmtInfSetter(CreditTransferTransaction24Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setRmtInf((RemittanceInformation11) data);	
		}
	}	
	/** Data setter class for Tax element. */
	private class TaxSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction24Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public TaxSetter(CreditTransferTransaction24Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setTax((TaxInformation3) data);	
		}
	}	
	/** Data setter class for UltmtCdtr element. */
	private class UltmtCdtrSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction24Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public UltmtCdtrSetter(CreditTransferTransaction24Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setUltmtCdtr((PartyIdentification43) data);	
		}
	}	
	/** Data setter class for UltmtDbtr element. */
	private class UltmtDbtrSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction24Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public UltmtDbtrSetter(CreditTransferTransaction24Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setUltmtDbtr((PartyIdentification43) data);	
		}
	}


	/**
	 * Constructor for handler class.
	 *
	 * @param reader the XML reader
	 * @param parentH the parent XML handler class
	 * @param elementName the name of the XML Element (tag)
	 * @param allocator the allocator for the data type
	 * @param pSetter the setter for the parent data
	 * @param doProcess indicates whether processing is active for this handler instance
	 */
	public CreditTransferTransaction24Handler(
			ParserTask application
			, XMLReader reader
			, XMLFragmentHandler<?> parentH
			, String elementName
			, CreditTransferTransaction24.Allocator allocator
			, DataSetter pSetter
			, boolean doProcess) {
		super(application, reader, parentH, elementName, allocator, pSetter, doProcess);

		// code for linking children..
		registerHandler(
			new PartyIdentification43Handler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of PartyIdentification43Handler
				, "Cdtr" // XML element name
				, doLink("Cdtr") // linking to parent
					? new CdtrSetter(this) // ON
					: null // OFF
				, doProcess("Cdtr")) // processing active or not
				);
  
		registerHandler(
			new CashAccount24Handler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of CashAccount24Handler
				, "CdtrAcct" // XML element name
				, doLink("CdtrAcct") // linking to parent
					? new CdtrAcctSetter(this) // ON
					: null // OFF
				, doProcess("CdtrAcct")) // processing active or not
				);
  
		registerHandler(
			new BranchAndFinancialInstitutionIdentification5Handler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of BranchAndFinancialInstitutionIdentification5Handler
				, "CdtrAgt" // XML element name
				, doLink("CdtrAgt") // linking to parent
					? new CdtrAgtSetter(this) // ON
					: null // OFF
				, doProcess("CdtrAgt")) // processing active or not
				);
  
		registerHandler(
			new CashAccount24Handler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of CashAccount24Handler
				, "CdtrAgtAcct" // XML element name
				, doLink("CdtrAgtAcct") // linking to parent
					? new CdtrAgtAcctSetter(this) // ON
					: null // OFF
				, doProcess("CdtrAgtAcct")) // processing active or not
				);
  
		registerHandler(
			new PartyIdentification43Handler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of PartyIdentification43Handler
				, "Dbtr" // XML element name
				, doLink("Dbtr") // linking to parent
					? new DbtrSetter(this) // ON
					: null // OFF
				, doProcess("Dbtr")) // processing active or not
				);
  
		registerHandler(
			new CashAccount24Handler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of CashAccount24Handler
				, "DbtrAcct" // XML element name
				, doLink("DbtrAcct") // linking to parent
					? new DbtrAcctSetter(this) // ON
					: null // OFF
				, doProcess("DbtrAcct")) // processing active or not
				);
  
		registerHandler(
			new BranchAndFinancialInstitutionIdentification5Handler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of BranchAndFinancialInstitutionIdentification5Handler
				, "DbtrAgt" // XML element name
				, doLink("DbtrAgt") // linking to parent
					? new DbtrAgtSetter(this) // ON
					: null // OFF
				, doProcess("DbtrAgt")) // processing active or not
				);
  
		registerHandler(
			new CashAccount24Handler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of CashAccount24Handler
				, "DbtrAgtAcct" // XML element name
				, doLink("DbtrAgtAcct") // linking to parent
					? new DbtrAgtAcctSetter(this) // ON
					: null // OFF
				, doProcess("DbtrAgtAcct")) // processing active or not
				);
  
		registerHandler(
			new PartyIdentification43Handler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of PartyIdentification43Handler
				, "InitgPty" // XML element name
				, doLink("InitgPty") // linking to parent
					? new InitgPtySetter(this) // ON
					: null // OFF
				, doProcess("InitgPty")) // processing active or not
				);
  
		registerHandler(
			new ActiveOrHistoricCurrencyAndAmountHandler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of ActiveOrHistoricCurrencyAndAmountHandler
				, "InstdAmt" // XML element name
				, doLink("InstdAmt") // linking to parent
					? new InstdAmtSetter(this) // ON
					: null // OFF
				, doProcess("InstdAmt")) // processing active or not
				);
  
		registerHandler(
			new InstructionForCreditorAgent1Handler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of InstructionForCreditorAgent1Handler
				, "InstrForCdtrAgt" // XML element name
				, doLink("InstrForCdtrAgt") // linking to parent
					? new InstrForCdtrAgtSetter(this) // ON
					: null // OFF
				, doProcess("InstrForCdtrAgt")) // processing active or not
				);
  
		registerHandler(
			new InstructionForNextAgent1Handler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of InstructionForNextAgent1Handler
				, "InstrForNxtAgt" // XML element name
				, doLink("InstrForNxtAgt") // linking to parent
					? new InstrForNxtAgtSetter(this) // ON
					: null // OFF
				, doProcess("InstrForNxtAgt")) // processing active or not
				);
  
		registerHandler(
			new BranchAndFinancialInstitutionIdentification5Handler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of BranchAndFinancialInstitutionIdentification5Handler
				, "IntrmyAgt1" // XML element name
				, doLink("IntrmyAgt1") // linking to parent
					? new IntrmyAgt1Setter(this) // ON
					: null // OFF
				, doProcess("IntrmyAgt1")) // processing active or not
				);
  
		registerHandler(
			new CashAccount24Handler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of CashAccount24Handler
				, "IntrmyAgt1Acct" // XML element name
				, doLink("IntrmyAgt1Acct") // linking to parent
					? new IntrmyAgt1AcctSetter(this) // ON
					: null // OFF
				, doProcess("IntrmyAgt1Acct")) // processing active or not
				);
  
		registerHandler(
			new BranchAndFinancialInstitutionIdentification5Handler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of BranchAndFinancialInstitutionIdentification5Handler
				, "IntrmyAgt2" // XML element name
				, doLink("IntrmyAgt2") // linking to parent
					? new IntrmyAgt2Setter(this) // ON
					: null // OFF
				, doProcess("IntrmyAgt2")) // processing active or not
				);
  
		registerHandler(
			new CashAccount24Handler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of CashAccount24Handler
				, "IntrmyAgt2Acct" // XML element name
				, doLink("IntrmyAgt2Acct") // linking to parent
					? new IntrmyAgt2AcctSetter(this) // ON
					: null // OFF
				, doProcess("IntrmyAgt2Acct")) // processing active or not
				);
  
		registerHandler(
			new BranchAndFinancialInstitutionIdentification5Handler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of BranchAndFinancialInstitutionIdentification5Handler
				, "IntrmyAgt3" // XML element name
				, doLink("IntrmyAgt3") // linking to parent
					? new IntrmyAgt3Setter(this) // ON
					: null // OFF
				, doProcess("IntrmyAgt3")) // processing active or not
				);
  
		registerHandler(
			new CashAccount24Handler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of CashAccount24Handler
				, "IntrmyAgt3Acct" // XML element name
				, doLink("IntrmyAgt3Acct") // linking to parent
					? new IntrmyAgt3AcctSetter(this) // ON
					: null // OFF
				, doProcess("IntrmyAgt3Acct")) // processing active or not
				);
  
		registerHandler(
			new BranchAndFinancialInstitutionIdentification5Handler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of BranchAndFinancialInstitutionIdentification5Handler
				, "PrvsInstgAgt" // XML element name
				, doLink("PrvsInstgAgt") // linking to parent
					? new PrvsInstgAgtSetter(this) // ON
					: null // OFF
				, doProcess("PrvsInstgAgt")) // processing active or not
				);
  
		registerHandler(
			new CashAccount24Handler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of CashAccount24Handler
				, "PrvsInstgAgtAcct" // XML element name
				, doLink("PrvsInstgAgtAcct") // linking to parent
					? new PrvsInstgAgtAcctSetter(this) // ON
					: null // OFF
				, doProcess("PrvsInstgAgtAcct")) // processing active or not
				);
  
		registerHandler(
			new RemittanceInformation11Handler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of RemittanceInformation11Handler
				, "RmtInf" // XML element name
				, doLink("RmtInf") // linking to parent
					? new RmtInfSetter(this) // ON
					: null // OFF
				, doProcess("RmtInf")) // processing active or not
				);
  
		registerHandler(
			new TaxInformation3Handler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of TaxInformation3Handler
				, "Tax" // XML element name
				, doLink("Tax") // linking to parent
					? new TaxSetter(this) // ON
					: null // OFF
				, doProcess("Tax")) // processing active or not
				);
  
		registerHandler(
			new PartyIdentification43Handler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of PartyIdentification43Handler
				, "UltmtCdtr" // XML element name
				, doLink("UltmtCdtr") // linking to parent
					? new UltmtCdtrSetter(this) // ON
					: null // OFF
				, doProcess("UltmtCdtr")) // processing active or not
				);
  
		registerHandler(
			new PartyIdentification43Handler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of PartyIdentification43Handler
				, "UltmtDbtr" // XML element name
				, doLink("UltmtDbtr") // linking to parent
					? new UltmtDbtrSetter(this) // ON
					: null // OFF
				, doProcess("UltmtDbtr")) // processing active or not
				);
  	}


	/**
	 * This method overrides the getData() of the super type, returning the more
	 * specific type.
	 * 
	 */
	@Override 
	public CreditTransferTransaction24 getData() {
		return (CreditTransferTransaction24)super.getData();
	}




	/**
	 * This method is called by the XML parser on an end of element event.
	 *
	 * If the localName == 'the name of the element that this handler handles' control is
	 * passed back to the parent handler (future events go there) and the data of
	 * the children of this handler is attached to the data associated to this handler.	
	 * 
	 * The data that is collected is connected to the parent data only if a parentDataSetter
	 * if found (getParentDataSetter()).
	 *
	 * If processing is enabled for a handler, the processor is called.
	 */
	@Override
	public void endElement(String uri, String localName, String name)
		throws SAXException {
		
		if (localName.equals(getXMLElementName())) {
				
			// return control to parent handler..
			this.deactivate();
			
			// get content of this item..
			getData().setContent(this.getValue());
			
			// attach data to parent (if parent data setter is found)..
			DataSetter setter = getParentDataSetter();
			if (setter != null) {
				setter.set(getData());
			} 
			
			// process data if required..
			if (doProcess()) {
				process(XMLEvent.END);
			}
		}
		
	}	
}
