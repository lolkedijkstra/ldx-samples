package com.ldx.sepa2016.pacs;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.com
		email: dijkstra@xml2java.com
		support: support@xml2java.com
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.3
  Generated code is compatible with ldx-framework v. 2.3
  License: Dijkstra ICT Consulting 
  Module: PACS 
  Generation date: Sun May 22 21:52:44 CEST 2016 

******************************************************************************/

import java.util.List;
import java.util.ArrayList;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * FIToFIPaymentStatusReportV07 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class FIToFIPaymentStatusReportV07 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for FIToFIPaymentStatusReportV07.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public FIToFIPaymentStatusReportV07(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type FIToFIPaymentStatusReportV07.
	 */
	static class Allocator implements TypeAllocator<FIToFIPaymentStatusReportV07> {
		/**
		 * method for getting a new instance of type FIToFIPaymentStatusReportV07.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public FIToFIPaymentStatusReportV07 newInstance(String elementName, ComplexDataType parent) {
			return new FIToFIPaymentStatusReportV07(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for GrpHdr element. 
	 *  @serial
	 */	
	private GroupHeader53 m_grpHdr = null;
	
	/** list of OrgnlGrpInfAndSts element. 
	 *  @serial
	 */	
	private List<OriginalGroupHeader1> m_orgnlGrpInfAndStsList = new ArrayList<OriginalGroupHeader1>();
	
	/** list of TxInfAndSts element. 
	 *  @serial
	 */	
	private List<PaymentTransaction63> m_txInfAndStsList = new ArrayList<PaymentTransaction63>();
	
	/** list of SplmtryData element. 
	 *  @serial
	 */	
	private List<SupplementaryData1> m_splmtryDataList = new ArrayList<SupplementaryData1>();
	
	/**
	 * Get the embedded GrpHdr element.
	 * @return the item.
	 */
	public GroupHeader53 getGrpHdr() {
		return m_grpHdr;
	}
		
	/**
	 * This method sets (overwrites) the element GrpHdr.
	 * @param data the item that needs to be added.
	 */
	void setGrpHdr(GroupHeader53 data) {
		m_grpHdr = data;
	}
		
	/**
	 * Get the embedded list of OrgnlGrpInfAndSts elements.
	 * @return list of items.
	 */
	public List<OriginalGroupHeader1> getOrgnlGrpInfAndStss() {
		return m_orgnlGrpInfAndStsList;
	}
		
	/**
	 * This method adds data to the list of OrgnlGrpInfAndSts.
	 * @param data the item that needs to be added.
	 */
	void setOrgnlGrpInfAndSts(OriginalGroupHeader1 data) {
		m_orgnlGrpInfAndStsList.add(data);
	}
		
	/**
	 * Get the embedded list of TxInfAndSts elements.
	 * @return list of items.
	 */
	public List<PaymentTransaction63> getTxInfAndStss() {
		return m_txInfAndStsList;
	}
		
	/**
	 * This method adds data to the list of TxInfAndSts.
	 * @param data the item that needs to be added.
	 */
	void setTxInfAndSts(PaymentTransaction63 data) {
		m_txInfAndStsList.add(data);
	}
		
	/**
	 * Get the embedded list of SplmtryData elements.
	 * @return list of items.
	 */
	public List<SupplementaryData1> getSplmtryDatas() {
		return m_splmtryDataList;
	}
		
	/**
	 * This method adds data to the list of SplmtryData.
	 * @param data the item that needs to be added.
	 */
	void setSplmtryData(SupplementaryData1 data) {
		m_splmtryDataList.add(data);
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_grpHdr, ((FIToFIPaymentStatusReportV07)that).m_grpHdr))
			return false;
		
		if (!Compare.equals(m_orgnlGrpInfAndStsList, ((FIToFIPaymentStatusReportV07)that).m_orgnlGrpInfAndStsList))
			return false;
		
		if (!Compare.equals(m_txInfAndStsList, ((FIToFIPaymentStatusReportV07)that).m_txInfAndStsList))
			return false;
		
		if (!Compare.equals(m_splmtryDataList, ((FIToFIPaymentStatusReportV07)that).m_splmtryDataList))
			return false;
		
		return true;
	}	

  
  
}
