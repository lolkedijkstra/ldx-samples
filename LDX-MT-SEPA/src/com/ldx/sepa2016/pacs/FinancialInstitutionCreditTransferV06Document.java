package com.ldx.sepa2016.pacs;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.com
		email: dijkstra@xml2java.com
		support: support@xml2java.com
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.3
  Generated code is compatible with ldx-framework v. 2.3
  License: Dijkstra ICT Consulting 
  Module: PACS 
  Generation date: Sun May 22 21:52:46 CEST 2016 

******************************************************************************/

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * FinancialInstitutionCreditTransferV06Document data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class FinancialInstitutionCreditTransferV06Document extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for FinancialInstitutionCreditTransferV06Document.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public FinancialInstitutionCreditTransferV06Document(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type FinancialInstitutionCreditTransferV06Document.
	 */
	static class Allocator implements TypeAllocator<FinancialInstitutionCreditTransferV06Document> {
		/**
		 * method for getting a new instance of type FinancialInstitutionCreditTransferV06Document.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public FinancialInstitutionCreditTransferV06Document newInstance(String elementName, ComplexDataType parent) {
			return new FinancialInstitutionCreditTransferV06Document(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for FICdtTrf element. 
	 *  @serial
	 */	
	private FinancialInstitutionCreditTransferV06 m_fICdtTrf = null;
	
	/**
	 * Get the embedded FICdtTrf element.
	 * @return the item.
	 */
	public FinancialInstitutionCreditTransferV06 getFICdtTrf() {
		return m_fICdtTrf;
	}
		
	/**
	 * This method sets (overwrites) the element FICdtTrf.
	 * @param data the item that needs to be added.
	 */
	void setFICdtTrf(FinancialInstitutionCreditTransferV06 data) {
		m_fICdtTrf = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_fICdtTrf, ((FinancialInstitutionCreditTransferV06Document)that).m_fICdtTrf))
			return false;
		
		return true;
	}	

  
  
}
