package com.ldx.sepa2016.pacs;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.com
		email: dijkstra@xml2java.com
		support: support@xml2java.com
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.3
  Generated code is compatible with ldx-framework v. 2.3
  License: Dijkstra ICT Consulting 
  Module: PACS 
  Generation date: Sun May 22 21:52:46 CEST 2016 

******************************************************************************/

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * PaymentTypeInformation21 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class PaymentTypeInformation21 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for PaymentTypeInformation21.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public PaymentTypeInformation21(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type PaymentTypeInformation21.
	 */
	static class Allocator implements TypeAllocator<PaymentTypeInformation21> {
		/**
		 * method for getting a new instance of type PaymentTypeInformation21.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public PaymentTypeInformation21 newInstance(String elementName, ComplexDataType parent) {
			return new PaymentTypeInformation21(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for InstrPrty element. 
	 *  @serial
	 */	
	private String m_instrPrty = null;
	
	/** element item for ClrChanl element. 
	 *  @serial
	 */	
	private String m_clrChanl = null;
	
	/** element item for SvcLvl element. 
	 *  @serial
	 */	
	private ServiceLevel8Choice m_svcLvl = null;
	
	/** element item for LclInstrm element. 
	 *  @serial
	 */	
	private LocalInstrument2Choice m_lclInstrm = null;
	
	/** element item for CtgyPurp element. 
	 *  @serial
	 */	
	private CategoryPurpose1Choice m_ctgyPurp = null;
	
	/**
	 * Get the embedded InstrPrty element.
	 * @return the item.
	 */
	public String getInstrPrty() {
		return m_instrPrty;
	}
		
	/**
	 * This method sets (overwrites) the element InstrPrty.
	 * @param data the item that needs to be added.
	 */
	void setInstrPrty(String data) {
		m_instrPrty = data;
	}
		
	/**
	 * Get the embedded ClrChanl element.
	 * @return the item.
	 */
	public String getClrChanl() {
		return m_clrChanl;
	}
		
	/**
	 * This method sets (overwrites) the element ClrChanl.
	 * @param data the item that needs to be added.
	 */
	void setClrChanl(String data) {
		m_clrChanl = data;
	}
		
	/**
	 * Get the embedded SvcLvl element.
	 * @return the item.
	 */
	public ServiceLevel8Choice getSvcLvl() {
		return m_svcLvl;
	}
		
	/**
	 * This method sets (overwrites) the element SvcLvl.
	 * @param data the item that needs to be added.
	 */
	void setSvcLvl(ServiceLevel8Choice data) {
		m_svcLvl = data;
	}
		
	/**
	 * Get the embedded LclInstrm element.
	 * @return the item.
	 */
	public LocalInstrument2Choice getLclInstrm() {
		return m_lclInstrm;
	}
		
	/**
	 * This method sets (overwrites) the element LclInstrm.
	 * @param data the item that needs to be added.
	 */
	void setLclInstrm(LocalInstrument2Choice data) {
		m_lclInstrm = data;
	}
		
	/**
	 * Get the embedded CtgyPurp element.
	 * @return the item.
	 */
	public CategoryPurpose1Choice getCtgyPurp() {
		return m_ctgyPurp;
	}
		
	/**
	 * This method sets (overwrites) the element CtgyPurp.
	 * @param data the item that needs to be added.
	 */
	void setCtgyPurp(CategoryPurpose1Choice data) {
		m_ctgyPurp = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_instrPrty, ((PaymentTypeInformation21)that).m_instrPrty))
			return false;
		
		if (!Compare.equals(m_clrChanl, ((PaymentTypeInformation21)that).m_clrChanl))
			return false;
		
		if (!Compare.equals(m_svcLvl, ((PaymentTypeInformation21)that).m_svcLvl))
			return false;
		
		if (!Compare.equals(m_lclInstrm, ((PaymentTypeInformation21)that).m_lclInstrm))
			return false;
		
		if (!Compare.equals(m_ctgyPurp, ((PaymentTypeInformation21)that).m_ctgyPurp))
			return false;
		
		return true;
	}	

  
  
}
