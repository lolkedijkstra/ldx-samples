package com.ldx.sepa2016.pacs;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.com
		email: dijkstra@xml2java.com
		support: support@xml2java.com
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.3
  Generated code is compatible with ldx-framework v. 2.3
  License: Dijkstra ICT Consulting 
  Module: PACS 
  Generation date: Sun May 22 21:52:44 CEST 2016 

******************************************************************************/

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * SettlementInstruction2 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class SettlementInstruction2 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for SettlementInstruction2.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public SettlementInstruction2(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type SettlementInstruction2.
	 */
	static class Allocator implements TypeAllocator<SettlementInstruction2> {
		/**
		 * method for getting a new instance of type SettlementInstruction2.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public SettlementInstruction2 newInstance(String elementName, ComplexDataType parent) {
			return new SettlementInstruction2(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for SttlmMtd element. 
	 *  @serial
	 */	
	private String m_sttlmMtd = null;
	
	/** element item for SttlmAcct element. 
	 *  @serial
	 */	
	private CashAccount24 m_sttlmAcct = null;
	
	/** element item for ClrSys element. 
	 *  @serial
	 */	
	private ClearingSystemIdentification3Choice m_clrSys = null;
	
	/**
	 * Get the embedded SttlmMtd element.
	 * @return the item.
	 */
	public String getSttlmMtd() {
		return m_sttlmMtd;
	}
		
	/**
	 * This method sets (overwrites) the element SttlmMtd.
	 * @param data the item that needs to be added.
	 */
	void setSttlmMtd(String data) {
		m_sttlmMtd = data;
	}
		
	/**
	 * Get the embedded SttlmAcct element.
	 * @return the item.
	 */
	public CashAccount24 getSttlmAcct() {
		return m_sttlmAcct;
	}
		
	/**
	 * This method sets (overwrites) the element SttlmAcct.
	 * @param data the item that needs to be added.
	 */
	void setSttlmAcct(CashAccount24 data) {
		m_sttlmAcct = data;
	}
		
	/**
	 * Get the embedded ClrSys element.
	 * @return the item.
	 */
	public ClearingSystemIdentification3Choice getClrSys() {
		return m_clrSys;
	}
		
	/**
	 * This method sets (overwrites) the element ClrSys.
	 * @param data the item that needs to be added.
	 */
	void setClrSys(ClearingSystemIdentification3Choice data) {
		m_clrSys = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_sttlmMtd, ((SettlementInstruction2)that).m_sttlmMtd))
			return false;
		
		if (!Compare.equals(m_sttlmAcct, ((SettlementInstruction2)that).m_sttlmAcct))
			return false;
		
		if (!Compare.equals(m_clrSys, ((SettlementInstruction2)that).m_clrSys))
			return false;
		
		return true;
	}	

  
  
}
