package com.ldx.sepa2016.pacs;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.com
		email: dijkstra@xml2java.com
		support: support@xml2java.com
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.3
  Generated code is compatible with ldx-framework v. 2.3
  License: Dijkstra ICT Consulting 
  Module: PACS 
  Generation date: Sun May 22 21:52:45 CEST 2016 

******************************************************************************/

/* SAX 2.0 dependencies */
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/* Framework dependencies */
import com.ldx.xml.core.DataSetter;
import com.ldx.xml.core.ComplexDataType;
import com.ldx.xml.core.XMLEvent;
import com.ldx.xml.core.XMLFragmentHandler;
import com.ldx.xml.parser.ParserTask;


/**
 * MandateRelatedInformation10 handler class.
 *
 * @see MandateRelatedInformation10
 * 
 */
public class MandateRelatedInformation10Handler extends XMLFragmentHandler<MandateRelatedInformation10> {
	/**
	 * Proxy for MandateRelatedInformation10Handler.
	 */
	static class Proxy extends HandlerProxy<MandateRelatedInformation10> {
		/**
		 * Allocator for MandateRelatedInformation10Handler.
		 */
		private static class Allocator 
				extends HandlerProxy.Allocator<MandateRelatedInformation10> {			
			public XMLFragmentHandler<MandateRelatedInformation10> create(
					ParserTask task
					, XMLReader reader
					, XMLFragmentHandler<?> handler
					, String elementName
					, DataSetter setter
					, boolean doProcess) {
				return new MandateRelatedInformation10Handler(
					task
					, reader
					, handler
					, elementName
					, MandateRelatedInformation10.getAllocator()
					, setter
					, doProcess);
			}
		}
		
		/**
		 * Constructor for Proxy.
		 *
		 * @param task the XML parser task
		 * @param reader the XML reader
		 * @param parentH the parent XML handler class
		 * @param elementName the name of the XML Element (tag).
		 * @param pSetter the setter for the parent data
		 * @param doProcess indicates whether processing is active for this handler instance
		 */
		public Proxy(ParserTask task, XMLReader reader, XMLFragmentHandler<?> parentH, String elementName,
				DataSetter pSetter, boolean doProcess) {
			super(task, reader, parentH, elementName, pSetter, new Allocator(), doProcess);
		}
	}
	
	/** Data setter class for AmdmntInfDtls element. */
	private class AmdmntInfDtlsSetter implements DataSetter {
		/** data target. */
		private MandateRelatedInformation10Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public AmdmntInfDtlsSetter(MandateRelatedInformation10Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setAmdmntInfDtls((AmendmentInformationDetails10) data);	
		}
	}	
	/** Data setter class for Frqcy element. */
	private class FrqcySetter implements DataSetter {
		/** data target. */
		private MandateRelatedInformation10Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public FrqcySetter(MandateRelatedInformation10Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setFrqcy((Frequency21Choice) data);	
		}
	}	
	/** Data setter class for Rsn element. */
	private class RsnSetter implements DataSetter {
		/** data target. */
		private MandateRelatedInformation10Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public RsnSetter(MandateRelatedInformation10Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setRsn((MandateSetupReason1Choice) data);	
		}
	}


	/**
	 * Constructor for handler class.
	 *
	 * @param reader the XML reader
	 * @param parentH the parent XML handler class
	 * @param elementName the name of the XML Element (tag)
	 * @param allocator the allocator for the data type
	 * @param pSetter the setter for the parent data
	 * @param doProcess indicates whether processing is active for this handler instance
	 */
	public MandateRelatedInformation10Handler(
			ParserTask application
			, XMLReader reader
			, XMLFragmentHandler<?> parentH
			, String elementName
			, MandateRelatedInformation10.Allocator allocator
			, DataSetter pSetter
			, boolean doProcess) {
		super(application, reader, parentH, elementName, allocator, pSetter, doProcess);

		// code for linking children..
		registerHandler(
			new AmendmentInformationDetails10Handler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of AmendmentInformationDetails10Handler
				, "AmdmntInfDtls" // XML element name
				, doLink("AmdmntInfDtls") // linking to parent
					? new AmdmntInfDtlsSetter(this) // ON
					: null // OFF
				, doProcess("AmdmntInfDtls")) // processing active or not
				);
  
		registerHandler(
			new Frequency21ChoiceHandler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of Frequency21ChoiceHandler
				, "Frqcy" // XML element name
				, doLink("Frqcy") // linking to parent
					? new FrqcySetter(this) // ON
					: null // OFF
				, doProcess("Frqcy")) // processing active or not
				);
  
		registerHandler(
			new MandateSetupReason1ChoiceHandler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of MandateSetupReason1ChoiceHandler
				, "Rsn" // XML element name
				, doLink("Rsn") // linking to parent
					? new RsnSetter(this) // ON
					: null // OFF
				, doProcess("Rsn")) // processing active or not
				);
  	}


	/**
	 * This method overrides the getData() of the super type, returning the more
	 * specific type.
	 * 
	 */
	@Override 
	public MandateRelatedInformation10 getData() {
		return (MandateRelatedInformation10)super.getData();
	}




	/**
	 * This method is called by the XML parser on an end of element event.
	 *
	 * If the localName == 'the name of the element that this handler handles' control is
	 * passed back to the parent handler (future events go there) and the data of
	 * the children of this handler is attached to the data associated to this handler.	
	 * 
	 * The data that is collected is connected to the parent data only if a parentDataSetter
	 * if found (getParentDataSetter()).
	 *
	 * If processing is enabled for a handler, the processor is called.
	 */
	@Override
	public void endElement(String uri, String localName, String name)
		throws SAXException {
		
		// collect data from children..
		if (localName.equals("AmdmntInd")) {
			getData().setAmdmntInd(getValue());
			getContents().reset();
		} else if (localName.equals("DtOfSgntr")) {
			getData().setDtOfSgntr(getValue());
			getContents().reset();
		} else if (localName.equals("ElctrncSgntr")) {
			getData().setElctrncSgntr(getValue());
			getContents().reset();
		} else if (localName.equals("FnlColltnDt")) {
			getData().setFnlColltnDt(getValue());
			getContents().reset();
		} else if (localName.equals("FrstColltnDt")) {
			getData().setFrstColltnDt(getValue());
			getContents().reset();
		} else if (localName.equals("MndtId")) {
			getData().setMndtId(getValue());
			getContents().reset();
		} else if (localName.equals(getXMLElementName())) {
				
			// return control to parent handler..
			this.deactivate();
			
			// get content of this item..
			getData().setContent(this.getValue());
			
			// attach data to parent (if parent data setter is found)..
			DataSetter setter = getParentDataSetter();
			if (setter != null) {
				setter.set(getData());
			} 
			
			// process data if required..
			if (doProcess()) {
				process(XMLEvent.END);
			}
		}
		
	}	
}
