package com.ldx.sepa2016.pacs;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.com
		email: dijkstra@xml2java.com
		support: support@xml2java.com
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.3
  Generated code is compatible with ldx-framework v. 2.3
  License: Dijkstra ICT Consulting 
  Module: PACS 
  Generation date: Sun May 22 21:52:46 CEST 2016 

******************************************************************************/

import java.util.List;
import java.util.ArrayList;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * CreditTransferTransaction9 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class CreditTransferTransaction9 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for CreditTransferTransaction9.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public CreditTransferTransaction9(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type CreditTransferTransaction9.
	 */
	static class Allocator implements TypeAllocator<CreditTransferTransaction9> {
		/**
		 * method for getting a new instance of type CreditTransferTransaction9.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public CreditTransferTransaction9 newInstance(String elementName, ComplexDataType parent) {
			return new CreditTransferTransaction9(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for CdtId element. 
	 *  @serial
	 */	
	private String m_cdtId = null;
	
	/** element item for BtchBookg element. 
	 *  @serial
	 */	
	private String m_btchBookg = null;
	
	/** element item for PmtTpInf element. 
	 *  @serial
	 */	
	private PaymentTypeInformation21 m_pmtTpInf = null;
	
	/** element item for TtlIntrBkSttlmAmt element. 
	 *  @serial
	 */	
	private ActiveCurrencyAndAmount m_ttlIntrBkSttlmAmt = null;
	
	/** element item for IntrBkSttlmDt element. 
	 *  @serial
	 */	
	private String m_intrBkSttlmDt = null;
	
	/** element item for InstgAgt element. 
	 *  @serial
	 */	
	private BranchAndFinancialInstitutionIdentification5 m_instgAgt = null;
	
	/** element item for InstdAgt element. 
	 *  @serial
	 */	
	private BranchAndFinancialInstitutionIdentification5 m_instdAgt = null;
	
	/** element item for IntrmyAgt1 element. 
	 *  @serial
	 */	
	private BranchAndFinancialInstitutionIdentification5 m_intrmyAgt1 = null;
	
	/** element item for IntrmyAgt1Acct element. 
	 *  @serial
	 */	
	private CashAccount24 m_intrmyAgt1Acct = null;
	
	/** element item for IntrmyAgt2 element. 
	 *  @serial
	 */	
	private BranchAndFinancialInstitutionIdentification5 m_intrmyAgt2 = null;
	
	/** element item for IntrmyAgt2Acct element. 
	 *  @serial
	 */	
	private CashAccount24 m_intrmyAgt2Acct = null;
	
	/** element item for IntrmyAgt3 element. 
	 *  @serial
	 */	
	private BranchAndFinancialInstitutionIdentification5 m_intrmyAgt3 = null;
	
	/** element item for IntrmyAgt3Acct element. 
	 *  @serial
	 */	
	private CashAccount24 m_intrmyAgt3Acct = null;
	
	/** element item for CdtrAgt element. 
	 *  @serial
	 */	
	private BranchAndFinancialInstitutionIdentification5 m_cdtrAgt = null;
	
	/** element item for CdtrAgtAcct element. 
	 *  @serial
	 */	
	private CashAccount24 m_cdtrAgtAcct = null;
	
	/** element item for Cdtr element. 
	 *  @serial
	 */	
	private BranchAndFinancialInstitutionIdentification5 m_cdtr = null;
	
	/** element item for CdtrAcct element. 
	 *  @serial
	 */	
	private CashAccount24 m_cdtrAcct = null;
	
	/** element item for UltmtCdtr element. 
	 *  @serial
	 */	
	private BranchAndFinancialInstitutionIdentification5 m_ultmtCdtr = null;
	
	/** list of InstrForCdtrAgt element. 
	 *  @serial
	 */	
	private List<InstructionForCreditorAgent2> m_instrForCdtrAgtList = new ArrayList<InstructionForCreditorAgent2>();
	
	/** list of DrctDbtTxInf element. 
	 *  @serial
	 */	
	private List<DirectDebitTransactionInformation15> m_drctDbtTxInfList = new ArrayList<DirectDebitTransactionInformation15>();
	
	/** list of SplmtryData element. 
	 *  @serial
	 */	
	private List<SupplementaryData1> m_splmtryDataList = new ArrayList<SupplementaryData1>();
	
	/**
	 * Get the embedded CdtId element.
	 * @return the item.
	 */
	public String getCdtId() {
		return m_cdtId;
	}
		
	/**
	 * This method sets (overwrites) the element CdtId.
	 * @param data the item that needs to be added.
	 */
	void setCdtId(String data) {
		m_cdtId = data;
	}
		
	/**
	 * Get the embedded BtchBookg element.
	 * @return the item.
	 */
	public String getBtchBookg() {
		return m_btchBookg;
	}
		
	/**
	 * This method sets (overwrites) the element BtchBookg.
	 * @param data the item that needs to be added.
	 */
	void setBtchBookg(String data) {
		m_btchBookg = data;
	}
		
	/**
	 * Get the embedded PmtTpInf element.
	 * @return the item.
	 */
	public PaymentTypeInformation21 getPmtTpInf() {
		return m_pmtTpInf;
	}
		
	/**
	 * This method sets (overwrites) the element PmtTpInf.
	 * @param data the item that needs to be added.
	 */
	void setPmtTpInf(PaymentTypeInformation21 data) {
		m_pmtTpInf = data;
	}
		
	/**
	 * Get the embedded TtlIntrBkSttlmAmt element.
	 * @return the item.
	 */
	public ActiveCurrencyAndAmount getTtlIntrBkSttlmAmt() {
		return m_ttlIntrBkSttlmAmt;
	}
		
	/**
	 * This method sets (overwrites) the element TtlIntrBkSttlmAmt.
	 * @param data the item that needs to be added.
	 */
	void setTtlIntrBkSttlmAmt(ActiveCurrencyAndAmount data) {
		m_ttlIntrBkSttlmAmt = data;
	}
		
	/**
	 * Get the embedded IntrBkSttlmDt element.
	 * @return the item.
	 */
	public String getIntrBkSttlmDt() {
		return m_intrBkSttlmDt;
	}
		
	/**
	 * This method sets (overwrites) the element IntrBkSttlmDt.
	 * @param data the item that needs to be added.
	 */
	void setIntrBkSttlmDt(String data) {
		m_intrBkSttlmDt = data;
	}
		
	/**
	 * Get the embedded InstgAgt element.
	 * @return the item.
	 */
	public BranchAndFinancialInstitutionIdentification5 getInstgAgt() {
		return m_instgAgt;
	}
		
	/**
	 * This method sets (overwrites) the element InstgAgt.
	 * @param data the item that needs to be added.
	 */
	void setInstgAgt(BranchAndFinancialInstitutionIdentification5 data) {
		m_instgAgt = data;
	}
		
	/**
	 * Get the embedded InstdAgt element.
	 * @return the item.
	 */
	public BranchAndFinancialInstitutionIdentification5 getInstdAgt() {
		return m_instdAgt;
	}
		
	/**
	 * This method sets (overwrites) the element InstdAgt.
	 * @param data the item that needs to be added.
	 */
	void setInstdAgt(BranchAndFinancialInstitutionIdentification5 data) {
		m_instdAgt = data;
	}
		
	/**
	 * Get the embedded IntrmyAgt1 element.
	 * @return the item.
	 */
	public BranchAndFinancialInstitutionIdentification5 getIntrmyAgt1() {
		return m_intrmyAgt1;
	}
		
	/**
	 * This method sets (overwrites) the element IntrmyAgt1.
	 * @param data the item that needs to be added.
	 */
	void setIntrmyAgt1(BranchAndFinancialInstitutionIdentification5 data) {
		m_intrmyAgt1 = data;
	}
		
	/**
	 * Get the embedded IntrmyAgt1Acct element.
	 * @return the item.
	 */
	public CashAccount24 getIntrmyAgt1Acct() {
		return m_intrmyAgt1Acct;
	}
		
	/**
	 * This method sets (overwrites) the element IntrmyAgt1Acct.
	 * @param data the item that needs to be added.
	 */
	void setIntrmyAgt1Acct(CashAccount24 data) {
		m_intrmyAgt1Acct = data;
	}
		
	/**
	 * Get the embedded IntrmyAgt2 element.
	 * @return the item.
	 */
	public BranchAndFinancialInstitutionIdentification5 getIntrmyAgt2() {
		return m_intrmyAgt2;
	}
		
	/**
	 * This method sets (overwrites) the element IntrmyAgt2.
	 * @param data the item that needs to be added.
	 */
	void setIntrmyAgt2(BranchAndFinancialInstitutionIdentification5 data) {
		m_intrmyAgt2 = data;
	}
		
	/**
	 * Get the embedded IntrmyAgt2Acct element.
	 * @return the item.
	 */
	public CashAccount24 getIntrmyAgt2Acct() {
		return m_intrmyAgt2Acct;
	}
		
	/**
	 * This method sets (overwrites) the element IntrmyAgt2Acct.
	 * @param data the item that needs to be added.
	 */
	void setIntrmyAgt2Acct(CashAccount24 data) {
		m_intrmyAgt2Acct = data;
	}
		
	/**
	 * Get the embedded IntrmyAgt3 element.
	 * @return the item.
	 */
	public BranchAndFinancialInstitutionIdentification5 getIntrmyAgt3() {
		return m_intrmyAgt3;
	}
		
	/**
	 * This method sets (overwrites) the element IntrmyAgt3.
	 * @param data the item that needs to be added.
	 */
	void setIntrmyAgt3(BranchAndFinancialInstitutionIdentification5 data) {
		m_intrmyAgt3 = data;
	}
		
	/**
	 * Get the embedded IntrmyAgt3Acct element.
	 * @return the item.
	 */
	public CashAccount24 getIntrmyAgt3Acct() {
		return m_intrmyAgt3Acct;
	}
		
	/**
	 * This method sets (overwrites) the element IntrmyAgt3Acct.
	 * @param data the item that needs to be added.
	 */
	void setIntrmyAgt3Acct(CashAccount24 data) {
		m_intrmyAgt3Acct = data;
	}
		
	/**
	 * Get the embedded CdtrAgt element.
	 * @return the item.
	 */
	public BranchAndFinancialInstitutionIdentification5 getCdtrAgt() {
		return m_cdtrAgt;
	}
		
	/**
	 * This method sets (overwrites) the element CdtrAgt.
	 * @param data the item that needs to be added.
	 */
	void setCdtrAgt(BranchAndFinancialInstitutionIdentification5 data) {
		m_cdtrAgt = data;
	}
		
	/**
	 * Get the embedded CdtrAgtAcct element.
	 * @return the item.
	 */
	public CashAccount24 getCdtrAgtAcct() {
		return m_cdtrAgtAcct;
	}
		
	/**
	 * This method sets (overwrites) the element CdtrAgtAcct.
	 * @param data the item that needs to be added.
	 */
	void setCdtrAgtAcct(CashAccount24 data) {
		m_cdtrAgtAcct = data;
	}
		
	/**
	 * Get the embedded Cdtr element.
	 * @return the item.
	 */
	public BranchAndFinancialInstitutionIdentification5 getCdtr() {
		return m_cdtr;
	}
		
	/**
	 * This method sets (overwrites) the element Cdtr.
	 * @param data the item that needs to be added.
	 */
	void setCdtr(BranchAndFinancialInstitutionIdentification5 data) {
		m_cdtr = data;
	}
		
	/**
	 * Get the embedded CdtrAcct element.
	 * @return the item.
	 */
	public CashAccount24 getCdtrAcct() {
		return m_cdtrAcct;
	}
		
	/**
	 * This method sets (overwrites) the element CdtrAcct.
	 * @param data the item that needs to be added.
	 */
	void setCdtrAcct(CashAccount24 data) {
		m_cdtrAcct = data;
	}
		
	/**
	 * Get the embedded UltmtCdtr element.
	 * @return the item.
	 */
	public BranchAndFinancialInstitutionIdentification5 getUltmtCdtr() {
		return m_ultmtCdtr;
	}
		
	/**
	 * This method sets (overwrites) the element UltmtCdtr.
	 * @param data the item that needs to be added.
	 */
	void setUltmtCdtr(BranchAndFinancialInstitutionIdentification5 data) {
		m_ultmtCdtr = data;
	}
		
	/**
	 * Get the embedded list of InstrForCdtrAgt elements.
	 * @return list of items.
	 */
	public List<InstructionForCreditorAgent2> getInstrForCdtrAgts() {
		return m_instrForCdtrAgtList;
	}
		
	/**
	 * This method adds data to the list of InstrForCdtrAgt.
	 * @param data the item that needs to be added.
	 */
	void setInstrForCdtrAgt(InstructionForCreditorAgent2 data) {
		m_instrForCdtrAgtList.add(data);
	}
		
	/**
	 * Get the embedded list of DrctDbtTxInf elements.
	 * @return list of items.
	 */
	public List<DirectDebitTransactionInformation15> getDrctDbtTxInfs() {
		return m_drctDbtTxInfList;
	}
		
	/**
	 * This method adds data to the list of DrctDbtTxInf.
	 * @param data the item that needs to be added.
	 */
	void setDrctDbtTxInf(DirectDebitTransactionInformation15 data) {
		m_drctDbtTxInfList.add(data);
	}
		
	/**
	 * Get the embedded list of SplmtryData elements.
	 * @return list of items.
	 */
	public List<SupplementaryData1> getSplmtryDatas() {
		return m_splmtryDataList;
	}
		
	/**
	 * This method adds data to the list of SplmtryData.
	 * @param data the item that needs to be added.
	 */
	void setSplmtryData(SupplementaryData1 data) {
		m_splmtryDataList.add(data);
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_cdtId, ((CreditTransferTransaction9)that).m_cdtId))
			return false;
		
		if (!Compare.equals(m_btchBookg, ((CreditTransferTransaction9)that).m_btchBookg))
			return false;
		
		if (!Compare.equals(m_pmtTpInf, ((CreditTransferTransaction9)that).m_pmtTpInf))
			return false;
		
		if (!Compare.equals(m_ttlIntrBkSttlmAmt, ((CreditTransferTransaction9)that).m_ttlIntrBkSttlmAmt))
			return false;
		
		if (!Compare.equals(m_intrBkSttlmDt, ((CreditTransferTransaction9)that).m_intrBkSttlmDt))
			return false;
		
		if (!Compare.equals(m_instgAgt, ((CreditTransferTransaction9)that).m_instgAgt))
			return false;
		
		if (!Compare.equals(m_instdAgt, ((CreditTransferTransaction9)that).m_instdAgt))
			return false;
		
		if (!Compare.equals(m_intrmyAgt1, ((CreditTransferTransaction9)that).m_intrmyAgt1))
			return false;
		
		if (!Compare.equals(m_intrmyAgt1Acct, ((CreditTransferTransaction9)that).m_intrmyAgt1Acct))
			return false;
		
		if (!Compare.equals(m_intrmyAgt2, ((CreditTransferTransaction9)that).m_intrmyAgt2))
			return false;
		
		if (!Compare.equals(m_intrmyAgt2Acct, ((CreditTransferTransaction9)that).m_intrmyAgt2Acct))
			return false;
		
		if (!Compare.equals(m_intrmyAgt3, ((CreditTransferTransaction9)that).m_intrmyAgt3))
			return false;
		
		if (!Compare.equals(m_intrmyAgt3Acct, ((CreditTransferTransaction9)that).m_intrmyAgt3Acct))
			return false;
		
		if (!Compare.equals(m_cdtrAgt, ((CreditTransferTransaction9)that).m_cdtrAgt))
			return false;
		
		if (!Compare.equals(m_cdtrAgtAcct, ((CreditTransferTransaction9)that).m_cdtrAgtAcct))
			return false;
		
		if (!Compare.equals(m_cdtr, ((CreditTransferTransaction9)that).m_cdtr))
			return false;
		
		if (!Compare.equals(m_cdtrAcct, ((CreditTransferTransaction9)that).m_cdtrAcct))
			return false;
		
		if (!Compare.equals(m_ultmtCdtr, ((CreditTransferTransaction9)that).m_ultmtCdtr))
			return false;
		
		if (!Compare.equals(m_instrForCdtrAgtList, ((CreditTransferTransaction9)that).m_instrForCdtrAgtList))
			return false;
		
		if (!Compare.equals(m_drctDbtTxInfList, ((CreditTransferTransaction9)that).m_drctDbtTxInfList))
			return false;
		
		if (!Compare.equals(m_splmtryDataList, ((CreditTransferTransaction9)that).m_splmtryDataList))
			return false;
		
		return true;
	}	

  
  
}
