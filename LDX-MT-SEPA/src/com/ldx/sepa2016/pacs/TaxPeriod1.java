package com.ldx.sepa2016.pacs;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.com
		email: dijkstra@xml2java.com
		support: support@xml2java.com
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.3
  Generated code is compatible with ldx-framework v. 2.3
  License: Dijkstra ICT Consulting 
  Module: PACS 
  Generation date: Sun May 22 21:52:46 CEST 2016 

******************************************************************************/

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * TaxPeriod1 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class TaxPeriod1 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for TaxPeriod1.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public TaxPeriod1(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type TaxPeriod1.
	 */
	static class Allocator implements TypeAllocator<TaxPeriod1> {
		/**
		 * method for getting a new instance of type TaxPeriod1.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public TaxPeriod1 newInstance(String elementName, ComplexDataType parent) {
			return new TaxPeriod1(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for Yr element. 
	 *  @serial
	 */	
	private String m_yr = null;
	
	/** element item for Tp element. 
	 *  @serial
	 */	
	private String m_tp = null;
	
	/** element item for FrToDt element. 
	 *  @serial
	 */	
	private DatePeriodDetails m_frToDt = null;
	
	/**
	 * Get the embedded Yr element.
	 * @return the item.
	 */
	public String getYr() {
		return m_yr;
	}
		
	/**
	 * This method sets (overwrites) the element Yr.
	 * @param data the item that needs to be added.
	 */
	void setYr(String data) {
		m_yr = data;
	}
		
	/**
	 * Get the embedded Tp element.
	 * @return the item.
	 */
	public String getTp() {
		return m_tp;
	}
		
	/**
	 * This method sets (overwrites) the element Tp.
	 * @param data the item that needs to be added.
	 */
	void setTp(String data) {
		m_tp = data;
	}
		
	/**
	 * Get the embedded FrToDt element.
	 * @return the item.
	 */
	public DatePeriodDetails getFrToDt() {
		return m_frToDt;
	}
		
	/**
	 * This method sets (overwrites) the element FrToDt.
	 * @param data the item that needs to be added.
	 */
	void setFrToDt(DatePeriodDetails data) {
		m_frToDt = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_yr, ((TaxPeriod1)that).m_yr))
			return false;
		
		if (!Compare.equals(m_tp, ((TaxPeriod1)that).m_tp))
			return false;
		
		if (!Compare.equals(m_frToDt, ((TaxPeriod1)that).m_frToDt))
			return false;
		
		return true;
	}	

  
  
}
