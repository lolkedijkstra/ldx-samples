package com.ldx.sepa2016.pacs;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.com
		email: dijkstra@xml2java.com
		support: support@xml2java.com
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.3
  Generated code is compatible with ldx-framework v. 2.3
  License: Dijkstra ICT Consulting 
  Module: PACS 
  Generation date: Sun May 22 21:52:46 CEST 2016 

******************************************************************************/

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * SettlementTimeRequest2 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class SettlementTimeRequest2 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for SettlementTimeRequest2.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public SettlementTimeRequest2(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type SettlementTimeRequest2.
	 */
	static class Allocator implements TypeAllocator<SettlementTimeRequest2> {
		/**
		 * method for getting a new instance of type SettlementTimeRequest2.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public SettlementTimeRequest2 newInstance(String elementName, ComplexDataType parent) {
			return new SettlementTimeRequest2(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for CLSTm element. 
	 *  @serial
	 */	
	private String m_cLSTm = null;
	
	/** element item for TillTm element. 
	 *  @serial
	 */	
	private String m_tillTm = null;
	
	/** element item for FrTm element. 
	 *  @serial
	 */	
	private String m_frTm = null;
	
	/** element item for RjctTm element. 
	 *  @serial
	 */	
	private String m_rjctTm = null;
	
	/**
	 * Get the embedded CLSTm element.
	 * @return the item.
	 */
	public String getCLSTm() {
		return m_cLSTm;
	}
		
	/**
	 * This method sets (overwrites) the element CLSTm.
	 * @param data the item that needs to be added.
	 */
	void setCLSTm(String data) {
		m_cLSTm = data;
	}
		
	/**
	 * Get the embedded TillTm element.
	 * @return the item.
	 */
	public String getTillTm() {
		return m_tillTm;
	}
		
	/**
	 * This method sets (overwrites) the element TillTm.
	 * @param data the item that needs to be added.
	 */
	void setTillTm(String data) {
		m_tillTm = data;
	}
		
	/**
	 * Get the embedded FrTm element.
	 * @return the item.
	 */
	public String getFrTm() {
		return m_frTm;
	}
		
	/**
	 * This method sets (overwrites) the element FrTm.
	 * @param data the item that needs to be added.
	 */
	void setFrTm(String data) {
		m_frTm = data;
	}
		
	/**
	 * Get the embedded RjctTm element.
	 * @return the item.
	 */
	public String getRjctTm() {
		return m_rjctTm;
	}
		
	/**
	 * This method sets (overwrites) the element RjctTm.
	 * @param data the item that needs to be added.
	 */
	void setRjctTm(String data) {
		m_rjctTm = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_cLSTm, ((SettlementTimeRequest2)that).m_cLSTm))
			return false;
		
		if (!Compare.equals(m_tillTm, ((SettlementTimeRequest2)that).m_tillTm))
			return false;
		
		if (!Compare.equals(m_frTm, ((SettlementTimeRequest2)that).m_frTm))
			return false;
		
		if (!Compare.equals(m_rjctTm, ((SettlementTimeRequest2)that).m_rjctTm))
			return false;
		
		return true;
	}	

  
  
}
