package com.ldx.sepa2016.pacs;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.com
		email: dijkstra@xml2java.com
		support: support@xml2java.com
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.3
  Generated code is compatible with ldx-framework v. 2.3
  License: Dijkstra ICT Consulting 
  Module: PACS 
  Generation date: Sun May 22 21:52:46 CEST 2016 

******************************************************************************/

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * PaymentIdentification3 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class PaymentIdentification3 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for PaymentIdentification3.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public PaymentIdentification3(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type PaymentIdentification3.
	 */
	static class Allocator implements TypeAllocator<PaymentIdentification3> {
		/**
		 * method for getting a new instance of type PaymentIdentification3.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public PaymentIdentification3 newInstance(String elementName, ComplexDataType parent) {
			return new PaymentIdentification3(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for InstrId element. 
	 *  @serial
	 */	
	private String m_instrId = null;
	
	/** element item for EndToEndId element. 
	 *  @serial
	 */	
	private String m_endToEndId = null;
	
	/** element item for TxId element. 
	 *  @serial
	 */	
	private String m_txId = null;
	
	/** element item for ClrSysRef element. 
	 *  @serial
	 */	
	private String m_clrSysRef = null;
	
	/**
	 * Get the embedded InstrId element.
	 * @return the item.
	 */
	public String getInstrId() {
		return m_instrId;
	}
		
	/**
	 * This method sets (overwrites) the element InstrId.
	 * @param data the item that needs to be added.
	 */
	void setInstrId(String data) {
		m_instrId = data;
	}
		
	/**
	 * Get the embedded EndToEndId element.
	 * @return the item.
	 */
	public String getEndToEndId() {
		return m_endToEndId;
	}
		
	/**
	 * This method sets (overwrites) the element EndToEndId.
	 * @param data the item that needs to be added.
	 */
	void setEndToEndId(String data) {
		m_endToEndId = data;
	}
		
	/**
	 * Get the embedded TxId element.
	 * @return the item.
	 */
	public String getTxId() {
		return m_txId;
	}
		
	/**
	 * This method sets (overwrites) the element TxId.
	 * @param data the item that needs to be added.
	 */
	void setTxId(String data) {
		m_txId = data;
	}
		
	/**
	 * Get the embedded ClrSysRef element.
	 * @return the item.
	 */
	public String getClrSysRef() {
		return m_clrSysRef;
	}
		
	/**
	 * This method sets (overwrites) the element ClrSysRef.
	 * @param data the item that needs to be added.
	 */
	void setClrSysRef(String data) {
		m_clrSysRef = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_instrId, ((PaymentIdentification3)that).m_instrId))
			return false;
		
		if (!Compare.equals(m_endToEndId, ((PaymentIdentification3)that).m_endToEndId))
			return false;
		
		if (!Compare.equals(m_txId, ((PaymentIdentification3)that).m_txId))
			return false;
		
		if (!Compare.equals(m_clrSysRef, ((PaymentIdentification3)that).m_clrSysRef))
			return false;
		
		return true;
	}	

  
  
}
