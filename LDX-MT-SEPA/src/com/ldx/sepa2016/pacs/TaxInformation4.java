package com.ldx.sepa2016.pacs;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.com
		email: dijkstra@xml2java.com
		support: support@xml2java.com
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.3
  Generated code is compatible with ldx-framework v. 2.3
  License: Dijkstra ICT Consulting 
  Module: PACS 
  Generation date: Sun May 22 21:52:46 CEST 2016 

******************************************************************************/

import java.util.List;
import java.util.ArrayList;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * TaxInformation4 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class TaxInformation4 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for TaxInformation4.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public TaxInformation4(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type TaxInformation4.
	 */
	static class Allocator implements TypeAllocator<TaxInformation4> {
		/**
		 * method for getting a new instance of type TaxInformation4.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public TaxInformation4 newInstance(String elementName, ComplexDataType parent) {
			return new TaxInformation4(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for Cdtr element. 
	 *  @serial
	 */	
	private TaxParty1 m_cdtr = null;
	
	/** element item for Dbtr element. 
	 *  @serial
	 */	
	private TaxParty2 m_dbtr = null;
	
	/** element item for UltmtDbtr element. 
	 *  @serial
	 */	
	private TaxParty2 m_ultmtDbtr = null;
	
	/** element item for AdmstnZone element. 
	 *  @serial
	 */	
	private String m_admstnZone = null;
	
	/** element item for RefNb element. 
	 *  @serial
	 */	
	private String m_refNb = null;
	
	/** element item for Mtd element. 
	 *  @serial
	 */	
	private String m_mtd = null;
	
	/** element item for TtlTaxblBaseAmt element. 
	 *  @serial
	 */	
	private ActiveOrHistoricCurrencyAndAmount m_ttlTaxblBaseAmt = null;
	
	/** element item for TtlTaxAmt element. 
	 *  @serial
	 */	
	private ActiveOrHistoricCurrencyAndAmount m_ttlTaxAmt = null;
	
	/** element item for Dt element. 
	 *  @serial
	 */	
	private String m_dt = null;
	
	/** element item for SeqNb element. 
	 *  @serial
	 */	
	private String m_seqNb = null;
	
	/** list of Rcrd element. 
	 *  @serial
	 */	
	private List<TaxRecord1> m_rcrdList = new ArrayList<TaxRecord1>();
	
	/**
	 * Get the embedded Cdtr element.
	 * @return the item.
	 */
	public TaxParty1 getCdtr() {
		return m_cdtr;
	}
		
	/**
	 * This method sets (overwrites) the element Cdtr.
	 * @param data the item that needs to be added.
	 */
	void setCdtr(TaxParty1 data) {
		m_cdtr = data;
	}
		
	/**
	 * Get the embedded Dbtr element.
	 * @return the item.
	 */
	public TaxParty2 getDbtr() {
		return m_dbtr;
	}
		
	/**
	 * This method sets (overwrites) the element Dbtr.
	 * @param data the item that needs to be added.
	 */
	void setDbtr(TaxParty2 data) {
		m_dbtr = data;
	}
		
	/**
	 * Get the embedded UltmtDbtr element.
	 * @return the item.
	 */
	public TaxParty2 getUltmtDbtr() {
		return m_ultmtDbtr;
	}
		
	/**
	 * This method sets (overwrites) the element UltmtDbtr.
	 * @param data the item that needs to be added.
	 */
	void setUltmtDbtr(TaxParty2 data) {
		m_ultmtDbtr = data;
	}
		
	/**
	 * Get the embedded AdmstnZone element.
	 * @return the item.
	 */
	public String getAdmstnZone() {
		return m_admstnZone;
	}
		
	/**
	 * This method sets (overwrites) the element AdmstnZone.
	 * @param data the item that needs to be added.
	 */
	void setAdmstnZone(String data) {
		m_admstnZone = data;
	}
		
	/**
	 * Get the embedded RefNb element.
	 * @return the item.
	 */
	public String getRefNb() {
		return m_refNb;
	}
		
	/**
	 * This method sets (overwrites) the element RefNb.
	 * @param data the item that needs to be added.
	 */
	void setRefNb(String data) {
		m_refNb = data;
	}
		
	/**
	 * Get the embedded Mtd element.
	 * @return the item.
	 */
	public String getMtd() {
		return m_mtd;
	}
		
	/**
	 * This method sets (overwrites) the element Mtd.
	 * @param data the item that needs to be added.
	 */
	void setMtd(String data) {
		m_mtd = data;
	}
		
	/**
	 * Get the embedded TtlTaxblBaseAmt element.
	 * @return the item.
	 */
	public ActiveOrHistoricCurrencyAndAmount getTtlTaxblBaseAmt() {
		return m_ttlTaxblBaseAmt;
	}
		
	/**
	 * This method sets (overwrites) the element TtlTaxblBaseAmt.
	 * @param data the item that needs to be added.
	 */
	void setTtlTaxblBaseAmt(ActiveOrHistoricCurrencyAndAmount data) {
		m_ttlTaxblBaseAmt = data;
	}
		
	/**
	 * Get the embedded TtlTaxAmt element.
	 * @return the item.
	 */
	public ActiveOrHistoricCurrencyAndAmount getTtlTaxAmt() {
		return m_ttlTaxAmt;
	}
		
	/**
	 * This method sets (overwrites) the element TtlTaxAmt.
	 * @param data the item that needs to be added.
	 */
	void setTtlTaxAmt(ActiveOrHistoricCurrencyAndAmount data) {
		m_ttlTaxAmt = data;
	}
		
	/**
	 * Get the embedded Dt element.
	 * @return the item.
	 */
	public String getDt() {
		return m_dt;
	}
		
	/**
	 * This method sets (overwrites) the element Dt.
	 * @param data the item that needs to be added.
	 */
	void setDt(String data) {
		m_dt = data;
	}
		
	/**
	 * Get the embedded SeqNb element.
	 * @return the item.
	 */
	public String getSeqNb() {
		return m_seqNb;
	}
		
	/**
	 * This method sets (overwrites) the element SeqNb.
	 * @param data the item that needs to be added.
	 */
	void setSeqNb(String data) {
		m_seqNb = data;
	}
		
	/**
	 * Get the embedded list of Rcrd elements.
	 * @return list of items.
	 */
	public List<TaxRecord1> getRcrds() {
		return m_rcrdList;
	}
		
	/**
	 * This method adds data to the list of Rcrd.
	 * @param data the item that needs to be added.
	 */
	void setRcrd(TaxRecord1 data) {
		m_rcrdList.add(data);
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_cdtr, ((TaxInformation4)that).m_cdtr))
			return false;
		
		if (!Compare.equals(m_dbtr, ((TaxInformation4)that).m_dbtr))
			return false;
		
		if (!Compare.equals(m_ultmtDbtr, ((TaxInformation4)that).m_ultmtDbtr))
			return false;
		
		if (!Compare.equals(m_admstnZone, ((TaxInformation4)that).m_admstnZone))
			return false;
		
		if (!Compare.equals(m_refNb, ((TaxInformation4)that).m_refNb))
			return false;
		
		if (!Compare.equals(m_mtd, ((TaxInformation4)that).m_mtd))
			return false;
		
		if (!Compare.equals(m_ttlTaxblBaseAmt, ((TaxInformation4)that).m_ttlTaxblBaseAmt))
			return false;
		
		if (!Compare.equals(m_ttlTaxAmt, ((TaxInformation4)that).m_ttlTaxAmt))
			return false;
		
		if (!Compare.equals(m_dt, ((TaxInformation4)that).m_dt))
			return false;
		
		if (!Compare.equals(m_seqNb, ((TaxInformation4)that).m_seqNb))
			return false;
		
		if (!Compare.equals(m_rcrdList, ((TaxInformation4)that).m_rcrdList))
			return false;
		
		return true;
	}	

  
  
}
