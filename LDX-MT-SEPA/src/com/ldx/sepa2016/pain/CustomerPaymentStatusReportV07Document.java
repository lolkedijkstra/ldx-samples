package com.ldx.sepa2016.pain;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.com
		email: dijkstra@xml2java.com
		support: support@xml2java.com
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.3
  Generated code is compatible with ldx-framework v. 2.3
  License: Dijkstra ICT Consulting 
  Module: PAIN 
  Generation date: Sun May 22 21:52:38 CEST 2016 

******************************************************************************/

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * CustomerPaymentStatusReportV07Document data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class CustomerPaymentStatusReportV07Document extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for CustomerPaymentStatusReportV07Document.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public CustomerPaymentStatusReportV07Document(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type CustomerPaymentStatusReportV07Document.
	 */
	static class Allocator implements TypeAllocator<CustomerPaymentStatusReportV07Document> {
		/**
		 * method for getting a new instance of type CustomerPaymentStatusReportV07Document.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public CustomerPaymentStatusReportV07Document newInstance(String elementName, ComplexDataType parent) {
			return new CustomerPaymentStatusReportV07Document(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for CstmrPmtStsRpt element. 
	 *  @serial
	 */	
	private CustomerPaymentStatusReportV07 m_cstmrPmtStsRpt = null;
	
	/**
	 * Get the embedded CstmrPmtStsRpt element.
	 * @return the item.
	 */
	public CustomerPaymentStatusReportV07 getCstmrPmtStsRpt() {
		return m_cstmrPmtStsRpt;
	}
		
	/**
	 * This method sets (overwrites) the element CstmrPmtStsRpt.
	 * @param data the item that needs to be added.
	 */
	void setCstmrPmtStsRpt(CustomerPaymentStatusReportV07 data) {
		m_cstmrPmtStsRpt = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_cstmrPmtStsRpt, ((CustomerPaymentStatusReportV07Document)that).m_cstmrPmtStsRpt))
			return false;
		
		return true;
	}	

  
  
}
