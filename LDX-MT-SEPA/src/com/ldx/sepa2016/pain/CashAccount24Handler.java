package com.ldx.sepa2016.pain;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.com
		email: dijkstra@xml2java.com
		support: support@xml2java.com
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.3
  Generated code is compatible with ldx-framework v. 2.3
  License: Dijkstra ICT Consulting 
  Module: PAIN 
  Generation date: Sun May 22 21:52:40 CEST 2016 

******************************************************************************/

/* SAX 2.0 dependencies */
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/* Framework dependencies */
import com.ldx.xml.core.DataSetter;
import com.ldx.xml.core.ComplexDataType;
import com.ldx.xml.core.XMLEvent;
import com.ldx.xml.core.XMLFragmentHandler;
import com.ldx.xml.parser.ParserTask;


/**
 * CashAccount24 handler class.
 *
 * @see CashAccount24
 * 
 */
public class CashAccount24Handler extends XMLFragmentHandler<CashAccount24> {
	/**
	 * Proxy for CashAccount24Handler.
	 */
	static class Proxy extends HandlerProxy<CashAccount24> {
		/**
		 * Allocator for CashAccount24Handler.
		 */
		private static class Allocator 
				extends HandlerProxy.Allocator<CashAccount24> {			
			public XMLFragmentHandler<CashAccount24> create(
					ParserTask task
					, XMLReader reader
					, XMLFragmentHandler<?> handler
					, String elementName
					, DataSetter setter
					, boolean doProcess) {
				return new CashAccount24Handler(
					task
					, reader
					, handler
					, elementName
					, CashAccount24.getAllocator()
					, setter
					, doProcess);
			}
		}
		
		/**
		 * Constructor for Proxy.
		 *
		 * @param task the XML parser task
		 * @param reader the XML reader
		 * @param parentH the parent XML handler class
		 * @param elementName the name of the XML Element (tag).
		 * @param pSetter the setter for the parent data
		 * @param doProcess indicates whether processing is active for this handler instance
		 */
		public Proxy(ParserTask task, XMLReader reader, XMLFragmentHandler<?> parentH, String elementName,
				DataSetter pSetter, boolean doProcess) {
			super(task, reader, parentH, elementName, pSetter, new Allocator(), doProcess);
		}
	}
	
	/** Data setter class for Id element. */
	private class IdSetter implements DataSetter {
		/** data target. */
		private CashAccount24Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public IdSetter(CashAccount24Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setId((AccountIdentification4Choice) data);	
		}
	}	
	/** Data setter class for Tp element. */
	private class TpSetter implements DataSetter {
		/** data target. */
		private CashAccount24Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public TpSetter(CashAccount24Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setTp((CashAccountType2Choice) data);	
		}
	}


	/**
	 * Constructor for handler class.
	 *
	 * @param reader the XML reader
	 * @param parentH the parent XML handler class
	 * @param elementName the name of the XML Element (tag)
	 * @param allocator the allocator for the data type
	 * @param pSetter the setter for the parent data
	 * @param doProcess indicates whether processing is active for this handler instance
	 */
	public CashAccount24Handler(
			ParserTask application
			, XMLReader reader
			, XMLFragmentHandler<?> parentH
			, String elementName
			, CashAccount24.Allocator allocator
			, DataSetter pSetter
			, boolean doProcess) {
		super(application, reader, parentH, elementName, allocator, pSetter, doProcess);

		// code for linking children..
		registerHandler(
			new AccountIdentification4ChoiceHandler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of AccountIdentification4ChoiceHandler
				, "Id" // XML element name
				, doLink("Id") // linking to parent
					? new IdSetter(this) // ON
					: null // OFF
				, doProcess("Id")) // processing active or not
				);
  
		registerHandler(
			new CashAccountType2ChoiceHandler.Proxy(
				application
				, reader	// XML reader
				, this	// 'this' is parent of CashAccountType2ChoiceHandler
				, "Tp" // XML element name
				, doLink("Tp") // linking to parent
					? new TpSetter(this) // ON
					: null // OFF
				, doProcess("Tp")) // processing active or not
				);
  	}


	/**
	 * This method overrides the getData() of the super type, returning the more
	 * specific type.
	 * 
	 */
	@Override 
	public CashAccount24 getData() {
		return (CashAccount24)super.getData();
	}




	/**
	 * This method is called by the XML parser on an end of element event.
	 *
	 * If the localName == 'the name of the element that this handler handles' control is
	 * passed back to the parent handler (future events go there) and the data of
	 * the children of this handler is attached to the data associated to this handler.	
	 * 
	 * The data that is collected is connected to the parent data only if a parentDataSetter
	 * if found (getParentDataSetter()).
	 *
	 * If processing is enabled for a handler, the processor is called.
	 */
	@Override
	public void endElement(String uri, String localName, String name)
		throws SAXException {
		
		// collect data from children..
		if (localName.equals("Ccy")) {
			getData().setCcy(getValue());
			getContents().reset();
		} else if (localName.equals("Nm")) {
			getData().setNm(getValue());
			getContents().reset();
		} else if (localName.equals(getXMLElementName())) {
				
			// return control to parent handler..
			this.deactivate();
			
			// get content of this item..
			getData().setContent(this.getValue());
			
			// attach data to parent (if parent data setter is found)..
			DataSetter setter = getParentDataSetter();
			if (setter != null) {
				setter.set(getData());
			} 
			
			// process data if required..
			if (doProcess()) {
				process(XMLEvent.END);
			}
		}
		
	}	
}
