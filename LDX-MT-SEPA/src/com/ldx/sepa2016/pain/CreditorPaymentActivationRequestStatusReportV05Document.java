package com.ldx.sepa2016.pain;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.com
		email: dijkstra@xml2java.com
		support: support@xml2java.com
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.3
  Generated code is compatible with ldx-framework v. 2.3
  License: Dijkstra ICT Consulting 
  Module: PAIN 
  Generation date: Sun May 22 21:52:40 CEST 2016 

******************************************************************************/

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * CreditorPaymentActivationRequestStatusReportV05Document data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class CreditorPaymentActivationRequestStatusReportV05Document extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for CreditorPaymentActivationRequestStatusReportV05Document.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public CreditorPaymentActivationRequestStatusReportV05Document(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type CreditorPaymentActivationRequestStatusReportV05Document.
	 */
	static class Allocator implements TypeAllocator<CreditorPaymentActivationRequestStatusReportV05Document> {
		/**
		 * method for getting a new instance of type CreditorPaymentActivationRequestStatusReportV05Document.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public CreditorPaymentActivationRequestStatusReportV05Document newInstance(String elementName, ComplexDataType parent) {
			return new CreditorPaymentActivationRequestStatusReportV05Document(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for CdtrPmtActvtnReqStsRpt element. 
	 *  @serial
	 */	
	private CreditorPaymentActivationRequestStatusReportV05 m_cdtrPmtActvtnReqStsRpt = null;
	
	/**
	 * Get the embedded CdtrPmtActvtnReqStsRpt element.
	 * @return the item.
	 */
	public CreditorPaymentActivationRequestStatusReportV05 getCdtrPmtActvtnReqStsRpt() {
		return m_cdtrPmtActvtnReqStsRpt;
	}
		
	/**
	 * This method sets (overwrites) the element CdtrPmtActvtnReqStsRpt.
	 * @param data the item that needs to be added.
	 */
	void setCdtrPmtActvtnReqStsRpt(CreditorPaymentActivationRequestStatusReportV05 data) {
		m_cdtrPmtActvtnReqStsRpt = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_cdtrPmtActvtnReqStsRpt, ((CreditorPaymentActivationRequestStatusReportV05Document)that).m_cdtrPmtActvtnReqStsRpt))
			return false;
		
		return true;
	}	

  
  
}
