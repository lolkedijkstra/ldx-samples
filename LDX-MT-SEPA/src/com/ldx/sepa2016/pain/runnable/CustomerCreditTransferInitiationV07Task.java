package com.ldx.sepa2016.pain.runnable;

/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.com
		email: dijkstra@xml2java.com
		support: support@xml2java.com
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.3
  Generated code is compatible with ldx-framework v. 2.3
  License: Dijkstra ICT Consulting 
  Module: PAIN 
  Generation date: Sun May 22 21:52:38 CEST 2016 

******************************************************************************/

import java.io.IOException;

//-----------------------    	SAX		-----------------------//
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
//-----------------------    	LDX		-----------------------//
import com.ldx.xml.core.MessageHandler;
import com.ldx.xml.core.ParserConfiguration;
import com.ldx.xml.core.ParserConfigurationException;
import com.ldx.xml.parser.ParserRunnable;

import com.ldx.sepa2016.pain.handlers.CustomerCreditTransferInitiationV07MessageHandler;

/**
 * An example implementation of a task.
 * Adapt this to meet your specific requirements.
 * Use XML parser tasks with threads.
 */
public class CustomerCreditTransferInitiationV07Task extends ParserRunnable {
	
	/**
	 * Constructor of the task.
	 * @param configuration the runtime configuration 
	 * @throws org.xml.sax.SAXException
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 */
	public CustomerCreditTransferInitiationV07Task(ParserConfiguration configuration) 
			throws SAXException, ParserConfigurationException, IOException {

		// To use a custom ErrorHandler:
		// a) implement the interface org.xml.sax.ErrorHandler, 
		// b) instantiate the custom ErrorHandler and pass it to the super constructor.
		super(configuration);
	}
	
	/**
	 * Constructor of the task.
	 * @param configuration the runtime configuration 
	 * @param errorHandler the custom error handler 
	 * @throws org.xml.sax.SAXException
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 */
	public CustomerCreditTransferInitiationV07Task(ParserConfiguration configuration, ErrorHandler errorHandler)
			throws SAXException, ParserConfigurationException, IOException {
		super(configuration, errorHandler);
	}

	@Override
	protected MessageHandler getMessageHandler(XMLReader reader) {
		return new CustomerCreditTransferInitiationV07MessageHandler(this, reader);
	}
	
}
