package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * PeriodType_d0e237_a453 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class PeriodType_d0e237_a453 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for PeriodType_d0e237_a453.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public PeriodType_d0e237_a453(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type PeriodType_d0e237_a453.
	 */
	static class Allocator implements TypeAllocator<PeriodType_d0e237_a453> {
		/**
		 * method for getting a new instance of type PeriodType_d0e237_a453.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public PeriodType_d0e237_a453 newInstance(String elementName, ComplexDataType parent) {
			return new PeriodType_d0e237_a453(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for periodNumber element. 
	 *  @serial
	 */	
	private String m_periodNumber = null;
	
	/** element item for periodDesc element. 
	 *  @serial
	 */	
	private String m_periodDesc = null;
	
	/** element item for startDatePeriod element. 
	 *  @serial
	 */	
	private String m_startDatePeriod = null;
	
	/** element item for startTimePeriod element. 
	 *  @serial
	 */	
	private String m_startTimePeriod = null;
	
	/** element item for endDatePeriod element. 
	 *  @serial
	 */	
	private String m_endDatePeriod = null;
	
	/** element item for endTimePeriod element. 
	 *  @serial
	 */	
	private String m_endTimePeriod = null;
	
	/**
	 * Get the embedded PeriodNumber element.
	 * @return the item.
	 */
	public String getPeriodNumber() {
		return m_periodNumber;
	}
		
	/**
	 * This method sets (overwrites) the element PeriodNumber.
	 * @param data the item that needs to be added.
	 */
	void setPeriodNumber(String data) {
		m_periodNumber = data;
	}
		
	/**
	 * Get the embedded PeriodDesc element.
	 * @return the item.
	 */
	public String getPeriodDesc() {
		return m_periodDesc;
	}
		
	/**
	 * This method sets (overwrites) the element PeriodDesc.
	 * @param data the item that needs to be added.
	 */
	void setPeriodDesc(String data) {
		m_periodDesc = data;
	}
		
	/**
	 * Get the embedded StartDatePeriod element.
	 * @return the item.
	 */
	public String getStartDatePeriod() {
		return m_startDatePeriod;
	}
		
	/**
	 * This method sets (overwrites) the element StartDatePeriod.
	 * @param data the item that needs to be added.
	 */
	void setStartDatePeriod(String data) {
		m_startDatePeriod = data;
	}
		
	/**
	 * Get the embedded StartTimePeriod element.
	 * @return the item.
	 */
	public String getStartTimePeriod() {
		return m_startTimePeriod;
	}
		
	/**
	 * This method sets (overwrites) the element StartTimePeriod.
	 * @param data the item that needs to be added.
	 */
	void setStartTimePeriod(String data) {
		m_startTimePeriod = data;
	}
		
	/**
	 * Get the embedded EndDatePeriod element.
	 * @return the item.
	 */
	public String getEndDatePeriod() {
		return m_endDatePeriod;
	}
		
	/**
	 * This method sets (overwrites) the element EndDatePeriod.
	 * @param data the item that needs to be added.
	 */
	void setEndDatePeriod(String data) {
		m_endDatePeriod = data;
	}
		
	/**
	 * Get the embedded EndTimePeriod element.
	 * @return the item.
	 */
	public String getEndTimePeriod() {
		return m_endTimePeriod;
	}
		
	/**
	 * This method sets (overwrites) the element EndTimePeriod.
	 * @param data the item that needs to be added.
	 */
	void setEndTimePeriod(String data) {
		m_endTimePeriod = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_periodNumber, ((PeriodType_d0e237_a453)that).m_periodNumber))
			return false;
		
		if (!Compare.equals(m_periodDesc, ((PeriodType_d0e237_a453)that).m_periodDesc))
			return false;
		
		if (!Compare.equals(m_startDatePeriod, ((PeriodType_d0e237_a453)that).m_startDatePeriod))
			return false;
		
		if (!Compare.equals(m_startTimePeriod, ((PeriodType_d0e237_a453)that).m_startTimePeriod))
			return false;
		
		if (!Compare.equals(m_endDatePeriod, ((PeriodType_d0e237_a453)that).m_endDatePeriod))
			return false;
		
		if (!Compare.equals(m_endTimePeriod, ((PeriodType_d0e237_a453)that).m_endTimePeriod))
			return false;
		
		return true;
	}	

  
  
}
