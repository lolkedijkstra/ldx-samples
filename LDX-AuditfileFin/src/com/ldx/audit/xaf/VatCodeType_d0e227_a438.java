package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * VatCodeType_d0e227_a438 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class VatCodeType_d0e227_a438 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for VatCodeType_d0e227_a438.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public VatCodeType_d0e227_a438(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type VatCodeType_d0e227_a438.
	 */
	static class Allocator implements TypeAllocator<VatCodeType_d0e227_a438> {
		/**
		 * method for getting a new instance of type VatCodeType_d0e227_a438.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public VatCodeType_d0e227_a438 newInstance(String elementName, ComplexDataType parent) {
			return new VatCodeType_d0e227_a438(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for vatID element. 
	 *  @serial
	 */	
	private String m_vatID = null;
	
	/** element item for vatDesc element. 
	 *  @serial
	 */	
	private String m_vatDesc = null;
	
	/** element item for vatToPayAccID element. 
	 *  @serial
	 */	
	private String m_vatToPayAccID = null;
	
	/** element item for vatToClaimAccID element. 
	 *  @serial
	 */	
	private String m_vatToClaimAccID = null;
	
	/**
	 * Get the embedded VatID element.
	 * @return the item.
	 */
	public String getVatID() {
		return m_vatID;
	}
		
	/**
	 * This method sets (overwrites) the element VatID.
	 * @param data the item that needs to be added.
	 */
	void setVatID(String data) {
		m_vatID = data;
	}
		
	/**
	 * Get the embedded VatDesc element.
	 * @return the item.
	 */
	public String getVatDesc() {
		return m_vatDesc;
	}
		
	/**
	 * This method sets (overwrites) the element VatDesc.
	 * @param data the item that needs to be added.
	 */
	void setVatDesc(String data) {
		m_vatDesc = data;
	}
		
	/**
	 * Get the embedded VatToPayAccID element.
	 * @return the item.
	 */
	public String getVatToPayAccID() {
		return m_vatToPayAccID;
	}
		
	/**
	 * This method sets (overwrites) the element VatToPayAccID.
	 * @param data the item that needs to be added.
	 */
	void setVatToPayAccID(String data) {
		m_vatToPayAccID = data;
	}
		
	/**
	 * Get the embedded VatToClaimAccID element.
	 * @return the item.
	 */
	public String getVatToClaimAccID() {
		return m_vatToClaimAccID;
	}
		
	/**
	 * This method sets (overwrites) the element VatToClaimAccID.
	 * @param data the item that needs to be added.
	 */
	void setVatToClaimAccID(String data) {
		m_vatToClaimAccID = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_vatID, ((VatCodeType_d0e227_a438)that).m_vatID))
			return false;
		
		if (!Compare.equals(m_vatDesc, ((VatCodeType_d0e227_a438)that).m_vatDesc))
			return false;
		
		if (!Compare.equals(m_vatToPayAccID, ((VatCodeType_d0e227_a438)that).m_vatToPayAccID))
			return false;
		
		if (!Compare.equals(m_vatToClaimAccID, ((VatCodeType_d0e227_a438)that).m_vatToClaimAccID))
			return false;
		
		return true;
	}	

  
  
}
