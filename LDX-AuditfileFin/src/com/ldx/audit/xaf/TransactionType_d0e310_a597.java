package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

import java.util.List;
import java.util.ArrayList;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * TransactionType_d0e310_a597 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class TransactionType_d0e310_a597 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for TransactionType_d0e310_a597.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public TransactionType_d0e310_a597(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type TransactionType_d0e310_a597.
	 */
	static class Allocator implements TypeAllocator<TransactionType_d0e310_a597> {
		/**
		 * method for getting a new instance of type TransactionType_d0e310_a597.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public TransactionType_d0e310_a597 newInstance(String elementName, ComplexDataType parent) {
			return new TransactionType_d0e310_a597(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for nr element. 
	 *  @serial
	 */	
	private String m_nr = null;
	
	/** element item for desc element. 
	 *  @serial
	 */	
	private String m_desc = null;
	
	/** element item for periodNumber element. 
	 *  @serial
	 */	
	private String m_periodNumber = null;
	
	/** element item for trDt element. 
	 *  @serial
	 */	
	private String m_trDt = null;
	
	/** element item for amnt element. 
	 *  @serial
	 */	
	private String m_amnt = null;
	
	/** element item for amntTp element. 
	 *  @serial
	 */	
	private String m_amntTp = null;
	
	/** element item for sourceID element. 
	 *  @serial
	 */	
	private String m_sourceID = null;
	
	/** element item for userID element. 
	 *  @serial
	 */	
	private String m_userID = null;
	
	/** list of trLine element. 
	 *  @serial
	 */	
	private List<TrLineType_d0e321_a621> m_trLineList = new ArrayList<TrLineType_d0e321_a621>();
	
	/**
	 * Get the embedded Nr element.
	 * @return the item.
	 */
	public String getNr() {
		return m_nr;
	}
		
	/**
	 * This method sets (overwrites) the element Nr.
	 * @param data the item that needs to be added.
	 */
	void setNr(String data) {
		m_nr = data;
	}
		
	/**
	 * Get the embedded Desc element.
	 * @return the item.
	 */
	public String getDesc() {
		return m_desc;
	}
		
	/**
	 * This method sets (overwrites) the element Desc.
	 * @param data the item that needs to be added.
	 */
	void setDesc(String data) {
		m_desc = data;
	}
		
	/**
	 * Get the embedded PeriodNumber element.
	 * @return the item.
	 */
	public String getPeriodNumber() {
		return m_periodNumber;
	}
		
	/**
	 * This method sets (overwrites) the element PeriodNumber.
	 * @param data the item that needs to be added.
	 */
	void setPeriodNumber(String data) {
		m_periodNumber = data;
	}
		
	/**
	 * Get the embedded TrDt element.
	 * @return the item.
	 */
	public String getTrDt() {
		return m_trDt;
	}
		
	/**
	 * This method sets (overwrites) the element TrDt.
	 * @param data the item that needs to be added.
	 */
	void setTrDt(String data) {
		m_trDt = data;
	}
		
	/**
	 * Get the embedded Amnt element.
	 * @return the item.
	 */
	public String getAmnt() {
		return m_amnt;
	}
		
	/**
	 * This method sets (overwrites) the element Amnt.
	 * @param data the item that needs to be added.
	 */
	void setAmnt(String data) {
		m_amnt = data;
	}
		
	/**
	 * Get the embedded AmntTp element.
	 * @return the item.
	 */
	public String getAmntTp() {
		return m_amntTp;
	}
		
	/**
	 * This method sets (overwrites) the element AmntTp.
	 * @param data the item that needs to be added.
	 */
	void setAmntTp(String data) {
		m_amntTp = data;
	}
		
	/**
	 * Get the embedded SourceID element.
	 * @return the item.
	 */
	public String getSourceID() {
		return m_sourceID;
	}
		
	/**
	 * This method sets (overwrites) the element SourceID.
	 * @param data the item that needs to be added.
	 */
	void setSourceID(String data) {
		m_sourceID = data;
	}
		
	/**
	 * Get the embedded UserID element.
	 * @return the item.
	 */
	public String getUserID() {
		return m_userID;
	}
		
	/**
	 * This method sets (overwrites) the element UserID.
	 * @param data the item that needs to be added.
	 */
	void setUserID(String data) {
		m_userID = data;
	}
		
	/**
	 * Get the embedded list of TrLine elements.
	 * @return list of items.
	 */
	public List<TrLineType_d0e321_a621> getTrLines() {
		return m_trLineList;
	}
		
	/**
	 * This method adds data to the list of TrLine.
	 * @param data the item that needs to be added.
	 */
	void setTrLine(TrLineType_d0e321_a621 data) {
		m_trLineList.add(data);
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_nr, ((TransactionType_d0e310_a597)that).m_nr))
			return false;
		
		if (!Compare.equals(m_desc, ((TransactionType_d0e310_a597)that).m_desc))
			return false;
		
		if (!Compare.equals(m_periodNumber, ((TransactionType_d0e310_a597)that).m_periodNumber))
			return false;
		
		if (!Compare.equals(m_trDt, ((TransactionType_d0e310_a597)that).m_trDt))
			return false;
		
		if (!Compare.equals(m_amnt, ((TransactionType_d0e310_a597)that).m_amnt))
			return false;
		
		if (!Compare.equals(m_amntTp, ((TransactionType_d0e310_a597)that).m_amntTp))
			return false;
		
		if (!Compare.equals(m_sourceID, ((TransactionType_d0e310_a597)that).m_sourceID))
			return false;
		
		if (!Compare.equals(m_userID, ((TransactionType_d0e310_a597)that).m_userID))
			return false;
		
		if (!Compare.equals(m_trLineList, ((TransactionType_d0e310_a597)that).m_trLineList))
			return false;
		
		return true;
	}	

  
  
}
