package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * PostalAddressType_d0e33_a55 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class PostalAddressType_d0e33_a55 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for PostalAddressType_d0e33_a55.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public PostalAddressType_d0e33_a55(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type PostalAddressType_d0e33_a55.
	 */
	static class Allocator implements TypeAllocator<PostalAddressType_d0e33_a55> {
		/**
		 * method for getting a new instance of type PostalAddressType_d0e33_a55.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public PostalAddressType_d0e33_a55 newInstance(String elementName, ComplexDataType parent) {
			return new PostalAddressType_d0e33_a55(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for streetname element. 
	 *  @serial
	 */	
	private String m_streetname = null;
	
	/** element item for number element. 
	 *  @serial
	 */	
	private String m_number = null;
	
	/** element item for numberExtension element. 
	 *  @serial
	 */	
	private String m_numberExtension = null;
	
	/** element item for property element. 
	 *  @serial
	 */	
	private String m_property = null;
	
	/** element item for city element. 
	 *  @serial
	 */	
	private String m_city = null;
	
	/** element item for postalCode element. 
	 *  @serial
	 */	
	private String m_postalCode = null;
	
	/** element item for region element. 
	 *  @serial
	 */	
	private String m_region = null;
	
	/** element item for country element. 
	 *  @serial
	 */	
	private String m_country = null;
	
	/**
	 * Get the embedded Streetname element.
	 * @return the item.
	 */
	public String getStreetname() {
		return m_streetname;
	}
		
	/**
	 * This method sets (overwrites) the element Streetname.
	 * @param data the item that needs to be added.
	 */
	void setStreetname(String data) {
		m_streetname = data;
	}
		
	/**
	 * Get the embedded Number element.
	 * @return the item.
	 */
	public String getNumber() {
		return m_number;
	}
		
	/**
	 * This method sets (overwrites) the element Number.
	 * @param data the item that needs to be added.
	 */
	void setNumber(String data) {
		m_number = data;
	}
		
	/**
	 * Get the embedded NumberExtension element.
	 * @return the item.
	 */
	public String getNumberExtension() {
		return m_numberExtension;
	}
		
	/**
	 * This method sets (overwrites) the element NumberExtension.
	 * @param data the item that needs to be added.
	 */
	void setNumberExtension(String data) {
		m_numberExtension = data;
	}
		
	/**
	 * Get the embedded Property element.
	 * @return the item.
	 */
	public String getProperty() {
		return m_property;
	}
		
	/**
	 * This method sets (overwrites) the element Property.
	 * @param data the item that needs to be added.
	 */
	void setProperty(String data) {
		m_property = data;
	}
		
	/**
	 * Get the embedded City element.
	 * @return the item.
	 */
	public String getCity() {
		return m_city;
	}
		
	/**
	 * This method sets (overwrites) the element City.
	 * @param data the item that needs to be added.
	 */
	void setCity(String data) {
		m_city = data;
	}
		
	/**
	 * Get the embedded PostalCode element.
	 * @return the item.
	 */
	public String getPostalCode() {
		return m_postalCode;
	}
		
	/**
	 * This method sets (overwrites) the element PostalCode.
	 * @param data the item that needs to be added.
	 */
	void setPostalCode(String data) {
		m_postalCode = data;
	}
		
	/**
	 * Get the embedded Region element.
	 * @return the item.
	 */
	public String getRegion() {
		return m_region;
	}
		
	/**
	 * This method sets (overwrites) the element Region.
	 * @param data the item that needs to be added.
	 */
	void setRegion(String data) {
		m_region = data;
	}
		
	/**
	 * Get the embedded Country element.
	 * @return the item.
	 */
	public String getCountry() {
		return m_country;
	}
		
	/**
	 * This method sets (overwrites) the element Country.
	 * @param data the item that needs to be added.
	 */
	void setCountry(String data) {
		m_country = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_streetname, ((PostalAddressType_d0e33_a55)that).m_streetname))
			return false;
		
		if (!Compare.equals(m_number, ((PostalAddressType_d0e33_a55)that).m_number))
			return false;
		
		if (!Compare.equals(m_numberExtension, ((PostalAddressType_d0e33_a55)that).m_numberExtension))
			return false;
		
		if (!Compare.equals(m_property, ((PostalAddressType_d0e33_a55)that).m_property))
			return false;
		
		if (!Compare.equals(m_city, ((PostalAddressType_d0e33_a55)that).m_city))
			return false;
		
		if (!Compare.equals(m_postalCode, ((PostalAddressType_d0e33_a55)that).m_postalCode))
			return false;
		
		if (!Compare.equals(m_region, ((PostalAddressType_d0e33_a55)that).m_region))
			return false;
		
		if (!Compare.equals(m_country, ((PostalAddressType_d0e33_a55)that).m_country))
			return false;
		
		return true;
	}	

  
  
}
