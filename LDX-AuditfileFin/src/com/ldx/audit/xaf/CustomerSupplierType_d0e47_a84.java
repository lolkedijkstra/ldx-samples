package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

import java.util.List;
import java.util.ArrayList;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * CustomerSupplierType_d0e47_a84 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class CustomerSupplierType_d0e47_a84 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for CustomerSupplierType_d0e47_a84.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public CustomerSupplierType_d0e47_a84(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type CustomerSupplierType_d0e47_a84.
	 */
	static class Allocator implements TypeAllocator<CustomerSupplierType_d0e47_a84> {
		/**
		 * method for getting a new instance of type CustomerSupplierType_d0e47_a84.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public CustomerSupplierType_d0e47_a84 newInstance(String elementName, ComplexDataType parent) {
			return new CustomerSupplierType_d0e47_a84(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for custSupID element. 
	 *  @serial
	 */	
	private String m_custSupID = null;
	
	/** element item for custSupName element. 
	 *  @serial
	 */	
	private String m_custSupName = null;
	
	/** element item for contact element. 
	 *  @serial
	 */	
	private String m_contact = null;
	
	/** element item for telephone element. 
	 *  @serial
	 */	
	private String m_telephone = null;
	
	/** element item for fax element. 
	 *  @serial
	 */	
	private String m_fax = null;
	
	/** element item for eMail element. 
	 *  @serial
	 */	
	private String m_eMail = null;
	
	/** element item for website element. 
	 *  @serial
	 */	
	private String m_website = null;
	
	/** element item for commerceNr element. 
	 *  @serial
	 */	
	private String m_commerceNr = null;
	
	/** element item for taxRegistrationCountry element. 
	 *  @serial
	 */	
	private String m_taxRegistrationCountry = null;
	
	/** element item for taxRegIdent element. 
	 *  @serial
	 */	
	private String m_taxRegIdent = null;
	
	/** element item for relationshipID element. 
	 *  @serial
	 */	
	private String m_relationshipID = null;
	
	/** element item for custSupTp element. 
	 *  @serial
	 */	
	private String m_custSupTp = null;
	
	/** element item for custSupGrpID element. 
	 *  @serial
	 */	
	private String m_custSupGrpID = null;
	
	/** element item for custCreditLimit element. 
	 *  @serial
	 */	
	private String m_custCreditLimit = null;
	
	/** element item for supplierLimit element. 
	 *  @serial
	 */	
	private String m_supplierLimit = null;
	
	/** list of streetAddress element. 
	 *  @serial
	 */	
	private List<StreetAddressType_d0e65_a131> m_streetAddressList = new ArrayList<StreetAddressType_d0e65_a131>();
	
	/** list of postalAddress element. 
	 *  @serial
	 */	
	private List<PostalAddressType_d0e76_a158> m_postalAddressList = new ArrayList<PostalAddressType_d0e76_a158>();
	
	/** list of bankAccount element. 
	 *  @serial
	 */	
	private List<BankAccountType_d0e87_a185> m_bankAccountList = new ArrayList<BankAccountType_d0e87_a185>();
	
	/** element item for changeInfo element. 
	 *  @serial
	 */	
	private ChangeInfoType_d0e92_a193 m_changeInfo = null;
	
	/** element item for customerSupplierHistory element. 
	 *  @serial
	 */	
	private CustomerSupplierHistoryType_d0e98_a204 m_customerSupplierHistory = null;
	
	/**
	 * Get the embedded CustSupID element.
	 * @return the item.
	 */
	public String getCustSupID() {
		return m_custSupID;
	}
		
	/**
	 * This method sets (overwrites) the element CustSupID.
	 * @param data the item that needs to be added.
	 */
	void setCustSupID(String data) {
		m_custSupID = data;
	}
		
	/**
	 * Get the embedded CustSupName element.
	 * @return the item.
	 */
	public String getCustSupName() {
		return m_custSupName;
	}
		
	/**
	 * This method sets (overwrites) the element CustSupName.
	 * @param data the item that needs to be added.
	 */
	void setCustSupName(String data) {
		m_custSupName = data;
	}
		
	/**
	 * Get the embedded Contact element.
	 * @return the item.
	 */
	public String getContact() {
		return m_contact;
	}
		
	/**
	 * This method sets (overwrites) the element Contact.
	 * @param data the item that needs to be added.
	 */
	void setContact(String data) {
		m_contact = data;
	}
		
	/**
	 * Get the embedded Telephone element.
	 * @return the item.
	 */
	public String getTelephone() {
		return m_telephone;
	}
		
	/**
	 * This method sets (overwrites) the element Telephone.
	 * @param data the item that needs to be added.
	 */
	void setTelephone(String data) {
		m_telephone = data;
	}
		
	/**
	 * Get the embedded Fax element.
	 * @return the item.
	 */
	public String getFax() {
		return m_fax;
	}
		
	/**
	 * This method sets (overwrites) the element Fax.
	 * @param data the item that needs to be added.
	 */
	void setFax(String data) {
		m_fax = data;
	}
		
	/**
	 * Get the embedded EMail element.
	 * @return the item.
	 */
	public String getEMail() {
		return m_eMail;
	}
		
	/**
	 * This method sets (overwrites) the element EMail.
	 * @param data the item that needs to be added.
	 */
	void setEMail(String data) {
		m_eMail = data;
	}
		
	/**
	 * Get the embedded Website element.
	 * @return the item.
	 */
	public String getWebsite() {
		return m_website;
	}
		
	/**
	 * This method sets (overwrites) the element Website.
	 * @param data the item that needs to be added.
	 */
	void setWebsite(String data) {
		m_website = data;
	}
		
	/**
	 * Get the embedded CommerceNr element.
	 * @return the item.
	 */
	public String getCommerceNr() {
		return m_commerceNr;
	}
		
	/**
	 * This method sets (overwrites) the element CommerceNr.
	 * @param data the item that needs to be added.
	 */
	void setCommerceNr(String data) {
		m_commerceNr = data;
	}
		
	/**
	 * Get the embedded TaxRegistrationCountry element.
	 * @return the item.
	 */
	public String getTaxRegistrationCountry() {
		return m_taxRegistrationCountry;
	}
		
	/**
	 * This method sets (overwrites) the element TaxRegistrationCountry.
	 * @param data the item that needs to be added.
	 */
	void setTaxRegistrationCountry(String data) {
		m_taxRegistrationCountry = data;
	}
		
	/**
	 * Get the embedded TaxRegIdent element.
	 * @return the item.
	 */
	public String getTaxRegIdent() {
		return m_taxRegIdent;
	}
		
	/**
	 * This method sets (overwrites) the element TaxRegIdent.
	 * @param data the item that needs to be added.
	 */
	void setTaxRegIdent(String data) {
		m_taxRegIdent = data;
	}
		
	/**
	 * Get the embedded RelationshipID element.
	 * @return the item.
	 */
	public String getRelationshipID() {
		return m_relationshipID;
	}
		
	/**
	 * This method sets (overwrites) the element RelationshipID.
	 * @param data the item that needs to be added.
	 */
	void setRelationshipID(String data) {
		m_relationshipID = data;
	}
		
	/**
	 * Get the embedded CustSupTp element.
	 * @return the item.
	 */
	public String getCustSupTp() {
		return m_custSupTp;
	}
		
	/**
	 * This method sets (overwrites) the element CustSupTp.
	 * @param data the item that needs to be added.
	 */
	void setCustSupTp(String data) {
		m_custSupTp = data;
	}
		
	/**
	 * Get the embedded CustSupGrpID element.
	 * @return the item.
	 */
	public String getCustSupGrpID() {
		return m_custSupGrpID;
	}
		
	/**
	 * This method sets (overwrites) the element CustSupGrpID.
	 * @param data the item that needs to be added.
	 */
	void setCustSupGrpID(String data) {
		m_custSupGrpID = data;
	}
		
	/**
	 * Get the embedded CustCreditLimit element.
	 * @return the item.
	 */
	public String getCustCreditLimit() {
		return m_custCreditLimit;
	}
		
	/**
	 * This method sets (overwrites) the element CustCreditLimit.
	 * @param data the item that needs to be added.
	 */
	void setCustCreditLimit(String data) {
		m_custCreditLimit = data;
	}
		
	/**
	 * Get the embedded SupplierLimit element.
	 * @return the item.
	 */
	public String getSupplierLimit() {
		return m_supplierLimit;
	}
		
	/**
	 * This method sets (overwrites) the element SupplierLimit.
	 * @param data the item that needs to be added.
	 */
	void setSupplierLimit(String data) {
		m_supplierLimit = data;
	}
		
	/**
	 * Get the embedded list of StreetAddress elements.
	 * @return list of items.
	 */
	public List<StreetAddressType_d0e65_a131> getStreetAddresss() {
		return m_streetAddressList;
	}
		
	/**
	 * This method adds data to the list of StreetAddress.
	 * @param data the item that needs to be added.
	 */
	void setStreetAddress(StreetAddressType_d0e65_a131 data) {
		m_streetAddressList.add(data);
	}
		
	/**
	 * Get the embedded list of PostalAddress elements.
	 * @return list of items.
	 */
	public List<PostalAddressType_d0e76_a158> getPostalAddresss() {
		return m_postalAddressList;
	}
		
	/**
	 * This method adds data to the list of PostalAddress.
	 * @param data the item that needs to be added.
	 */
	void setPostalAddress(PostalAddressType_d0e76_a158 data) {
		m_postalAddressList.add(data);
	}
		
	/**
	 * Get the embedded list of BankAccount elements.
	 * @return list of items.
	 */
	public List<BankAccountType_d0e87_a185> getBankAccounts() {
		return m_bankAccountList;
	}
		
	/**
	 * This method adds data to the list of BankAccount.
	 * @param data the item that needs to be added.
	 */
	void setBankAccount(BankAccountType_d0e87_a185 data) {
		m_bankAccountList.add(data);
	}
		
	/**
	 * Get the embedded ChangeInfo element.
	 * @return the item.
	 */
	public ChangeInfoType_d0e92_a193 getChangeInfo() {
		return m_changeInfo;
	}
		
	/**
	 * This method sets (overwrites) the element ChangeInfo.
	 * @param data the item that needs to be added.
	 */
	void setChangeInfo(ChangeInfoType_d0e92_a193 data) {
		m_changeInfo = data;
	}
		
	/**
	 * Get the embedded CustomerSupplierHistory element.
	 * @return the item.
	 */
	public CustomerSupplierHistoryType_d0e98_a204 getCustomerSupplierHistory() {
		return m_customerSupplierHistory;
	}
		
	/**
	 * This method sets (overwrites) the element CustomerSupplierHistory.
	 * @param data the item that needs to be added.
	 */
	void setCustomerSupplierHistory(CustomerSupplierHistoryType_d0e98_a204 data) {
		m_customerSupplierHistory = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_custSupID, ((CustomerSupplierType_d0e47_a84)that).m_custSupID))
			return false;
		
		if (!Compare.equals(m_custSupName, ((CustomerSupplierType_d0e47_a84)that).m_custSupName))
			return false;
		
		if (!Compare.equals(m_contact, ((CustomerSupplierType_d0e47_a84)that).m_contact))
			return false;
		
		if (!Compare.equals(m_telephone, ((CustomerSupplierType_d0e47_a84)that).m_telephone))
			return false;
		
		if (!Compare.equals(m_fax, ((CustomerSupplierType_d0e47_a84)that).m_fax))
			return false;
		
		if (!Compare.equals(m_eMail, ((CustomerSupplierType_d0e47_a84)that).m_eMail))
			return false;
		
		if (!Compare.equals(m_website, ((CustomerSupplierType_d0e47_a84)that).m_website))
			return false;
		
		if (!Compare.equals(m_commerceNr, ((CustomerSupplierType_d0e47_a84)that).m_commerceNr))
			return false;
		
		if (!Compare.equals(m_taxRegistrationCountry, ((CustomerSupplierType_d0e47_a84)that).m_taxRegistrationCountry))
			return false;
		
		if (!Compare.equals(m_taxRegIdent, ((CustomerSupplierType_d0e47_a84)that).m_taxRegIdent))
			return false;
		
		if (!Compare.equals(m_relationshipID, ((CustomerSupplierType_d0e47_a84)that).m_relationshipID))
			return false;
		
		if (!Compare.equals(m_custSupTp, ((CustomerSupplierType_d0e47_a84)that).m_custSupTp))
			return false;
		
		if (!Compare.equals(m_custSupGrpID, ((CustomerSupplierType_d0e47_a84)that).m_custSupGrpID))
			return false;
		
		if (!Compare.equals(m_custCreditLimit, ((CustomerSupplierType_d0e47_a84)that).m_custCreditLimit))
			return false;
		
		if (!Compare.equals(m_supplierLimit, ((CustomerSupplierType_d0e47_a84)that).m_supplierLimit))
			return false;
		
		if (!Compare.equals(m_streetAddressList, ((CustomerSupplierType_d0e47_a84)that).m_streetAddressList))
			return false;
		
		if (!Compare.equals(m_postalAddressList, ((CustomerSupplierType_d0e47_a84)that).m_postalAddressList))
			return false;
		
		if (!Compare.equals(m_bankAccountList, ((CustomerSupplierType_d0e47_a84)that).m_bankAccountList))
			return false;
		
		if (!Compare.equals(m_changeInfo, ((CustomerSupplierType_d0e47_a84)that).m_changeInfo))
			return false;
		
		if (!Compare.equals(m_customerSupplierHistory, ((CustomerSupplierType_d0e47_a84)that).m_customerSupplierHistory))
			return false;
		
		return true;
	}	

  
  
}
