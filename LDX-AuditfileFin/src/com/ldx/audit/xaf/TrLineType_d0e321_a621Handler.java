package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

/* SAX 2.0 dependencies */
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/* Framework dependencies */
import com.ldx.xml.core.DataSetter;
import com.ldx.xml.core.ComplexDataType;
import com.ldx.xml.core.XMLEvent;
import com.ldx.xml.core.XMLFragmentHandler;


/**
 * TrLineType_d0e321_a621 handler class.
 *
 * @see TrLineType_d0e321_a621
 * 
 */
public class TrLineType_d0e321_a621Handler extends XMLFragmentHandler<TrLineType_d0e321_a621> {
	/**
	 * Proxy for TrLineType_d0e321_a621Handler.
	 */
	static class Proxy extends HandlerProxy<TrLineType_d0e321_a621> {
		/**
		 * Allocator for TrLineType_d0e321_a621Handler.
		 */
		private static class Allocator 
				extends HandlerProxy.Allocator<TrLineType_d0e321_a621> {			
			public XMLFragmentHandler<TrLineType_d0e321_a621> create(
					XMLReader reader
					, XMLFragmentHandler<?> handler
					, String elementName
					, DataSetter setter
					, boolean doProcess) {
				return new TrLineType_d0e321_a621Handler(
					reader
					, handler
					, elementName
					, TrLineType_d0e321_a621.getAllocator()
					, setter
					, doProcess);
			}
		}
		
		/**
		 * Constructor for Proxy.
		 *
		 * @param reader the XML reader
		 * @param parentH the parent XML handler class
		 * @param elementName the name of the XML Element (tag).
		 * @param pSetter the setter for the parent data
		 * @param doProcess indicates whether processing is active for this handler instance
		 */
		public Proxy(XMLReader reader, XMLFragmentHandler<?> parentH, String elementName,
				DataSetter pSetter, boolean doProcess) {
			super(reader, parentH, elementName, pSetter, new Allocator(), doProcess);
		}
	}
	
	/** Data setter class for currency element. */
	private class CurrencySetter implements DataSetter {
		/** data target. */
		private TrLineType_d0e321_a621Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public CurrencySetter(TrLineType_d0e321_a621Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setCurrency((CurrencyType_d0e354_a698) data);	
		}
	}	
	/** Data setter class for vat element. */
	private class VatSetter implements DataSetter {
		/** data target. */
		private TrLineType_d0e321_a621Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public VatSetter(TrLineType_d0e321_a621Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setVat((VatType_d0e347_a687) data);	
		}
	}


	/**
	 * Constructor for handler class.
	 *
	 * @param reader the XML reader
	 * @param parentH the parent XML handler class
	 * @param elementName the name of the XML Element (tag)
	 * @param allocator the allocator for the data type
	 * @param pSetter the setter for the parent data
	 * @param doProcess indicates whether processing is active for this handler instance
	 */
	public TrLineType_d0e321_a621Handler(
			XMLReader reader
			, XMLFragmentHandler<?> parentH
			, String elementName
			, TrLineType_d0e321_a621.Allocator allocator
			, DataSetter pSetter
			, boolean doProcess) {
		super(reader, parentH, elementName, allocator, pSetter, doProcess);

		// code for linking children..
		registerHandler(
			new CurrencyType_d0e354_a698Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of currencyType_d0e354_a698Handler
				, "currency" // XML element name
				, doLink("currency") // linking to parent
					? new CurrencySetter(this) // ON
					: null // OFF
				, doProcess("currency")) // processing active or not
				);
  
		registerHandler(
			new VatType_d0e347_a687Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of vatType_d0e347_a687Handler
				, "vat" // XML element name
				, doLink("vat") // linking to parent
					? new VatSetter(this) // ON
					: null // OFF
				, doProcess("vat")) // processing active or not
				);
  	}


	/**
	 * This method overrides the getData() of the super type, returning the more
	 * specific type.
	 * 
	 */
	@Override 
	public TrLineType_d0e321_a621 getData() {
		return (TrLineType_d0e321_a621)super.getData();
	}




	/**
	 * This method is called by the XML parser on an end of element event.
	 *
	 * If the localName == 'the name of the element that this handler handles' control is
	 * passed back to the parent handler (future events go there) and the data of
	 * the children of this handler is attached to the data associated to this handler.	
	 * 
	 * The data that is collected is connected to the parent data only if a parentDataSetter
	 * if found (getParentDataSetter()).
	 *
	 * If processing is enabled for a handler, the processor is called.
	 */
	@Override
	public void endElement(String uri, String localName, String name)
		throws SAXException {
		
		// collect data from children..
		if (localName.equals("accID")) {
			getData().setAccID(getValue());
			getContents().reset();
		} else if (localName.equals("amnt")) {
			getData().setAmnt(getValue());
			getContents().reset();
		} else if (localName.equals("amntTp")) {
			getData().setAmntTp(getValue());
			getContents().reset();
		} else if (localName.equals("artGrpID")) {
			getData().setArtGrpID(getValue());
			getContents().reset();
		} else if (localName.equals("bankAccNr")) {
			getData().setBankAccNr(getValue());
			getContents().reset();
		} else if (localName.equals("bankIdCd")) {
			getData().setBankIdCd(getValue());
			getContents().reset();
		} else if (localName.equals("costID")) {
			getData().setCostID(getValue());
			getContents().reset();
		} else if (localName.equals("custSupID")) {
			getData().setCustSupID(getValue());
			getContents().reset();
		} else if (localName.equals("desc")) {
			getData().setDesc(getValue());
			getContents().reset();
		} else if (localName.equals("docRef")) {
			getData().setDocRef(getValue());
			getContents().reset();
		} else if (localName.equals("effDate")) {
			getData().setEffDate(getValue());
			getContents().reset();
		} else if (localName.equals("invRef")) {
			getData().setInvRef(getValue());
			getContents().reset();
		} else if (localName.equals("matchKeyID")) {
			getData().setMatchKeyID(getValue());
			getContents().reset();
		} else if (localName.equals("nr")) {
			getData().setNr(getValue());
			getContents().reset();
		} else if (localName.equals("orderRef")) {
			getData().setOrderRef(getValue());
			getContents().reset();
		} else if (localName.equals("prodID")) {
			getData().setProdID(getValue());
			getContents().reset();
		} else if (localName.equals("projID")) {
			getData().setProjID(getValue());
			getContents().reset();
		} else if (localName.equals("qntity")) {
			getData().setQntity(getValue());
			getContents().reset();
		} else if (localName.equals("qntityID")) {
			getData().setQntityID(getValue());
			getContents().reset();
		} else if (localName.equals("receivingDocRef")) {
			getData().setReceivingDocRef(getValue());
			getContents().reset();
		} else if (localName.equals("recRef")) {
			getData().setRecRef(getValue());
			getContents().reset();
		} else if (localName.equals("shipDocRef")) {
			getData().setShipDocRef(getValue());
			getContents().reset();
		} else if (localName.equals("workCostArrRef")) {
			getData().setWorkCostArrRef(getValue());
			getContents().reset();
		} else if (localName.equals(getXMLElementName())) {
				
			// return control to parent handler..
			this.deactivate();
			
			// get content of this item..
			getData().setContent(this.getValue());
			
			// attach data to parent (if parent data setter is found)..
			DataSetter setter = getParentDataSetter();
			if (setter != null) {
				setter.set(getData());
			} 
			
			// process data if required..
			if (doProcess()) {
				process(XMLEvent.END);
			}
		}
		
	}	
}
