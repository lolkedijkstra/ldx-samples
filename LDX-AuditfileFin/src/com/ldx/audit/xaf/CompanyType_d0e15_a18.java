package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

import java.util.List;
import java.util.ArrayList;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * CompanyType_d0e15_a18 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class CompanyType_d0e15_a18 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for CompanyType_d0e15_a18.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public CompanyType_d0e15_a18(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type CompanyType_d0e15_a18.
	 */
	static class Allocator implements TypeAllocator<CompanyType_d0e15_a18> {
		/**
		 * method for getting a new instance of type CompanyType_d0e15_a18.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public CompanyType_d0e15_a18 newInstance(String elementName, ComplexDataType parent) {
			return new CompanyType_d0e15_a18(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for companyIdent element. 
	 *  @serial
	 */	
	private String m_companyIdent = null;
	
	/** element item for companyName element. 
	 *  @serial
	 */	
	private String m_companyName = null;
	
	/** element item for taxRegistrationCountry element. 
	 *  @serial
	 */	
	private String m_taxRegistrationCountry = null;
	
	/** element item for taxRegIdent element. 
	 *  @serial
	 */	
	private String m_taxRegIdent = null;
	
	/** list of streetAddress element. 
	 *  @serial
	 */	
	private List<StreetAddressType_d0e22_a28> m_streetAddressList = new ArrayList<StreetAddressType_d0e22_a28>();
	
	/** list of postalAddress element. 
	 *  @serial
	 */	
	private List<PostalAddressType_d0e33_a55> m_postalAddressList = new ArrayList<PostalAddressType_d0e33_a55>();
	
	/** element item for customersSuppliers element. 
	 *  @serial
	 */	
	private CustomersSuppliersType_d0e44_a82 m_customersSuppliers = null;
	
	/** element item for generalLedger element. 
	 *  @serial
	 */	
	private GeneralLedgerType_d0e152_a328 m_generalLedger = null;
	
	/** element item for vatCodes element. 
	 *  @serial
	 */	
	private VatCodesType_d0e224_a436 m_vatCodes = null;
	
	/** element item for periods element. 
	 *  @serial
	 */	
	private PeriodsType_d0e234_a451 m_periods = null;
	
	/** element item for openingBalance element. 
	 *  @serial
	 */	
	private OpeningBalanceType_d0e246_a471 m_openingBalance = null;
	
	/** element item for transactions element. 
	 *  @serial
	 */	
	private TransactionsType_d0e296_a573 m_transactions = null;
	
	/**
	 * Get the embedded CompanyIdent element.
	 * @return the item.
	 */
	public String getCompanyIdent() {
		return m_companyIdent;
	}
		
	/**
	 * This method sets (overwrites) the element CompanyIdent.
	 * @param data the item that needs to be added.
	 */
	void setCompanyIdent(String data) {
		m_companyIdent = data;
	}
		
	/**
	 * Get the embedded CompanyName element.
	 * @return the item.
	 */
	public String getCompanyName() {
		return m_companyName;
	}
		
	/**
	 * This method sets (overwrites) the element CompanyName.
	 * @param data the item that needs to be added.
	 */
	void setCompanyName(String data) {
		m_companyName = data;
	}
		
	/**
	 * Get the embedded TaxRegistrationCountry element.
	 * @return the item.
	 */
	public String getTaxRegistrationCountry() {
		return m_taxRegistrationCountry;
	}
		
	/**
	 * This method sets (overwrites) the element TaxRegistrationCountry.
	 * @param data the item that needs to be added.
	 */
	void setTaxRegistrationCountry(String data) {
		m_taxRegistrationCountry = data;
	}
		
	/**
	 * Get the embedded TaxRegIdent element.
	 * @return the item.
	 */
	public String getTaxRegIdent() {
		return m_taxRegIdent;
	}
		
	/**
	 * This method sets (overwrites) the element TaxRegIdent.
	 * @param data the item that needs to be added.
	 */
	void setTaxRegIdent(String data) {
		m_taxRegIdent = data;
	}
		
	/**
	 * Get the embedded list of StreetAddress elements.
	 * @return list of items.
	 */
	public List<StreetAddressType_d0e22_a28> getStreetAddresss() {
		return m_streetAddressList;
	}
		
	/**
	 * This method adds data to the list of StreetAddress.
	 * @param data the item that needs to be added.
	 */
	void setStreetAddress(StreetAddressType_d0e22_a28 data) {
		m_streetAddressList.add(data);
	}
		
	/**
	 * Get the embedded list of PostalAddress elements.
	 * @return list of items.
	 */
	public List<PostalAddressType_d0e33_a55> getPostalAddresss() {
		return m_postalAddressList;
	}
		
	/**
	 * This method adds data to the list of PostalAddress.
	 * @param data the item that needs to be added.
	 */
	void setPostalAddress(PostalAddressType_d0e33_a55 data) {
		m_postalAddressList.add(data);
	}
		
	/**
	 * Get the embedded CustomersSuppliers element.
	 * @return the item.
	 */
	public CustomersSuppliersType_d0e44_a82 getCustomersSuppliers() {
		return m_customersSuppliers;
	}
		
	/**
	 * This method sets (overwrites) the element CustomersSuppliers.
	 * @param data the item that needs to be added.
	 */
	void setCustomersSuppliers(CustomersSuppliersType_d0e44_a82 data) {
		m_customersSuppliers = data;
	}
		
	/**
	 * Get the embedded GeneralLedger element.
	 * @return the item.
	 */
	public GeneralLedgerType_d0e152_a328 getGeneralLedger() {
		return m_generalLedger;
	}
		
	/**
	 * This method sets (overwrites) the element GeneralLedger.
	 * @param data the item that needs to be added.
	 */
	void setGeneralLedger(GeneralLedgerType_d0e152_a328 data) {
		m_generalLedger = data;
	}
		
	/**
	 * Get the embedded VatCodes element.
	 * @return the item.
	 */
	public VatCodesType_d0e224_a436 getVatCodes() {
		return m_vatCodes;
	}
		
	/**
	 * This method sets (overwrites) the element VatCodes.
	 * @param data the item that needs to be added.
	 */
	void setVatCodes(VatCodesType_d0e224_a436 data) {
		m_vatCodes = data;
	}
		
	/**
	 * Get the embedded Periods element.
	 * @return the item.
	 */
	public PeriodsType_d0e234_a451 getPeriods() {
		return m_periods;
	}
		
	/**
	 * This method sets (overwrites) the element Periods.
	 * @param data the item that needs to be added.
	 */
	void setPeriods(PeriodsType_d0e234_a451 data) {
		m_periods = data;
	}
		
	/**
	 * Get the embedded OpeningBalance element.
	 * @return the item.
	 */
	public OpeningBalanceType_d0e246_a471 getOpeningBalance() {
		return m_openingBalance;
	}
		
	/**
	 * This method sets (overwrites) the element OpeningBalance.
	 * @param data the item that needs to be added.
	 */
	void setOpeningBalance(OpeningBalanceType_d0e246_a471 data) {
		m_openingBalance = data;
	}
		
	/**
	 * Get the embedded Transactions element.
	 * @return the item.
	 */
	public TransactionsType_d0e296_a573 getTransactions() {
		return m_transactions;
	}
		
	/**
	 * This method sets (overwrites) the element Transactions.
	 * @param data the item that needs to be added.
	 */
	void setTransactions(TransactionsType_d0e296_a573 data) {
		m_transactions = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_companyIdent, ((CompanyType_d0e15_a18)that).m_companyIdent))
			return false;
		
		if (!Compare.equals(m_companyName, ((CompanyType_d0e15_a18)that).m_companyName))
			return false;
		
		if (!Compare.equals(m_taxRegistrationCountry, ((CompanyType_d0e15_a18)that).m_taxRegistrationCountry))
			return false;
		
		if (!Compare.equals(m_taxRegIdent, ((CompanyType_d0e15_a18)that).m_taxRegIdent))
			return false;
		
		if (!Compare.equals(m_streetAddressList, ((CompanyType_d0e15_a18)that).m_streetAddressList))
			return false;
		
		if (!Compare.equals(m_postalAddressList, ((CompanyType_d0e15_a18)that).m_postalAddressList))
			return false;
		
		if (!Compare.equals(m_customersSuppliers, ((CompanyType_d0e15_a18)that).m_customersSuppliers))
			return false;
		
		if (!Compare.equals(m_generalLedger, ((CompanyType_d0e15_a18)that).m_generalLedger))
			return false;
		
		if (!Compare.equals(m_vatCodes, ((CompanyType_d0e15_a18)that).m_vatCodes))
			return false;
		
		if (!Compare.equals(m_periods, ((CompanyType_d0e15_a18)that).m_periods))
			return false;
		
		if (!Compare.equals(m_openingBalance, ((CompanyType_d0e15_a18)that).m_openingBalance))
			return false;
		
		if (!Compare.equals(m_transactions, ((CompanyType_d0e15_a18)that).m_transactions))
			return false;
		
		return true;
	}	

  
  
}
