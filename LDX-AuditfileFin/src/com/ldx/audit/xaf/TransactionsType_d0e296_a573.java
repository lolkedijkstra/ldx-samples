package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

import java.util.List;
import java.util.ArrayList;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * TransactionsType_d0e296_a573 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class TransactionsType_d0e296_a573 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for TransactionsType_d0e296_a573.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public TransactionsType_d0e296_a573(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type TransactionsType_d0e296_a573.
	 */
	static class Allocator implements TypeAllocator<TransactionsType_d0e296_a573> {
		/**
		 * method for getting a new instance of type TransactionsType_d0e296_a573.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public TransactionsType_d0e296_a573 newInstance(String elementName, ComplexDataType parent) {
			return new TransactionsType_d0e296_a573(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for linesCount element. 
	 *  @serial
	 */	
	private String m_linesCount = null;
	
	/** element item for totalDebit element. 
	 *  @serial
	 */	
	private String m_totalDebit = null;
	
	/** element item for totalCredit element. 
	 *  @serial
	 */	
	private String m_totalCredit = null;
	
	/** list of journal element. 
	 *  @serial
	 */	
	private List<JournalType_d0e302_a581> m_journalList = new ArrayList<JournalType_d0e302_a581>();
	
	/** element item for subledgers element. 
	 *  @serial
	 */	
	private SubledgersType_d0e359_a704 m_subledgers = null;
	
	/**
	 * Get the embedded LinesCount element.
	 * @return the item.
	 */
	public String getLinesCount() {
		return m_linesCount;
	}
		
	/**
	 * This method sets (overwrites) the element LinesCount.
	 * @param data the item that needs to be added.
	 */
	void setLinesCount(String data) {
		m_linesCount = data;
	}
		
	/**
	 * Get the embedded TotalDebit element.
	 * @return the item.
	 */
	public String getTotalDebit() {
		return m_totalDebit;
	}
		
	/**
	 * This method sets (overwrites) the element TotalDebit.
	 * @param data the item that needs to be added.
	 */
	void setTotalDebit(String data) {
		m_totalDebit = data;
	}
		
	/**
	 * Get the embedded TotalCredit element.
	 * @return the item.
	 */
	public String getTotalCredit() {
		return m_totalCredit;
	}
		
	/**
	 * This method sets (overwrites) the element TotalCredit.
	 * @param data the item that needs to be added.
	 */
	void setTotalCredit(String data) {
		m_totalCredit = data;
	}
		
	/**
	 * Get the embedded list of Journal elements.
	 * @return list of items.
	 */
	public List<JournalType_d0e302_a581> getJournals() {
		return m_journalList;
	}
		
	/**
	 * This method adds data to the list of Journal.
	 * @param data the item that needs to be added.
	 */
	void setJournal(JournalType_d0e302_a581 data) {
		m_journalList.add(data);
	}
		
	/**
	 * Get the embedded Subledgers element.
	 * @return the item.
	 */
	public SubledgersType_d0e359_a704 getSubledgers() {
		return m_subledgers;
	}
		
	/**
	 * This method sets (overwrites) the element Subledgers.
	 * @param data the item that needs to be added.
	 */
	void setSubledgers(SubledgersType_d0e359_a704 data) {
		m_subledgers = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_linesCount, ((TransactionsType_d0e296_a573)that).m_linesCount))
			return false;
		
		if (!Compare.equals(m_totalDebit, ((TransactionsType_d0e296_a573)that).m_totalDebit))
			return false;
		
		if (!Compare.equals(m_totalCredit, ((TransactionsType_d0e296_a573)that).m_totalCredit))
			return false;
		
		if (!Compare.equals(m_journalList, ((TransactionsType_d0e296_a573)that).m_journalList))
			return false;
		
		if (!Compare.equals(m_subledgers, ((TransactionsType_d0e296_a573)that).m_subledgers))
			return false;
		
		return true;
	}	

  
  
}
