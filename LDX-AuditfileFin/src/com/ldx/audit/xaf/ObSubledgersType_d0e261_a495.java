package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

import java.util.List;
import java.util.ArrayList;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * ObSubledgersType_d0e261_a495 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class ObSubledgersType_d0e261_a495 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for ObSubledgersType_d0e261_a495.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public ObSubledgersType_d0e261_a495(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type ObSubledgersType_d0e261_a495.
	 */
	static class Allocator implements TypeAllocator<ObSubledgersType_d0e261_a495> {
		/**
		 * method for getting a new instance of type ObSubledgersType_d0e261_a495.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public ObSubledgersType_d0e261_a495 newInstance(String elementName, ComplexDataType parent) {
			return new ObSubledgersType_d0e261_a495(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** list of obSubledger element. 
	 *  @serial
	 */	
	private List<ObSubledgerType_d0e264_a497> m_obSubledgerList = new ArrayList<ObSubledgerType_d0e264_a497>();
	
	/**
	 * Get the embedded list of ObSubledger elements.
	 * @return list of items.
	 */
	public List<ObSubledgerType_d0e264_a497> getObSubledgers() {
		return m_obSubledgerList;
	}
		
	/**
	 * This method adds data to the list of ObSubledger.
	 * @param data the item that needs to be added.
	 */
	void setObSubledger(ObSubledgerType_d0e264_a497 data) {
		m_obSubledgerList.add(data);
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_obSubledgerList, ((ObSubledgersType_d0e261_a495)that).m_obSubledgerList))
			return false;
		
		return true;
	}	

  
  
}
