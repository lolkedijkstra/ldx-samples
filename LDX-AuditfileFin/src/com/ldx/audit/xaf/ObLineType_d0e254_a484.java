package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * ObLineType_d0e254_a484 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class ObLineType_d0e254_a484 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for ObLineType_d0e254_a484.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public ObLineType_d0e254_a484(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type ObLineType_d0e254_a484.
	 */
	static class Allocator implements TypeAllocator<ObLineType_d0e254_a484> {
		/**
		 * method for getting a new instance of type ObLineType_d0e254_a484.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public ObLineType_d0e254_a484 newInstance(String elementName, ComplexDataType parent) {
			return new ObLineType_d0e254_a484(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for nr element. 
	 *  @serial
	 */	
	private String m_nr = null;
	
	/** element item for accID element. 
	 *  @serial
	 */	
	private String m_accID = null;
	
	/** element item for amnt element. 
	 *  @serial
	 */	
	private String m_amnt = null;
	
	/** element item for amntTp element. 
	 *  @serial
	 */	
	private String m_amntTp = null;
	
	/**
	 * Get the embedded Nr element.
	 * @return the item.
	 */
	public String getNr() {
		return m_nr;
	}
		
	/**
	 * This method sets (overwrites) the element Nr.
	 * @param data the item that needs to be added.
	 */
	void setNr(String data) {
		m_nr = data;
	}
		
	/**
	 * Get the embedded AccID element.
	 * @return the item.
	 */
	public String getAccID() {
		return m_accID;
	}
		
	/**
	 * This method sets (overwrites) the element AccID.
	 * @param data the item that needs to be added.
	 */
	void setAccID(String data) {
		m_accID = data;
	}
		
	/**
	 * Get the embedded Amnt element.
	 * @return the item.
	 */
	public String getAmnt() {
		return m_amnt;
	}
		
	/**
	 * This method sets (overwrites) the element Amnt.
	 * @param data the item that needs to be added.
	 */
	void setAmnt(String data) {
		m_amnt = data;
	}
		
	/**
	 * Get the embedded AmntTp element.
	 * @return the item.
	 */
	public String getAmntTp() {
		return m_amntTp;
	}
		
	/**
	 * This method sets (overwrites) the element AmntTp.
	 * @param data the item that needs to be added.
	 */
	void setAmntTp(String data) {
		m_amntTp = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_nr, ((ObLineType_d0e254_a484)that).m_nr))
			return false;
		
		if (!Compare.equals(m_accID, ((ObLineType_d0e254_a484)that).m_accID))
			return false;
		
		if (!Compare.equals(m_amnt, ((ObLineType_d0e254_a484)that).m_amnt))
			return false;
		
		if (!Compare.equals(m_amntTp, ((ObLineType_d0e254_a484)that).m_amntTp))
			return false;
		
		return true;
	}	

  
  
}
