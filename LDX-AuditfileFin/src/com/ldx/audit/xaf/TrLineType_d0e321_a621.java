package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

import java.util.List;
import java.util.ArrayList;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * TrLineType_d0e321_a621 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class TrLineType_d0e321_a621 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for TrLineType_d0e321_a621.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public TrLineType_d0e321_a621(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type TrLineType_d0e321_a621.
	 */
	static class Allocator implements TypeAllocator<TrLineType_d0e321_a621> {
		/**
		 * method for getting a new instance of type TrLineType_d0e321_a621.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public TrLineType_d0e321_a621 newInstance(String elementName, ComplexDataType parent) {
			return new TrLineType_d0e321_a621(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for nr element. 
	 *  @serial
	 */	
	private String m_nr = null;
	
	/** element item for accID element. 
	 *  @serial
	 */	
	private String m_accID = null;
	
	/** element item for docRef element. 
	 *  @serial
	 */	
	private String m_docRef = null;
	
	/** element item for effDate element. 
	 *  @serial
	 */	
	private String m_effDate = null;
	
	/** element item for desc element. 
	 *  @serial
	 */	
	private String m_desc = null;
	
	/** element item for amnt element. 
	 *  @serial
	 */	
	private String m_amnt = null;
	
	/** element item for amntTp element. 
	 *  @serial
	 */	
	private String m_amntTp = null;
	
	/** element item for recRef element. 
	 *  @serial
	 */	
	private String m_recRef = null;
	
	/** element item for matchKeyID element. 
	 *  @serial
	 */	
	private String m_matchKeyID = null;
	
	/** element item for custSupID element. 
	 *  @serial
	 */	
	private String m_custSupID = null;
	
	/** element item for invRef element. 
	 *  @serial
	 */	
	private String m_invRef = null;
	
	/** element item for orderRef element. 
	 *  @serial
	 */	
	private String m_orderRef = null;
	
	/** element item for receivingDocRef element. 
	 *  @serial
	 */	
	private String m_receivingDocRef = null;
	
	/** element item for shipDocRef element. 
	 *  @serial
	 */	
	private String m_shipDocRef = null;
	
	/** element item for costID element. 
	 *  @serial
	 */	
	private String m_costID = null;
	
	/** element item for prodID element. 
	 *  @serial
	 */	
	private String m_prodID = null;
	
	/** element item for projID element. 
	 *  @serial
	 */	
	private String m_projID = null;
	
	/** element item for artGrpID element. 
	 *  @serial
	 */	
	private String m_artGrpID = null;
	
	/** element item for workCostArrRef element. 
	 *  @serial
	 */	
	private String m_workCostArrRef = null;
	
	/** element item for qntityID element. 
	 *  @serial
	 */	
	private String m_qntityID = null;
	
	/** element item for qntity element. 
	 *  @serial
	 */	
	private String m_qntity = null;
	
	/** element item for bankAccNr element. 
	 *  @serial
	 */	
	private String m_bankAccNr = null;
	
	/** element item for bankIdCd element. 
	 *  @serial
	 */	
	private String m_bankIdCd = null;
	
	/** list of vat element. 
	 *  @serial
	 */	
	private List<VatType_d0e347_a687> m_vatList = new ArrayList<VatType_d0e347_a687>();
	
	/** element item for currency element. 
	 *  @serial
	 */	
	private CurrencyType_d0e354_a698 m_currency = null;
	
	/**
	 * Get the embedded Nr element.
	 * @return the item.
	 */
	public String getNr() {
		return m_nr;
	}
		
	/**
	 * This method sets (overwrites) the element Nr.
	 * @param data the item that needs to be added.
	 */
	void setNr(String data) {
		m_nr = data;
	}
		
	/**
	 * Get the embedded AccID element.
	 * @return the item.
	 */
	public String getAccID() {
		return m_accID;
	}
		
	/**
	 * This method sets (overwrites) the element AccID.
	 * @param data the item that needs to be added.
	 */
	void setAccID(String data) {
		m_accID = data;
	}
		
	/**
	 * Get the embedded DocRef element.
	 * @return the item.
	 */
	public String getDocRef() {
		return m_docRef;
	}
		
	/**
	 * This method sets (overwrites) the element DocRef.
	 * @param data the item that needs to be added.
	 */
	void setDocRef(String data) {
		m_docRef = data;
	}
		
	/**
	 * Get the embedded EffDate element.
	 * @return the item.
	 */
	public String getEffDate() {
		return m_effDate;
	}
		
	/**
	 * This method sets (overwrites) the element EffDate.
	 * @param data the item that needs to be added.
	 */
	void setEffDate(String data) {
		m_effDate = data;
	}
		
	/**
	 * Get the embedded Desc element.
	 * @return the item.
	 */
	public String getDesc() {
		return m_desc;
	}
		
	/**
	 * This method sets (overwrites) the element Desc.
	 * @param data the item that needs to be added.
	 */
	void setDesc(String data) {
		m_desc = data;
	}
		
	/**
	 * Get the embedded Amnt element.
	 * @return the item.
	 */
	public String getAmnt() {
		return m_amnt;
	}
		
	/**
	 * This method sets (overwrites) the element Amnt.
	 * @param data the item that needs to be added.
	 */
	void setAmnt(String data) {
		m_amnt = data;
	}
		
	/**
	 * Get the embedded AmntTp element.
	 * @return the item.
	 */
	public String getAmntTp() {
		return m_amntTp;
	}
		
	/**
	 * This method sets (overwrites) the element AmntTp.
	 * @param data the item that needs to be added.
	 */
	void setAmntTp(String data) {
		m_amntTp = data;
	}
		
	/**
	 * Get the embedded RecRef element.
	 * @return the item.
	 */
	public String getRecRef() {
		return m_recRef;
	}
		
	/**
	 * This method sets (overwrites) the element RecRef.
	 * @param data the item that needs to be added.
	 */
	void setRecRef(String data) {
		m_recRef = data;
	}
		
	/**
	 * Get the embedded MatchKeyID element.
	 * @return the item.
	 */
	public String getMatchKeyID() {
		return m_matchKeyID;
	}
		
	/**
	 * This method sets (overwrites) the element MatchKeyID.
	 * @param data the item that needs to be added.
	 */
	void setMatchKeyID(String data) {
		m_matchKeyID = data;
	}
		
	/**
	 * Get the embedded CustSupID element.
	 * @return the item.
	 */
	public String getCustSupID() {
		return m_custSupID;
	}
		
	/**
	 * This method sets (overwrites) the element CustSupID.
	 * @param data the item that needs to be added.
	 */
	void setCustSupID(String data) {
		m_custSupID = data;
	}
		
	/**
	 * Get the embedded InvRef element.
	 * @return the item.
	 */
	public String getInvRef() {
		return m_invRef;
	}
		
	/**
	 * This method sets (overwrites) the element InvRef.
	 * @param data the item that needs to be added.
	 */
	void setInvRef(String data) {
		m_invRef = data;
	}
		
	/**
	 * Get the embedded OrderRef element.
	 * @return the item.
	 */
	public String getOrderRef() {
		return m_orderRef;
	}
		
	/**
	 * This method sets (overwrites) the element OrderRef.
	 * @param data the item that needs to be added.
	 */
	void setOrderRef(String data) {
		m_orderRef = data;
	}
		
	/**
	 * Get the embedded ReceivingDocRef element.
	 * @return the item.
	 */
	public String getReceivingDocRef() {
		return m_receivingDocRef;
	}
		
	/**
	 * This method sets (overwrites) the element ReceivingDocRef.
	 * @param data the item that needs to be added.
	 */
	void setReceivingDocRef(String data) {
		m_receivingDocRef = data;
	}
		
	/**
	 * Get the embedded ShipDocRef element.
	 * @return the item.
	 */
	public String getShipDocRef() {
		return m_shipDocRef;
	}
		
	/**
	 * This method sets (overwrites) the element ShipDocRef.
	 * @param data the item that needs to be added.
	 */
	void setShipDocRef(String data) {
		m_shipDocRef = data;
	}
		
	/**
	 * Get the embedded CostID element.
	 * @return the item.
	 */
	public String getCostID() {
		return m_costID;
	}
		
	/**
	 * This method sets (overwrites) the element CostID.
	 * @param data the item that needs to be added.
	 */
	void setCostID(String data) {
		m_costID = data;
	}
		
	/**
	 * Get the embedded ProdID element.
	 * @return the item.
	 */
	public String getProdID() {
		return m_prodID;
	}
		
	/**
	 * This method sets (overwrites) the element ProdID.
	 * @param data the item that needs to be added.
	 */
	void setProdID(String data) {
		m_prodID = data;
	}
		
	/**
	 * Get the embedded ProjID element.
	 * @return the item.
	 */
	public String getProjID() {
		return m_projID;
	}
		
	/**
	 * This method sets (overwrites) the element ProjID.
	 * @param data the item that needs to be added.
	 */
	void setProjID(String data) {
		m_projID = data;
	}
		
	/**
	 * Get the embedded ArtGrpID element.
	 * @return the item.
	 */
	public String getArtGrpID() {
		return m_artGrpID;
	}
		
	/**
	 * This method sets (overwrites) the element ArtGrpID.
	 * @param data the item that needs to be added.
	 */
	void setArtGrpID(String data) {
		m_artGrpID = data;
	}
		
	/**
	 * Get the embedded WorkCostArrRef element.
	 * @return the item.
	 */
	public String getWorkCostArrRef() {
		return m_workCostArrRef;
	}
		
	/**
	 * This method sets (overwrites) the element WorkCostArrRef.
	 * @param data the item that needs to be added.
	 */
	void setWorkCostArrRef(String data) {
		m_workCostArrRef = data;
	}
		
	/**
	 * Get the embedded QntityID element.
	 * @return the item.
	 */
	public String getQntityID() {
		return m_qntityID;
	}
		
	/**
	 * This method sets (overwrites) the element QntityID.
	 * @param data the item that needs to be added.
	 */
	void setQntityID(String data) {
		m_qntityID = data;
	}
		
	/**
	 * Get the embedded Qntity element.
	 * @return the item.
	 */
	public String getQntity() {
		return m_qntity;
	}
		
	/**
	 * This method sets (overwrites) the element Qntity.
	 * @param data the item that needs to be added.
	 */
	void setQntity(String data) {
		m_qntity = data;
	}
		
	/**
	 * Get the embedded BankAccNr element.
	 * @return the item.
	 */
	public String getBankAccNr() {
		return m_bankAccNr;
	}
		
	/**
	 * This method sets (overwrites) the element BankAccNr.
	 * @param data the item that needs to be added.
	 */
	void setBankAccNr(String data) {
		m_bankAccNr = data;
	}
		
	/**
	 * Get the embedded BankIdCd element.
	 * @return the item.
	 */
	public String getBankIdCd() {
		return m_bankIdCd;
	}
		
	/**
	 * This method sets (overwrites) the element BankIdCd.
	 * @param data the item that needs to be added.
	 */
	void setBankIdCd(String data) {
		m_bankIdCd = data;
	}
		
	/**
	 * Get the embedded list of Vat elements.
	 * @return list of items.
	 */
	public List<VatType_d0e347_a687> getVats() {
		return m_vatList;
	}
		
	/**
	 * This method adds data to the list of Vat.
	 * @param data the item that needs to be added.
	 */
	void setVat(VatType_d0e347_a687 data) {
		m_vatList.add(data);
	}
		
	/**
	 * Get the embedded Currency element.
	 * @return the item.
	 */
	public CurrencyType_d0e354_a698 getCurrency() {
		return m_currency;
	}
		
	/**
	 * This method sets (overwrites) the element Currency.
	 * @param data the item that needs to be added.
	 */
	void setCurrency(CurrencyType_d0e354_a698 data) {
		m_currency = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_nr, ((TrLineType_d0e321_a621)that).m_nr))
			return false;
		
		if (!Compare.equals(m_accID, ((TrLineType_d0e321_a621)that).m_accID))
			return false;
		
		if (!Compare.equals(m_docRef, ((TrLineType_d0e321_a621)that).m_docRef))
			return false;
		
		if (!Compare.equals(m_effDate, ((TrLineType_d0e321_a621)that).m_effDate))
			return false;
		
		if (!Compare.equals(m_desc, ((TrLineType_d0e321_a621)that).m_desc))
			return false;
		
		if (!Compare.equals(m_amnt, ((TrLineType_d0e321_a621)that).m_amnt))
			return false;
		
		if (!Compare.equals(m_amntTp, ((TrLineType_d0e321_a621)that).m_amntTp))
			return false;
		
		if (!Compare.equals(m_recRef, ((TrLineType_d0e321_a621)that).m_recRef))
			return false;
		
		if (!Compare.equals(m_matchKeyID, ((TrLineType_d0e321_a621)that).m_matchKeyID))
			return false;
		
		if (!Compare.equals(m_custSupID, ((TrLineType_d0e321_a621)that).m_custSupID))
			return false;
		
		if (!Compare.equals(m_invRef, ((TrLineType_d0e321_a621)that).m_invRef))
			return false;
		
		if (!Compare.equals(m_orderRef, ((TrLineType_d0e321_a621)that).m_orderRef))
			return false;
		
		if (!Compare.equals(m_receivingDocRef, ((TrLineType_d0e321_a621)that).m_receivingDocRef))
			return false;
		
		if (!Compare.equals(m_shipDocRef, ((TrLineType_d0e321_a621)that).m_shipDocRef))
			return false;
		
		if (!Compare.equals(m_costID, ((TrLineType_d0e321_a621)that).m_costID))
			return false;
		
		if (!Compare.equals(m_prodID, ((TrLineType_d0e321_a621)that).m_prodID))
			return false;
		
		if (!Compare.equals(m_projID, ((TrLineType_d0e321_a621)that).m_projID))
			return false;
		
		if (!Compare.equals(m_artGrpID, ((TrLineType_d0e321_a621)that).m_artGrpID))
			return false;
		
		if (!Compare.equals(m_workCostArrRef, ((TrLineType_d0e321_a621)that).m_workCostArrRef))
			return false;
		
		if (!Compare.equals(m_qntityID, ((TrLineType_d0e321_a621)that).m_qntityID))
			return false;
		
		if (!Compare.equals(m_qntity, ((TrLineType_d0e321_a621)that).m_qntity))
			return false;
		
		if (!Compare.equals(m_bankAccNr, ((TrLineType_d0e321_a621)that).m_bankAccNr))
			return false;
		
		if (!Compare.equals(m_bankIdCd, ((TrLineType_d0e321_a621)that).m_bankIdCd))
			return false;
		
		if (!Compare.equals(m_vatList, ((TrLineType_d0e321_a621)that).m_vatList))
			return false;
		
		if (!Compare.equals(m_currency, ((TrLineType_d0e321_a621)that).m_currency))
			return false;
		
		return true;
	}	

  
  
}
