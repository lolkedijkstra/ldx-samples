package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

/* SAX 2.0 dependencies */
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/* Framework dependencies */
import com.ldx.xml.core.DataSetter;
import com.ldx.xml.core.ComplexDataType;
import com.ldx.xml.core.XMLEvent;
import com.ldx.xml.core.XMLFragmentHandler;


/**
 * OpeningBalanceType_d0e246_a471 handler class.
 *
 * @see OpeningBalanceType_d0e246_a471
 * 
 */
public class OpeningBalanceType_d0e246_a471Handler extends XMLFragmentHandler<OpeningBalanceType_d0e246_a471> {
	/**
	 * Proxy for OpeningBalanceType_d0e246_a471Handler.
	 */
	static class Proxy extends HandlerProxy<OpeningBalanceType_d0e246_a471> {
		/**
		 * Allocator for OpeningBalanceType_d0e246_a471Handler.
		 */
		private static class Allocator 
				extends HandlerProxy.Allocator<OpeningBalanceType_d0e246_a471> {			
			public XMLFragmentHandler<OpeningBalanceType_d0e246_a471> create(
					XMLReader reader
					, XMLFragmentHandler<?> handler
					, String elementName
					, DataSetter setter
					, boolean doProcess) {
				return new OpeningBalanceType_d0e246_a471Handler(
					reader
					, handler
					, elementName
					, OpeningBalanceType_d0e246_a471.getAllocator()
					, setter
					, doProcess);
			}
		}
		
		/**
		 * Constructor for Proxy.
		 *
		 * @param reader the XML reader
		 * @param parentH the parent XML handler class
		 * @param elementName the name of the XML Element (tag).
		 * @param pSetter the setter for the parent data
		 * @param doProcess indicates whether processing is active for this handler instance
		 */
		public Proxy(XMLReader reader, XMLFragmentHandler<?> parentH, String elementName,
				DataSetter pSetter, boolean doProcess) {
			super(reader, parentH, elementName, pSetter, new Allocator(), doProcess);
		}
	}
	
	/** Data setter class for obLine element. */
	private class ObLineSetter implements DataSetter {
		/** data target. */
		private OpeningBalanceType_d0e246_a471Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public ObLineSetter(OpeningBalanceType_d0e246_a471Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setObLine((ObLineType_d0e254_a484) data);	
		}
	}	
	/** Data setter class for obSubledgers element. */
	private class ObSubledgersSetter implements DataSetter {
		/** data target. */
		private OpeningBalanceType_d0e246_a471Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public ObSubledgersSetter(OpeningBalanceType_d0e246_a471Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setObSubledgers((ObSubledgersType_d0e261_a495) data);	
		}
	}


	/**
	 * Constructor for handler class.
	 *
	 * @param reader the XML reader
	 * @param parentH the parent XML handler class
	 * @param elementName the name of the XML Element (tag)
	 * @param allocator the allocator for the data type
	 * @param pSetter the setter for the parent data
	 * @param doProcess indicates whether processing is active for this handler instance
	 */
	public OpeningBalanceType_d0e246_a471Handler(
			XMLReader reader
			, XMLFragmentHandler<?> parentH
			, String elementName
			, OpeningBalanceType_d0e246_a471.Allocator allocator
			, DataSetter pSetter
			, boolean doProcess) {
		super(reader, parentH, elementName, allocator, pSetter, doProcess);

		// code for linking children..
		registerHandler(
			new ObLineType_d0e254_a484Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of obLineType_d0e254_a484Handler
				, "obLine" // XML element name
				, doLink("obLine") // linking to parent
					? new ObLineSetter(this) // ON
					: null // OFF
				, doProcess("obLine")) // processing active or not
				);
  
		registerHandler(
			new ObSubledgersType_d0e261_a495Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of obSubledgersType_d0e261_a495Handler
				, "obSubledgers" // XML element name
				, doLink("obSubledgers") // linking to parent
					? new ObSubledgersSetter(this) // ON
					: null // OFF
				, doProcess("obSubledgers")) // processing active or not
				);
  	}


	/**
	 * This method overrides the getData() of the super type, returning the more
	 * specific type.
	 * 
	 */
	@Override 
	public OpeningBalanceType_d0e246_a471 getData() {
		return (OpeningBalanceType_d0e246_a471)super.getData();
	}




	/**
	 * This method is called by the XML parser on an end of element event.
	 *
	 * If the localName == 'the name of the element that this handler handles' control is
	 * passed back to the parent handler (future events go there) and the data of
	 * the children of this handler is attached to the data associated to this handler.	
	 * 
	 * The data that is collected is connected to the parent data only if a parentDataSetter
	 * if found (getParentDataSetter()).
	 *
	 * If processing is enabled for a handler, the processor is called.
	 */
	@Override
	public void endElement(String uri, String localName, String name)
		throws SAXException {
		
		// collect data from children..
		if (localName.equals("linesCount")) {
			getData().setLinesCount(getValue());
			getContents().reset();
		} else if (localName.equals("opBalDate")) {
			getData().setOpBalDate(getValue());
			getContents().reset();
		} else if (localName.equals("opBalDesc")) {
			getData().setOpBalDesc(getValue());
			getContents().reset();
		} else if (localName.equals("totalCredit")) {
			getData().setTotalCredit(getValue());
			getContents().reset();
		} else if (localName.equals("totalDebit")) {
			getData().setTotalDebit(getValue());
			getContents().reset();
		} else if (localName.equals(getXMLElementName())) {
				
			// return control to parent handler..
			this.deactivate();
			
			// get content of this item..
			getData().setContent(this.getValue());
			
			// attach data to parent (if parent data setter is found)..
			DataSetter setter = getParentDataSetter();
			if (setter != null) {
				setter.set(getData());
			} 
			
			// process data if required..
			if (doProcess()) {
				process(XMLEvent.END);
			}
		}
		
	}	
}
