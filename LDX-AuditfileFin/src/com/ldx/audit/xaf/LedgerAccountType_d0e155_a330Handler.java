package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

/* SAX 2.0 dependencies */
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/* Framework dependencies */
import com.ldx.xml.core.DataSetter;
import com.ldx.xml.core.ComplexDataType;
import com.ldx.xml.core.XMLEvent;
import com.ldx.xml.core.XMLFragmentHandler;


/**
 * LedgerAccountType_d0e155_a330 handler class.
 *
 * @see LedgerAccountType_d0e155_a330
 * 
 */
public class LedgerAccountType_d0e155_a330Handler extends XMLFragmentHandler<LedgerAccountType_d0e155_a330> {
	/**
	 * Proxy for LedgerAccountType_d0e155_a330Handler.
	 */
	static class Proxy extends HandlerProxy<LedgerAccountType_d0e155_a330> {
		/**
		 * Allocator for LedgerAccountType_d0e155_a330Handler.
		 */
		private static class Allocator 
				extends HandlerProxy.Allocator<LedgerAccountType_d0e155_a330> {			
			public XMLFragmentHandler<LedgerAccountType_d0e155_a330> create(
					XMLReader reader
					, XMLFragmentHandler<?> handler
					, String elementName
					, DataSetter setter
					, boolean doProcess) {
				return new LedgerAccountType_d0e155_a330Handler(
					reader
					, handler
					, elementName
					, LedgerAccountType_d0e155_a330.getAllocator()
					, setter
					, doProcess);
			}
		}
		
		/**
		 * Constructor for Proxy.
		 *
		 * @param reader the XML reader
		 * @param parentH the parent XML handler class
		 * @param elementName the name of the XML Element (tag).
		 * @param pSetter the setter for the parent data
		 * @param doProcess indicates whether processing is active for this handler instance
		 */
		public Proxy(XMLReader reader, XMLFragmentHandler<?> parentH, String elementName,
				DataSetter pSetter, boolean doProcess) {
			super(reader, parentH, elementName, pSetter, new Allocator(), doProcess);
		}
	}
	
	/** Data setter class for changeInfo element. */
	private class ChangeInfoSetter implements DataSetter {
		/** data target. */
		private LedgerAccountType_d0e155_a330Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public ChangeInfoSetter(LedgerAccountType_d0e155_a330Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setChangeInfo((ChangeInfoType_d0e178_a367) data);	
		}
	}	
	/** Data setter class for glAccountHistory element. */
	private class GlAccountHistorySetter implements DataSetter {
		/** data target. */
		private LedgerAccountType_d0e155_a330Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public GlAccountHistorySetter(LedgerAccountType_d0e155_a330Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setGlAccountHistory((GlAccountHistoryType_d0e184_a378) data);	
		}
	}	
	/** Data setter class for taxonomy element. */
	private class TaxonomySetter implements DataSetter {
		/** data target. */
		private LedgerAccountType_d0e155_a330Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public TaxonomySetter(LedgerAccountType_d0e155_a330Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setTaxonomy((TaxonomyType_d0e165_a350) data);	
		}
	}


	/**
	 * Constructor for handler class.
	 *
	 * @param reader the XML reader
	 * @param parentH the parent XML handler class
	 * @param elementName the name of the XML Element (tag)
	 * @param allocator the allocator for the data type
	 * @param pSetter the setter for the parent data
	 * @param doProcess indicates whether processing is active for this handler instance
	 */
	public LedgerAccountType_d0e155_a330Handler(
			XMLReader reader
			, XMLFragmentHandler<?> parentH
			, String elementName
			, LedgerAccountType_d0e155_a330.Allocator allocator
			, DataSetter pSetter
			, boolean doProcess) {
		super(reader, parentH, elementName, allocator, pSetter, doProcess);

		// code for linking children..
		registerHandler(
			new ChangeInfoType_d0e178_a367Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of changeInfoType_d0e178_a367Handler
				, "changeInfo" // XML element name
				, doLink("changeInfo") // linking to parent
					? new ChangeInfoSetter(this) // ON
					: null // OFF
				, doProcess("changeInfo")) // processing active or not
				);
  
		registerHandler(
			new GlAccountHistoryType_d0e184_a378Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of glAccountHistoryType_d0e184_a378Handler
				, "glAccountHistory" // XML element name
				, doLink("glAccountHistory") // linking to parent
					? new GlAccountHistorySetter(this) // ON
					: null // OFF
				, doProcess("glAccountHistory")) // processing active or not
				);
  
		registerHandler(
			new TaxonomyType_d0e165_a350Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of taxonomyType_d0e165_a350Handler
				, "taxonomy" // XML element name
				, doLink("taxonomy") // linking to parent
					? new TaxonomySetter(this) // ON
					: null // OFF
				, doProcess("taxonomy")) // processing active or not
				);
  	}


	/**
	 * This method overrides the getData() of the super type, returning the more
	 * specific type.
	 * 
	 */
	@Override 
	public LedgerAccountType_d0e155_a330 getData() {
		return (LedgerAccountType_d0e155_a330)super.getData();
	}




	/**
	 * This method is called by the XML parser on an end of element event.
	 *
	 * If the localName == 'the name of the element that this handler handles' control is
	 * passed back to the parent handler (future events go there) and the data of
	 * the children of this handler is attached to the data associated to this handler.	
	 * 
	 * The data that is collected is connected to the parent data only if a parentDataSetter
	 * if found (getParentDataSetter()).
	 *
	 * If processing is enabled for a handler, the processor is called.
	 */
	@Override
	public void endElement(String uri, String localName, String name)
		throws SAXException {
		
		// collect data from children..
		if (localName.equals("accDesc")) {
			getData().setAccDesc(getValue());
			getContents().reset();
		} else if (localName.equals("accID")) {
			getData().setAccID(getValue());
			getContents().reset();
		} else if (localName.equals("accTp")) {
			getData().setAccTp(getValue());
			getContents().reset();
		} else if (localName.equals("leadCode")) {
			getData().setLeadCode(getValue());
			getContents().reset();
		} else if (localName.equals("leadCrossRef")) {
			getData().setLeadCrossRef(getValue());
			getContents().reset();
		} else if (localName.equals("leadDescription")) {
			getData().setLeadDescription(getValue());
			getContents().reset();
		} else if (localName.equals("leadReference")) {
			getData().setLeadReference(getValue());
			getContents().reset();
		} else if (localName.equals(getXMLElementName())) {
				
			// return control to parent handler..
			this.deactivate();
			
			// get content of this item..
			getData().setContent(this.getValue());
			
			// attach data to parent (if parent data setter is found)..
			DataSetter setter = getParentDataSetter();
			if (setter != null) {
				setter.set(getData());
			} 
			
			// process data if required..
			if (doProcess()) {
				process(XMLEvent.END);
			}
		}
		
	}	
}
