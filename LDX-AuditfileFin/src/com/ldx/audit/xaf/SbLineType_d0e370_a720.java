package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

import java.util.List;
import java.util.ArrayList;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * SbLineType_d0e370_a720 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class SbLineType_d0e370_a720 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for SbLineType_d0e370_a720.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public SbLineType_d0e370_a720(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type SbLineType_d0e370_a720.
	 */
	static class Allocator implements TypeAllocator<SbLineType_d0e370_a720> {
		/**
		 * method for getting a new instance of type SbLineType_d0e370_a720.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public SbLineType_d0e370_a720 newInstance(String elementName, ComplexDataType parent) {
			return new SbLineType_d0e370_a720(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for nr element. 
	 *  @serial
	 */	
	private String m_nr = null;
	
	/** element item for jrnID element. 
	 *  @serial
	 */	
	private String m_jrnID = null;
	
	/** element item for trNr element. 
	 *  @serial
	 */	
	private String m_trNr = null;
	
	/** element item for trLineNr element. 
	 *  @serial
	 */	
	private String m_trLineNr = null;
	
	/** element item for desc element. 
	 *  @serial
	 */	
	private String m_desc = null;
	
	/** element item for amnt element. 
	 *  @serial
	 */	
	private String m_amnt = null;
	
	/** element item for amntTp element. 
	 *  @serial
	 */	
	private String m_amntTp = null;
	
	/** element item for docRef element. 
	 *  @serial
	 */	
	private String m_docRef = null;
	
	/** element item for recRef element. 
	 *  @serial
	 */	
	private String m_recRef = null;
	
	/** element item for matchKeyID element. 
	 *  @serial
	 */	
	private String m_matchKeyID = null;
	
	/** element item for custSupID element. 
	 *  @serial
	 */	
	private String m_custSupID = null;
	
	/** element item for invRef element. 
	 *  @serial
	 */	
	private String m_invRef = null;
	
	/** element item for invPurSalTp element. 
	 *  @serial
	 */	
	private String m_invPurSalTp = null;
	
	/** element item for invTp element. 
	 *  @serial
	 */	
	private String m_invTp = null;
	
	/** element item for invDt element. 
	 *  @serial
	 */	
	private String m_invDt = null;
	
	/** element item for invDueDt element. 
	 *  @serial
	 */	
	private String m_invDueDt = null;
	
	/** element item for mutTp element. 
	 *  @serial
	 */	
	private String m_mutTp = null;
	
	/** element item for costID element. 
	 *  @serial
	 */	
	private String m_costID = null;
	
	/** element item for prodID element. 
	 *  @serial
	 */	
	private String m_prodID = null;
	
	/** element item for projID element. 
	 *  @serial
	 */	
	private String m_projID = null;
	
	/** element item for artGrpID element. 
	 *  @serial
	 */	
	private String m_artGrpID = null;
	
	/** element item for qntityID element. 
	 *  @serial
	 */	
	private String m_qntityID = null;
	
	/** element item for qntity element. 
	 *  @serial
	 */	
	private String m_qntity = null;
	
	/** list of vat element. 
	 *  @serial
	 */	
	private List<VatType_d0e396_a786> m_vatList = new ArrayList<VatType_d0e396_a786>();
	
	/** element item for currency element. 
	 *  @serial
	 */	
	private CurrencyType_d0e403_a797 m_currency = null;
	
	/**
	 * Get the embedded Nr element.
	 * @return the item.
	 */
	public String getNr() {
		return m_nr;
	}
		
	/**
	 * This method sets (overwrites) the element Nr.
	 * @param data the item that needs to be added.
	 */
	void setNr(String data) {
		m_nr = data;
	}
		
	/**
	 * Get the embedded JrnID element.
	 * @return the item.
	 */
	public String getJrnID() {
		return m_jrnID;
	}
		
	/**
	 * This method sets (overwrites) the element JrnID.
	 * @param data the item that needs to be added.
	 */
	void setJrnID(String data) {
		m_jrnID = data;
	}
		
	/**
	 * Get the embedded TrNr element.
	 * @return the item.
	 */
	public String getTrNr() {
		return m_trNr;
	}
		
	/**
	 * This method sets (overwrites) the element TrNr.
	 * @param data the item that needs to be added.
	 */
	void setTrNr(String data) {
		m_trNr = data;
	}
		
	/**
	 * Get the embedded TrLineNr element.
	 * @return the item.
	 */
	public String getTrLineNr() {
		return m_trLineNr;
	}
		
	/**
	 * This method sets (overwrites) the element TrLineNr.
	 * @param data the item that needs to be added.
	 */
	void setTrLineNr(String data) {
		m_trLineNr = data;
	}
		
	/**
	 * Get the embedded Desc element.
	 * @return the item.
	 */
	public String getDesc() {
		return m_desc;
	}
		
	/**
	 * This method sets (overwrites) the element Desc.
	 * @param data the item that needs to be added.
	 */
	void setDesc(String data) {
		m_desc = data;
	}
		
	/**
	 * Get the embedded Amnt element.
	 * @return the item.
	 */
	public String getAmnt() {
		return m_amnt;
	}
		
	/**
	 * This method sets (overwrites) the element Amnt.
	 * @param data the item that needs to be added.
	 */
	void setAmnt(String data) {
		m_amnt = data;
	}
		
	/**
	 * Get the embedded AmntTp element.
	 * @return the item.
	 */
	public String getAmntTp() {
		return m_amntTp;
	}
		
	/**
	 * This method sets (overwrites) the element AmntTp.
	 * @param data the item that needs to be added.
	 */
	void setAmntTp(String data) {
		m_amntTp = data;
	}
		
	/**
	 * Get the embedded DocRef element.
	 * @return the item.
	 */
	public String getDocRef() {
		return m_docRef;
	}
		
	/**
	 * This method sets (overwrites) the element DocRef.
	 * @param data the item that needs to be added.
	 */
	void setDocRef(String data) {
		m_docRef = data;
	}
		
	/**
	 * Get the embedded RecRef element.
	 * @return the item.
	 */
	public String getRecRef() {
		return m_recRef;
	}
		
	/**
	 * This method sets (overwrites) the element RecRef.
	 * @param data the item that needs to be added.
	 */
	void setRecRef(String data) {
		m_recRef = data;
	}
		
	/**
	 * Get the embedded MatchKeyID element.
	 * @return the item.
	 */
	public String getMatchKeyID() {
		return m_matchKeyID;
	}
		
	/**
	 * This method sets (overwrites) the element MatchKeyID.
	 * @param data the item that needs to be added.
	 */
	void setMatchKeyID(String data) {
		m_matchKeyID = data;
	}
		
	/**
	 * Get the embedded CustSupID element.
	 * @return the item.
	 */
	public String getCustSupID() {
		return m_custSupID;
	}
		
	/**
	 * This method sets (overwrites) the element CustSupID.
	 * @param data the item that needs to be added.
	 */
	void setCustSupID(String data) {
		m_custSupID = data;
	}
		
	/**
	 * Get the embedded InvRef element.
	 * @return the item.
	 */
	public String getInvRef() {
		return m_invRef;
	}
		
	/**
	 * This method sets (overwrites) the element InvRef.
	 * @param data the item that needs to be added.
	 */
	void setInvRef(String data) {
		m_invRef = data;
	}
		
	/**
	 * Get the embedded InvPurSalTp element.
	 * @return the item.
	 */
	public String getInvPurSalTp() {
		return m_invPurSalTp;
	}
		
	/**
	 * This method sets (overwrites) the element InvPurSalTp.
	 * @param data the item that needs to be added.
	 */
	void setInvPurSalTp(String data) {
		m_invPurSalTp = data;
	}
		
	/**
	 * Get the embedded InvTp element.
	 * @return the item.
	 */
	public String getInvTp() {
		return m_invTp;
	}
		
	/**
	 * This method sets (overwrites) the element InvTp.
	 * @param data the item that needs to be added.
	 */
	void setInvTp(String data) {
		m_invTp = data;
	}
		
	/**
	 * Get the embedded InvDt element.
	 * @return the item.
	 */
	public String getInvDt() {
		return m_invDt;
	}
		
	/**
	 * This method sets (overwrites) the element InvDt.
	 * @param data the item that needs to be added.
	 */
	void setInvDt(String data) {
		m_invDt = data;
	}
		
	/**
	 * Get the embedded InvDueDt element.
	 * @return the item.
	 */
	public String getInvDueDt() {
		return m_invDueDt;
	}
		
	/**
	 * This method sets (overwrites) the element InvDueDt.
	 * @param data the item that needs to be added.
	 */
	void setInvDueDt(String data) {
		m_invDueDt = data;
	}
		
	/**
	 * Get the embedded MutTp element.
	 * @return the item.
	 */
	public String getMutTp() {
		return m_mutTp;
	}
		
	/**
	 * This method sets (overwrites) the element MutTp.
	 * @param data the item that needs to be added.
	 */
	void setMutTp(String data) {
		m_mutTp = data;
	}
		
	/**
	 * Get the embedded CostID element.
	 * @return the item.
	 */
	public String getCostID() {
		return m_costID;
	}
		
	/**
	 * This method sets (overwrites) the element CostID.
	 * @param data the item that needs to be added.
	 */
	void setCostID(String data) {
		m_costID = data;
	}
		
	/**
	 * Get the embedded ProdID element.
	 * @return the item.
	 */
	public String getProdID() {
		return m_prodID;
	}
		
	/**
	 * This method sets (overwrites) the element ProdID.
	 * @param data the item that needs to be added.
	 */
	void setProdID(String data) {
		m_prodID = data;
	}
		
	/**
	 * Get the embedded ProjID element.
	 * @return the item.
	 */
	public String getProjID() {
		return m_projID;
	}
		
	/**
	 * This method sets (overwrites) the element ProjID.
	 * @param data the item that needs to be added.
	 */
	void setProjID(String data) {
		m_projID = data;
	}
		
	/**
	 * Get the embedded ArtGrpID element.
	 * @return the item.
	 */
	public String getArtGrpID() {
		return m_artGrpID;
	}
		
	/**
	 * This method sets (overwrites) the element ArtGrpID.
	 * @param data the item that needs to be added.
	 */
	void setArtGrpID(String data) {
		m_artGrpID = data;
	}
		
	/**
	 * Get the embedded QntityID element.
	 * @return the item.
	 */
	public String getQntityID() {
		return m_qntityID;
	}
		
	/**
	 * This method sets (overwrites) the element QntityID.
	 * @param data the item that needs to be added.
	 */
	void setQntityID(String data) {
		m_qntityID = data;
	}
		
	/**
	 * Get the embedded Qntity element.
	 * @return the item.
	 */
	public String getQntity() {
		return m_qntity;
	}
		
	/**
	 * This method sets (overwrites) the element Qntity.
	 * @param data the item that needs to be added.
	 */
	void setQntity(String data) {
		m_qntity = data;
	}
		
	/**
	 * Get the embedded list of Vat elements.
	 * @return list of items.
	 */
	public List<VatType_d0e396_a786> getVats() {
		return m_vatList;
	}
		
	/**
	 * This method adds data to the list of Vat.
	 * @param data the item that needs to be added.
	 */
	void setVat(VatType_d0e396_a786 data) {
		m_vatList.add(data);
	}
		
	/**
	 * Get the embedded Currency element.
	 * @return the item.
	 */
	public CurrencyType_d0e403_a797 getCurrency() {
		return m_currency;
	}
		
	/**
	 * This method sets (overwrites) the element Currency.
	 * @param data the item that needs to be added.
	 */
	void setCurrency(CurrencyType_d0e403_a797 data) {
		m_currency = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_nr, ((SbLineType_d0e370_a720)that).m_nr))
			return false;
		
		if (!Compare.equals(m_jrnID, ((SbLineType_d0e370_a720)that).m_jrnID))
			return false;
		
		if (!Compare.equals(m_trNr, ((SbLineType_d0e370_a720)that).m_trNr))
			return false;
		
		if (!Compare.equals(m_trLineNr, ((SbLineType_d0e370_a720)that).m_trLineNr))
			return false;
		
		if (!Compare.equals(m_desc, ((SbLineType_d0e370_a720)that).m_desc))
			return false;
		
		if (!Compare.equals(m_amnt, ((SbLineType_d0e370_a720)that).m_amnt))
			return false;
		
		if (!Compare.equals(m_amntTp, ((SbLineType_d0e370_a720)that).m_amntTp))
			return false;
		
		if (!Compare.equals(m_docRef, ((SbLineType_d0e370_a720)that).m_docRef))
			return false;
		
		if (!Compare.equals(m_recRef, ((SbLineType_d0e370_a720)that).m_recRef))
			return false;
		
		if (!Compare.equals(m_matchKeyID, ((SbLineType_d0e370_a720)that).m_matchKeyID))
			return false;
		
		if (!Compare.equals(m_custSupID, ((SbLineType_d0e370_a720)that).m_custSupID))
			return false;
		
		if (!Compare.equals(m_invRef, ((SbLineType_d0e370_a720)that).m_invRef))
			return false;
		
		if (!Compare.equals(m_invPurSalTp, ((SbLineType_d0e370_a720)that).m_invPurSalTp))
			return false;
		
		if (!Compare.equals(m_invTp, ((SbLineType_d0e370_a720)that).m_invTp))
			return false;
		
		if (!Compare.equals(m_invDt, ((SbLineType_d0e370_a720)that).m_invDt))
			return false;
		
		if (!Compare.equals(m_invDueDt, ((SbLineType_d0e370_a720)that).m_invDueDt))
			return false;
		
		if (!Compare.equals(m_mutTp, ((SbLineType_d0e370_a720)that).m_mutTp))
			return false;
		
		if (!Compare.equals(m_costID, ((SbLineType_d0e370_a720)that).m_costID))
			return false;
		
		if (!Compare.equals(m_prodID, ((SbLineType_d0e370_a720)that).m_prodID))
			return false;
		
		if (!Compare.equals(m_projID, ((SbLineType_d0e370_a720)that).m_projID))
			return false;
		
		if (!Compare.equals(m_artGrpID, ((SbLineType_d0e370_a720)that).m_artGrpID))
			return false;
		
		if (!Compare.equals(m_qntityID, ((SbLineType_d0e370_a720)that).m_qntityID))
			return false;
		
		if (!Compare.equals(m_qntity, ((SbLineType_d0e370_a720)that).m_qntity))
			return false;
		
		if (!Compare.equals(m_vatList, ((SbLineType_d0e370_a720)that).m_vatList))
			return false;
		
		if (!Compare.equals(m_currency, ((SbLineType_d0e370_a720)that).m_currency))
			return false;
		
		return true;
	}	

  
  
}
