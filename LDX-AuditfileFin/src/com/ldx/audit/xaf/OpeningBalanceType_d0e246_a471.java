package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

import java.util.List;
import java.util.ArrayList;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * OpeningBalanceType_d0e246_a471 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class OpeningBalanceType_d0e246_a471 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for OpeningBalanceType_d0e246_a471.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public OpeningBalanceType_d0e246_a471(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type OpeningBalanceType_d0e246_a471.
	 */
	static class Allocator implements TypeAllocator<OpeningBalanceType_d0e246_a471> {
		/**
		 * method for getting a new instance of type OpeningBalanceType_d0e246_a471.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public OpeningBalanceType_d0e246_a471 newInstance(String elementName, ComplexDataType parent) {
			return new OpeningBalanceType_d0e246_a471(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for opBalDate element. 
	 *  @serial
	 */	
	private String m_opBalDate = null;
	
	/** element item for opBalDesc element. 
	 *  @serial
	 */	
	private String m_opBalDesc = null;
	
	/** element item for linesCount element. 
	 *  @serial
	 */	
	private String m_linesCount = null;
	
	/** element item for totalDebit element. 
	 *  @serial
	 */	
	private String m_totalDebit = null;
	
	/** element item for totalCredit element. 
	 *  @serial
	 */	
	private String m_totalCredit = null;
	
	/** list of obLine element. 
	 *  @serial
	 */	
	private List<ObLineType_d0e254_a484> m_obLineList = new ArrayList<ObLineType_d0e254_a484>();
	
	/** element item for obSubledgers element. 
	 *  @serial
	 */	
	private ObSubledgersType_d0e261_a495 m_obSubledgers = null;
	
	/**
	 * Get the embedded OpBalDate element.
	 * @return the item.
	 */
	public String getOpBalDate() {
		return m_opBalDate;
	}
		
	/**
	 * This method sets (overwrites) the element OpBalDate.
	 * @param data the item that needs to be added.
	 */
	void setOpBalDate(String data) {
		m_opBalDate = data;
	}
		
	/**
	 * Get the embedded OpBalDesc element.
	 * @return the item.
	 */
	public String getOpBalDesc() {
		return m_opBalDesc;
	}
		
	/**
	 * This method sets (overwrites) the element OpBalDesc.
	 * @param data the item that needs to be added.
	 */
	void setOpBalDesc(String data) {
		m_opBalDesc = data;
	}
		
	/**
	 * Get the embedded LinesCount element.
	 * @return the item.
	 */
	public String getLinesCount() {
		return m_linesCount;
	}
		
	/**
	 * This method sets (overwrites) the element LinesCount.
	 * @param data the item that needs to be added.
	 */
	void setLinesCount(String data) {
		m_linesCount = data;
	}
		
	/**
	 * Get the embedded TotalDebit element.
	 * @return the item.
	 */
	public String getTotalDebit() {
		return m_totalDebit;
	}
		
	/**
	 * This method sets (overwrites) the element TotalDebit.
	 * @param data the item that needs to be added.
	 */
	void setTotalDebit(String data) {
		m_totalDebit = data;
	}
		
	/**
	 * Get the embedded TotalCredit element.
	 * @return the item.
	 */
	public String getTotalCredit() {
		return m_totalCredit;
	}
		
	/**
	 * This method sets (overwrites) the element TotalCredit.
	 * @param data the item that needs to be added.
	 */
	void setTotalCredit(String data) {
		m_totalCredit = data;
	}
		
	/**
	 * Get the embedded list of ObLine elements.
	 * @return list of items.
	 */
	public List<ObLineType_d0e254_a484> getObLines() {
		return m_obLineList;
	}
		
	/**
	 * This method adds data to the list of ObLine.
	 * @param data the item that needs to be added.
	 */
	void setObLine(ObLineType_d0e254_a484 data) {
		m_obLineList.add(data);
	}
		
	/**
	 * Get the embedded ObSubledgers element.
	 * @return the item.
	 */
	public ObSubledgersType_d0e261_a495 getObSubledgers() {
		return m_obSubledgers;
	}
		
	/**
	 * This method sets (overwrites) the element ObSubledgers.
	 * @param data the item that needs to be added.
	 */
	void setObSubledgers(ObSubledgersType_d0e261_a495 data) {
		m_obSubledgers = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_opBalDate, ((OpeningBalanceType_d0e246_a471)that).m_opBalDate))
			return false;
		
		if (!Compare.equals(m_opBalDesc, ((OpeningBalanceType_d0e246_a471)that).m_opBalDesc))
			return false;
		
		if (!Compare.equals(m_linesCount, ((OpeningBalanceType_d0e246_a471)that).m_linesCount))
			return false;
		
		if (!Compare.equals(m_totalDebit, ((OpeningBalanceType_d0e246_a471)that).m_totalDebit))
			return false;
		
		if (!Compare.equals(m_totalCredit, ((OpeningBalanceType_d0e246_a471)that).m_totalCredit))
			return false;
		
		if (!Compare.equals(m_obLineList, ((OpeningBalanceType_d0e246_a471)that).m_obLineList))
			return false;
		
		if (!Compare.equals(m_obSubledgers, ((OpeningBalanceType_d0e246_a471)that).m_obSubledgers))
			return false;
		
		return true;
	}	

  
  
}
