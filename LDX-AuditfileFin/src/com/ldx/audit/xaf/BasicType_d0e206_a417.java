package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * BasicType_d0e206_a417 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class BasicType_d0e206_a417 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for BasicType_d0e206_a417.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public BasicType_d0e206_a417(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type BasicType_d0e206_a417.
	 */
	static class Allocator implements TypeAllocator<BasicType_d0e206_a417> {
		/**
		 * method for getting a new instance of type BasicType_d0e206_a417.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public BasicType_d0e206_a417 newInstance(String elementName, ComplexDataType parent) {
			return new BasicType_d0e206_a417(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for basicType element. 
	 *  @serial
	 */	
	private String m_basicType = null;
	
	/** element item for basicID element. 
	 *  @serial
	 */	
	private String m_basicID = null;
	
	/** element item for basicDesc element. 
	 *  @serial
	 */	
	private String m_basicDesc = null;
	
	/**
	 * Get the embedded BasicType element.
	 * @return the item.
	 */
	public String getBasicType() {
		return m_basicType;
	}
		
	/**
	 * This method sets (overwrites) the element BasicType.
	 * @param data the item that needs to be added.
	 */
	void setBasicType(String data) {
		m_basicType = data;
	}
		
	/**
	 * Get the embedded BasicID element.
	 * @return the item.
	 */
	public String getBasicID() {
		return m_basicID;
	}
		
	/**
	 * This method sets (overwrites) the element BasicID.
	 * @param data the item that needs to be added.
	 */
	void setBasicID(String data) {
		m_basicID = data;
	}
		
	/**
	 * Get the embedded BasicDesc element.
	 * @return the item.
	 */
	public String getBasicDesc() {
		return m_basicDesc;
	}
		
	/**
	 * This method sets (overwrites) the element BasicDesc.
	 * @param data the item that needs to be added.
	 */
	void setBasicDesc(String data) {
		m_basicDesc = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_basicType, ((BasicType_d0e206_a417)that).m_basicType))
			return false;
		
		if (!Compare.equals(m_basicID, ((BasicType_d0e206_a417)that).m_basicID))
			return false;
		
		if (!Compare.equals(m_basicDesc, ((BasicType_d0e206_a417)that).m_basicDesc))
			return false;
		
		return true;
	}	

  
  
}
