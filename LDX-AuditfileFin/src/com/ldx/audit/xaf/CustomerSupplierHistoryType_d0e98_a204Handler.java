package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

/* SAX 2.0 dependencies */
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/* Framework dependencies */
import com.ldx.xml.core.DataSetter;
import com.ldx.xml.core.ComplexDataType;
import com.ldx.xml.core.XMLEvent;
import com.ldx.xml.core.XMLFragmentHandler;


/**
 * CustomerSupplierHistoryType_d0e98_a204 handler class.
 *
 * @see CustomerSupplierHistoryType_d0e98_a204
 * 
 */
public class CustomerSupplierHistoryType_d0e98_a204Handler extends XMLFragmentHandler<CustomerSupplierHistoryType_d0e98_a204> {
	/**
	 * Proxy for CustomerSupplierHistoryType_d0e98_a204Handler.
	 */
	static class Proxy extends HandlerProxy<CustomerSupplierHistoryType_d0e98_a204> {
		/**
		 * Allocator for CustomerSupplierHistoryType_d0e98_a204Handler.
		 */
		private static class Allocator 
				extends HandlerProxy.Allocator<CustomerSupplierHistoryType_d0e98_a204> {			
			public XMLFragmentHandler<CustomerSupplierHistoryType_d0e98_a204> create(
					XMLReader reader
					, XMLFragmentHandler<?> handler
					, String elementName
					, DataSetter setter
					, boolean doProcess) {
				return new CustomerSupplierHistoryType_d0e98_a204Handler(
					reader
					, handler
					, elementName
					, CustomerSupplierHistoryType_d0e98_a204.getAllocator()
					, setter
					, doProcess);
			}
		}
		
		/**
		 * Constructor for Proxy.
		 *
		 * @param reader the XML reader
		 * @param parentH the parent XML handler class
		 * @param elementName the name of the XML Element (tag).
		 * @param pSetter the setter for the parent data
		 * @param doProcess indicates whether processing is active for this handler instance
		 */
		public Proxy(XMLReader reader, XMLFragmentHandler<?> parentH, String elementName,
				DataSetter pSetter, boolean doProcess) {
			super(reader, parentH, elementName, pSetter, new Allocator(), doProcess);
		}
	}
	
	/** Data setter class for customerSupplier element. */
	private class CustomerSupplierSetter implements DataSetter {
		/** data target. */
		private CustomerSupplierHistoryType_d0e98_a204Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public CustomerSupplierSetter(CustomerSupplierHistoryType_d0e98_a204Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setCustomerSupplier((CustomerSupplierType_d0e101_a206) data);	
		}
	}


	/**
	 * Constructor for handler class.
	 *
	 * @param reader the XML reader
	 * @param parentH the parent XML handler class
	 * @param elementName the name of the XML Element (tag)
	 * @param allocator the allocator for the data type
	 * @param pSetter the setter for the parent data
	 * @param doProcess indicates whether processing is active for this handler instance
	 */
	public CustomerSupplierHistoryType_d0e98_a204Handler(
			XMLReader reader
			, XMLFragmentHandler<?> parentH
			, String elementName
			, CustomerSupplierHistoryType_d0e98_a204.Allocator allocator
			, DataSetter pSetter
			, boolean doProcess) {
		super(reader, parentH, elementName, allocator, pSetter, doProcess);

		// code for linking children..
		registerHandler(
			new CustomerSupplierType_d0e101_a206Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of customerSupplierType_d0e101_a206Handler
				, "customerSupplier" // XML element name
				, doLink("customerSupplier") // linking to parent
					? new CustomerSupplierSetter(this) // ON
					: null // OFF
				, doProcess("customerSupplier")) // processing active or not
				);
  	}


	/**
	 * This method overrides the getData() of the super type, returning the more
	 * specific type.
	 * 
	 */
	@Override 
	public CustomerSupplierHistoryType_d0e98_a204 getData() {
		return (CustomerSupplierHistoryType_d0e98_a204)super.getData();
	}




	/**
	 * This method is called by the XML parser on an end of element event.
	 *
	 * If the localName == 'the name of the element that this handler handles' control is
	 * passed back to the parent handler (future events go there) and the data of
	 * the children of this handler is attached to the data associated to this handler.	
	 * 
	 * The data that is collected is connected to the parent data only if a parentDataSetter
	 * if found (getParentDataSetter()).
	 *
	 * If processing is enabled for a handler, the processor is called.
	 */
	@Override
	public void endElement(String uri, String localName, String name)
		throws SAXException {
		
		if (localName.equals(getXMLElementName())) {
				
			// return control to parent handler..
			this.deactivate();
			
			// get content of this item..
			getData().setContent(this.getValue());
			
			// attach data to parent (if parent data setter is found)..
			DataSetter setter = getParentDataSetter();
			if (setter != null) {
				setter.set(getData());
			} 
			
			// process data if required..
			if (doProcess()) {
				process(XMLEvent.END);
			}
		}
		
	}	
}
