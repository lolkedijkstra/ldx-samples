package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

/* SAX 2.0 dependencies */
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/* Framework dependencies */
import com.ldx.xml.core.DataSetter;
import com.ldx.xml.core.ComplexDataType;
import com.ldx.xml.core.XMLEvent;
import com.ldx.xml.core.XMLFragmentHandler;


/**
 * CompanyType_d0e15_a18 handler class.
 *
 * @see CompanyType_d0e15_a18
 * 
 */
public class CompanyType_d0e15_a18Handler extends XMLFragmentHandler<CompanyType_d0e15_a18> {
	/**
	 * Proxy for CompanyType_d0e15_a18Handler.
	 */
	static class Proxy extends HandlerProxy<CompanyType_d0e15_a18> {
		/**
		 * Allocator for CompanyType_d0e15_a18Handler.
		 */
		private static class Allocator 
				extends HandlerProxy.Allocator<CompanyType_d0e15_a18> {			
			public XMLFragmentHandler<CompanyType_d0e15_a18> create(
					XMLReader reader
					, XMLFragmentHandler<?> handler
					, String elementName
					, DataSetter setter
					, boolean doProcess) {
				return new CompanyType_d0e15_a18Handler(
					reader
					, handler
					, elementName
					, CompanyType_d0e15_a18.getAllocator()
					, setter
					, doProcess);
			}
		}
		
		/**
		 * Constructor for Proxy.
		 *
		 * @param reader the XML reader
		 * @param parentH the parent XML handler class
		 * @param elementName the name of the XML Element (tag).
		 * @param pSetter the setter for the parent data
		 * @param doProcess indicates whether processing is active for this handler instance
		 */
		public Proxy(XMLReader reader, XMLFragmentHandler<?> parentH, String elementName,
				DataSetter pSetter, boolean doProcess) {
			super(reader, parentH, elementName, pSetter, new Allocator(), doProcess);
		}
	}
	
	/** Data setter class for customersSuppliers element. */
	private class CustomersSuppliersSetter implements DataSetter {
		/** data target. */
		private CompanyType_d0e15_a18Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public CustomersSuppliersSetter(CompanyType_d0e15_a18Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setCustomersSuppliers((CustomersSuppliersType_d0e44_a82) data);	
		}
	}	
	/** Data setter class for generalLedger element. */
	private class GeneralLedgerSetter implements DataSetter {
		/** data target. */
		private CompanyType_d0e15_a18Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public GeneralLedgerSetter(CompanyType_d0e15_a18Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setGeneralLedger((GeneralLedgerType_d0e152_a328) data);	
		}
	}	
	/** Data setter class for openingBalance element. */
	private class OpeningBalanceSetter implements DataSetter {
		/** data target. */
		private CompanyType_d0e15_a18Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public OpeningBalanceSetter(CompanyType_d0e15_a18Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setOpeningBalance((OpeningBalanceType_d0e246_a471) data);	
		}
	}	
	/** Data setter class for periods element. */
	private class PeriodsSetter implements DataSetter {
		/** data target. */
		private CompanyType_d0e15_a18Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public PeriodsSetter(CompanyType_d0e15_a18Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setPeriods((PeriodsType_d0e234_a451) data);	
		}
	}	
	/** Data setter class for postalAddress element. */
	private class PostalAddressSetter implements DataSetter {
		/** data target. */
		private CompanyType_d0e15_a18Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public PostalAddressSetter(CompanyType_d0e15_a18Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setPostalAddress((PostalAddressType_d0e33_a55) data);	
		}
	}	
	/** Data setter class for streetAddress element. */
	private class StreetAddressSetter implements DataSetter {
		/** data target. */
		private CompanyType_d0e15_a18Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public StreetAddressSetter(CompanyType_d0e15_a18Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setStreetAddress((StreetAddressType_d0e22_a28) data);	
		}
	}	
	/** Data setter class for transactions element. */
	private class TransactionsSetter implements DataSetter {
		/** data target. */
		private CompanyType_d0e15_a18Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public TransactionsSetter(CompanyType_d0e15_a18Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setTransactions((TransactionsType_d0e296_a573) data);	
		}
	}	
	/** Data setter class for vatCodes element. */
	private class VatCodesSetter implements DataSetter {
		/** data target. */
		private CompanyType_d0e15_a18Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public VatCodesSetter(CompanyType_d0e15_a18Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setVatCodes((VatCodesType_d0e224_a436) data);	
		}
	}


	/**
	 * Constructor for handler class.
	 *
	 * @param reader the XML reader
	 * @param parentH the parent XML handler class
	 * @param elementName the name of the XML Element (tag)
	 * @param allocator the allocator for the data type
	 * @param pSetter the setter for the parent data
	 * @param doProcess indicates whether processing is active for this handler instance
	 */
	public CompanyType_d0e15_a18Handler(
			XMLReader reader
			, XMLFragmentHandler<?> parentH
			, String elementName
			, CompanyType_d0e15_a18.Allocator allocator
			, DataSetter pSetter
			, boolean doProcess) {
		super(reader, parentH, elementName, allocator, pSetter, doProcess);

		// code for linking children..
		registerHandler(
			new CustomersSuppliersType_d0e44_a82Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of customersSuppliersType_d0e44_a82Handler
				, "customersSuppliers" // XML element name
				, doLink("customersSuppliers") // linking to parent
					? new CustomersSuppliersSetter(this) // ON
					: null // OFF
				, doProcess("customersSuppliers")) // processing active or not
				);
  
		registerHandler(
			new GeneralLedgerType_d0e152_a328Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of generalLedgerType_d0e152_a328Handler
				, "generalLedger" // XML element name
				, doLink("generalLedger") // linking to parent
					? new GeneralLedgerSetter(this) // ON
					: null // OFF
				, doProcess("generalLedger")) // processing active or not
				);
  
		registerHandler(
			new OpeningBalanceType_d0e246_a471Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of openingBalanceType_d0e246_a471Handler
				, "openingBalance" // XML element name
				, doLink("openingBalance") // linking to parent
					? new OpeningBalanceSetter(this) // ON
					: null // OFF
				, doProcess("openingBalance")) // processing active or not
				);
  
		registerHandler(
			new PeriodsType_d0e234_a451Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of periodsType_d0e234_a451Handler
				, "periods" // XML element name
				, doLink("periods") // linking to parent
					? new PeriodsSetter(this) // ON
					: null // OFF
				, doProcess("periods")) // processing active or not
				);
  
		registerHandler(
			new PostalAddressType_d0e33_a55Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of postalAddressType_d0e33_a55Handler
				, "postalAddress" // XML element name
				, doLink("postalAddress") // linking to parent
					? new PostalAddressSetter(this) // ON
					: null // OFF
				, doProcess("postalAddress")) // processing active or not
				);
  
		registerHandler(
			new StreetAddressType_d0e22_a28Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of streetAddressType_d0e22_a28Handler
				, "streetAddress" // XML element name
				, doLink("streetAddress") // linking to parent
					? new StreetAddressSetter(this) // ON
					: null // OFF
				, doProcess("streetAddress")) // processing active or not
				);
  
		registerHandler(
			new TransactionsType_d0e296_a573Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of transactionsType_d0e296_a573Handler
				, "transactions" // XML element name
				, doLink("transactions") // linking to parent
					? new TransactionsSetter(this) // ON
					: null // OFF
				, doProcess("transactions")) // processing active or not
				);
  
		registerHandler(
			new VatCodesType_d0e224_a436Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of vatCodesType_d0e224_a436Handler
				, "vatCodes" // XML element name
				, doLink("vatCodes") // linking to parent
					? new VatCodesSetter(this) // ON
					: null // OFF
				, doProcess("vatCodes")) // processing active or not
				);
  	}


	/**
	 * This method overrides the getData() of the super type, returning the more
	 * specific type.
	 * 
	 */
	@Override 
	public CompanyType_d0e15_a18 getData() {
		return (CompanyType_d0e15_a18)super.getData();
	}




	/**
	 * This method is called by the XML parser on an end of element event.
	 *
	 * If the localName == 'the name of the element that this handler handles' control is
	 * passed back to the parent handler (future events go there) and the data of
	 * the children of this handler is attached to the data associated to this handler.	
	 * 
	 * The data that is collected is connected to the parent data only if a parentDataSetter
	 * if found (getParentDataSetter()).
	 *
	 * If processing is enabled for a handler, the processor is called.
	 */
	@Override
	public void endElement(String uri, String localName, String name)
		throws SAXException {
		
		// collect data from children..
		if (localName.equals("companyIdent")) {
			getData().setCompanyIdent(getValue());
			getContents().reset();
		} else if (localName.equals("companyName")) {
			getData().setCompanyName(getValue());
			getContents().reset();
		} else if (localName.equals("taxRegIdent")) {
			getData().setTaxRegIdent(getValue());
			getContents().reset();
		} else if (localName.equals("taxRegistrationCountry")) {
			getData().setTaxRegistrationCountry(getValue());
			getContents().reset();
		} else if (localName.equals(getXMLElementName())) {
				
			// return control to parent handler..
			this.deactivate();
			
			// get content of this item..
			getData().setContent(this.getValue());
			
			// attach data to parent (if parent data setter is found)..
			DataSetter setter = getParentDataSetter();
			if (setter != null) {
				setter.set(getData());
			} 
			
			// process data if required..
			if (doProcess()) {
				process(XMLEvent.END);
			}
		}
		
	}	
}
