package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

import java.util.List;
import java.util.ArrayList;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * ObSubledgerType_d0e264_a497 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class ObSubledgerType_d0e264_a497 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for ObSubledgerType_d0e264_a497.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public ObSubledgerType_d0e264_a497(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type ObSubledgerType_d0e264_a497.
	 */
	static class Allocator implements TypeAllocator<ObSubledgerType_d0e264_a497> {
		/**
		 * method for getting a new instance of type ObSubledgerType_d0e264_a497.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public ObSubledgerType_d0e264_a497 newInstance(String elementName, ComplexDataType parent) {
			return new ObSubledgerType_d0e264_a497(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for sbType element. 
	 *  @serial
	 */	
	private String m_sbType = null;
	
	/** element item for sbDesc element. 
	 *  @serial
	 */	
	private String m_sbDesc = null;
	
	/** element item for linesCount element. 
	 *  @serial
	 */	
	private String m_linesCount = null;
	
	/** element item for totalDebit element. 
	 *  @serial
	 */	
	private String m_totalDebit = null;
	
	/** element item for totalCredit element. 
	 *  @serial
	 */	
	private String m_totalCredit = null;
	
	/** list of obSbLine element. 
	 *  @serial
	 */	
	private List<ObSbLineType_d0e272_a511> m_obSbLineList = new ArrayList<ObSbLineType_d0e272_a511>();
	
	/**
	 * Get the embedded SbType element.
	 * @return the item.
	 */
	public String getSbType() {
		return m_sbType;
	}
		
	/**
	 * This method sets (overwrites) the element SbType.
	 * @param data the item that needs to be added.
	 */
	void setSbType(String data) {
		m_sbType = data;
	}
		
	/**
	 * Get the embedded SbDesc element.
	 * @return the item.
	 */
	public String getSbDesc() {
		return m_sbDesc;
	}
		
	/**
	 * This method sets (overwrites) the element SbDesc.
	 * @param data the item that needs to be added.
	 */
	void setSbDesc(String data) {
		m_sbDesc = data;
	}
		
	/**
	 * Get the embedded LinesCount element.
	 * @return the item.
	 */
	public String getLinesCount() {
		return m_linesCount;
	}
		
	/**
	 * This method sets (overwrites) the element LinesCount.
	 * @param data the item that needs to be added.
	 */
	void setLinesCount(String data) {
		m_linesCount = data;
	}
		
	/**
	 * Get the embedded TotalDebit element.
	 * @return the item.
	 */
	public String getTotalDebit() {
		return m_totalDebit;
	}
		
	/**
	 * This method sets (overwrites) the element TotalDebit.
	 * @param data the item that needs to be added.
	 */
	void setTotalDebit(String data) {
		m_totalDebit = data;
	}
		
	/**
	 * Get the embedded TotalCredit element.
	 * @return the item.
	 */
	public String getTotalCredit() {
		return m_totalCredit;
	}
		
	/**
	 * This method sets (overwrites) the element TotalCredit.
	 * @param data the item that needs to be added.
	 */
	void setTotalCredit(String data) {
		m_totalCredit = data;
	}
		
	/**
	 * Get the embedded list of ObSbLine elements.
	 * @return list of items.
	 */
	public List<ObSbLineType_d0e272_a511> getObSbLines() {
		return m_obSbLineList;
	}
		
	/**
	 * This method adds data to the list of ObSbLine.
	 * @param data the item that needs to be added.
	 */
	void setObSbLine(ObSbLineType_d0e272_a511 data) {
		m_obSbLineList.add(data);
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_sbType, ((ObSubledgerType_d0e264_a497)that).m_sbType))
			return false;
		
		if (!Compare.equals(m_sbDesc, ((ObSubledgerType_d0e264_a497)that).m_sbDesc))
			return false;
		
		if (!Compare.equals(m_linesCount, ((ObSubledgerType_d0e264_a497)that).m_linesCount))
			return false;
		
		if (!Compare.equals(m_totalDebit, ((ObSubledgerType_d0e264_a497)that).m_totalDebit))
			return false;
		
		if (!Compare.equals(m_totalCredit, ((ObSubledgerType_d0e264_a497)that).m_totalCredit))
			return false;
		
		if (!Compare.equals(m_obSbLineList, ((ObSubledgerType_d0e264_a497)that).m_obSbLineList))
			return false;
		
		return true;
	}	

  
  
}
