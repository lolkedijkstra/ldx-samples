package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

import java.util.List;
import java.util.ArrayList;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * EntryPointType_d0e169_a355 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class EntryPointType_d0e169_a355 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for EntryPointType_d0e169_a355.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public EntryPointType_d0e169_a355(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type EntryPointType_d0e169_a355.
	 */
	static class Allocator implements TypeAllocator<EntryPointType_d0e169_a355> {
		/**
		 * method for getting a new instance of type EntryPointType_d0e169_a355.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public EntryPointType_d0e169_a355 newInstance(String elementName, ComplexDataType parent) {
			return new EntryPointType_d0e169_a355(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for entryPointRef element. 
	 *  @serial
	 */	
	private String m_entryPointRef = null;
	
	/** element item for conceptRef element. 
	 *  @serial
	 */	
	private String m_conceptRef = null;
	
	/** list of domainMember element. 
	 *  @serial
	 */	
	private List<DomainMemberType_d0e174_a362> m_domainMemberList = new ArrayList<DomainMemberType_d0e174_a362>();
	
	/**
	 * Get the embedded EntryPointRef element.
	 * @return the item.
	 */
	public String getEntryPointRef() {
		return m_entryPointRef;
	}
		
	/**
	 * This method sets (overwrites) the element EntryPointRef.
	 * @param data the item that needs to be added.
	 */
	void setEntryPointRef(String data) {
		m_entryPointRef = data;
	}
		
	/**
	 * Get the embedded ConceptRef element.
	 * @return the item.
	 */
	public String getConceptRef() {
		return m_conceptRef;
	}
		
	/**
	 * This method sets (overwrites) the element ConceptRef.
	 * @param data the item that needs to be added.
	 */
	void setConceptRef(String data) {
		m_conceptRef = data;
	}
		
	/**
	 * Get the embedded list of DomainMember elements.
	 * @return list of items.
	 */
	public List<DomainMemberType_d0e174_a362> getDomainMembers() {
		return m_domainMemberList;
	}
		
	/**
	 * This method adds data to the list of DomainMember.
	 * @param data the item that needs to be added.
	 */
	void setDomainMember(DomainMemberType_d0e174_a362 data) {
		m_domainMemberList.add(data);
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_entryPointRef, ((EntryPointType_d0e169_a355)that).m_entryPointRef))
			return false;
		
		if (!Compare.equals(m_conceptRef, ((EntryPointType_d0e169_a355)that).m_conceptRef))
			return false;
		
		if (!Compare.equals(m_domainMemberList, ((EntryPointType_d0e169_a355)that).m_domainMemberList))
			return false;
		
		return true;
	}	

  
  
}
