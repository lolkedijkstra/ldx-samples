package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * HeaderType_d0e5_a1 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class HeaderType_d0e5_a1 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for HeaderType_d0e5_a1.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public HeaderType_d0e5_a1(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type HeaderType_d0e5_a1.
	 */
	static class Allocator implements TypeAllocator<HeaderType_d0e5_a1> {
		/**
		 * method for getting a new instance of type HeaderType_d0e5_a1.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public HeaderType_d0e5_a1 newInstance(String elementName, ComplexDataType parent) {
			return new HeaderType_d0e5_a1(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for fiscalYear element. 
	 *  @serial
	 */	
	private String m_fiscalYear = null;
	
	/** element item for startDate element. 
	 *  @serial
	 */	
	private String m_startDate = null;
	
	/** element item for endDate element. 
	 *  @serial
	 */	
	private String m_endDate = null;
	
	/** element item for curCode element. 
	 *  @serial
	 */	
	private String m_curCode = null;
	
	/** element item for dateCreated element. 
	 *  @serial
	 */	
	private String m_dateCreated = null;
	
	/** element item for softwareDesc element. 
	 *  @serial
	 */	
	private String m_softwareDesc = null;
	
	/** element item for softwareVersion element. 
	 *  @serial
	 */	
	private String m_softwareVersion = null;
	
	/**
	 * Get the embedded FiscalYear element.
	 * @return the item.
	 */
	public String getFiscalYear() {
		return m_fiscalYear;
	}
		
	/**
	 * This method sets (overwrites) the element FiscalYear.
	 * @param data the item that needs to be added.
	 */
	void setFiscalYear(String data) {
		m_fiscalYear = data;
	}
		
	/**
	 * Get the embedded StartDate element.
	 * @return the item.
	 */
	public String getStartDate() {
		return m_startDate;
	}
		
	/**
	 * This method sets (overwrites) the element StartDate.
	 * @param data the item that needs to be added.
	 */
	void setStartDate(String data) {
		m_startDate = data;
	}
		
	/**
	 * Get the embedded EndDate element.
	 * @return the item.
	 */
	public String getEndDate() {
		return m_endDate;
	}
		
	/**
	 * This method sets (overwrites) the element EndDate.
	 * @param data the item that needs to be added.
	 */
	void setEndDate(String data) {
		m_endDate = data;
	}
		
	/**
	 * Get the embedded CurCode element.
	 * @return the item.
	 */
	public String getCurCode() {
		return m_curCode;
	}
		
	/**
	 * This method sets (overwrites) the element CurCode.
	 * @param data the item that needs to be added.
	 */
	void setCurCode(String data) {
		m_curCode = data;
	}
		
	/**
	 * Get the embedded DateCreated element.
	 * @return the item.
	 */
	public String getDateCreated() {
		return m_dateCreated;
	}
		
	/**
	 * This method sets (overwrites) the element DateCreated.
	 * @param data the item that needs to be added.
	 */
	void setDateCreated(String data) {
		m_dateCreated = data;
	}
		
	/**
	 * Get the embedded SoftwareDesc element.
	 * @return the item.
	 */
	public String getSoftwareDesc() {
		return m_softwareDesc;
	}
		
	/**
	 * This method sets (overwrites) the element SoftwareDesc.
	 * @param data the item that needs to be added.
	 */
	void setSoftwareDesc(String data) {
		m_softwareDesc = data;
	}
		
	/**
	 * Get the embedded SoftwareVersion element.
	 * @return the item.
	 */
	public String getSoftwareVersion() {
		return m_softwareVersion;
	}
		
	/**
	 * This method sets (overwrites) the element SoftwareVersion.
	 * @param data the item that needs to be added.
	 */
	void setSoftwareVersion(String data) {
		m_softwareVersion = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_fiscalYear, ((HeaderType_d0e5_a1)that).m_fiscalYear))
			return false;
		
		if (!Compare.equals(m_startDate, ((HeaderType_d0e5_a1)that).m_startDate))
			return false;
		
		if (!Compare.equals(m_endDate, ((HeaderType_d0e5_a1)that).m_endDate))
			return false;
		
		if (!Compare.equals(m_curCode, ((HeaderType_d0e5_a1)that).m_curCode))
			return false;
		
		if (!Compare.equals(m_dateCreated, ((HeaderType_d0e5_a1)that).m_dateCreated))
			return false;
		
		if (!Compare.equals(m_softwareDesc, ((HeaderType_d0e5_a1)that).m_softwareDesc))
			return false;
		
		if (!Compare.equals(m_softwareVersion, ((HeaderType_d0e5_a1)that).m_softwareVersion))
			return false;
		
		return true;
	}	

  
  
}
