package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

/* SAX 2.0 dependencies */
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/* Framework dependencies */
import com.ldx.xml.core.DataSetter;
import com.ldx.xml.core.ComplexDataType;
import com.ldx.xml.core.XMLEvent;
import com.ldx.xml.core.XMLFragmentHandler;


/**
 * JournalType_d0e302_a581 handler class.
 *
 * @see JournalType_d0e302_a581
 * 
 */
public class JournalType_d0e302_a581Handler extends XMLFragmentHandler<JournalType_d0e302_a581> {
	/**
	 * Proxy for JournalType_d0e302_a581Handler.
	 */
	static class Proxy extends HandlerProxy<JournalType_d0e302_a581> {
		/**
		 * Allocator for JournalType_d0e302_a581Handler.
		 */
		private static class Allocator 
				extends HandlerProxy.Allocator<JournalType_d0e302_a581> {			
			public XMLFragmentHandler<JournalType_d0e302_a581> create(
					XMLReader reader
					, XMLFragmentHandler<?> handler
					, String elementName
					, DataSetter setter
					, boolean doProcess) {
				return new JournalType_d0e302_a581Handler(
					reader
					, handler
					, elementName
					, JournalType_d0e302_a581.getAllocator()
					, setter
					, doProcess);
			}
		}
		
		/**
		 * Constructor for Proxy.
		 *
		 * @param reader the XML reader
		 * @param parentH the parent XML handler class
		 * @param elementName the name of the XML Element (tag).
		 * @param pSetter the setter for the parent data
		 * @param doProcess indicates whether processing is active for this handler instance
		 */
		public Proxy(XMLReader reader, XMLFragmentHandler<?> parentH, String elementName,
				DataSetter pSetter, boolean doProcess) {
			super(reader, parentH, elementName, pSetter, new Allocator(), doProcess);
		}
	}
	
	/** Data setter class for transaction element. */
	private class TransactionSetter implements DataSetter {
		/** data target. */
		private JournalType_d0e302_a581Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public TransactionSetter(JournalType_d0e302_a581Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setTransaction((TransactionType_d0e310_a597) data);	
		}
	}


	/**
	 * Constructor for handler class.
	 *
	 * @param reader the XML reader
	 * @param parentH the parent XML handler class
	 * @param elementName the name of the XML Element (tag)
	 * @param allocator the allocator for the data type
	 * @param pSetter the setter for the parent data
	 * @param doProcess indicates whether processing is active for this handler instance
	 */
	public JournalType_d0e302_a581Handler(
			XMLReader reader
			, XMLFragmentHandler<?> parentH
			, String elementName
			, JournalType_d0e302_a581.Allocator allocator
			, DataSetter pSetter
			, boolean doProcess) {
		super(reader, parentH, elementName, allocator, pSetter, doProcess);

		// code for linking children..
		registerHandler(
			new TransactionType_d0e310_a597Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of transactionType_d0e310_a597Handler
				, "transaction" // XML element name
				, doLink("transaction") // linking to parent
					? new TransactionSetter(this) // ON
					: null // OFF
				, doProcess("transaction")) // processing active or not
				);
  	}


	/**
	 * This method overrides the getData() of the super type, returning the more
	 * specific type.
	 * 
	 */
	@Override 
	public JournalType_d0e302_a581 getData() {
		return (JournalType_d0e302_a581)super.getData();
	}




	/**
	 * This method is called by the XML parser on an end of element event.
	 *
	 * If the localName == 'the name of the element that this handler handles' control is
	 * passed back to the parent handler (future events go there) and the data of
	 * the children of this handler is attached to the data associated to this handler.	
	 * 
	 * The data that is collected is connected to the parent data only if a parentDataSetter
	 * if found (getParentDataSetter()).
	 *
	 * If processing is enabled for a handler, the processor is called.
	 */
	@Override
	public void endElement(String uri, String localName, String name)
		throws SAXException {
		
		// collect data from children..
		if (localName.equals("bankAccNr")) {
			getData().setBankAccNr(getValue());
			getContents().reset();
		} else if (localName.equals("desc")) {
			getData().setDesc(getValue());
			getContents().reset();
		} else if (localName.equals("jrnID")) {
			getData().setJrnID(getValue());
			getContents().reset();
		} else if (localName.equals("jrnTp")) {
			getData().setJrnTp(getValue());
			getContents().reset();
		} else if (localName.equals("offsetAccID")) {
			getData().setOffsetAccID(getValue());
			getContents().reset();
		} else if (localName.equals(getXMLElementName())) {
				
			// return control to parent handler..
			this.deactivate();
			
			// get content of this item..
			getData().setContent(this.getValue());
			
			// attach data to parent (if parent data setter is found)..
			DataSetter setter = getParentDataSetter();
			if (setter != null) {
				setter.set(getData());
			} 
			
			// process data if required..
			if (doProcess()) {
				process(XMLEvent.END);
			}
		}
		
	}	
}
