package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * VatType_d0e396_a786 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class VatType_d0e396_a786 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for VatType_d0e396_a786.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public VatType_d0e396_a786(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type VatType_d0e396_a786.
	 */
	static class Allocator implements TypeAllocator<VatType_d0e396_a786> {
		/**
		 * method for getting a new instance of type VatType_d0e396_a786.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public VatType_d0e396_a786 newInstance(String elementName, ComplexDataType parent) {
			return new VatType_d0e396_a786(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for vatID element. 
	 *  @serial
	 */	
	private String m_vatID = null;
	
	/** element item for vatPerc element. 
	 *  @serial
	 */	
	private String m_vatPerc = null;
	
	/** element item for vatAmnt element. 
	 *  @serial
	 */	
	private String m_vatAmnt = null;
	
	/** element item for vatAmntTp element. 
	 *  @serial
	 */	
	private String m_vatAmntTp = null;
	
	/**
	 * Get the embedded VatID element.
	 * @return the item.
	 */
	public String getVatID() {
		return m_vatID;
	}
		
	/**
	 * This method sets (overwrites) the element VatID.
	 * @param data the item that needs to be added.
	 */
	void setVatID(String data) {
		m_vatID = data;
	}
		
	/**
	 * Get the embedded VatPerc element.
	 * @return the item.
	 */
	public String getVatPerc() {
		return m_vatPerc;
	}
		
	/**
	 * This method sets (overwrites) the element VatPerc.
	 * @param data the item that needs to be added.
	 */
	void setVatPerc(String data) {
		m_vatPerc = data;
	}
		
	/**
	 * Get the embedded VatAmnt element.
	 * @return the item.
	 */
	public String getVatAmnt() {
		return m_vatAmnt;
	}
		
	/**
	 * This method sets (overwrites) the element VatAmnt.
	 * @param data the item that needs to be added.
	 */
	void setVatAmnt(String data) {
		m_vatAmnt = data;
	}
		
	/**
	 * Get the embedded VatAmntTp element.
	 * @return the item.
	 */
	public String getVatAmntTp() {
		return m_vatAmntTp;
	}
		
	/**
	 * This method sets (overwrites) the element VatAmntTp.
	 * @param data the item that needs to be added.
	 */
	void setVatAmntTp(String data) {
		m_vatAmntTp = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_vatID, ((VatType_d0e396_a786)that).m_vatID))
			return false;
		
		if (!Compare.equals(m_vatPerc, ((VatType_d0e396_a786)that).m_vatPerc))
			return false;
		
		if (!Compare.equals(m_vatAmnt, ((VatType_d0e396_a786)that).m_vatAmnt))
			return false;
		
		if (!Compare.equals(m_vatAmntTp, ((VatType_d0e396_a786)that).m_vatAmntTp))
			return false;
		
		return true;
	}	

  
  
}
