package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

/* SAX 2.0 dependencies */
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/* Framework dependencies */
import com.ldx.xml.core.DataSetter;
import com.ldx.xml.core.XMLEvent;
import com.ldx.xml.core.XMLFragmentHandler;


/**
 * ObSbLineType_d0e272_a511 handler class.
 *
 * @see ObSbLineType_d0e272_a511
 * 
 */
public class ObSbLineType_d0e272_a511Handler extends XMLFragmentHandler<ObSbLineType_d0e272_a511> {
	/**
	 * Proxy for ObSbLineType_d0e272_a511Handler.
	 */
	static class Proxy extends HandlerProxy<ObSbLineType_d0e272_a511> {
		/**
		 * Allocator for ObSbLineType_d0e272_a511Handler.
		 */
		private static class Allocator 
				extends HandlerProxy.Allocator<ObSbLineType_d0e272_a511> {			
			public XMLFragmentHandler<ObSbLineType_d0e272_a511> create(
					XMLReader reader
					, XMLFragmentHandler<?> handler
					, String elementName
					, DataSetter setter
					, boolean doProcess) {
				return new ObSbLineType_d0e272_a511Handler(
					reader
					, handler
					, elementName
					, ObSbLineType_d0e272_a511.getAllocator()
					, setter
					, doProcess);
			}
		}
		
		/**
		 * Constructor for Proxy.
		 *
		 * @param reader the XML reader
		 * @param parentH the parent XML handler class
		 * @param elementName the name of the XML Element (tag).
		 * @param pSetter the setter for the parent data
		 * @param doProcess indicates whether processing is active for this handler instance
		 */
		public Proxy(XMLReader reader, XMLFragmentHandler<?> parentH, String elementName,
				DataSetter pSetter, boolean doProcess) {
			super(reader, parentH, elementName, pSetter, new Allocator(), doProcess);
		}
	}



	/**
	 * Constructor for handler class.
	 *
	 * @param reader the XML reader
	 * @param parentH the parent XML handler class
	 * @param elementName the name of the XML Element (tag)
	 * @param allocator the allocator for the data type
	 * @param pSetter the setter for the parent data
	 * @param doProcess indicates whether processing is active for this handler instance
	 */
	public ObSbLineType_d0e272_a511Handler(
			XMLReader reader
			, XMLFragmentHandler<?> parentH
			, String elementName
			, ObSbLineType_d0e272_a511.Allocator allocator
			, DataSetter pSetter
			, boolean doProcess) {
		super(reader, parentH, elementName, allocator, pSetter, doProcess);
	}


	/**
	 * This method overrides the getData() of the super type, returning the more
	 * specific type.
	 * 
	 */
	@Override 
	public ObSbLineType_d0e272_a511 getData() {
		return (ObSbLineType_d0e272_a511)super.getData();
	}




	/**
	 * This method is called by the XML parser on an end of element event.
	 *
	 * If the localName == 'the name of the element that this handler handles' control is
	 * passed back to the parent handler (future events go there) and the data of
	 * the children of this handler is attached to the data associated to this handler.	
	 * 
	 * The data that is collected is connected to the parent data only if a parentDataSetter
	 * if found (getParentDataSetter()).
	 *
	 * If processing is enabled for a handler, the processor is called.
	 */
	@Override
	public void endElement(String uri, String localName, String name)
		throws SAXException {
		
		// collect data from children..
		if (localName.equals("amnt")) {
			getData().setAmnt(getValue());
			getContents().reset();
		} else if (localName.equals("amntTp")) {
			getData().setAmntTp(getValue());
			getContents().reset();
		} else if (localName.equals("artGrpID")) {
			getData().setArtGrpID(getValue());
			getContents().reset();
		} else if (localName.equals("costID")) {
			getData().setCostID(getValue());
			getContents().reset();
		} else if (localName.equals("custSupID")) {
			getData().setCustSupID(getValue());
			getContents().reset();
		} else if (localName.equals("desc")) {
			getData().setDesc(getValue());
			getContents().reset();
		} else if (localName.equals("docRef")) {
			getData().setDocRef(getValue());
			getContents().reset();
		} else if (localName.equals("invDt")) {
			getData().setInvDt(getValue());
			getContents().reset();
		} else if (localName.equals("invDueDt")) {
			getData().setInvDueDt(getValue());
			getContents().reset();
		} else if (localName.equals("invPurSalTp")) {
			getData().setInvPurSalTp(getValue());
			getContents().reset();
		} else if (localName.equals("invRef")) {
			getData().setInvRef(getValue());
			getContents().reset();
		} else if (localName.equals("invTp")) {
			getData().setInvTp(getValue());
			getContents().reset();
		} else if (localName.equals("matchKeyID")) {
			getData().setMatchKeyID(getValue());
			getContents().reset();
		} else if (localName.equals("mutTp")) {
			getData().setMutTp(getValue());
			getContents().reset();
		} else if (localName.equals("nr")) {
			getData().setNr(getValue());
			getContents().reset();
		} else if (localName.equals("obLineNr")) {
			getData().setObLineNr(getValue());
			getContents().reset();
		} else if (localName.equals("prodID")) {
			getData().setProdID(getValue());
			getContents().reset();
		} else if (localName.equals("projID")) {
			getData().setProjID(getValue());
			getContents().reset();
		} else if (localName.equals("qntity")) {
			getData().setQntity(getValue());
			getContents().reset();
		} else if (localName.equals("qntityID")) {
			getData().setQntityID(getValue());
			getContents().reset();
		} else if (localName.equals("recRef")) {
			getData().setRecRef(getValue());
			getContents().reset();
		} else if (localName.equals(getXMLElementName())) {
				
			// return control to parent handler..
			this.deactivate();
			
			// get content of this item..
			getData().setContent(this.getValue());
			
			// attach data to parent (if parent data setter is found)..
			DataSetter setter = getParentDataSetter();
			if (setter != null) {
				setter.set(getData());
			} 
			
			// process data if required..
			if (doProcess()) {
				process(XMLEvent.END);
			}
		}
		
	}	
}
