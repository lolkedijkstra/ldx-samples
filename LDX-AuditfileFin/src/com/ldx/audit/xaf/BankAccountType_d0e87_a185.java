package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * BankAccountType_d0e87_a185 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class BankAccountType_d0e87_a185 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for BankAccountType_d0e87_a185.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public BankAccountType_d0e87_a185(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type BankAccountType_d0e87_a185.
	 */
	static class Allocator implements TypeAllocator<BankAccountType_d0e87_a185> {
		/**
		 * method for getting a new instance of type BankAccountType_d0e87_a185.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public BankAccountType_d0e87_a185 newInstance(String elementName, ComplexDataType parent) {
			return new BankAccountType_d0e87_a185(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for bankAccNr element. 
	 *  @serial
	 */	
	private String m_bankAccNr = null;
	
	/** element item for bankIdCd element. 
	 *  @serial
	 */	
	private String m_bankIdCd = null;
	
	/**
	 * Get the embedded BankAccNr element.
	 * @return the item.
	 */
	public String getBankAccNr() {
		return m_bankAccNr;
	}
		
	/**
	 * This method sets (overwrites) the element BankAccNr.
	 * @param data the item that needs to be added.
	 */
	void setBankAccNr(String data) {
		m_bankAccNr = data;
	}
		
	/**
	 * Get the embedded BankIdCd element.
	 * @return the item.
	 */
	public String getBankIdCd() {
		return m_bankIdCd;
	}
		
	/**
	 * This method sets (overwrites) the element BankIdCd.
	 * @param data the item that needs to be added.
	 */
	void setBankIdCd(String data) {
		m_bankIdCd = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_bankAccNr, ((BankAccountType_d0e87_a185)that).m_bankAccNr))
			return false;
		
		if (!Compare.equals(m_bankIdCd, ((BankAccountType_d0e87_a185)that).m_bankIdCd))
			return false;
		
		return true;
	}	

  
  
}
