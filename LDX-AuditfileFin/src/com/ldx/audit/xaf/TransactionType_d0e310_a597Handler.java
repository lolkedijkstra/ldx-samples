package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

/* SAX 2.0 dependencies */
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/* Framework dependencies */
import com.ldx.xml.core.DataSetter;
import com.ldx.xml.core.ComplexDataType;
import com.ldx.xml.core.XMLEvent;
import com.ldx.xml.core.XMLFragmentHandler;


/**
 * TransactionType_d0e310_a597 handler class.
 *
 * @see TransactionType_d0e310_a597
 * 
 */
public class TransactionType_d0e310_a597Handler extends XMLFragmentHandler<TransactionType_d0e310_a597> {
	/**
	 * Proxy for TransactionType_d0e310_a597Handler.
	 */
	static class Proxy extends HandlerProxy<TransactionType_d0e310_a597> {
		/**
		 * Allocator for TransactionType_d0e310_a597Handler.
		 */
		private static class Allocator 
				extends HandlerProxy.Allocator<TransactionType_d0e310_a597> {			
			public XMLFragmentHandler<TransactionType_d0e310_a597> create(
					XMLReader reader
					, XMLFragmentHandler<?> handler
					, String elementName
					, DataSetter setter
					, boolean doProcess) {
				return new TransactionType_d0e310_a597Handler(
					reader
					, handler
					, elementName
					, TransactionType_d0e310_a597.getAllocator()
					, setter
					, doProcess);
			}
		}
		
		/**
		 * Constructor for Proxy.
		 *
		 * @param reader the XML reader
		 * @param parentH the parent XML handler class
		 * @param elementName the name of the XML Element (tag).
		 * @param pSetter the setter for the parent data
		 * @param doProcess indicates whether processing is active for this handler instance
		 */
		public Proxy(XMLReader reader, XMLFragmentHandler<?> parentH, String elementName,
				DataSetter pSetter, boolean doProcess) {
			super(reader, parentH, elementName, pSetter, new Allocator(), doProcess);
		}
	}
	
	/** Data setter class for trLine element. */
	private class TrLineSetter implements DataSetter {
		/** data target. */
		private TransactionType_d0e310_a597Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public TrLineSetter(TransactionType_d0e310_a597Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setTrLine((TrLineType_d0e321_a621) data);	
		}
	}


	/**
	 * Constructor for handler class.
	 *
	 * @param reader the XML reader
	 * @param parentH the parent XML handler class
	 * @param elementName the name of the XML Element (tag)
	 * @param allocator the allocator for the data type
	 * @param pSetter the setter for the parent data
	 * @param doProcess indicates whether processing is active for this handler instance
	 */
	public TransactionType_d0e310_a597Handler(
			XMLReader reader
			, XMLFragmentHandler<?> parentH
			, String elementName
			, TransactionType_d0e310_a597.Allocator allocator
			, DataSetter pSetter
			, boolean doProcess) {
		super(reader, parentH, elementName, allocator, pSetter, doProcess);

		// code for linking children..
		registerHandler(
			new TrLineType_d0e321_a621Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of trLineType_d0e321_a621Handler
				, "trLine" // XML element name
				, doLink("trLine") // linking to parent
					? new TrLineSetter(this) // ON
					: null // OFF
				, doProcess("trLine")) // processing active or not
				);
  	}


	/**
	 * This method overrides the getData() of the super type, returning the more
	 * specific type.
	 * 
	 */
	@Override 
	public TransactionType_d0e310_a597 getData() {
		return (TransactionType_d0e310_a597)super.getData();
	}




	/**
	 * This method is called by the XML parser on an end of element event.
	 *
	 * If the localName == 'the name of the element that this handler handles' control is
	 * passed back to the parent handler (future events go there) and the data of
	 * the children of this handler is attached to the data associated to this handler.	
	 * 
	 * The data that is collected is connected to the parent data only if a parentDataSetter
	 * if found (getParentDataSetter()).
	 *
	 * If processing is enabled for a handler, the processor is called.
	 */
	@Override
	public void endElement(String uri, String localName, String name)
		throws SAXException {
		
		// collect data from children..
		if (localName.equals("amnt")) {
			getData().setAmnt(getValue());
			getContents().reset();
		} else if (localName.equals("amntTp")) {
			getData().setAmntTp(getValue());
			getContents().reset();
		} else if (localName.equals("desc")) {
			getData().setDesc(getValue());
			getContents().reset();
		} else if (localName.equals("nr")) {
			getData().setNr(getValue());
			getContents().reset();
		} else if (localName.equals("periodNumber")) {
			getData().setPeriodNumber(getValue());
			getContents().reset();
		} else if (localName.equals("sourceID")) {
			getData().setSourceID(getValue());
			getContents().reset();
		} else if (localName.equals("trDt")) {
			getData().setTrDt(getValue());
			getContents().reset();
		} else if (localName.equals("userID")) {
			getData().setUserID(getValue());
			getContents().reset();
		} else if (localName.equals(getXMLElementName())) {
				
			// return control to parent handler..
			this.deactivate();
			
			// get content of this item..
			getData().setContent(this.getValue());
			
			// attach data to parent (if parent data setter is found)..
			DataSetter setter = getParentDataSetter();
			if (setter != null) {
				setter.set(getData());
			} 
			
			// process data if required..
			if (doProcess()) {
				process(XMLEvent.END);
			}
		}
		
	}	
}
