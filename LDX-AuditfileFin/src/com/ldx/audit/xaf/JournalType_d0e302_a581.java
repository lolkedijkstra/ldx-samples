package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

import java.util.List;
import java.util.ArrayList;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * JournalType_d0e302_a581 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class JournalType_d0e302_a581 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for JournalType_d0e302_a581.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public JournalType_d0e302_a581(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type JournalType_d0e302_a581.
	 */
	static class Allocator implements TypeAllocator<JournalType_d0e302_a581> {
		/**
		 * method for getting a new instance of type JournalType_d0e302_a581.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public JournalType_d0e302_a581 newInstance(String elementName, ComplexDataType parent) {
			return new JournalType_d0e302_a581(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for jrnID element. 
	 *  @serial
	 */	
	private String m_jrnID = null;
	
	/** element item for desc element. 
	 *  @serial
	 */	
	private String m_desc = null;
	
	/** element item for jrnTp element. 
	 *  @serial
	 */	
	private String m_jrnTp = null;
	
	/** element item for offsetAccID element. 
	 *  @serial
	 */	
	private String m_offsetAccID = null;
	
	/** element item for bankAccNr element. 
	 *  @serial
	 */	
	private String m_bankAccNr = null;
	
	/** list of transaction element. 
	 *  @serial
	 */	
	private List<TransactionType_d0e310_a597> m_transactionList = new ArrayList<TransactionType_d0e310_a597>();
	
	/**
	 * Get the embedded JrnID element.
	 * @return the item.
	 */
	public String getJrnID() {
		return m_jrnID;
	}
		
	/**
	 * This method sets (overwrites) the element JrnID.
	 * @param data the item that needs to be added.
	 */
	void setJrnID(String data) {
		m_jrnID = data;
	}
		
	/**
	 * Get the embedded Desc element.
	 * @return the item.
	 */
	public String getDesc() {
		return m_desc;
	}
		
	/**
	 * This method sets (overwrites) the element Desc.
	 * @param data the item that needs to be added.
	 */
	void setDesc(String data) {
		m_desc = data;
	}
		
	/**
	 * Get the embedded JrnTp element.
	 * @return the item.
	 */
	public String getJrnTp() {
		return m_jrnTp;
	}
		
	/**
	 * This method sets (overwrites) the element JrnTp.
	 * @param data the item that needs to be added.
	 */
	void setJrnTp(String data) {
		m_jrnTp = data;
	}
		
	/**
	 * Get the embedded OffsetAccID element.
	 * @return the item.
	 */
	public String getOffsetAccID() {
		return m_offsetAccID;
	}
		
	/**
	 * This method sets (overwrites) the element OffsetAccID.
	 * @param data the item that needs to be added.
	 */
	void setOffsetAccID(String data) {
		m_offsetAccID = data;
	}
		
	/**
	 * Get the embedded BankAccNr element.
	 * @return the item.
	 */
	public String getBankAccNr() {
		return m_bankAccNr;
	}
		
	/**
	 * This method sets (overwrites) the element BankAccNr.
	 * @param data the item that needs to be added.
	 */
	void setBankAccNr(String data) {
		m_bankAccNr = data;
	}
		
	/**
	 * Get the embedded list of Transaction elements.
	 * @return list of items.
	 */
	public List<TransactionType_d0e310_a597> getTransactions() {
		return m_transactionList;
	}
		
	/**
	 * This method adds data to the list of Transaction.
	 * @param data the item that needs to be added.
	 */
	void setTransaction(TransactionType_d0e310_a597 data) {
		m_transactionList.add(data);
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_jrnID, ((JournalType_d0e302_a581)that).m_jrnID))
			return false;
		
		if (!Compare.equals(m_desc, ((JournalType_d0e302_a581)that).m_desc))
			return false;
		
		if (!Compare.equals(m_jrnTp, ((JournalType_d0e302_a581)that).m_jrnTp))
			return false;
		
		if (!Compare.equals(m_offsetAccID, ((JournalType_d0e302_a581)that).m_offsetAccID))
			return false;
		
		if (!Compare.equals(m_bankAccNr, ((JournalType_d0e302_a581)that).m_bankAccNr))
			return false;
		
		if (!Compare.equals(m_transactionList, ((JournalType_d0e302_a581)that).m_transactionList))
			return false;
		
		return true;
	}	

  
  
}
