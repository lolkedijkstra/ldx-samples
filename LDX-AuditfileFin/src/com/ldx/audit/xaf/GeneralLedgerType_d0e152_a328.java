package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

import java.util.List;
import java.util.ArrayList;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * GeneralLedgerType_d0e152_a328 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class GeneralLedgerType_d0e152_a328 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for GeneralLedgerType_d0e152_a328.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public GeneralLedgerType_d0e152_a328(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type GeneralLedgerType_d0e152_a328.
	 */
	static class Allocator implements TypeAllocator<GeneralLedgerType_d0e152_a328> {
		/**
		 * method for getting a new instance of type GeneralLedgerType_d0e152_a328.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public GeneralLedgerType_d0e152_a328 newInstance(String elementName, ComplexDataType parent) {
			return new GeneralLedgerType_d0e152_a328(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** list of ledgerAccount element. 
	 *  @serial
	 */	
	private List<LedgerAccountType_d0e155_a330> m_ledgerAccountList = new ArrayList<LedgerAccountType_d0e155_a330>();
	
	/** element item for basics element. 
	 *  @serial
	 */	
	private BasicsType_d0e203_a415 m_basics = null;
	
	/**
	 * Get the embedded list of LedgerAccount elements.
	 * @return list of items.
	 */
	public List<LedgerAccountType_d0e155_a330> getLedgerAccounts() {
		return m_ledgerAccountList;
	}
		
	/**
	 * This method adds data to the list of LedgerAccount.
	 * @param data the item that needs to be added.
	 */
	void setLedgerAccount(LedgerAccountType_d0e155_a330 data) {
		m_ledgerAccountList.add(data);
	}
		
	/**
	 * Get the embedded Basics element.
	 * @return the item.
	 */
	public BasicsType_d0e203_a415 getBasics() {
		return m_basics;
	}
		
	/**
	 * This method sets (overwrites) the element Basics.
	 * @param data the item that needs to be added.
	 */
	void setBasics(BasicsType_d0e203_a415 data) {
		m_basics = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_ledgerAccountList, ((GeneralLedgerType_d0e152_a328)that).m_ledgerAccountList))
			return false;
		
		if (!Compare.equals(m_basics, ((GeneralLedgerType_d0e152_a328)that).m_basics))
			return false;
		
		return true;
	}	

  
  
}
