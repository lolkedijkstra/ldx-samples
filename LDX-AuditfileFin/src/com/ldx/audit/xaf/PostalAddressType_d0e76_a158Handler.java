package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

/* SAX 2.0 dependencies */
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/* Framework dependencies */
import com.ldx.xml.core.DataSetter;
import com.ldx.xml.core.XMLEvent;
import com.ldx.xml.core.XMLFragmentHandler;


/**
 * PostalAddressType_d0e76_a158 handler class.
 *
 * @see PostalAddressType_d0e76_a158
 * 
 */
public class PostalAddressType_d0e76_a158Handler extends XMLFragmentHandler<PostalAddressType_d0e76_a158> {
	/**
	 * Proxy for PostalAddressType_d0e76_a158Handler.
	 */
	static class Proxy extends HandlerProxy<PostalAddressType_d0e76_a158> {
		/**
		 * Allocator for PostalAddressType_d0e76_a158Handler.
		 */
		private static class Allocator 
				extends HandlerProxy.Allocator<PostalAddressType_d0e76_a158> {			
			public XMLFragmentHandler<PostalAddressType_d0e76_a158> create(
					XMLReader reader
					, XMLFragmentHandler<?> handler
					, String elementName
					, DataSetter setter
					, boolean doProcess) {
				return new PostalAddressType_d0e76_a158Handler(
					reader
					, handler
					, elementName
					, PostalAddressType_d0e76_a158.getAllocator()
					, setter
					, doProcess);
			}
		}
		
		/**
		 * Constructor for Proxy.
		 *
		 * @param reader the XML reader
		 * @param parentH the parent XML handler class
		 * @param elementName the name of the XML Element (tag).
		 * @param pSetter the setter for the parent data
		 * @param doProcess indicates whether processing is active for this handler instance
		 */
		public Proxy(XMLReader reader, XMLFragmentHandler<?> parentH, String elementName,
				DataSetter pSetter, boolean doProcess) {
			super(reader, parentH, elementName, pSetter, new Allocator(), doProcess);
		}
	}



	/**
	 * Constructor for handler class.
	 *
	 * @param reader the XML reader
	 * @param parentH the parent XML handler class
	 * @param elementName the name of the XML Element (tag)
	 * @param allocator the allocator for the data type
	 * @param pSetter the setter for the parent data
	 * @param doProcess indicates whether processing is active for this handler instance
	 */
	public PostalAddressType_d0e76_a158Handler(
			XMLReader reader
			, XMLFragmentHandler<?> parentH
			, String elementName
			, PostalAddressType_d0e76_a158.Allocator allocator
			, DataSetter pSetter
			, boolean doProcess) {
		super(reader, parentH, elementName, allocator, pSetter, doProcess);
	}


	/**
	 * This method overrides the getData() of the super type, returning the more
	 * specific type.
	 * 
	 */
	@Override 
	public PostalAddressType_d0e76_a158 getData() {
		return (PostalAddressType_d0e76_a158)super.getData();
	}




	/**
	 * This method is called by the XML parser on an end of element event.
	 *
	 * If the localName == 'the name of the element that this handler handles' control is
	 * passed back to the parent handler (future events go there) and the data of
	 * the children of this handler is attached to the data associated to this handler.	
	 * 
	 * The data that is collected is connected to the parent data only if a parentDataSetter
	 * if found (getParentDataSetter()).
	 *
	 * If processing is enabled for a handler, the processor is called.
	 */
	@Override
	public void endElement(String uri, String localName, String name)
		throws SAXException {
		
		// collect data from children..
		if (localName.equals("city")) {
			getData().setCity(getValue());
			getContents().reset();
		} else if (localName.equals("country")) {
			getData().setCountry(getValue());
			getContents().reset();
		} else if (localName.equals("number")) {
			getData().setNumber(getValue());
			getContents().reset();
		} else if (localName.equals("numberExtension")) {
			getData().setNumberExtension(getValue());
			getContents().reset();
		} else if (localName.equals("postalCode")) {
			getData().setPostalCode(getValue());
			getContents().reset();
		} else if (localName.equals("property")) {
			getData().setProperty(getValue());
			getContents().reset();
		} else if (localName.equals("region")) {
			getData().setRegion(getValue());
			getContents().reset();
		} else if (localName.equals("streetname")) {
			getData().setStreetname(getValue());
			getContents().reset();
		} else if (localName.equals(getXMLElementName())) {
				
			// return control to parent handler..
			this.deactivate();
			
			// get content of this item..
			getData().setContent(this.getValue());
			
			// attach data to parent (if parent data setter is found)..
			DataSetter setter = getParentDataSetter();
			if (setter != null) {
				setter.set(getData());
			} 
			
			// process data if required..
			if (doProcess()) {
				process(XMLEvent.END);
			}
		}
		
	}	
}
