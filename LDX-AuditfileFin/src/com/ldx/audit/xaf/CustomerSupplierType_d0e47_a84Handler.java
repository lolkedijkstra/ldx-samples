package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

/* SAX 2.0 dependencies */
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/* Framework dependencies */
import com.ldx.xml.core.DataSetter;
import com.ldx.xml.core.ComplexDataType;
import com.ldx.xml.core.XMLEvent;
import com.ldx.xml.core.XMLFragmentHandler;


/**
 * CustomerSupplierType_d0e47_a84 handler class.
 *
 * @see CustomerSupplierType_d0e47_a84
 * 
 */
public class CustomerSupplierType_d0e47_a84Handler extends XMLFragmentHandler<CustomerSupplierType_d0e47_a84> {
	/**
	 * Proxy for CustomerSupplierType_d0e47_a84Handler.
	 */
	static class Proxy extends HandlerProxy<CustomerSupplierType_d0e47_a84> {
		/**
		 * Allocator for CustomerSupplierType_d0e47_a84Handler.
		 */
		private static class Allocator 
				extends HandlerProxy.Allocator<CustomerSupplierType_d0e47_a84> {			
			public XMLFragmentHandler<CustomerSupplierType_d0e47_a84> create(
					XMLReader reader
					, XMLFragmentHandler<?> handler
					, String elementName
					, DataSetter setter
					, boolean doProcess) {
				return new CustomerSupplierType_d0e47_a84Handler(
					reader
					, handler
					, elementName
					, CustomerSupplierType_d0e47_a84.getAllocator()
					, setter
					, doProcess);
			}
		}
		
		/**
		 * Constructor for Proxy.
		 *
		 * @param reader the XML reader
		 * @param parentH the parent XML handler class
		 * @param elementName the name of the XML Element (tag).
		 * @param pSetter the setter for the parent data
		 * @param doProcess indicates whether processing is active for this handler instance
		 */
		public Proxy(XMLReader reader, XMLFragmentHandler<?> parentH, String elementName,
				DataSetter pSetter, boolean doProcess) {
			super(reader, parentH, elementName, pSetter, new Allocator(), doProcess);
		}
	}
	
	/** Data setter class for bankAccount element. */
	private class BankAccountSetter implements DataSetter {
		/** data target. */
		private CustomerSupplierType_d0e47_a84Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public BankAccountSetter(CustomerSupplierType_d0e47_a84Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setBankAccount((BankAccountType_d0e87_a185) data);	
		}
	}	
	/** Data setter class for changeInfo element. */
	private class ChangeInfoSetter implements DataSetter {
		/** data target. */
		private CustomerSupplierType_d0e47_a84Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public ChangeInfoSetter(CustomerSupplierType_d0e47_a84Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setChangeInfo((ChangeInfoType_d0e92_a193) data);	
		}
	}	
	/** Data setter class for customerSupplierHistory element. */
	private class CustomerSupplierHistorySetter implements DataSetter {
		/** data target. */
		private CustomerSupplierType_d0e47_a84Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public CustomerSupplierHistorySetter(CustomerSupplierType_d0e47_a84Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setCustomerSupplierHistory((CustomerSupplierHistoryType_d0e98_a204) data);	
		}
	}	
	/** Data setter class for postalAddress element. */
	private class PostalAddressSetter implements DataSetter {
		/** data target. */
		private CustomerSupplierType_d0e47_a84Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public PostalAddressSetter(CustomerSupplierType_d0e47_a84Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setPostalAddress((PostalAddressType_d0e76_a158) data);	
		}
	}	
	/** Data setter class for streetAddress element. */
	private class StreetAddressSetter implements DataSetter {
		/** data target. */
		private CustomerSupplierType_d0e47_a84Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public StreetAddressSetter(CustomerSupplierType_d0e47_a84Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setStreetAddress((StreetAddressType_d0e65_a131) data);	
		}
	}


	/**
	 * Constructor for handler class.
	 *
	 * @param reader the XML reader
	 * @param parentH the parent XML handler class
	 * @param elementName the name of the XML Element (tag)
	 * @param allocator the allocator for the data type
	 * @param pSetter the setter for the parent data
	 * @param doProcess indicates whether processing is active for this handler instance
	 */
	public CustomerSupplierType_d0e47_a84Handler(
			XMLReader reader
			, XMLFragmentHandler<?> parentH
			, String elementName
			, CustomerSupplierType_d0e47_a84.Allocator allocator
			, DataSetter pSetter
			, boolean doProcess) {
		super(reader, parentH, elementName, allocator, pSetter, doProcess);

		// code for linking children..
		registerHandler(
			new BankAccountType_d0e87_a185Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of bankAccountType_d0e87_a185Handler
				, "bankAccount" // XML element name
				, doLink("bankAccount") // linking to parent
					? new BankAccountSetter(this) // ON
					: null // OFF
				, doProcess("bankAccount")) // processing active or not
				);
  
		registerHandler(
			new ChangeInfoType_d0e92_a193Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of changeInfoType_d0e92_a193Handler
				, "changeInfo" // XML element name
				, doLink("changeInfo") // linking to parent
					? new ChangeInfoSetter(this) // ON
					: null // OFF
				, doProcess("changeInfo")) // processing active or not
				);
  
		registerHandler(
			new CustomerSupplierHistoryType_d0e98_a204Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of customerSupplierHistoryType_d0e98_a204Handler
				, "customerSupplierHistory" // XML element name
				, doLink("customerSupplierHistory") // linking to parent
					? new CustomerSupplierHistorySetter(this) // ON
					: null // OFF
				, doProcess("customerSupplierHistory")) // processing active or not
				);
  
		registerHandler(
			new PostalAddressType_d0e76_a158Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of postalAddressType_d0e76_a158Handler
				, "postalAddress" // XML element name
				, doLink("postalAddress") // linking to parent
					? new PostalAddressSetter(this) // ON
					: null // OFF
				, doProcess("postalAddress")) // processing active or not
				);
  
		registerHandler(
			new StreetAddressType_d0e65_a131Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of streetAddressType_d0e65_a131Handler
				, "streetAddress" // XML element name
				, doLink("streetAddress") // linking to parent
					? new StreetAddressSetter(this) // ON
					: null // OFF
				, doProcess("streetAddress")) // processing active or not
				);
  	}


	/**
	 * This method overrides the getData() of the super type, returning the more
	 * specific type.
	 * 
	 */
	@Override 
	public CustomerSupplierType_d0e47_a84 getData() {
		return (CustomerSupplierType_d0e47_a84)super.getData();
	}




	/**
	 * This method is called by the XML parser on an end of element event.
	 *
	 * If the localName == 'the name of the element that this handler handles' control is
	 * passed back to the parent handler (future events go there) and the data of
	 * the children of this handler is attached to the data associated to this handler.	
	 * 
	 * The data that is collected is connected to the parent data only if a parentDataSetter
	 * if found (getParentDataSetter()).
	 *
	 * If processing is enabled for a handler, the processor is called.
	 */
	@Override
	public void endElement(String uri, String localName, String name)
		throws SAXException {
		
		// collect data from children..
		if (localName.equals("commerceNr")) {
			getData().setCommerceNr(getValue());
			getContents().reset();
		} else if (localName.equals("contact")) {
			getData().setContact(getValue());
			getContents().reset();
		} else if (localName.equals("custCreditLimit")) {
			getData().setCustCreditLimit(getValue());
			getContents().reset();
		} else if (localName.equals("custSupGrpID")) {
			getData().setCustSupGrpID(getValue());
			getContents().reset();
		} else if (localName.equals("custSupID")) {
			getData().setCustSupID(getValue());
			getContents().reset();
		} else if (localName.equals("custSupName")) {
			getData().setCustSupName(getValue());
			getContents().reset();
		} else if (localName.equals("custSupTp")) {
			getData().setCustSupTp(getValue());
			getContents().reset();
		} else if (localName.equals("eMail")) {
			getData().setEMail(getValue());
			getContents().reset();
		} else if (localName.equals("fax")) {
			getData().setFax(getValue());
			getContents().reset();
		} else if (localName.equals("relationshipID")) {
			getData().setRelationshipID(getValue());
			getContents().reset();
		} else if (localName.equals("supplierLimit")) {
			getData().setSupplierLimit(getValue());
			getContents().reset();
		} else if (localName.equals("taxRegIdent")) {
			getData().setTaxRegIdent(getValue());
			getContents().reset();
		} else if (localName.equals("taxRegistrationCountry")) {
			getData().setTaxRegistrationCountry(getValue());
			getContents().reset();
		} else if (localName.equals("telephone")) {
			getData().setTelephone(getValue());
			getContents().reset();
		} else if (localName.equals("website")) {
			getData().setWebsite(getValue());
			getContents().reset();
		} else if (localName.equals(getXMLElementName())) {
				
			// return control to parent handler..
			this.deactivate();
			
			// get content of this item..
			getData().setContent(this.getValue());
			
			// attach data to parent (if parent data setter is found)..
			DataSetter setter = getParentDataSetter();
			if (setter != null) {
				setter.set(getData());
			} 
			
			// process data if required..
			if (doProcess()) {
				process(XMLEvent.END);
			}
		}
		
	}	
}
