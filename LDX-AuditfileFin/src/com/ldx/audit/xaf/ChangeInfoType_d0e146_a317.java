package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * ChangeInfoType_d0e146_a317 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class ChangeInfoType_d0e146_a317 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for ChangeInfoType_d0e146_a317.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public ChangeInfoType_d0e146_a317(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type ChangeInfoType_d0e146_a317.
	 */
	static class Allocator implements TypeAllocator<ChangeInfoType_d0e146_a317> {
		/**
		 * method for getting a new instance of type ChangeInfoType_d0e146_a317.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public ChangeInfoType_d0e146_a317 newInstance(String elementName, ComplexDataType parent) {
			return new ChangeInfoType_d0e146_a317(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for userID element. 
	 *  @serial
	 */	
	private String m_userID = null;
	
	/** element item for changeDateTime element. 
	 *  @serial
	 */	
	private String m_changeDateTime = null;
	
	/** element item for changeDescription element. 
	 *  @serial
	 */	
	private String m_changeDescription = null;
	
	/**
	 * Get the embedded UserID element.
	 * @return the item.
	 */
	public String getUserID() {
		return m_userID;
	}
		
	/**
	 * This method sets (overwrites) the element UserID.
	 * @param data the item that needs to be added.
	 */
	void setUserID(String data) {
		m_userID = data;
	}
		
	/**
	 * Get the embedded ChangeDateTime element.
	 * @return the item.
	 */
	public String getChangeDateTime() {
		return m_changeDateTime;
	}
		
	/**
	 * This method sets (overwrites) the element ChangeDateTime.
	 * @param data the item that needs to be added.
	 */
	void setChangeDateTime(String data) {
		m_changeDateTime = data;
	}
		
	/**
	 * Get the embedded ChangeDescription element.
	 * @return the item.
	 */
	public String getChangeDescription() {
		return m_changeDescription;
	}
		
	/**
	 * This method sets (overwrites) the element ChangeDescription.
	 * @param data the item that needs to be added.
	 */
	void setChangeDescription(String data) {
		m_changeDescription = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_userID, ((ChangeInfoType_d0e146_a317)that).m_userID))
			return false;
		
		if (!Compare.equals(m_changeDateTime, ((ChangeInfoType_d0e146_a317)that).m_changeDateTime))
			return false;
		
		if (!Compare.equals(m_changeDescription, ((ChangeInfoType_d0e146_a317)that).m_changeDescription))
			return false;
		
		return true;
	}	

  
  
}
