package com.ldx.audit.xaf.processor;


import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

import com.ldx.audit.xaf.AuditfileDocument;
import com.ldx.audit.xaf.repo.AuditfileDocumentRepo;

/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/
	
import com.ldx.xml.core.ComplexDataType;
import com.ldx.xml.core.MessageProcessor;
import com.ldx.xml.core.ProcessorException;
import com.ldx.xml.core.XMLEvent;

/**
 *	This class processes events that are sent by the LDX+ framework.
 */
public class AuditfileProcessor implements MessageProcessor {
	
	// --- spring data framework: begin
	final String path = new ClassPathResource("spring-config.xml").getPath();
	ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(path);
	
	// --- initialize repos
	AuditfileDocumentRepo afRepo = context.getBean(AuditfileDocumentRepo.class);
	
	public void close() {
		context.close();
	}
	
	protected void finalize() throws Throwable {
	     try {
	    	 if (context.isActive())
	    		 context.close();
	     } finally {
	         super.finalize();
	     }
	}
	// --- spring data framework: end

	private void info(String msg) {
		System.out.println(msg);
	}
	

	@Override
	public void process(XMLEvent evt, ComplexDataType data)
			throws ProcessorException {

		if (evt == XMLEvent.START) {
			if (data instanceof AuditfileDocument) {
				info("auditfile/@process=true : START");
			}
		}
			
		if (evt == XMLEvent.END) {
			if (data instanceof AuditfileDocument) {
				info("auditfile/@process=true : END");
				process((AuditfileDocument)data);
			}
		}
	}

	private void process(AuditfileDocument data) {
		afRepo.save(data);
	}
}
