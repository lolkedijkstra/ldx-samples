package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

import java.util.List;
import java.util.ArrayList;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * LedgerAccountType_d0e155_a330 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class LedgerAccountType_d0e155_a330 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for LedgerAccountType_d0e155_a330.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public LedgerAccountType_d0e155_a330(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type LedgerAccountType_d0e155_a330.
	 */
	static class Allocator implements TypeAllocator<LedgerAccountType_d0e155_a330> {
		/**
		 * method for getting a new instance of type LedgerAccountType_d0e155_a330.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public LedgerAccountType_d0e155_a330 newInstance(String elementName, ComplexDataType parent) {
			return new LedgerAccountType_d0e155_a330(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for accID element. 
	 *  @serial
	 */	
	private String m_accID = null;
	
	/** element item for accDesc element. 
	 *  @serial
	 */	
	private String m_accDesc = null;
	
	/** element item for accTp element. 
	 *  @serial
	 */	
	private String m_accTp = null;
	
	/** element item for leadCode element. 
	 *  @serial
	 */	
	private String m_leadCode = null;
	
	/** element item for leadDescription element. 
	 *  @serial
	 */	
	private String m_leadDescription = null;
	
	/** element item for leadReference element. 
	 *  @serial
	 */	
	private String m_leadReference = null;
	
	/** element item for leadCrossRef element. 
	 *  @serial
	 */	
	private String m_leadCrossRef = null;
	
	/** list of taxonomy element. 
	 *  @serial
	 */	
	private List<TaxonomyType_d0e165_a350> m_taxonomyList = new ArrayList<TaxonomyType_d0e165_a350>();
	
	/** element item for changeInfo element. 
	 *  @serial
	 */	
	private ChangeInfoType_d0e178_a367 m_changeInfo = null;
	
	/** element item for glAccountHistory element. 
	 *  @serial
	 */	
	private GlAccountHistoryType_d0e184_a378 m_glAccountHistory = null;
	
	/**
	 * Get the embedded AccID element.
	 * @return the item.
	 */
	public String getAccID() {
		return m_accID;
	}
		
	/**
	 * This method sets (overwrites) the element AccID.
	 * @param data the item that needs to be added.
	 */
	void setAccID(String data) {
		m_accID = data;
	}
		
	/**
	 * Get the embedded AccDesc element.
	 * @return the item.
	 */
	public String getAccDesc() {
		return m_accDesc;
	}
		
	/**
	 * This method sets (overwrites) the element AccDesc.
	 * @param data the item that needs to be added.
	 */
	void setAccDesc(String data) {
		m_accDesc = data;
	}
		
	/**
	 * Get the embedded AccTp element.
	 * @return the item.
	 */
	public String getAccTp() {
		return m_accTp;
	}
		
	/**
	 * This method sets (overwrites) the element AccTp.
	 * @param data the item that needs to be added.
	 */
	void setAccTp(String data) {
		m_accTp = data;
	}
		
	/**
	 * Get the embedded LeadCode element.
	 * @return the item.
	 */
	public String getLeadCode() {
		return m_leadCode;
	}
		
	/**
	 * This method sets (overwrites) the element LeadCode.
	 * @param data the item that needs to be added.
	 */
	void setLeadCode(String data) {
		m_leadCode = data;
	}
		
	/**
	 * Get the embedded LeadDescription element.
	 * @return the item.
	 */
	public String getLeadDescription() {
		return m_leadDescription;
	}
		
	/**
	 * This method sets (overwrites) the element LeadDescription.
	 * @param data the item that needs to be added.
	 */
	void setLeadDescription(String data) {
		m_leadDescription = data;
	}
		
	/**
	 * Get the embedded LeadReference element.
	 * @return the item.
	 */
	public String getLeadReference() {
		return m_leadReference;
	}
		
	/**
	 * This method sets (overwrites) the element LeadReference.
	 * @param data the item that needs to be added.
	 */
	void setLeadReference(String data) {
		m_leadReference = data;
	}
		
	/**
	 * Get the embedded LeadCrossRef element.
	 * @return the item.
	 */
	public String getLeadCrossRef() {
		return m_leadCrossRef;
	}
		
	/**
	 * This method sets (overwrites) the element LeadCrossRef.
	 * @param data the item that needs to be added.
	 */
	void setLeadCrossRef(String data) {
		m_leadCrossRef = data;
	}
		
	/**
	 * Get the embedded list of Taxonomy elements.
	 * @return list of items.
	 */
	public List<TaxonomyType_d0e165_a350> getTaxonomys() {
		return m_taxonomyList;
	}
		
	/**
	 * This method adds data to the list of Taxonomy.
	 * @param data the item that needs to be added.
	 */
	void setTaxonomy(TaxonomyType_d0e165_a350 data) {
		m_taxonomyList.add(data);
	}
		
	/**
	 * Get the embedded ChangeInfo element.
	 * @return the item.
	 */
	public ChangeInfoType_d0e178_a367 getChangeInfo() {
		return m_changeInfo;
	}
		
	/**
	 * This method sets (overwrites) the element ChangeInfo.
	 * @param data the item that needs to be added.
	 */
	void setChangeInfo(ChangeInfoType_d0e178_a367 data) {
		m_changeInfo = data;
	}
		
	/**
	 * Get the embedded GlAccountHistory element.
	 * @return the item.
	 */
	public GlAccountHistoryType_d0e184_a378 getGlAccountHistory() {
		return m_glAccountHistory;
	}
		
	/**
	 * This method sets (overwrites) the element GlAccountHistory.
	 * @param data the item that needs to be added.
	 */
	void setGlAccountHistory(GlAccountHistoryType_d0e184_a378 data) {
		m_glAccountHistory = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_accID, ((LedgerAccountType_d0e155_a330)that).m_accID))
			return false;
		
		if (!Compare.equals(m_accDesc, ((LedgerAccountType_d0e155_a330)that).m_accDesc))
			return false;
		
		if (!Compare.equals(m_accTp, ((LedgerAccountType_d0e155_a330)that).m_accTp))
			return false;
		
		if (!Compare.equals(m_leadCode, ((LedgerAccountType_d0e155_a330)that).m_leadCode))
			return false;
		
		if (!Compare.equals(m_leadDescription, ((LedgerAccountType_d0e155_a330)that).m_leadDescription))
			return false;
		
		if (!Compare.equals(m_leadReference, ((LedgerAccountType_d0e155_a330)that).m_leadReference))
			return false;
		
		if (!Compare.equals(m_leadCrossRef, ((LedgerAccountType_d0e155_a330)that).m_leadCrossRef))
			return false;
		
		if (!Compare.equals(m_taxonomyList, ((LedgerAccountType_d0e155_a330)that).m_taxonomyList))
			return false;
		
		if (!Compare.equals(m_changeInfo, ((LedgerAccountType_d0e155_a330)that).m_changeInfo))
			return false;
		
		if (!Compare.equals(m_glAccountHistory, ((LedgerAccountType_d0e155_a330)that).m_glAccountHistory))
			return false;
		
		return true;
	}	

  
  
}
