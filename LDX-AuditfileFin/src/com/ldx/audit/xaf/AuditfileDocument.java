package com.ldx.audit.xaf;


import org.springframework.data.mongodb.core.mapping.Document;

/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * AuditfileDocument data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
@Document(collection="AuditfileFin")
public class AuditfileDocument extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for AuditfileDocument.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public AuditfileDocument(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type AuditfileDocument.
	 */
	static class Allocator implements TypeAllocator<AuditfileDocument> {
		/**
		 * method for getting a new instance of type AuditfileDocument.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public AuditfileDocument newInstance(String elementName, ComplexDataType parent) {
			return new AuditfileDocument(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for header element. 
	 *  @serial
	 */	
	private HeaderType_d0e5_a1 m_header = null;
	
	/** element item for company element. 
	 *  @serial
	 */	
	private CompanyType_d0e15_a18 m_company = null;
	
	/**
	 * Get the embedded Header element.
	 * @return the item.
	 */
	public HeaderType_d0e5_a1 getHeader() {
		return m_header;
	}
		
	/**
	 * This method sets (overwrites) the element Header.
	 * @param data the item that needs to be added.
	 */
	void setHeader(HeaderType_d0e5_a1 data) {
		m_header = data;
	}
		
	/**
	 * Get the embedded Company element.
	 * @return the item.
	 */
	public CompanyType_d0e15_a18 getCompany() {
		return m_company;
	}
		
	/**
	 * This method sets (overwrites) the element Company.
	 * @param data the item that needs to be added.
	 */
	void setCompany(CompanyType_d0e15_a18 data) {
		m_company = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_header, ((AuditfileDocument)that).m_header))
			return false;
		
		if (!Compare.equals(m_company, ((AuditfileDocument)that).m_company))
			return false;
		
		return true;
	}	

  
  
}
