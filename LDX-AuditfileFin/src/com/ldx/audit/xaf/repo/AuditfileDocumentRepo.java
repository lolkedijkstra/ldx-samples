package com.ldx.audit.xaf.repo;

import org.springframework.data.repository.CrudRepository;

import com.ldx.audit.xaf.AuditfileDocument;

public interface AuditfileDocumentRepo extends CrudRepository<AuditfileDocument, Long> {}
