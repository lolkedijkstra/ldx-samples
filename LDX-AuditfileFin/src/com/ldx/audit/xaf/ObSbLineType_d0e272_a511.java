package com.ldx.audit.xaf;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: XAF 
  Generation date: Fri Apr 29 11:23:12 CEST 2016 

******************************************************************************/

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * ObSbLineType_d0e272_a511 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class ObSbLineType_d0e272_a511 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for ObSbLineType_d0e272_a511.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public ObSbLineType_d0e272_a511(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type ObSbLineType_d0e272_a511.
	 */
	static class Allocator implements TypeAllocator<ObSbLineType_d0e272_a511> {
		/**
		 * method for getting a new instance of type ObSbLineType_d0e272_a511.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public ObSbLineType_d0e272_a511 newInstance(String elementName, ComplexDataType parent) {
			return new ObSbLineType_d0e272_a511(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for nr element. 
	 *  @serial
	 */	
	private String m_nr = null;
	
	/** element item for obLineNr element. 
	 *  @serial
	 */	
	private String m_obLineNr = null;
	
	/** element item for desc element. 
	 *  @serial
	 */	
	private String m_desc = null;
	
	/** element item for amnt element. 
	 *  @serial
	 */	
	private String m_amnt = null;
	
	/** element item for amntTp element. 
	 *  @serial
	 */	
	private String m_amntTp = null;
	
	/** element item for docRef element. 
	 *  @serial
	 */	
	private String m_docRef = null;
	
	/** element item for recRef element. 
	 *  @serial
	 */	
	private String m_recRef = null;
	
	/** element item for matchKeyID element. 
	 *  @serial
	 */	
	private String m_matchKeyID = null;
	
	/** element item for custSupID element. 
	 *  @serial
	 */	
	private String m_custSupID = null;
	
	/** element item for invRef element. 
	 *  @serial
	 */	
	private String m_invRef = null;
	
	/** element item for invPurSalTp element. 
	 *  @serial
	 */	
	private String m_invPurSalTp = null;
	
	/** element item for invTp element. 
	 *  @serial
	 */	
	private String m_invTp = null;
	
	/** element item for invDt element. 
	 *  @serial
	 */	
	private String m_invDt = null;
	
	/** element item for invDueDt element. 
	 *  @serial
	 */	
	private String m_invDueDt = null;
	
	/** element item for mutTp element. 
	 *  @serial
	 */	
	private String m_mutTp = null;
	
	/** element item for costID element. 
	 *  @serial
	 */	
	private String m_costID = null;
	
	/** element item for prodID element. 
	 *  @serial
	 */	
	private String m_prodID = null;
	
	/** element item for projID element. 
	 *  @serial
	 */	
	private String m_projID = null;
	
	/** element item for artGrpID element. 
	 *  @serial
	 */	
	private String m_artGrpID = null;
	
	/** element item for qntityID element. 
	 *  @serial
	 */	
	private String m_qntityID = null;
	
	/** element item for qntity element. 
	 *  @serial
	 */	
	private String m_qntity = null;
	
	/**
	 * Get the embedded Nr element.
	 * @return the item.
	 */
	public String getNr() {
		return m_nr;
	}
		
	/**
	 * This method sets (overwrites) the element Nr.
	 * @param data the item that needs to be added.
	 */
	void setNr(String data) {
		m_nr = data;
	}
		
	/**
	 * Get the embedded ObLineNr element.
	 * @return the item.
	 */
	public String getObLineNr() {
		return m_obLineNr;
	}
		
	/**
	 * This method sets (overwrites) the element ObLineNr.
	 * @param data the item that needs to be added.
	 */
	void setObLineNr(String data) {
		m_obLineNr = data;
	}
		
	/**
	 * Get the embedded Desc element.
	 * @return the item.
	 */
	public String getDesc() {
		return m_desc;
	}
		
	/**
	 * This method sets (overwrites) the element Desc.
	 * @param data the item that needs to be added.
	 */
	void setDesc(String data) {
		m_desc = data;
	}
		
	/**
	 * Get the embedded Amnt element.
	 * @return the item.
	 */
	public String getAmnt() {
		return m_amnt;
	}
		
	/**
	 * This method sets (overwrites) the element Amnt.
	 * @param data the item that needs to be added.
	 */
	void setAmnt(String data) {
		m_amnt = data;
	}
		
	/**
	 * Get the embedded AmntTp element.
	 * @return the item.
	 */
	public String getAmntTp() {
		return m_amntTp;
	}
		
	/**
	 * This method sets (overwrites) the element AmntTp.
	 * @param data the item that needs to be added.
	 */
	void setAmntTp(String data) {
		m_amntTp = data;
	}
		
	/**
	 * Get the embedded DocRef element.
	 * @return the item.
	 */
	public String getDocRef() {
		return m_docRef;
	}
		
	/**
	 * This method sets (overwrites) the element DocRef.
	 * @param data the item that needs to be added.
	 */
	void setDocRef(String data) {
		m_docRef = data;
	}
		
	/**
	 * Get the embedded RecRef element.
	 * @return the item.
	 */
	public String getRecRef() {
		return m_recRef;
	}
		
	/**
	 * This method sets (overwrites) the element RecRef.
	 * @param data the item that needs to be added.
	 */
	void setRecRef(String data) {
		m_recRef = data;
	}
		
	/**
	 * Get the embedded MatchKeyID element.
	 * @return the item.
	 */
	public String getMatchKeyID() {
		return m_matchKeyID;
	}
		
	/**
	 * This method sets (overwrites) the element MatchKeyID.
	 * @param data the item that needs to be added.
	 */
	void setMatchKeyID(String data) {
		m_matchKeyID = data;
	}
		
	/**
	 * Get the embedded CustSupID element.
	 * @return the item.
	 */
	public String getCustSupID() {
		return m_custSupID;
	}
		
	/**
	 * This method sets (overwrites) the element CustSupID.
	 * @param data the item that needs to be added.
	 */
	void setCustSupID(String data) {
		m_custSupID = data;
	}
		
	/**
	 * Get the embedded InvRef element.
	 * @return the item.
	 */
	public String getInvRef() {
		return m_invRef;
	}
		
	/**
	 * This method sets (overwrites) the element InvRef.
	 * @param data the item that needs to be added.
	 */
	void setInvRef(String data) {
		m_invRef = data;
	}
		
	/**
	 * Get the embedded InvPurSalTp element.
	 * @return the item.
	 */
	public String getInvPurSalTp() {
		return m_invPurSalTp;
	}
		
	/**
	 * This method sets (overwrites) the element InvPurSalTp.
	 * @param data the item that needs to be added.
	 */
	void setInvPurSalTp(String data) {
		m_invPurSalTp = data;
	}
		
	/**
	 * Get the embedded InvTp element.
	 * @return the item.
	 */
	public String getInvTp() {
		return m_invTp;
	}
		
	/**
	 * This method sets (overwrites) the element InvTp.
	 * @param data the item that needs to be added.
	 */
	void setInvTp(String data) {
		m_invTp = data;
	}
		
	/**
	 * Get the embedded InvDt element.
	 * @return the item.
	 */
	public String getInvDt() {
		return m_invDt;
	}
		
	/**
	 * This method sets (overwrites) the element InvDt.
	 * @param data the item that needs to be added.
	 */
	void setInvDt(String data) {
		m_invDt = data;
	}
		
	/**
	 * Get the embedded InvDueDt element.
	 * @return the item.
	 */
	public String getInvDueDt() {
		return m_invDueDt;
	}
		
	/**
	 * This method sets (overwrites) the element InvDueDt.
	 * @param data the item that needs to be added.
	 */
	void setInvDueDt(String data) {
		m_invDueDt = data;
	}
		
	/**
	 * Get the embedded MutTp element.
	 * @return the item.
	 */
	public String getMutTp() {
		return m_mutTp;
	}
		
	/**
	 * This method sets (overwrites) the element MutTp.
	 * @param data the item that needs to be added.
	 */
	void setMutTp(String data) {
		m_mutTp = data;
	}
		
	/**
	 * Get the embedded CostID element.
	 * @return the item.
	 */
	public String getCostID() {
		return m_costID;
	}
		
	/**
	 * This method sets (overwrites) the element CostID.
	 * @param data the item that needs to be added.
	 */
	void setCostID(String data) {
		m_costID = data;
	}
		
	/**
	 * Get the embedded ProdID element.
	 * @return the item.
	 */
	public String getProdID() {
		return m_prodID;
	}
		
	/**
	 * This method sets (overwrites) the element ProdID.
	 * @param data the item that needs to be added.
	 */
	void setProdID(String data) {
		m_prodID = data;
	}
		
	/**
	 * Get the embedded ProjID element.
	 * @return the item.
	 */
	public String getProjID() {
		return m_projID;
	}
		
	/**
	 * This method sets (overwrites) the element ProjID.
	 * @param data the item that needs to be added.
	 */
	void setProjID(String data) {
		m_projID = data;
	}
		
	/**
	 * Get the embedded ArtGrpID element.
	 * @return the item.
	 */
	public String getArtGrpID() {
		return m_artGrpID;
	}
		
	/**
	 * This method sets (overwrites) the element ArtGrpID.
	 * @param data the item that needs to be added.
	 */
	void setArtGrpID(String data) {
		m_artGrpID = data;
	}
		
	/**
	 * Get the embedded QntityID element.
	 * @return the item.
	 */
	public String getQntityID() {
		return m_qntityID;
	}
		
	/**
	 * This method sets (overwrites) the element QntityID.
	 * @param data the item that needs to be added.
	 */
	void setQntityID(String data) {
		m_qntityID = data;
	}
		
	/**
	 * Get the embedded Qntity element.
	 * @return the item.
	 */
	public String getQntity() {
		return m_qntity;
	}
		
	/**
	 * This method sets (overwrites) the element Qntity.
	 * @param data the item that needs to be added.
	 */
	void setQntity(String data) {
		m_qntity = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_nr, ((ObSbLineType_d0e272_a511)that).m_nr))
			return false;
		
		if (!Compare.equals(m_obLineNr, ((ObSbLineType_d0e272_a511)that).m_obLineNr))
			return false;
		
		if (!Compare.equals(m_desc, ((ObSbLineType_d0e272_a511)that).m_desc))
			return false;
		
		if (!Compare.equals(m_amnt, ((ObSbLineType_d0e272_a511)that).m_amnt))
			return false;
		
		if (!Compare.equals(m_amntTp, ((ObSbLineType_d0e272_a511)that).m_amntTp))
			return false;
		
		if (!Compare.equals(m_docRef, ((ObSbLineType_d0e272_a511)that).m_docRef))
			return false;
		
		if (!Compare.equals(m_recRef, ((ObSbLineType_d0e272_a511)that).m_recRef))
			return false;
		
		if (!Compare.equals(m_matchKeyID, ((ObSbLineType_d0e272_a511)that).m_matchKeyID))
			return false;
		
		if (!Compare.equals(m_custSupID, ((ObSbLineType_d0e272_a511)that).m_custSupID))
			return false;
		
		if (!Compare.equals(m_invRef, ((ObSbLineType_d0e272_a511)that).m_invRef))
			return false;
		
		if (!Compare.equals(m_invPurSalTp, ((ObSbLineType_d0e272_a511)that).m_invPurSalTp))
			return false;
		
		if (!Compare.equals(m_invTp, ((ObSbLineType_d0e272_a511)that).m_invTp))
			return false;
		
		if (!Compare.equals(m_invDt, ((ObSbLineType_d0e272_a511)that).m_invDt))
			return false;
		
		if (!Compare.equals(m_invDueDt, ((ObSbLineType_d0e272_a511)that).m_invDueDt))
			return false;
		
		if (!Compare.equals(m_mutTp, ((ObSbLineType_d0e272_a511)that).m_mutTp))
			return false;
		
		if (!Compare.equals(m_costID, ((ObSbLineType_d0e272_a511)that).m_costID))
			return false;
		
		if (!Compare.equals(m_prodID, ((ObSbLineType_d0e272_a511)that).m_prodID))
			return false;
		
		if (!Compare.equals(m_projID, ((ObSbLineType_d0e272_a511)that).m_projID))
			return false;
		
		if (!Compare.equals(m_artGrpID, ((ObSbLineType_d0e272_a511)that).m_artGrpID))
			return false;
		
		if (!Compare.equals(m_qntityID, ((ObSbLineType_d0e272_a511)that).m_qntityID))
			return false;
		
		if (!Compare.equals(m_qntity, ((ObSbLineType_d0e272_a511)that).m_qntity))
			return false;
		
		return true;
	}	

  
  
}
