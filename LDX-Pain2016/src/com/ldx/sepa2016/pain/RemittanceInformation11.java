package com.ldx.sepa2016.pain;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.com
		email: dijkstra@xml2java.com
		support: support@xml2java.com
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.3
  Generated code is compatible with ldx-framework v. 2.3
  License: Dijkstra ICT Consulting 
  Module: PAIN 
  Generation date: Mon May 23 15:08:18 CEST 2016 

******************************************************************************/

import com.ldx.util.StringList;

import java.util.List;
import java.util.ArrayList;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * RemittanceInformation11 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class RemittanceInformation11 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for RemittanceInformation11.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public RemittanceInformation11(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type RemittanceInformation11.
	 */
	static class Allocator implements TypeAllocator<RemittanceInformation11> {
		/**
		 * method for getting a new instance of type RemittanceInformation11.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public RemittanceInformation11 newInstance(String elementName, ComplexDataType parent) {
			return new RemittanceInformation11(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** list of Ustrd element. 
	 *  @serial
	 */	
	private StringList m_ustrdList = new StringList("Ustrd");
	
	/** list of Strd element. 
	 *  @serial
	 */	
	private List<StructuredRemittanceInformation13> m_strdList = new ArrayList<StructuredRemittanceInformation13>();
		
	/**
	 * Get the embedded list of Ustrd elements.
	 * @return list of items.
	 */
	public StringList getUstrds() {
		return m_ustrdList;
	}
		
	/**
	 * This method adds data to the list of Ustrd.
	 * @param data the item that needs to be added.
	 */
	void setUstrd(String data) {
		m_ustrdList.add(data);
	}
		
	/**
	 * Get the embedded list of Strd elements.
	 * @return list of items.
	 */
	public List<StructuredRemittanceInformation13> getStrds() {
		return m_strdList;
	}
		
	/**
	 * This method adds data to the list of Strd.
	 * @param data the item that needs to be added.
	 */
	void setStrd(StructuredRemittanceInformation13 data) {
		m_strdList.add(data);
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_ustrdList, ((RemittanceInformation11)that).m_ustrdList))
			return false;
		
		if (!Compare.equals(m_strdList, ((RemittanceInformation11)that).m_strdList))
			return false;
		
		return true;
	}	

  
  
}
