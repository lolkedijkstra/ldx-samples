package com.ldx.sepa2016.pain.application;

/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.com
		email: dijkstra@xml2java.com
		support: support@xml2java.com
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.3
  Generated code is compatible with ldx-framework v. 2.3
  License: Dijkstra ICT Consulting 
  Module: PAIN 
  Generation date: Mon May 23 15:08:18 CEST 2016 

******************************************************************************/


//----------------------- 		IO		-----------------------//
import java.io.FileInputStream;
import java.io.IOException;
//-----------------------    LOGGING	-----------------------//
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//-----------------------    	SAX		-----------------------//
import org.xml.sax.SAXException;
//-----------------------    	LDX		-----------------------//
import com.ldx.xml.core.ParserConfiguration;
import com.ldx.xml.core.ProcessorException;

import com.ldx.xml.parser.ParserRunnable;
import com.ldx.util.DefaultThreadManager;
	
import com.ldx.sepa2016.pain.runnable.CustomerDirectDebitInitiationV06Runnable;

import com.ldx.sepa2016.pain.processor.CustomerDirectDebitInitiationV06Processor;

/**
 * An example implementation of a parser application.
 * You will need to adapt this to meet your specific requirements.
 * This example demonstrates:
 * - the glue code that connects reader and processor
 * - how you can customize error handling
 *
 * The application uses arguments passed on the command line, however
 * you can connect any class derived from java.io.InputStream.
 */
public class CustomerDirectDebitInitiationV06Application {
	
	/** command line parameters */
	private final static String usage = "parameters:\n\t(1): xml input \n\t(2): config \n\t(3): schema";

	/** print command line usage */
	private static void usage() {
		System.out.println(usage);
		System.exit(0);
	}

	/**
	 * Entry point of the application.
	 * @param args args[0] = XML, args[1] = runtime configuration, args[2] = XML Schema
	 */
	public static void main(String[] args) {
		if (args.length < 2) {
			usage();
		}

		// get program arguments
		final String xml = args[0];
		final String config = args[1];
		final String schema = args.length >= 3 ? args[2] : null;
		
		// initialize logger component
		Logger log = LoggerFactory.getLogger(CustomerDirectDebitInitiationV06Application.class);
		
		try {
			log.info("Starting application.");
			
			// load runtime configuration
			log.info("Loading runtime configuration");
			ParserConfiguration configuration = new ParserConfiguration(config);
	
			// create thread manager
			DefaultThreadManager manager = new DefaultThreadManager();

			// create a task object
			ParserRunnable task = new CustomerDirectDebitInitiationV06Runnable(configuration);

			// validate (optional)
			if (schema != null) {
				log.info("Validating {} against {}", xml, schema);
				task.validateXML(new FileInputStream(xml), new FileInputStream(schema));
			}

			// prepare the XML parser
			task.prepareStart(new FileInputStream(xml), new CustomerDirectDebitInitiationV06Processor());
				
			// add the task to list of tasks
			manager.addTask("CustomerDirectDebitInitiationV06Runnable", task);

			// when all tasks have been added start threads	
			manager.startThreads();
			
			log.info("Processing complete.");
			
		} catch (ProcessorException e) {
			log.error("Execution aborted due to PROCESSING error (\n\tmessage: {}\n\tcause: {})"
						, e.getMessage()
						, e.getCause());
		} catch (SAXException | IOException e) {
			log.error(e.getMessage());
		} 
	}	 
}
