package com.ldx.sepa2016.pain.processor;

import com.ldx.mongo.sequence.SequenceService;
import com.ldx.sepa2016.pain.*;
import com.ldx.sepa2016.repo.CreditTransferTransaction26Repo;
import com.ldx.sepa2016.repo.CustomerCreditTransferInitiationV07Repo;
import com.ldx.sepa2016.repo.PainRepo;
import com.ldx.sepa2016.repo.PaymentInstructionRepo;

/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.com
		email: dijkstra@xml2java.com
		support: support@xml2java.com
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.3
  Generated code is compatible with ldx-framework v. 2.3
  License: Dijkstra ICT Consulting 
  Module: PAIN 
  Generation date: Mon May 23 15:08:16 CEST 2016 

******************************************************************************/
	
import com.ldx.xml.core.ComplexDataType;
import com.ldx.xml.core.MessageProcessor;
import com.ldx.xml.core.ProcessorException;
import com.ldx.xml.core.XMLEvent;

/**
 *	This class processes events that are sent by the LDX+ framework.
 */
public class CustomerCreditTransferInitiationV07Processor implements MessageProcessor {
	
	CustomerCreditTransferInitiationV07Repo msgRepo = PainRepo.getCustomerCreditTransferInitiationV07Repo();
	PaymentInstructionRepo paymentInstructionRepo = PainRepo.getPaymentInstructionRepo();
	CreditTransferTransaction26Repo crdtTrfrTxRepo = PainRepo.getCreditTransferTransaction26Repo();
	

	private long msgId = 0L;
	private long pmtInfId = 0L;
	
	@Override
	public void process(XMLEvent evt, ComplexDataType data)
			throws ProcessorException {

		if (evt == XMLEvent.START) {
			if (data instanceof CustomerCreditTransferInitiationV07) {
				msgId = SequenceService.getNextMessageId();
			} else if (data instanceof PaymentInstruction20) {
				pmtInfId = SequenceService.getNextPaymentInstructionId();
			} 
		}

		if (evt == XMLEvent.END) {
			if (data instanceof CustomerCreditTransferInitiationV07) {
				process((CustomerCreditTransferInitiationV07)data);
				
			} else if (data instanceof GroupHeader48) {
				process((GroupHeader48)data);
				
			} else if (data instanceof PaymentInstruction20) {
				process((PaymentInstruction20)data);
				
			} else if (data instanceof CreditTransferTransaction26) {
				process((CreditTransferTransaction26)data);

			}
		}
	}

	private void process(CustomerCreditTransferInitiationV07 data) {
		data.setKeyMessageId(msgId);
		msgRepo.save(data);
	}

	private void process(GroupHeader48 data) {
		// header is contained within message
	}

	private void process(PaymentInstruction20 data) {
		data.setKeyPmtInfId(pmtInfId);
		data.setKeyMsgId(msgId);
		paymentInstructionRepo.save(data);
	}

	private void process(CreditTransferTransaction26 data) {
		long cdtTrfTxInfId = SequenceService.getNextCreditTransferTransctionId();
		data.setKeyCdtTrfTxInf(cdtTrfTxInfId);
		data.setKeyPmtInfId(pmtInfId);
		crdtTrfrTxRepo.save(data);
	}
}
