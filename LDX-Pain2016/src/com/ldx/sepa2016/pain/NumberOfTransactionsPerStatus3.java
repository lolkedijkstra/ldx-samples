package com.ldx.sepa2016.pain;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.com
		email: dijkstra@xml2java.com
		support: support@xml2java.com
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.3
  Generated code is compatible with ldx-framework v. 2.3
  License: Dijkstra ICT Consulting 
  Module: PAIN 
  Generation date: Mon May 23 15:08:18 CEST 2016 

******************************************************************************/

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * NumberOfTransactionsPerStatus3 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class NumberOfTransactionsPerStatus3 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for NumberOfTransactionsPerStatus3.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public NumberOfTransactionsPerStatus3(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type NumberOfTransactionsPerStatus3.
	 */
	static class Allocator implements TypeAllocator<NumberOfTransactionsPerStatus3> {
		/**
		 * method for getting a new instance of type NumberOfTransactionsPerStatus3.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public NumberOfTransactionsPerStatus3 newInstance(String elementName, ComplexDataType parent) {
			return new NumberOfTransactionsPerStatus3(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for DtldNbOfTxs element. 
	 *  @serial
	 */	
	private String m_dtldNbOfTxs = null;
	
	/** element item for DtldSts element. 
	 *  @serial
	 */	
	private String m_dtldSts = null;
	
	/** element item for DtldCtrlSum element. 
	 *  @serial
	 */	
	private String m_dtldCtrlSum = null;
	
	/**
	 * Get the embedded DtldNbOfTxs element.
	 * @return the item.
	 */
	public String getDtldNbOfTxs() {
		return m_dtldNbOfTxs;
	}
		
	/**
	 * This method sets (overwrites) the element DtldNbOfTxs.
	 * @param data the item that needs to be added.
	 */
	void setDtldNbOfTxs(String data) {
		m_dtldNbOfTxs = data;
	}
		
	/**
	 * Get the embedded DtldSts element.
	 * @return the item.
	 */
	public String getDtldSts() {
		return m_dtldSts;
	}
		
	/**
	 * This method sets (overwrites) the element DtldSts.
	 * @param data the item that needs to be added.
	 */
	void setDtldSts(String data) {
		m_dtldSts = data;
	}
		
	/**
	 * Get the embedded DtldCtrlSum element.
	 * @return the item.
	 */
	public String getDtldCtrlSum() {
		return m_dtldCtrlSum;
	}
		
	/**
	 * This method sets (overwrites) the element DtldCtrlSum.
	 * @param data the item that needs to be added.
	 */
	void setDtldCtrlSum(String data) {
		m_dtldCtrlSum = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_dtldNbOfTxs, ((NumberOfTransactionsPerStatus3)that).m_dtldNbOfTxs))
			return false;
		
		if (!Compare.equals(m_dtldSts, ((NumberOfTransactionsPerStatus3)that).m_dtldSts))
			return false;
		
		if (!Compare.equals(m_dtldCtrlSum, ((NumberOfTransactionsPerStatus3)that).m_dtldCtrlSum))
			return false;
		
		return true;
	}	

  
  
}
