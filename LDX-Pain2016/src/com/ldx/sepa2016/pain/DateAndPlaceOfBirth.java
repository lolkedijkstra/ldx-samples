package com.ldx.sepa2016.pain;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.com
		email: dijkstra@xml2java.com
		support: support@xml2java.com
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.3
  Generated code is compatible with ldx-framework v. 2.3
  License: Dijkstra ICT Consulting 
  Module: PAIN 
  Generation date: Mon May 23 15:08:18 CEST 2016 

******************************************************************************/

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * DateAndPlaceOfBirth data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class DateAndPlaceOfBirth extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for DateAndPlaceOfBirth.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public DateAndPlaceOfBirth(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type DateAndPlaceOfBirth.
	 */
	static class Allocator implements TypeAllocator<DateAndPlaceOfBirth> {
		/**
		 * method for getting a new instance of type DateAndPlaceOfBirth.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public DateAndPlaceOfBirth newInstance(String elementName, ComplexDataType parent) {
			return new DateAndPlaceOfBirth(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for BirthDt element. 
	 *  @serial
	 */	
	private String m_birthDt = null;
	
	/** element item for PrvcOfBirth element. 
	 *  @serial
	 */	
	private String m_prvcOfBirth = null;
	
	/** element item for CityOfBirth element. 
	 *  @serial
	 */	
	private String m_cityOfBirth = null;
	
	/** element item for CtryOfBirth element. 
	 *  @serial
	 */	
	private String m_ctryOfBirth = null;
	
	/**
	 * Get the embedded BirthDt element.
	 * @return the item.
	 */
	public String getBirthDt() {
		return m_birthDt;
	}
		
	/**
	 * This method sets (overwrites) the element BirthDt.
	 * @param data the item that needs to be added.
	 */
	void setBirthDt(String data) {
		m_birthDt = data;
	}
		
	/**
	 * Get the embedded PrvcOfBirth element.
	 * @return the item.
	 */
	public String getPrvcOfBirth() {
		return m_prvcOfBirth;
	}
		
	/**
	 * This method sets (overwrites) the element PrvcOfBirth.
	 * @param data the item that needs to be added.
	 */
	void setPrvcOfBirth(String data) {
		m_prvcOfBirth = data;
	}
		
	/**
	 * Get the embedded CityOfBirth element.
	 * @return the item.
	 */
	public String getCityOfBirth() {
		return m_cityOfBirth;
	}
		
	/**
	 * This method sets (overwrites) the element CityOfBirth.
	 * @param data the item that needs to be added.
	 */
	void setCityOfBirth(String data) {
		m_cityOfBirth = data;
	}
		
	/**
	 * Get the embedded CtryOfBirth element.
	 * @return the item.
	 */
	public String getCtryOfBirth() {
		return m_ctryOfBirth;
	}
		
	/**
	 * This method sets (overwrites) the element CtryOfBirth.
	 * @param data the item that needs to be added.
	 */
	void setCtryOfBirth(String data) {
		m_ctryOfBirth = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_birthDt, ((DateAndPlaceOfBirth)that).m_birthDt))
			return false;
		
		if (!Compare.equals(m_prvcOfBirth, ((DateAndPlaceOfBirth)that).m_prvcOfBirth))
			return false;
		
		if (!Compare.equals(m_cityOfBirth, ((DateAndPlaceOfBirth)that).m_cityOfBirth))
			return false;
		
		if (!Compare.equals(m_ctryOfBirth, ((DateAndPlaceOfBirth)that).m_ctryOfBirth))
			return false;
		
		return true;
	}	

  
  
}
