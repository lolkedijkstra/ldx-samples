package com.ldx.sepa2016.repo;

import org.springframework.data.repository.CrudRepository;

import com.ldx.sepa2016.pain.CreditTransferTransaction26;

public interface CreditTransferTransaction26Repo extends CrudRepository<CreditTransferTransaction26, String> {}
