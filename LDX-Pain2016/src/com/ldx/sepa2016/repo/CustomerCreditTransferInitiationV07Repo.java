package com.ldx.sepa2016.repo;

import org.springframework.data.repository.CrudRepository;

import com.ldx.sepa2016.pain.CustomerCreditTransferInitiationV07;

public interface CustomerCreditTransferInitiationV07Repo extends CrudRepository<CustomerCreditTransferInitiationV07, String> {}
