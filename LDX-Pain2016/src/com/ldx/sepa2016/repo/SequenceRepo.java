package com.ldx.sepa2016.repo;

import org.springframework.data.repository.CrudRepository;

import com.ldx.mongo.sequence.Sequence;

public interface SequenceRepo extends CrudRepository<Sequence, String> {}
