java -jar "$LDX_HOME/ldxg.jar" -cconf/2016paincfg.xml -p

echo STARTING BUILD PAIN2016

if [ $1 ]
then
	if [ $2 ]
	then
		echo Building $2.jar with main class: com.ldx.sepa2016.pain.application.$1
		ant runnable-jar -f 2016build-pain.xml -Dmain-class=com.ldx.sepa2016.pain.application.$1 -Djar.name=$2.jar -f 2016build-pain.xml
		mv pain/lib/$2.jar ./$2.jar
	else
		echo Building $1.jar with main class: com.ldx.sepa2016.pain.application.$1
		ant runnable-jar -f 2016build-pain.xml -Dmain-class=com.ldx.sepa2016.pain.application.$1 -Djar.name=$1.jar -f 2016build-pain.xml
		mv pain/lib/$1.jar ./$1.jar
	fi	
else
	echo Building pain2016.jar..
	ant all -Djar.name=pain2016.jar -f 2016build-pain.xml
fi	



