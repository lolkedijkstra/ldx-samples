package com.data.abonados;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: ABONADOS 
  Generation date: Mon Apr 25 17:27:43 CEST 2016 

******************************************************************************/

import com.ldx.util.Printer;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * NumeracionAbonadoType data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class NumeracionAbonadoType extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for NumeracionAbonadoType.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public NumeracionAbonadoType(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type NumeracionAbonadoType.
	 */
	static class Allocator implements TypeAllocator<NumeracionAbonadoType> {
		/**
		 * method for getting a new instance of type NumeracionAbonadoType.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public NumeracionAbonadoType newInstance(String elementName, ComplexDataType parent) {
			return new NumeracionAbonadoType(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for Rangos element. 
	 *  @serial
	 */	
	private RangosType m_rangos = null;
	
	/**
	 * Get the embedded Rangos element.
	 * @return the item.
	 */
	public RangosType getRangos() {
		return m_rangos;
	}
		
	/**
	 * This method sets (overwrites) the element Rangos.
	 * @param data the item that needs to be added.
	 */
	void setRangos(RangosType data) {
		m_rangos = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_rangos, ((NumeracionAbonadoType)that).m_rangos))
			return false;
		
		return true;
	}	

  
  
  
	/**
	 *	Printing method, prints the XML element to a Printer.
	 *  This method prints an XML fragment starting from NumeracionAbonadoType.
	 *
	 *  @param out the Printer that the element is printed to
	 *  @see com.ldx.util.Printer
	 */
	protected void printElements(Printer out) {
		super.printElements(out);
  
		if (m_rangos != null)
			m_rangos.print(out);
		else {
			// out.print("<Rangos>null</Rangos>");
		}
		
	}
}
