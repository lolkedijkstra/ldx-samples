package com.data.abonados;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: ABONADOS 
  Generation date: Mon Apr 25 17:27:43 CEST 2016 

******************************************************************************/

import com.ldx.util.Printer;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * DatosAbonadoType data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class DatosAbonadoType extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for DatosAbonadoType.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public DatosAbonadoType(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type DatosAbonadoType.
	 */
	static class Allocator implements TypeAllocator<DatosAbonadoType> {
		/**
		 * method for getting a new instance of type DatosAbonadoType.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public DatosAbonadoType newInstance(String elementName, ComplexDataType parent) {
			return new DatosAbonadoType(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for Titular element. 
	 *  @serial
	 */	
	private TitularType m_titular = null;
	
	/** element item for Domicilio element. 
	 *  @serial
	 */	
	private DomicilioType m_domicilio = null;
	
	/** element item for NumeracionAbonado element. 
	 *  @serial
	 */	
	private NumeracionAbonadoType m_numeracionAbonado = null;
	
	/**
	 * Get the embedded Titular element.
	 * @return the item.
	 */
	public TitularType getTitular() {
		return m_titular;
	}
		
	/**
	 * This method sets (overwrites) the element Titular.
	 * @param data the item that needs to be added.
	 */
	void setTitular(TitularType data) {
		m_titular = data;
	}
		
	/**
	 * Get the embedded Domicilio element.
	 * @return the item.
	 */
	public DomicilioType getDomicilio() {
		return m_domicilio;
	}
		
	/**
	 * This method sets (overwrites) the element Domicilio.
	 * @param data the item that needs to be added.
	 */
	void setDomicilio(DomicilioType data) {
		m_domicilio = data;
	}
		
	/**
	 * Get the embedded NumeracionAbonado element.
	 * @return the item.
	 */
	public NumeracionAbonadoType getNumeracionAbonado() {
		return m_numeracionAbonado;
	}
		
	/**
	 * This method sets (overwrites) the element NumeracionAbonado.
	 * @param data the item that needs to be added.
	 */
	void setNumeracionAbonado(NumeracionAbonadoType data) {
		m_numeracionAbonado = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_titular, ((DatosAbonadoType)that).m_titular))
			return false;
		
		if (!Compare.equals(m_domicilio, ((DatosAbonadoType)that).m_domicilio))
			return false;
		
		if (!Compare.equals(m_numeracionAbonado, ((DatosAbonadoType)that).m_numeracionAbonado))
			return false;
		
		return true;
	}	

  
  

	/**
	 * Get 'Operacion' attribute.
	 * @return the item.
	 */
	public String getOperacion() {
		return getAttr("Operacion");
	}

	/**
	 * Set 'Operacion' attribute.
	 * 
	 * Set (overwrite) the attribute data.
	 * @param data the item that needs to be added.
	 */
	public void setOperacion(String data) {
		setAttr("Operacion", data);
	}
  

	/**
	 * Get 'FechaExtraccion' attribute.
	 * @return the item.
	 */
	public String getFechaExtraccion() {
		return getAttr("FechaExtraccion");
	}

	/**
	 * Set 'FechaExtraccion' attribute.
	 * 
	 * Set (overwrite) the attribute data.
	 * @param data the item that needs to be added.
	 */
	public void setFechaExtraccion(String data) {
		setAttr("FechaExtraccion", data);
	}
  
  
	/**
	 *	Printing method, prints the XML element to a Printer.
	 *  This method prints an XML fragment starting from DatosAbonadoType.
	 *
	 *  @param out the Printer that the element is printed to
	 *  @see com.ldx.util.Printer
	 */
	protected void printElements(Printer out) {
		super.printElements(out);
  
		if (m_titular != null)
			m_titular.print(out);
		else {
			// out.print("<Titular>null</Titular>");
		}
		
		if (m_domicilio != null)
			m_domicilio.print(out);
		else {
			// out.print("<Domicilio>null</Domicilio>");
		}
		
		if (m_numeracionAbonado != null)
			m_numeracionAbonado.print(out);
		else {
			// out.print("<NumeracionAbonado>null</NumeracionAbonado>");
		}
		
	}
}
