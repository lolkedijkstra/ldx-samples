package com.data.abonados;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: ABONADOS 
  Generation date: Mon Apr 25 17:27:43 CEST 2016 

******************************************************************************/

import com.ldx.util.Printer;

import java.util.List;
import java.util.ArrayList;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * DatosAbonados data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class DatosAbonados extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for DatosAbonados.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public DatosAbonados(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type DatosAbonados.
	 */
	static class Allocator implements TypeAllocator<DatosAbonados> {
		/**
		 * method for getting a new instance of type DatosAbonados.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public DatosAbonados newInstance(String elementName, ComplexDataType parent) {
			return new DatosAbonados(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** list of DatosAbonado element. 
	 *  @serial
	 */	
	private List<DatosAbonadoType> m_datosAbonadoList = new ArrayList<DatosAbonadoType>();
	
	/**
	 * Get the embedded list of DatosAbonado elements.
	 * @return list of items.
	 */
	public List<DatosAbonadoType> getDatosAbonados() {
		return m_datosAbonadoList;
	}
		
	/**
	 * This method adds data to the list of DatosAbonado.
	 * @param data the item that needs to be added.
	 */
	void setDatosAbonado(DatosAbonadoType data) {
		m_datosAbonadoList.add(data);
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_datosAbonadoList, ((DatosAbonados)that).m_datosAbonadoList))
			return false;
		
		return true;
	}	

  
  
  
	/**
	 *	Printing method, prints the XML element to a Printer.
	 *  This method prints an XML fragment starting from DatosAbonados.
	 *
	 *  @param out the Printer that the element is printed to
	 *  @see com.ldx.util.Printer
	 */
	protected void printElements(Printer out) {
		super.printElements(out);
  
		if (m_datosAbonadoList != null)
			for(DatosAbonadoType l_DatosAbonado : m_datosAbonadoList) {
				l_DatosAbonado.print(out);
			}
		else {
			// out.print("<DatosAbonado>null</DatosAbonado>");
		}
		
	}
}
