package com.data.abonados.handlers;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: ABONADOS 
  Generation date: Mon Apr 25 17:27:43 CEST 2016 

******************************************************************************/

import org.xml.sax.XMLReader;

import com.data.abonados.DatosAbonados;
import com.data.abonados.DatosAbonadosHandler;

import com.ldx.xml.core.XMLMessageHandler;

/**
 * This class reads the XML document from an XML inputsource.
 *
 * This class is the entry point for the client application.
 */
public class DatosAbonadosMessageHandler extends
		XMLMessageHandler<DatosAbonados> {
	
	/** root element. */	
	static final String ELEMENT_NAME = "DatosAbonados";	
	
	/**
	 * Constructor.
	 *
	 * @see XMLMessageHandler XMLMessageHandler
	 * @param reader
	 *            The (SAX) XML Reader object
	 */
	public DatosAbonadosMessageHandler(XMLReader reader) {
		super(reader
		, new DatosAbonadosHandler(
			reader
			, null	// root has no parent
			, ELEMENT_NAME
			, DatosAbonados.getAllocator()
			, null	// not applicable for root
			, doProcess(ELEMENT_NAME))
		);
	}
}

