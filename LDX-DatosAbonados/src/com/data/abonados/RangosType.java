package com.data.abonados;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: ABONADOS 
  Generation date: Mon Apr 25 17:27:43 CEST 2016 

******************************************************************************/

import com.ldx.util.Printer;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * RangosType data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class RangosType extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for RangosType.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public RangosType(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type RangosType.
	 */
	static class Allocator implements TypeAllocator<RangosType> {
		/**
		 * method for getting a new instance of type RangosType.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public RangosType newInstance(String elementName, ComplexDataType parent) {
			return new RangosType(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for Operador element. 
	 *  @serial
	 */	
	private OperadorType m_operador = null;
	
	/**
	 * Get the embedded Operador element.
	 * @return the item.
	 */
	public OperadorType getOperador() {
		return m_operador;
	}
		
	/**
	 * This method sets (overwrites) the element Operador.
	 * @param data the item that needs to be added.
	 */
	void setOperador(OperadorType data) {
		m_operador = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_operador, ((RangosType)that).m_operador))
			return false;
		
		return true;
	}	

  
  

	/**
	 * Get 'NumeroDesde' attribute.
	 * @return the item.
	 */
	public String getNumeroDesde() {
		return getAttr("NumeroDesde");
	}

	/**
	 * Set 'NumeroDesde' attribute.
	 * 
	 * Set (overwrite) the attribute data.
	 * @param data the item that needs to be added.
	 */
	public void setNumeroDesde(String data) {
		setAttr("NumeroDesde", data);
	}
  

	/**
	 * Get 'NumeroHasta' attribute.
	 * @return the item.
	 */
	public String getNumeroHasta() {
		return getAttr("NumeroHasta");
	}

	/**
	 * Set 'NumeroHasta' attribute.
	 * 
	 * Set (overwrite) the attribute data.
	 * @param data the item that needs to be added.
	 */
	public void setNumeroHasta(String data) {
		setAttr("NumeroHasta", data);
	}
  

	/**
	 * Get 'ConsentimientoGuias-Consulta' attribute.
	 * @return the item.
	 */
	public String getConsentimientoGuiasConsulta() {
		return getAttr("ConsentimientoGuias-Consulta");
	}

	/**
	 * Set 'ConsentimientoGuias-Consulta' attribute.
	 * 
	 * Set (overwrite) the attribute data.
	 * @param data the item that needs to be added.
	 */
	public void setConsentimientoGuiasConsulta(String data) {
		setAttr("ConsentimientoGuias-Consulta", data);
	}
  

	/**
	 * Get 'VentaDirecta-Publicidad' attribute.
	 * @return the item.
	 */
	public String getVentaDirectaPublicidad() {
		return getAttr("VentaDirecta-Publicidad");
	}

	/**
	 * Set 'VentaDirecta-Publicidad' attribute.
	 * 
	 * Set (overwrite) the attribute data.
	 * @param data the item that needs to be added.
	 */
	public void setVentaDirectaPublicidad(String data) {
		setAttr("VentaDirecta-Publicidad", data);
	}
  

	/**
	 * Get 'ModoPago' attribute.
	 * @return the item.
	 */
	public String getModoPago() {
		return getAttr("ModoPago");
	}

	/**
	 * Set 'ModoPago' attribute.
	 * 
	 * Set (overwrite) the attribute data.
	 * @param data the item that needs to be added.
	 */
	public void setModoPago(String data) {
		setAttr("ModoPago", data);
	}
  
  
	/**
	 *	Printing method, prints the XML element to a Printer.
	 *  This method prints an XML fragment starting from RangosType.
	 *
	 *  @param out the Printer that the element is printed to
	 *  @see com.ldx.util.Printer
	 */
	protected void printElements(Printer out) {
		super.printElements(out);
  
		if (m_operador != null)
			m_operador.print(out);
		else {
			// out.print("<Operador>null</Operador>");
		}
		
	}
}
