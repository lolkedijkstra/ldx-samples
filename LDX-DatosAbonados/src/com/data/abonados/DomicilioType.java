package com.data.abonados;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: ABONADOS 
  Generation date: Mon Apr 25 17:27:43 CEST 2016 

******************************************************************************/

import com.ldx.util.Printer;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * DomicilioType data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class DomicilioType extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for DomicilioType.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public DomicilioType(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type DomicilioType.
	 */
	static class Allocator implements TypeAllocator<DomicilioType> {
		/**
		 * method for getting a new instance of type DomicilioType.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public DomicilioType newInstance(String elementName, ComplexDataType parent) {
			return new DomicilioType(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		return true;
	}	

  
  

	/**
	 * Get 'Escalera' attribute.
	 * @return the item.
	 */
	public String getEscalera() {
		return getAttr("Escalera");
	}

	/**
	 * Set 'Escalera' attribute.
	 * 
	 * Set (overwrite) the attribute data.
	 * @param data the item that needs to be added.
	 */
	public void setEscalera(String data) {
		setAttr("Escalera", data);
	}
  

	/**
	 * Get 'Piso' attribute.
	 * @return the item.
	 */
	public String getPiso() {
		return getAttr("Piso");
	}

	/**
	 * Set 'Piso' attribute.
	 * 
	 * Set (overwrite) the attribute data.
	 * @param data the item that needs to be added.
	 */
	public void setPiso(String data) {
		setAttr("Piso", data);
	}
  

	/**
	 * Get 'Puerta' attribute.
	 * @return the item.
	 */
	public String getPuerta() {
		return getAttr("Puerta");
	}

	/**
	 * Set 'Puerta' attribute.
	 * 
	 * Set (overwrite) the attribute data.
	 * @param data the item that needs to be added.
	 */
	public void setPuerta(String data) {
		setAttr("Puerta", data);
	}
  

	/**
	 * Get 'TipoVia' attribute.
	 * @return the item.
	 */
	public String getTipoVia() {
		return getAttr("TipoVia");
	}

	/**
	 * Set 'TipoVia' attribute.
	 * 
	 * Set (overwrite) the attribute data.
	 * @param data the item that needs to be added.
	 */
	public void setTipoVia(String data) {
		setAttr("TipoVia", data);
	}
  

	/**
	 * Get 'NombreVia' attribute.
	 * @return the item.
	 */
	public String getNombreVia() {
		return getAttr("NombreVia");
	}

	/**
	 * Set 'NombreVia' attribute.
	 * 
	 * Set (overwrite) the attribute data.
	 * @param data the item that needs to be added.
	 */
	public void setNombreVia(String data) {
		setAttr("NombreVia", data);
	}
  

	/**
	 * Get 'NumeroCalle' attribute.
	 * @return the item.
	 */
	public String getNumeroCalle() {
		return getAttr("NumeroCalle");
	}

	/**
	 * Set 'NumeroCalle' attribute.
	 * 
	 * Set (overwrite) the attribute data.
	 * @param data the item that needs to be added.
	 */
	public void setNumeroCalle(String data) {
		setAttr("NumeroCalle", data);
	}
  

	/**
	 * Get 'Portal' attribute.
	 * @return the item.
	 */
	public String getPortal() {
		return getAttr("Portal");
	}

	/**
	 * Set 'Portal' attribute.
	 * 
	 * Set (overwrite) the attribute data.
	 * @param data the item that needs to be added.
	 */
	public void setPortal(String data) {
		setAttr("Portal", data);
	}
  

	/**
	 * Get 'CodigoPostal' attribute.
	 * @return the item.
	 */
	public String getCodigoPostal() {
		return getAttr("CodigoPostal");
	}

	/**
	 * Set 'CodigoPostal' attribute.
	 * 
	 * Set (overwrite) the attribute data.
	 * @param data the item that needs to be added.
	 */
	public void setCodigoPostal(String data) {
		setAttr("CodigoPostal", data);
	}
  

	/**
	 * Get 'Poblacion' attribute.
	 * @return the item.
	 */
	public String getPoblacion() {
		return getAttr("Poblacion");
	}

	/**
	 * Set 'Poblacion' attribute.
	 * 
	 * Set (overwrite) the attribute data.
	 * @param data the item that needs to be added.
	 */
	public void setPoblacion(String data) {
		setAttr("Poblacion", data);
	}
  

	/**
	 * Get 'Provincia' attribute.
	 * @return the item.
	 */
	public String getProvincia() {
		return getAttr("Provincia");
	}

	/**
	 * Set 'Provincia' attribute.
	 * 
	 * Set (overwrite) the attribute data.
	 * @param data the item that needs to be added.
	 */
	public void setProvincia(String data) {
		setAttr("Provincia", data);
	}
  
  
	/**
	 *	Printing method, prints the XML element to a Printer.
	 *  This method prints an XML fragment starting from DomicilioType.
	 *
	 *  @param out the Printer that the element is printed to
	 *  @see com.ldx.util.Printer
	 */
	protected void printElements(Printer out) {
		super.printElements(out);
  
	}
}
