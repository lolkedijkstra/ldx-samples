package com.data.abonados;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: ABONADOS 
  Generation date: Mon Apr 25 17:27:43 CEST 2016 

******************************************************************************/

/* SAX 2.0 dependencies */
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/* Framework dependencies */
import com.ldx.xml.core.DataSetter;
import com.ldx.xml.core.ComplexDataType;
import com.ldx.xml.core.XMLEvent;
import com.ldx.xml.core.XMLFragmentHandler;


/**
 * DatosAbonadoType handler class.
 *
 * @see DatosAbonadoType
 * 
 */
public class DatosAbonadoTypeHandler extends XMLFragmentHandler<DatosAbonadoType> {
	/**
	 * Proxy for DatosAbonadoTypeHandler.
	 */
	static class Proxy extends HandlerProxy<DatosAbonadoType> {
		/**
		 * Allocator for DatosAbonadoTypeHandler.
		 */
		private static class Allocator 
				extends HandlerProxy.Allocator<DatosAbonadoType> {			
			public XMLFragmentHandler<DatosAbonadoType> create(
					XMLReader reader
					, XMLFragmentHandler<?> handler
					, String elementName
					, DataSetter setter
					, boolean doProcess) {
				return new DatosAbonadoTypeHandler(
					reader
					, handler
					, elementName
					, DatosAbonadoType.getAllocator()
					, setter
					, doProcess);
			}
		}
		
		/**
		 * Constructor for Proxy.
		 *
		 * @param reader the XML reader
		 * @param parentH the parent XML handler class
		 * @param elementName the name of the XML Element (tag).
		 * @param pSetter the setter for the parent data
		 * @param doProcess indicates whether processing is active for this handler instance
		 */
		public Proxy(XMLReader reader, XMLFragmentHandler<?> parentH, String elementName,
				DataSetter pSetter, boolean doProcess) {
			super(reader, parentH, elementName, pSetter, new Allocator(), doProcess);
		}
	}
	
	/** Data setter class for Domicilio element. */
	private class DomicilioSetter implements DataSetter {
		/** data target. */
		private DatosAbonadoTypeHandler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public DomicilioSetter(DatosAbonadoTypeHandler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setDomicilio((DomicilioType) data);	
		}
	}	
	/** Data setter class for NumeracionAbonado element. */
	private class NumeracionAbonadoSetter implements DataSetter {
		/** data target. */
		private DatosAbonadoTypeHandler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public NumeracionAbonadoSetter(DatosAbonadoTypeHandler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setNumeracionAbonado((NumeracionAbonadoType) data);	
		}
	}	
	/** Data setter class for Titular element. */
	private class TitularSetter implements DataSetter {
		/** data target. */
		private DatosAbonadoTypeHandler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public TitularSetter(DatosAbonadoTypeHandler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setTitular((TitularType) data);	
		}
	}


	/**
	 * Constructor for handler class.
	 *
	 * @param reader the XML reader
	 * @param parentH the parent XML handler class
	 * @param elementName the name of the XML Element (tag)
	 * @param allocator the allocator for the data type
	 * @param pSetter the setter for the parent data
	 * @param doProcess indicates whether processing is active for this handler instance
	 */
	public DatosAbonadoTypeHandler(
			XMLReader reader
			, XMLFragmentHandler<?> parentH
			, String elementName
			, DatosAbonadoType.Allocator allocator
			, DataSetter pSetter
			, boolean doProcess) {
		super(reader, parentH, elementName, allocator, pSetter, doProcess);

		// code for linking children..
		registerHandler(
			new DomicilioTypeHandler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of DomicilioTypeHandler
				, "Domicilio" // XML element name
				, doLink("Domicilio") // linking to parent
					? new DomicilioSetter(this) // ON
					: null // OFF
				, doProcess("Domicilio")) // processing active or not
				);
  
		registerHandler(
			new NumeracionAbonadoTypeHandler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of NumeracionAbonadoTypeHandler
				, "NumeracionAbonado" // XML element name
				, doLink("NumeracionAbonado") // linking to parent
					? new NumeracionAbonadoSetter(this) // ON
					: null // OFF
				, doProcess("NumeracionAbonado")) // processing active or not
				);
  
		registerHandler(
			new TitularTypeHandler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of TitularTypeHandler
				, "Titular" // XML element name
				, doLink("Titular") // linking to parent
					? new TitularSetter(this) // ON
					: null // OFF
				, doProcess("Titular")) // processing active or not
				);
  	}


	/**
	 * This method overrides the getData() of the super type, returning the more
	 * specific type.
	 * 
	 */
	@Override 
	public DatosAbonadoType getData() {
		return (DatosAbonadoType)super.getData();
	}




	/**
	 * This method is called by the XML parser on an end of element event.
	 *
	 * If the localName == 'the name of the element that this handler handles' control is
	 * passed back to the parent handler (future events go there) and the data of
	 * the children of this handler is attached to the data associated to this handler.	
	 * 
	 * The data that is collected is connected to the parent data only if a parentDataSetter
	 * if found (getParentDataSetter()).
	 *
	 * If processing is enabled for a handler, the processor is called.
	 */
	@Override
	public void endElement(String uri, String localName, String name)
		throws SAXException {
		
		if (localName.equals(getXMLElementName())) {
				
			// return control to parent handler..
			this.deactivate();
			
			// get content of this item..
			getData().setContent(this.getValue());
			
			// attach data to parent (if parent data setter is found)..
			DataSetter setter = getParentDataSetter();
			if (setter != null) {
				setter.set(getData());
			} 
			
			// process data if required..
			if (doProcess()) {
				process(XMLEvent.END);
			}
		}
		
	}	
}
