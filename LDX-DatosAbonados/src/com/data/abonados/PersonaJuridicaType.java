package com.data.abonados;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: ABONADOS 
  Generation date: Mon Apr 25 17:27:43 CEST 2016 

******************************************************************************/

import com.ldx.util.Printer;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * PersonaJuridicaType data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class PersonaJuridicaType extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for PersonaJuridicaType.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public PersonaJuridicaType(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type PersonaJuridicaType.
	 */
	static class Allocator implements TypeAllocator<PersonaJuridicaType> {
		/**
		 * method for getting a new instance of type PersonaJuridicaType.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public PersonaJuridicaType newInstance(String elementName, ComplexDataType parent) {
			return new PersonaJuridicaType(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		return true;
	}	

  
  

	/**
	 * Get 'DocIdentificacionJuridica' attribute.
	 * @return the item.
	 */
	public String getDocIdentificacionJuridica() {
		return getAttr("DocIdentificacionJuridica");
	}

	/**
	 * Set 'DocIdentificacionJuridica' attribute.
	 * 
	 * Set (overwrite) the attribute data.
	 * @param data the item that needs to be added.
	 */
	public void setDocIdentificacionJuridica(String data) {
		setAttr("DocIdentificacionJuridica", data);
	}
  

	/**
	 * Get 'RazonSocial' attribute.
	 * @return the item.
	 */
	public String getRazonSocial() {
		return getAttr("RazonSocial");
	}

	/**
	 * Set 'RazonSocial' attribute.
	 * 
	 * Set (overwrite) the attribute data.
	 * @param data the item that needs to be added.
	 */
	public void setRazonSocial(String data) {
		setAttr("RazonSocial", data);
	}
  

	/**
	 * Get 'NombreComercial' attribute.
	 * @return the item.
	 */
	public String getNombreComercial() {
		return getAttr("NombreComercial");
	}

	/**
	 * Set 'NombreComercial' attribute.
	 * 
	 * Set (overwrite) the attribute data.
	 * @param data the item that needs to be added.
	 */
	public void setNombreComercial(String data) {
		setAttr("NombreComercial", data);
	}
  
  
	/**
	 *	Printing method, prints the XML element to a Printer.
	 *  This method prints an XML fragment starting from PersonaJuridicaType.
	 *
	 *  @param out the Printer that the element is printed to
	 *  @see com.ldx.util.Printer
	 */
	protected void printElements(Printer out) {
		super.printElements(out);
  
	}
}
