# README #

This project several sample projects amoung which the messages from the SEPA PAIN and PACS domains.
The generated Java code based on the 2016 versions of the PAIN and PACS  messages (iso20022.org).

Added: amis' company file problem

The code was generated using the following commands:

* LDX_HOME />java -jar ldxg.jar -wsamples/sepa -s1 -cconf/2016paincfg.xml
* LDX_HOME />java -jar ldxg.jar -wsamples/sepa -s1 -cconf/2016paincfg.xml


### What is this repository for? ###

* It illustrates how the code was generated given the configuration file used.
* Version: 2016 versions from both PACS and PAIN

### How do I get set up? ###

* Install LDX+
* Follow the instructions available in the LDX usermanual.
* Clone the repository

### Prerequisites ###

* LDX+ framework is required to use the generated code

### Who do I talk to? ###

* Dijkstra ICT Consulting
* support@xml2java.com
* [http://www.xml2java.com](http://www.xml2java.com)