package com.ldx.discogs.releases;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.com
		email: dijkstra@xml2java.com
		support: support@xml2java.com
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.3
  Generated code is compatible with ldx-framework v. 2.3
  License: Dijkstra ICT Consulting 
  Module: RELEASES 
  Generation date: Sun May 22 21:46:52 CEST 2016 

******************************************************************************/

import java.util.List;
import java.util.ArrayList;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * CompaniesType data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class CompaniesType extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for CompaniesType.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public CompaniesType(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type CompaniesType.
	 */
	static class Allocator implements TypeAllocator<CompaniesType> {
		/**
		 * method for getting a new instance of type CompaniesType.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public CompaniesType newInstance(String elementName, ComplexDataType parent) {
			return new CompaniesType(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** list of company element. 
	 *  @serial
	 */	
	private List<CompanyType> m_companyList = new ArrayList<CompanyType>();
	
	/**
	 * Get the embedded list of Company elements.
	 * @return list of items.
	 */
	public List<CompanyType> getCompanys() {
		return m_companyList;
	}
		
	/**
	 * This method adds data to the list of Company.
	 * @param data the item that needs to be added.
	 */
	void setCompany(CompanyType data) {
		m_companyList.add(data);
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_companyList, ((CompaniesType)that).m_companyList))
			return false;
		
		return true;
	}	

  
  
}
