package com.ldx.discogs.releases;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.com
		email: dijkstra@xml2java.com
		support: support@xml2java.com
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.3
  Generated code is compatible with ldx-framework v. 2.3
  License: Dijkstra ICT Consulting 
  Module: RELEASES 
  Generation date: Sun May 22 21:46:52 CEST 2016 

******************************************************************************/

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * ArtistType data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class ArtistType extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for ArtistType.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public ArtistType(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type ArtistType.
	 */
	static class Allocator implements TypeAllocator<ArtistType> {
		/**
		 * method for getting a new instance of type ArtistType.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public ArtistType newInstance(String elementName, ComplexDataType parent) {
			return new ArtistType(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for id element. 
	 *  @serial
	 */	
	private String m_id = null;
	
	/** element item for name element. 
	 *  @serial
	 */	
	private String m_name = null;
	
	/** element item for anv element. 
	 *  @serial
	 */	
	private String m_anv = null;
	
	/** element item for join element. 
	 *  @serial
	 */	
	private String m_join = null;
	
	/** element item for role element. 
	 *  @serial
	 */	
	private String m_role = null;
	
	/** element item for tracks element. 
	 *  @serial
	 */	
	private String m_tracks = null;
	
	/**
	 * Get the embedded Id element.
	 * @return the item.
	 */
	public String getId() {
		return m_id;
	}
		
	/**
	 * This method sets (overwrites) the element Id.
	 * @param data the item that needs to be added.
	 */
	void setId(String data) {
		m_id = data;
	}
		
	/**
	 * Get the embedded Name element.
	 * @return the item.
	 */
	public String getName() {
		return m_name;
	}
		
	/**
	 * This method sets (overwrites) the element Name.
	 * @param data the item that needs to be added.
	 */
	void setName(String data) {
		m_name = data;
	}
		
	/**
	 * Get the embedded Anv element.
	 * @return the item.
	 */
	public String getAnv() {
		return m_anv;
	}
		
	/**
	 * This method sets (overwrites) the element Anv.
	 * @param data the item that needs to be added.
	 */
	void setAnv(String data) {
		m_anv = data;
	}
		
	/**
	 * Get the embedded Join element.
	 * @return the item.
	 */
	public String getJoin() {
		return m_join;
	}
		
	/**
	 * This method sets (overwrites) the element Join.
	 * @param data the item that needs to be added.
	 */
	void setJoin(String data) {
		m_join = data;
	}
		
	/**
	 * Get the embedded Role element.
	 * @return the item.
	 */
	public String getRole() {
		return m_role;
	}
		
	/**
	 * This method sets (overwrites) the element Role.
	 * @param data the item that needs to be added.
	 */
	void setRole(String data) {
		m_role = data;
	}
		
	/**
	 * Get the embedded Tracks element.
	 * @return the item.
	 */
	public String getTracks() {
		return m_tracks;
	}
		
	/**
	 * This method sets (overwrites) the element Tracks.
	 * @param data the item that needs to be added.
	 */
	void setTracks(String data) {
		m_tracks = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_id, ((ArtistType)that).m_id))
			return false;
		
		if (!Compare.equals(m_name, ((ArtistType)that).m_name))
			return false;
		
		if (!Compare.equals(m_anv, ((ArtistType)that).m_anv))
			return false;
		
		if (!Compare.equals(m_join, ((ArtistType)that).m_join))
			return false;
		
		if (!Compare.equals(m_role, ((ArtistType)that).m_role))
			return false;
		
		if (!Compare.equals(m_tracks, ((ArtistType)that).m_tracks))
			return false;
		
		return true;
	}	

  
  
}
