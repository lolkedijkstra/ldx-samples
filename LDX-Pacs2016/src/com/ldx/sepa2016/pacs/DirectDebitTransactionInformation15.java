package com.ldx.sepa2016.pacs;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: PACS 
  Generation date: Mon Apr 25 12:08:30 CEST 2016 

******************************************************************************/

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * DirectDebitTransactionInformation15 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class DirectDebitTransactionInformation15 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for DirectDebitTransactionInformation15.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public DirectDebitTransactionInformation15(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type DirectDebitTransactionInformation15.
	 */
	static class Allocator implements TypeAllocator<DirectDebitTransactionInformation15> {
		/**
		 * method for getting a new instance of type DirectDebitTransactionInformation15.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public DirectDebitTransactionInformation15 newInstance(String elementName, ComplexDataType parent) {
			return new DirectDebitTransactionInformation15(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for PmtId element. 
	 *  @serial
	 */	
	private PaymentIdentification3 m_pmtId = null;
	
	/** element item for PmtTpInf element. 
	 *  @serial
	 */	
	private PaymentTypeInformation21 m_pmtTpInf = null;
	
	/** element item for IntrBkSttlmAmt element. 
	 *  @serial
	 */	
	private ActiveCurrencyAndAmount m_intrBkSttlmAmt = null;
	
	/** element item for IntrBkSttlmDt element. 
	 *  @serial
	 */	
	private String m_intrBkSttlmDt = null;
	
	/** element item for SttlmPrty element. 
	 *  @serial
	 */	
	private String m_sttlmPrty = null;
	
	/** element item for SttlmTmReq element. 
	 *  @serial
	 */	
	private SettlementTimeRequest2 m_sttlmTmReq = null;
	
	/** element item for UltmtDbtr element. 
	 *  @serial
	 */	
	private BranchAndFinancialInstitutionIdentification5 m_ultmtDbtr = null;
	
	/** element item for Dbtr element. 
	 *  @serial
	 */	
	private BranchAndFinancialInstitutionIdentification5 m_dbtr = null;
	
	/** element item for DbtrAcct element. 
	 *  @serial
	 */	
	private CashAccount24 m_dbtrAcct = null;
	
	/** element item for DbtrAgt element. 
	 *  @serial
	 */	
	private BranchAndFinancialInstitutionIdentification5 m_dbtrAgt = null;
	
	/** element item for DbtrAgtAcct element. 
	 *  @serial
	 */	
	private CashAccount24 m_dbtrAgtAcct = null;
	
	/** element item for InstrForDbtrAgt element. 
	 *  @serial
	 */	
	private String m_instrForDbtrAgt = null;
	
	/** element item for RmtInf element. 
	 *  @serial
	 */	
	private RemittanceInformation2 m_rmtInf = null;
	
	/**
	 * Get the embedded PmtId element.
	 * @return the item.
	 */
	public PaymentIdentification3 getPmtId() {
		return m_pmtId;
	}
		
	/**
	 * This method sets (overwrites) the element PmtId.
	 * @param data the item that needs to be added.
	 */
	void setPmtId(PaymentIdentification3 data) {
		m_pmtId = data;
	}
		
	/**
	 * Get the embedded PmtTpInf element.
	 * @return the item.
	 */
	public PaymentTypeInformation21 getPmtTpInf() {
		return m_pmtTpInf;
	}
		
	/**
	 * This method sets (overwrites) the element PmtTpInf.
	 * @param data the item that needs to be added.
	 */
	void setPmtTpInf(PaymentTypeInformation21 data) {
		m_pmtTpInf = data;
	}
		
	/**
	 * Get the embedded IntrBkSttlmAmt element.
	 * @return the item.
	 */
	public ActiveCurrencyAndAmount getIntrBkSttlmAmt() {
		return m_intrBkSttlmAmt;
	}
		
	/**
	 * This method sets (overwrites) the element IntrBkSttlmAmt.
	 * @param data the item that needs to be added.
	 */
	void setIntrBkSttlmAmt(ActiveCurrencyAndAmount data) {
		m_intrBkSttlmAmt = data;
	}
		
	/**
	 * Get the embedded IntrBkSttlmDt element.
	 * @return the item.
	 */
	public String getIntrBkSttlmDt() {
		return m_intrBkSttlmDt;
	}
		
	/**
	 * This method sets (overwrites) the element IntrBkSttlmDt.
	 * @param data the item that needs to be added.
	 */
	void setIntrBkSttlmDt(String data) {
		m_intrBkSttlmDt = data;
	}
		
	/**
	 * Get the embedded SttlmPrty element.
	 * @return the item.
	 */
	public String getSttlmPrty() {
		return m_sttlmPrty;
	}
		
	/**
	 * This method sets (overwrites) the element SttlmPrty.
	 * @param data the item that needs to be added.
	 */
	void setSttlmPrty(String data) {
		m_sttlmPrty = data;
	}
		
	/**
	 * Get the embedded SttlmTmReq element.
	 * @return the item.
	 */
	public SettlementTimeRequest2 getSttlmTmReq() {
		return m_sttlmTmReq;
	}
		
	/**
	 * This method sets (overwrites) the element SttlmTmReq.
	 * @param data the item that needs to be added.
	 */
	void setSttlmTmReq(SettlementTimeRequest2 data) {
		m_sttlmTmReq = data;
	}
		
	/**
	 * Get the embedded UltmtDbtr element.
	 * @return the item.
	 */
	public BranchAndFinancialInstitutionIdentification5 getUltmtDbtr() {
		return m_ultmtDbtr;
	}
		
	/**
	 * This method sets (overwrites) the element UltmtDbtr.
	 * @param data the item that needs to be added.
	 */
	void setUltmtDbtr(BranchAndFinancialInstitutionIdentification5 data) {
		m_ultmtDbtr = data;
	}
		
	/**
	 * Get the embedded Dbtr element.
	 * @return the item.
	 */
	public BranchAndFinancialInstitutionIdentification5 getDbtr() {
		return m_dbtr;
	}
		
	/**
	 * This method sets (overwrites) the element Dbtr.
	 * @param data the item that needs to be added.
	 */
	void setDbtr(BranchAndFinancialInstitutionIdentification5 data) {
		m_dbtr = data;
	}
		
	/**
	 * Get the embedded DbtrAcct element.
	 * @return the item.
	 */
	public CashAccount24 getDbtrAcct() {
		return m_dbtrAcct;
	}
		
	/**
	 * This method sets (overwrites) the element DbtrAcct.
	 * @param data the item that needs to be added.
	 */
	void setDbtrAcct(CashAccount24 data) {
		m_dbtrAcct = data;
	}
		
	/**
	 * Get the embedded DbtrAgt element.
	 * @return the item.
	 */
	public BranchAndFinancialInstitutionIdentification5 getDbtrAgt() {
		return m_dbtrAgt;
	}
		
	/**
	 * This method sets (overwrites) the element DbtrAgt.
	 * @param data the item that needs to be added.
	 */
	void setDbtrAgt(BranchAndFinancialInstitutionIdentification5 data) {
		m_dbtrAgt = data;
	}
		
	/**
	 * Get the embedded DbtrAgtAcct element.
	 * @return the item.
	 */
	public CashAccount24 getDbtrAgtAcct() {
		return m_dbtrAgtAcct;
	}
		
	/**
	 * This method sets (overwrites) the element DbtrAgtAcct.
	 * @param data the item that needs to be added.
	 */
	void setDbtrAgtAcct(CashAccount24 data) {
		m_dbtrAgtAcct = data;
	}
		
	/**
	 * Get the embedded InstrForDbtrAgt element.
	 * @return the item.
	 */
	public String getInstrForDbtrAgt() {
		return m_instrForDbtrAgt;
	}
		
	/**
	 * This method sets (overwrites) the element InstrForDbtrAgt.
	 * @param data the item that needs to be added.
	 */
	void setInstrForDbtrAgt(String data) {
		m_instrForDbtrAgt = data;
	}
		
	/**
	 * Get the embedded RmtInf element.
	 * @return the item.
	 */
	public RemittanceInformation2 getRmtInf() {
		return m_rmtInf;
	}
		
	/**
	 * This method sets (overwrites) the element RmtInf.
	 * @param data the item that needs to be added.
	 */
	void setRmtInf(RemittanceInformation2 data) {
		m_rmtInf = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_pmtId, ((DirectDebitTransactionInformation15)that).m_pmtId))
			return false;
		
		if (!Compare.equals(m_pmtTpInf, ((DirectDebitTransactionInformation15)that).m_pmtTpInf))
			return false;
		
		if (!Compare.equals(m_intrBkSttlmAmt, ((DirectDebitTransactionInformation15)that).m_intrBkSttlmAmt))
			return false;
		
		if (!Compare.equals(m_intrBkSttlmDt, ((DirectDebitTransactionInformation15)that).m_intrBkSttlmDt))
			return false;
		
		if (!Compare.equals(m_sttlmPrty, ((DirectDebitTransactionInformation15)that).m_sttlmPrty))
			return false;
		
		if (!Compare.equals(m_sttlmTmReq, ((DirectDebitTransactionInformation15)that).m_sttlmTmReq))
			return false;
		
		if (!Compare.equals(m_ultmtDbtr, ((DirectDebitTransactionInformation15)that).m_ultmtDbtr))
			return false;
		
		if (!Compare.equals(m_dbtr, ((DirectDebitTransactionInformation15)that).m_dbtr))
			return false;
		
		if (!Compare.equals(m_dbtrAcct, ((DirectDebitTransactionInformation15)that).m_dbtrAcct))
			return false;
		
		if (!Compare.equals(m_dbtrAgt, ((DirectDebitTransactionInformation15)that).m_dbtrAgt))
			return false;
		
		if (!Compare.equals(m_dbtrAgtAcct, ((DirectDebitTransactionInformation15)that).m_dbtrAgtAcct))
			return false;
		
		if (!Compare.equals(m_instrForDbtrAgt, ((DirectDebitTransactionInformation15)that).m_instrForDbtrAgt))
			return false;
		
		if (!Compare.equals(m_rmtInf, ((DirectDebitTransactionInformation15)that).m_rmtInf))
			return false;
		
		return true;
	}	

  
  
}
