package com.ldx.sepa2016.pacs;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: PACS 
  Generation date: Mon Apr 25 12:08:28 CEST 2016 

******************************************************************************/

/* SAX 2.0 dependencies */
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/* Framework dependencies */
import com.ldx.xml.core.DataSetter;
import com.ldx.xml.core.ComplexDataType;
import com.ldx.xml.core.XMLEvent;
import com.ldx.xml.core.XMLFragmentHandler;


/**
 * GroupHeader50 handler class.
 *
 * @see GroupHeader50
 * 
 */
public class GroupHeader50Handler extends XMLFragmentHandler<GroupHeader50> {
	/**
	 * Proxy for GroupHeader50Handler.
	 */
	static class Proxy extends HandlerProxy<GroupHeader50> {
		/**
		 * Allocator for GroupHeader50Handler.
		 */
		private static class Allocator 
				extends HandlerProxy.Allocator<GroupHeader50> {			
			public XMLFragmentHandler<GroupHeader50> create(
					XMLReader reader
					, XMLFragmentHandler<?> handler
					, String elementName
					, DataSetter setter
					, boolean doProcess) {
				return new GroupHeader50Handler(
					reader
					, handler
					, elementName
					, GroupHeader50.getAllocator()
					, setter
					, doProcess);
			}
		}
		
		/**
		 * Constructor for Proxy.
		 *
		 * @param reader the XML reader
		 * @param parentH the parent XML handler class
		 * @param elementName the name of the XML Element (tag).
		 * @param pSetter the setter for the parent data
		 * @param doProcess indicates whether processing is active for this handler instance
		 */
		public Proxy(XMLReader reader, XMLFragmentHandler<?> parentH, String elementName,
				DataSetter pSetter, boolean doProcess) {
			super(reader, parentH, elementName, pSetter, new Allocator(), doProcess);
		}
	}
	
	/** Data setter class for Authstn element. */
	private class AuthstnSetter implements DataSetter {
		/** data target. */
		private GroupHeader50Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public AuthstnSetter(GroupHeader50Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setAuthstn((Authorisation1Choice) data);	
		}
	}	
	/** Data setter class for InstdAgt element. */
	private class InstdAgtSetter implements DataSetter {
		/** data target. */
		private GroupHeader50Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public InstdAgtSetter(GroupHeader50Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setInstdAgt((BranchAndFinancialInstitutionIdentification5) data);	
		}
	}	
	/** Data setter class for InstgAgt element. */
	private class InstgAgtSetter implements DataSetter {
		/** data target. */
		private GroupHeader50Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public InstgAgtSetter(GroupHeader50Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setInstgAgt((BranchAndFinancialInstitutionIdentification5) data);	
		}
	}	
	/** Data setter class for PmtTpInf element. */
	private class PmtTpInfSetter implements DataSetter {
		/** data target. */
		private GroupHeader50Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public PmtTpInfSetter(GroupHeader50Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setPmtTpInf((PaymentTypeInformation25) data);	
		}
	}	
	/** Data setter class for SttlmInf element. */
	private class SttlmInfSetter implements DataSetter {
		/** data target. */
		private GroupHeader50Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public SttlmInfSetter(GroupHeader50Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setSttlmInf((SettlementInstruction2) data);	
		}
	}	
	/** Data setter class for TtlIntrBkSttlmAmt element. */
	private class TtlIntrBkSttlmAmtSetter implements DataSetter {
		/** data target. */
		private GroupHeader50Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public TtlIntrBkSttlmAmtSetter(GroupHeader50Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setTtlIntrBkSttlmAmt((ActiveCurrencyAndAmount) data);	
		}
	}


	/**
	 * Constructor for handler class.
	 *
	 * @param reader the XML reader
	 * @param parentH the parent XML handler class
	 * @param elementName the name of the XML Element (tag)
	 * @param allocator the allocator for the data type
	 * @param pSetter the setter for the parent data
	 * @param doProcess indicates whether processing is active for this handler instance
	 */
	public GroupHeader50Handler(
			XMLReader reader
			, XMLFragmentHandler<?> parentH
			, String elementName
			, GroupHeader50.Allocator allocator
			, DataSetter pSetter
			, boolean doProcess) {
		super(reader, parentH, elementName, allocator, pSetter, doProcess);

		// code for linking children..
		registerHandler(
			new Authorisation1ChoiceHandler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of Authorisation1ChoiceHandler
				, "Authstn" // XML element name
				, doLink("Authstn") // linking to parent
					? new AuthstnSetter(this) // ON
					: null // OFF
				, doProcess("Authstn")) // processing active or not
				);
  
		registerHandler(
			new BranchAndFinancialInstitutionIdentification5Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of BranchAndFinancialInstitutionIdentification5Handler
				, "InstdAgt" // XML element name
				, doLink("InstdAgt") // linking to parent
					? new InstdAgtSetter(this) // ON
					: null // OFF
				, doProcess("InstdAgt")) // processing active or not
				);
  
		registerHandler(
			new BranchAndFinancialInstitutionIdentification5Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of BranchAndFinancialInstitutionIdentification5Handler
				, "InstgAgt" // XML element name
				, doLink("InstgAgt") // linking to parent
					? new InstgAgtSetter(this) // ON
					: null // OFF
				, doProcess("InstgAgt")) // processing active or not
				);
  
		registerHandler(
			new PaymentTypeInformation25Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of PaymentTypeInformation25Handler
				, "PmtTpInf" // XML element name
				, doLink("PmtTpInf") // linking to parent
					? new PmtTpInfSetter(this) // ON
					: null // OFF
				, doProcess("PmtTpInf")) // processing active or not
				);
  
		registerHandler(
			new SettlementInstruction2Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of SettlementInstruction2Handler
				, "SttlmInf" // XML element name
				, doLink("SttlmInf") // linking to parent
					? new SttlmInfSetter(this) // ON
					: null // OFF
				, doProcess("SttlmInf")) // processing active or not
				);
  
		registerHandler(
			new ActiveCurrencyAndAmountHandler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of ActiveCurrencyAndAmountHandler
				, "TtlIntrBkSttlmAmt" // XML element name
				, doLink("TtlIntrBkSttlmAmt") // linking to parent
					? new TtlIntrBkSttlmAmtSetter(this) // ON
					: null // OFF
				, doProcess("TtlIntrBkSttlmAmt")) // processing active or not
				);
  	}


	/**
	 * This method overrides the getData() of the super type, returning the more
	 * specific type.
	 * 
	 */
	@Override 
	public GroupHeader50 getData() {
		return (GroupHeader50)super.getData();
	}




	/**
	 * This method is called by the XML parser on an end of element event.
	 *
	 * If the localName == 'the name of the element that this handler handles' control is
	 * passed back to the parent handler (future events go there) and the data of
	 * the children of this handler is attached to the data associated to this handler.	
	 * 
	 * The data that is collected is connected to the parent data only if a parentDataSetter
	 * if found (getParentDataSetter()).
	 *
	 * If processing is enabled for a handler, the processor is called.
	 */
	@Override
	public void endElement(String uri, String localName, String name)
		throws SAXException {
		
		// collect data from children..
		if (localName.equals("BtchBookg")) {
			getData().setBtchBookg(getValue());
			getContents().reset();
		} else if (localName.equals("CreDtTm")) {
			getData().setCreDtTm(getValue());
			getContents().reset();
		} else if (localName.equals("CtrlSum")) {
			getData().setCtrlSum(getValue());
			getContents().reset();
		} else if (localName.equals("IntrBkSttlmDt")) {
			getData().setIntrBkSttlmDt(getValue());
			getContents().reset();
		} else if (localName.equals("MsgId")) {
			getData().setMsgId(getValue());
			getContents().reset();
		} else if (localName.equals("NbOfTxs")) {
			getData().setNbOfTxs(getValue());
			getContents().reset();
		} else if (localName.equals(getXMLElementName())) {
				
			// return control to parent handler..
			this.deactivate();
			
			// get content of this item..
			getData().setContent(this.getValue());
			
			// attach data to parent (if parent data setter is found)..
			DataSetter setter = getParentDataSetter();
			if (setter != null) {
				setter.set(getData());
			} 
			
			// process data if required..
			if (doProcess()) {
				process(XMLEvent.END);
			}
		}
		
	}	
}
