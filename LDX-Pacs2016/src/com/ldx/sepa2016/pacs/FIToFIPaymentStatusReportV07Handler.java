package com.ldx.sepa2016.pacs;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: PACS 
  Generation date: Mon Apr 25 12:08:27 CEST 2016 

******************************************************************************/

/* SAX 2.0 dependencies */
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/* Framework dependencies */
import com.ldx.xml.core.DataSetter;
import com.ldx.xml.core.ComplexDataType;
import com.ldx.xml.core.XMLEvent;
import com.ldx.xml.core.XMLFragmentHandler;


/**
 * FIToFIPaymentStatusReportV07 handler class.
 *
 * @see FIToFIPaymentStatusReportV07
 * 
 */
public class FIToFIPaymentStatusReportV07Handler extends XMLFragmentHandler<FIToFIPaymentStatusReportV07> {
	/**
	 * Proxy for FIToFIPaymentStatusReportV07Handler.
	 */
	static class Proxy extends HandlerProxy<FIToFIPaymentStatusReportV07> {
		/**
		 * Allocator for FIToFIPaymentStatusReportV07Handler.
		 */
		private static class Allocator 
				extends HandlerProxy.Allocator<FIToFIPaymentStatusReportV07> {			
			public XMLFragmentHandler<FIToFIPaymentStatusReportV07> create(
					XMLReader reader
					, XMLFragmentHandler<?> handler
					, String elementName
					, DataSetter setter
					, boolean doProcess) {
				return new FIToFIPaymentStatusReportV07Handler(
					reader
					, handler
					, elementName
					, FIToFIPaymentStatusReportV07.getAllocator()
					, setter
					, doProcess);
			}
		}
		
		/**
		 * Constructor for Proxy.
		 *
		 * @param reader the XML reader
		 * @param parentH the parent XML handler class
		 * @param elementName the name of the XML Element (tag).
		 * @param pSetter the setter for the parent data
		 * @param doProcess indicates whether processing is active for this handler instance
		 */
		public Proxy(XMLReader reader, XMLFragmentHandler<?> parentH, String elementName,
				DataSetter pSetter, boolean doProcess) {
			super(reader, parentH, elementName, pSetter, new Allocator(), doProcess);
		}
	}
	
	/** Data setter class for GrpHdr element. */
	private class GrpHdrSetter implements DataSetter {
		/** data target. */
		private FIToFIPaymentStatusReportV07Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public GrpHdrSetter(FIToFIPaymentStatusReportV07Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setGrpHdr((GroupHeader53) data);	
		}
	}	
	/** Data setter class for OrgnlGrpInfAndSts element. */
	private class OrgnlGrpInfAndStsSetter implements DataSetter {
		/** data target. */
		private FIToFIPaymentStatusReportV07Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public OrgnlGrpInfAndStsSetter(FIToFIPaymentStatusReportV07Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setOrgnlGrpInfAndSts((OriginalGroupHeader1) data);	
		}
	}	
	/** Data setter class for SplmtryData element. */
	private class SplmtryDataSetter implements DataSetter {
		/** data target. */
		private FIToFIPaymentStatusReportV07Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public SplmtryDataSetter(FIToFIPaymentStatusReportV07Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setSplmtryData((SupplementaryData1) data);	
		}
	}	
	/** Data setter class for TxInfAndSts element. */
	private class TxInfAndStsSetter implements DataSetter {
		/** data target. */
		private FIToFIPaymentStatusReportV07Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public TxInfAndStsSetter(FIToFIPaymentStatusReportV07Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setTxInfAndSts((PaymentTransaction63) data);	
		}
	}


	/**
	 * Constructor for handler class.
	 *
	 * @param reader the XML reader
	 * @param parentH the parent XML handler class
	 * @param elementName the name of the XML Element (tag)
	 * @param allocator the allocator for the data type
	 * @param pSetter the setter for the parent data
	 * @param doProcess indicates whether processing is active for this handler instance
	 */
	public FIToFIPaymentStatusReportV07Handler(
			XMLReader reader
			, XMLFragmentHandler<?> parentH
			, String elementName
			, FIToFIPaymentStatusReportV07.Allocator allocator
			, DataSetter pSetter
			, boolean doProcess) {
		super(reader, parentH, elementName, allocator, pSetter, doProcess);

		// code for linking children..
		registerHandler(
			new GroupHeader53Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of GroupHeader53Handler
				, "GrpHdr" // XML element name
				, doLink("GrpHdr") // linking to parent
					? new GrpHdrSetter(this) // ON
					: null // OFF
				, doProcess("GrpHdr")) // processing active or not
				);
  
		registerHandler(
			new OriginalGroupHeader1Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of OriginalGroupHeader1Handler
				, "OrgnlGrpInfAndSts" // XML element name
				, doLink("OrgnlGrpInfAndSts") // linking to parent
					? new OrgnlGrpInfAndStsSetter(this) // ON
					: null // OFF
				, doProcess("OrgnlGrpInfAndSts")) // processing active or not
				);
  
		registerHandler(
			new SupplementaryData1Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of SupplementaryData1Handler
				, "SplmtryData" // XML element name
				, doLink("SplmtryData") // linking to parent
					? new SplmtryDataSetter(this) // ON
					: null // OFF
				, doProcess("SplmtryData")) // processing active or not
				);
  
		registerHandler(
			new PaymentTransaction63Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of PaymentTransaction63Handler
				, "TxInfAndSts" // XML element name
				, doLink("TxInfAndSts") // linking to parent
					? new TxInfAndStsSetter(this) // ON
					: null // OFF
				, doProcess("TxInfAndSts")) // processing active or not
				);
  	}


	/**
	 * This method overrides the getData() of the super type, returning the more
	 * specific type.
	 * 
	 */
	@Override 
	public FIToFIPaymentStatusReportV07 getData() {
		return (FIToFIPaymentStatusReportV07)super.getData();
	}




	/**
	 * This method is called by the XML parser on an end of element event.
	 *
	 * If the localName == 'the name of the element that this handler handles' control is
	 * passed back to the parent handler (future events go there) and the data of
	 * the children of this handler is attached to the data associated to this handler.	
	 * 
	 * The data that is collected is connected to the parent data only if a parentDataSetter
	 * if found (getParentDataSetter()).
	 *
	 * If processing is enabled for a handler, the processor is called.
	 */
	@Override
	public void endElement(String uri, String localName, String name)
		throws SAXException {
		
		if (localName.equals(getXMLElementName())) {
				
			// return control to parent handler..
			this.deactivate();
			
			// get content of this item..
			getData().setContent(this.getValue());
			
			// attach data to parent (if parent data setter is found)..
			DataSetter setter = getParentDataSetter();
			if (setter != null) {
				setter.set(getData());
			} 
			
			// process data if required..
			if (doProcess()) {
				process(XMLEvent.END);
			}
		}
		
	}	
}
