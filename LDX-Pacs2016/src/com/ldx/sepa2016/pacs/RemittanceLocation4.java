package com.ldx.sepa2016.pacs;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: PACS 
  Generation date: Mon Apr 25 12:08:30 CEST 2016 

******************************************************************************/

import java.util.List;
import java.util.ArrayList;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * RemittanceLocation4 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class RemittanceLocation4 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for RemittanceLocation4.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public RemittanceLocation4(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type RemittanceLocation4.
	 */
	static class Allocator implements TypeAllocator<RemittanceLocation4> {
		/**
		 * method for getting a new instance of type RemittanceLocation4.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public RemittanceLocation4 newInstance(String elementName, ComplexDataType parent) {
			return new RemittanceLocation4(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for RmtId element. 
	 *  @serial
	 */	
	private String m_rmtId = null;
	
	/** list of RmtLctnDtls element. 
	 *  @serial
	 */	
	private List<RemittanceLocationDetails1> m_rmtLctnDtlsList = new ArrayList<RemittanceLocationDetails1>();
	
	/**
	 * Get the embedded RmtId element.
	 * @return the item.
	 */
	public String getRmtId() {
		return m_rmtId;
	}
		
	/**
	 * This method sets (overwrites) the element RmtId.
	 * @param data the item that needs to be added.
	 */
	void setRmtId(String data) {
		m_rmtId = data;
	}
		
	/**
	 * Get the embedded list of RmtLctnDtls elements.
	 * @return list of items.
	 */
	public List<RemittanceLocationDetails1> getRmtLctnDtlss() {
		return m_rmtLctnDtlsList;
	}
		
	/**
	 * This method adds data to the list of RmtLctnDtls.
	 * @param data the item that needs to be added.
	 */
	void setRmtLctnDtls(RemittanceLocationDetails1 data) {
		m_rmtLctnDtlsList.add(data);
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_rmtId, ((RemittanceLocation4)that).m_rmtId))
			return false;
		
		if (!Compare.equals(m_rmtLctnDtlsList, ((RemittanceLocation4)that).m_rmtLctnDtlsList))
			return false;
		
		return true;
	}	

  
  
}
