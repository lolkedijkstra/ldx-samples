package com.ldx.sepa2016.pacs.handlers;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: PACS 
  Generation date: Mon Apr 25 12:08:29 CEST 2016 

******************************************************************************/

import org.xml.sax.XMLReader;

import com.ldx.sepa2016.pacs.PaymentReturnV06Document;
import com.ldx.sepa2016.pacs.PaymentReturnV06DocumentHandler;

import com.ldx.xml.core.XMLMessageHandler;

/**
 * This class reads the XML document from an XML inputsource.
 *
 * This class is the entry point for the client application.
 */
public class PaymentReturnV06MessageHandler extends
		XMLMessageHandler<PaymentReturnV06Document> {
	
	/** root element. */	
	static final String ELEMENT_NAME = "Document";	
	
	/**
	 * Constructor.
	 *
	 * @see XMLMessageHandler XMLMessageHandler
	 * @param reader
	 *            The (SAX) XML Reader object
	 */
	public PaymentReturnV06MessageHandler(XMLReader reader) {
		super(reader
		, new PaymentReturnV06DocumentHandler(
			reader
			, null	// root has no parent
			, ELEMENT_NAME
			, PaymentReturnV06Document.getAllocator()
			, null	// not applicable for root
			, doProcess(ELEMENT_NAME))
		);
	}
}

