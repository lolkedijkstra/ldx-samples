package com.ldx.sepa2016.pacs;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: PACS 
  Generation date: Mon Apr 25 12:08:28 CEST 2016 

******************************************************************************/

/* SAX 2.0 dependencies */
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/* Framework dependencies */
import com.ldx.xml.core.DataSetter;
import com.ldx.xml.core.ComplexDataType;
import com.ldx.xml.core.XMLEvent;
import com.ldx.xml.core.XMLFragmentHandler;


/**
 * SettlementInstruction2 handler class.
 *
 * @see SettlementInstruction2
 * 
 */
public class SettlementInstruction2Handler extends XMLFragmentHandler<SettlementInstruction2> {
	/**
	 * Proxy for SettlementInstruction2Handler.
	 */
	static class Proxy extends HandlerProxy<SettlementInstruction2> {
		/**
		 * Allocator for SettlementInstruction2Handler.
		 */
		private static class Allocator 
				extends HandlerProxy.Allocator<SettlementInstruction2> {			
			public XMLFragmentHandler<SettlementInstruction2> create(
					XMLReader reader
					, XMLFragmentHandler<?> handler
					, String elementName
					, DataSetter setter
					, boolean doProcess) {
				return new SettlementInstruction2Handler(
					reader
					, handler
					, elementName
					, SettlementInstruction2.getAllocator()
					, setter
					, doProcess);
			}
		}
		
		/**
		 * Constructor for Proxy.
		 *
		 * @param reader the XML reader
		 * @param parentH the parent XML handler class
		 * @param elementName the name of the XML Element (tag).
		 * @param pSetter the setter for the parent data
		 * @param doProcess indicates whether processing is active for this handler instance
		 */
		public Proxy(XMLReader reader, XMLFragmentHandler<?> parentH, String elementName,
				DataSetter pSetter, boolean doProcess) {
			super(reader, parentH, elementName, pSetter, new Allocator(), doProcess);
		}
	}
	
	/** Data setter class for ClrSys element. */
	private class ClrSysSetter implements DataSetter {
		/** data target. */
		private SettlementInstruction2Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public ClrSysSetter(SettlementInstruction2Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setClrSys((ClearingSystemIdentification3Choice) data);	
		}
	}	
	/** Data setter class for SttlmAcct element. */
	private class SttlmAcctSetter implements DataSetter {
		/** data target. */
		private SettlementInstruction2Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public SttlmAcctSetter(SettlementInstruction2Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setSttlmAcct((CashAccount24) data);	
		}
	}


	/**
	 * Constructor for handler class.
	 *
	 * @param reader the XML reader
	 * @param parentH the parent XML handler class
	 * @param elementName the name of the XML Element (tag)
	 * @param allocator the allocator for the data type
	 * @param pSetter the setter for the parent data
	 * @param doProcess indicates whether processing is active for this handler instance
	 */
	public SettlementInstruction2Handler(
			XMLReader reader
			, XMLFragmentHandler<?> parentH
			, String elementName
			, SettlementInstruction2.Allocator allocator
			, DataSetter pSetter
			, boolean doProcess) {
		super(reader, parentH, elementName, allocator, pSetter, doProcess);

		// code for linking children..
		registerHandler(
			new ClearingSystemIdentification3ChoiceHandler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of ClearingSystemIdentification3ChoiceHandler
				, "ClrSys" // XML element name
				, doLink("ClrSys") // linking to parent
					? new ClrSysSetter(this) // ON
					: null // OFF
				, doProcess("ClrSys")) // processing active or not
				);
  
		registerHandler(
			new CashAccount24Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of CashAccount24Handler
				, "SttlmAcct" // XML element name
				, doLink("SttlmAcct") // linking to parent
					? new SttlmAcctSetter(this) // ON
					: null // OFF
				, doProcess("SttlmAcct")) // processing active or not
				);
  	}


	/**
	 * This method overrides the getData() of the super type, returning the more
	 * specific type.
	 * 
	 */
	@Override 
	public SettlementInstruction2 getData() {
		return (SettlementInstruction2)super.getData();
	}




	/**
	 * This method is called by the XML parser on an end of element event.
	 *
	 * If the localName == 'the name of the element that this handler handles' control is
	 * passed back to the parent handler (future events go there) and the data of
	 * the children of this handler is attached to the data associated to this handler.	
	 * 
	 * The data that is collected is connected to the parent data only if a parentDataSetter
	 * if found (getParentDataSetter()).
	 *
	 * If processing is enabled for a handler, the processor is called.
	 */
	@Override
	public void endElement(String uri, String localName, String name)
		throws SAXException {
		
		// collect data from children..
		if (localName.equals("SttlmMtd")) {
			getData().setSttlmMtd(getValue());
			getContents().reset();
		} else if (localName.equals(getXMLElementName())) {
				
			// return control to parent handler..
			this.deactivate();
			
			// get content of this item..
			getData().setContent(this.getValue());
			
			// attach data to parent (if parent data setter is found)..
			DataSetter setter = getParentDataSetter();
			if (setter != null) {
				setter.set(getData());
			} 
			
			// process data if required..
			if (doProcess()) {
				process(XMLEvent.END);
			}
		}
		
	}	
}
