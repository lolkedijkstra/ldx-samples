package com.ldx.sepa2016.pacs.application;

/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: PACS 
  Generation date: Mon Apr 25 12:08:30 CEST 2016 

******************************************************************************/

//----------------------- 		IO		-----------------------//
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringWriter;
//-----------------------    LOGGING	-----------------------//
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
//-----------------------    	SAX		-----------------------//
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
//-----------------------    	LDX		-----------------------//
import com.ldx.xml.core.MessageHandler;
import com.ldx.xml.core.ParserConfiguration;
import com.ldx.xml.core.ProcessorException;
import com.ldx.xml.parser.ParserApplication;
import com.ldx.sepa2016.pacs.handlers.FinancialInstitutionCreditTransferV06MessageHandler;
import com.ldx.sepa2016.pacs.processor.FinancialInstitutionCreditTransferV06Processor;

/**
 * An example implementation of a parser application.
 * You will need to adapt this to meet your specific requirements.
 * This example demonstrates:
 * - the glue code that connects reader and processor
 * - how you can customize error handling
 *
 * The application uses arguments passed on the command line, however
 * you can connect any class derived from java.io.InputStream.
 */
public class FinancialInstitutionCreditTransferV06Application extends ParserApplication {
	
	/**
	 *  Constructor of the application.
	 *  You can use the call to super to pass a parameter to a custom ErrorHandler implementation
	 *  simply implement the interface: org.xml.sax.ErrorHandler, instantiate the custom ErrorHandler
	 *  and pass it to the super constructor.
	 *  @throws org.xml.sax.SAXException
	 */
	protected FinancialInstitutionCreditTransferV06Application() throws SAXException {
		super();
	}

	@Override
	protected MessageHandler getMessageHandler(XMLReader reader) {
		return new FinancialInstitutionCreditTransferV06MessageHandler(reader);
	}

	/** command line parameters */
	private final static String usage = "parameters:\n\t(1): xml input \n\t(2): config \n\t(3): schema";

	/** print command line usage */
	private static void usage() {
		System.out.println(usage);
		System.exit(0);
	}


	/**
	 * Entry point of the application.
	 * @param args args[0] = XML, args[1] = runtime configuration, args[2] = XML Schema
	 */
	public static void main(String[] args) {
		if (args.length < 2) {
			usage();
		}

		// get program arguments
		final String xml = args[0];
		final String config = args[1];
		final String schema = args.length >= 3 ? args[2] : null;
		
		final String logfile = "logging.txt";
		final Level level = Level.FINE;

		// initialize logger component
		Logger log = Logger.getLogger(FinancialInstitutionCreditTransferV06Application.class.getName());
		log.setLevel(level);
		
		// add logging handler
		try {
			Handler handler = new FileHandler(logfile);
			handler.setFormatter(new SimpleFormatter());
			handler.setLevel(level);
			log.addHandler(handler);
		} catch (SecurityException | IOException e) {
			log.severe(String.format("Unable to add LOGGING handler: %s. (\n\tmessage: %s\n\tcause: %s)"
					, logfile
					, e.getMessage()
					, e.getCause()));
		}

		try {
			log.info("Starting application.");
			
			// load runtime configuration
			log.fine("Loading runtime configuration");
			ParserConfiguration.instance().load(config);
			
			// log the configuration
			StringWriter writer = new StringWriter();
			ParserConfiguration.instance().write(writer, config);
			log.fine(writer.getBuffer().toString());
			writer.close();

			// create the application object
			ParserApplication app = new FinancialInstitutionCreditTransferV06Application();
			
			// validate (optional)
			if (schema != null) {
				log.fine(String.format("Validating %s against %s", xml, schema));
				app.validateXML(new FileInputStream(xml), new FileInputStream(schema));
			}
			
			// process the XML file
			log.info("Start Processing..");	
			app.processXML( new FileInputStream(xml), new FinancialInstitutionCreditTransferV06Processor());
			log.info("Processing complete.");
			
		} catch (ProcessorException e) {
			log.severe(String.format("Execution aborted due to PROCESSING error (\n\tmessage: %s\n\tcause: %s)"
						, e.getMessage()
						, e.getCause()));
		} catch (SAXException | IOException e) {
			log.severe(e.getMessage());
		} 
	}	 
}
