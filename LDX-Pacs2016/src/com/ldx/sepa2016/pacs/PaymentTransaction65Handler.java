package com.ldx.sepa2016.pacs;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: PACS 
  Generation date: Mon Apr 25 12:08:29 CEST 2016 

******************************************************************************/

/* SAX 2.0 dependencies */
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/* Framework dependencies */
import com.ldx.xml.core.DataSetter;
import com.ldx.xml.core.ComplexDataType;
import com.ldx.xml.core.XMLEvent;
import com.ldx.xml.core.XMLFragmentHandler;


/**
 * PaymentTransaction65 handler class.
 *
 * @see PaymentTransaction65
 * 
 */
public class PaymentTransaction65Handler extends XMLFragmentHandler<PaymentTransaction65> {
	/**
	 * Proxy for PaymentTransaction65Handler.
	 */
	static class Proxy extends HandlerProxy<PaymentTransaction65> {
		/**
		 * Allocator for PaymentTransaction65Handler.
		 */
		private static class Allocator 
				extends HandlerProxy.Allocator<PaymentTransaction65> {			
			public XMLFragmentHandler<PaymentTransaction65> create(
					XMLReader reader
					, XMLFragmentHandler<?> handler
					, String elementName
					, DataSetter setter
					, boolean doProcess) {
				return new PaymentTransaction65Handler(
					reader
					, handler
					, elementName
					, PaymentTransaction65.getAllocator()
					, setter
					, doProcess);
			}
		}
		
		/**
		 * Constructor for Proxy.
		 *
		 * @param reader the XML reader
		 * @param parentH the parent XML handler class
		 * @param elementName the name of the XML Element (tag).
		 * @param pSetter the setter for the parent data
		 * @param doProcess indicates whether processing is active for this handler instance
		 */
		public Proxy(XMLReader reader, XMLFragmentHandler<?> parentH, String elementName,
				DataSetter pSetter, boolean doProcess) {
			super(reader, parentH, elementName, pSetter, new Allocator(), doProcess);
		}
	}
	
	/** Data setter class for ChrgsInf element. */
	private class ChrgsInfSetter implements DataSetter {
		/** data target. */
		private PaymentTransaction65Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public ChrgsInfSetter(PaymentTransaction65Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setChrgsInf((Charges2) data);	
		}
	}	
	/** Data setter class for CompstnAmt element. */
	private class CompstnAmtSetter implements DataSetter {
		/** data target. */
		private PaymentTransaction65Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public CompstnAmtSetter(PaymentTransaction65Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setCompstnAmt((ActiveOrHistoricCurrencyAndAmount) data);	
		}
	}	
	/** Data setter class for InstdAgt element. */
	private class InstdAgtSetter implements DataSetter {
		/** data target. */
		private PaymentTransaction65Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public InstdAgtSetter(PaymentTransaction65Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setInstdAgt((BranchAndFinancialInstitutionIdentification5) data);	
		}
	}	
	/** Data setter class for InstgAgt element. */
	private class InstgAgtSetter implements DataSetter {
		/** data target. */
		private PaymentTransaction65Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public InstgAgtSetter(PaymentTransaction65Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setInstgAgt((BranchAndFinancialInstitutionIdentification5) data);	
		}
	}	
	/** Data setter class for OrgnlGrpInf element. */
	private class OrgnlGrpInfSetter implements DataSetter {
		/** data target. */
		private PaymentTransaction65Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public OrgnlGrpInfSetter(PaymentTransaction65Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setOrgnlGrpInf((OriginalGroupInformation3) data);	
		}
	}	
	/** Data setter class for OrgnlIntrBkSttlmAmt element. */
	private class OrgnlIntrBkSttlmAmtSetter implements DataSetter {
		/** data target. */
		private PaymentTransaction65Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public OrgnlIntrBkSttlmAmtSetter(PaymentTransaction65Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setOrgnlIntrBkSttlmAmt((ActiveOrHistoricCurrencyAndAmount) data);	
		}
	}	
	/** Data setter class for OrgnlTxRef element. */
	private class OrgnlTxRefSetter implements DataSetter {
		/** data target. */
		private PaymentTransaction65Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public OrgnlTxRefSetter(PaymentTransaction65Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setOrgnlTxRef((OriginalTransactionReference22) data);	
		}
	}	
	/** Data setter class for RtrdInstdAmt element. */
	private class RtrdInstdAmtSetter implements DataSetter {
		/** data target. */
		private PaymentTransaction65Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public RtrdInstdAmtSetter(PaymentTransaction65Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setRtrdInstdAmt((ActiveOrHistoricCurrencyAndAmount) data);	
		}
	}	
	/** Data setter class for RtrdIntrBkSttlmAmt element. */
	private class RtrdIntrBkSttlmAmtSetter implements DataSetter {
		/** data target. */
		private PaymentTransaction65Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public RtrdIntrBkSttlmAmtSetter(PaymentTransaction65Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setRtrdIntrBkSttlmAmt((ActiveCurrencyAndAmount) data);	
		}
	}	
	/** Data setter class for RtrRsnInf element. */
	private class RtrRsnInfSetter implements DataSetter {
		/** data target. */
		private PaymentTransaction65Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public RtrRsnInfSetter(PaymentTransaction65Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setRtrRsnInf((PaymentReturnReason1) data);	
		}
	}	
	/** Data setter class for SplmtryData element. */
	private class SplmtryDataSetter implements DataSetter {
		/** data target. */
		private PaymentTransaction65Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public SplmtryDataSetter(PaymentTransaction65Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setSplmtryData((SupplementaryData1) data);	
		}
	}


	/**
	 * Constructor for handler class.
	 *
	 * @param reader the XML reader
	 * @param parentH the parent XML handler class
	 * @param elementName the name of the XML Element (tag)
	 * @param allocator the allocator for the data type
	 * @param pSetter the setter for the parent data
	 * @param doProcess indicates whether processing is active for this handler instance
	 */
	public PaymentTransaction65Handler(
			XMLReader reader
			, XMLFragmentHandler<?> parentH
			, String elementName
			, PaymentTransaction65.Allocator allocator
			, DataSetter pSetter
			, boolean doProcess) {
		super(reader, parentH, elementName, allocator, pSetter, doProcess);

		// code for linking children..
		registerHandler(
			new Charges2Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of Charges2Handler
				, "ChrgsInf" // XML element name
				, doLink("ChrgsInf") // linking to parent
					? new ChrgsInfSetter(this) // ON
					: null // OFF
				, doProcess("ChrgsInf")) // processing active or not
				);
  
		registerHandler(
			new ActiveOrHistoricCurrencyAndAmountHandler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of ActiveOrHistoricCurrencyAndAmountHandler
				, "CompstnAmt" // XML element name
				, doLink("CompstnAmt") // linking to parent
					? new CompstnAmtSetter(this) // ON
					: null // OFF
				, doProcess("CompstnAmt")) // processing active or not
				);
  
		registerHandler(
			new BranchAndFinancialInstitutionIdentification5Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of BranchAndFinancialInstitutionIdentification5Handler
				, "InstdAgt" // XML element name
				, doLink("InstdAgt") // linking to parent
					? new InstdAgtSetter(this) // ON
					: null // OFF
				, doProcess("InstdAgt")) // processing active or not
				);
  
		registerHandler(
			new BranchAndFinancialInstitutionIdentification5Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of BranchAndFinancialInstitutionIdentification5Handler
				, "InstgAgt" // XML element name
				, doLink("InstgAgt") // linking to parent
					? new InstgAgtSetter(this) // ON
					: null // OFF
				, doProcess("InstgAgt")) // processing active or not
				);
  
		registerHandler(
			new OriginalGroupInformation3Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of OriginalGroupInformation3Handler
				, "OrgnlGrpInf" // XML element name
				, doLink("OrgnlGrpInf") // linking to parent
					? new OrgnlGrpInfSetter(this) // ON
					: null // OFF
				, doProcess("OrgnlGrpInf")) // processing active or not
				);
  
		registerHandler(
			new ActiveOrHistoricCurrencyAndAmountHandler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of ActiveOrHistoricCurrencyAndAmountHandler
				, "OrgnlIntrBkSttlmAmt" // XML element name
				, doLink("OrgnlIntrBkSttlmAmt") // linking to parent
					? new OrgnlIntrBkSttlmAmtSetter(this) // ON
					: null // OFF
				, doProcess("OrgnlIntrBkSttlmAmt")) // processing active or not
				);
  
		registerHandler(
			new OriginalTransactionReference22Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of OriginalTransactionReference22Handler
				, "OrgnlTxRef" // XML element name
				, doLink("OrgnlTxRef") // linking to parent
					? new OrgnlTxRefSetter(this) // ON
					: null // OFF
				, doProcess("OrgnlTxRef")) // processing active or not
				);
  
		registerHandler(
			new ActiveOrHistoricCurrencyAndAmountHandler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of ActiveOrHistoricCurrencyAndAmountHandler
				, "RtrdInstdAmt" // XML element name
				, doLink("RtrdInstdAmt") // linking to parent
					? new RtrdInstdAmtSetter(this) // ON
					: null // OFF
				, doProcess("RtrdInstdAmt")) // processing active or not
				);
  
		registerHandler(
			new ActiveCurrencyAndAmountHandler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of ActiveCurrencyAndAmountHandler
				, "RtrdIntrBkSttlmAmt" // XML element name
				, doLink("RtrdIntrBkSttlmAmt") // linking to parent
					? new RtrdIntrBkSttlmAmtSetter(this) // ON
					: null // OFF
				, doProcess("RtrdIntrBkSttlmAmt")) // processing active or not
				);
  
		registerHandler(
			new PaymentReturnReason1Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of PaymentReturnReason1Handler
				, "RtrRsnInf" // XML element name
				, doLink("RtrRsnInf") // linking to parent
					? new RtrRsnInfSetter(this) // ON
					: null // OFF
				, doProcess("RtrRsnInf")) // processing active or not
				);
  
		registerHandler(
			new SupplementaryData1Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of SupplementaryData1Handler
				, "SplmtryData" // XML element name
				, doLink("SplmtryData") // linking to parent
					? new SplmtryDataSetter(this) // ON
					: null // OFF
				, doProcess("SplmtryData")) // processing active or not
				);
  	}


	/**
	 * This method overrides the getData() of the super type, returning the more
	 * specific type.
	 * 
	 */
	@Override 
	public PaymentTransaction65 getData() {
		return (PaymentTransaction65)super.getData();
	}




	/**
	 * This method is called by the XML parser on an end of element event.
	 *
	 * If the localName == 'the name of the element that this handler handles' control is
	 * passed back to the parent handler (future events go there) and the data of
	 * the children of this handler is attached to the data associated to this handler.	
	 * 
	 * The data that is collected is connected to the parent data only if a parentDataSetter
	 * if found (getParentDataSetter()).
	 *
	 * If processing is enabled for a handler, the processor is called.
	 */
	@Override
	public void endElement(String uri, String localName, String name)
		throws SAXException {
		
		// collect data from children..
		if (localName.equals("ChrgBr")) {
			getData().setChrgBr(getValue());
			getContents().reset();
		} else if (localName.equals("IntrBkSttlmDt")) {
			getData().setIntrBkSttlmDt(getValue());
			getContents().reset();
		} else if (localName.equals("OrgnlClrSysRef")) {
			getData().setOrgnlClrSysRef(getValue());
			getContents().reset();
		} else if (localName.equals("OrgnlEndToEndId")) {
			getData().setOrgnlEndToEndId(getValue());
			getContents().reset();
		} else if (localName.equals("OrgnlInstrId")) {
			getData().setOrgnlInstrId(getValue());
			getContents().reset();
		} else if (localName.equals("OrgnlTxId")) {
			getData().setOrgnlTxId(getValue());
			getContents().reset();
		} else if (localName.equals("RtrId")) {
			getData().setRtrId(getValue());
			getContents().reset();
		} else if (localName.equals("SttlmPrty")) {
			getData().setSttlmPrty(getValue());
			getContents().reset();
		} else if (localName.equals("XchgRate")) {
			getData().setXchgRate(getValue());
			getContents().reset();
		} else if (localName.equals(getXMLElementName())) {
				
			// return control to parent handler..
			this.deactivate();
			
			// get content of this item..
			getData().setContent(this.getValue());
			
			// attach data to parent (if parent data setter is found)..
			DataSetter setter = getParentDataSetter();
			if (setter != null) {
				setter.set(getData());
			} 
			
			// process data if required..
			if (doProcess()) {
				process(XMLEvent.END);
			}
		}
		
	}	
}
