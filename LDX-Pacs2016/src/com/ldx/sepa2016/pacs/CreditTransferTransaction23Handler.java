package com.ldx.sepa2016.pacs;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: PACS 
  Generation date: Mon Apr 25 12:08:30 CEST 2016 

******************************************************************************/

/* SAX 2.0 dependencies */
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/* Framework dependencies */
import com.ldx.xml.core.DataSetter;
import com.ldx.xml.core.ComplexDataType;
import com.ldx.xml.core.XMLEvent;
import com.ldx.xml.core.XMLFragmentHandler;


/**
 * CreditTransferTransaction23 handler class.
 *
 * @see CreditTransferTransaction23
 * 
 */
public class CreditTransferTransaction23Handler extends XMLFragmentHandler<CreditTransferTransaction23> {
	/**
	 * Proxy for CreditTransferTransaction23Handler.
	 */
	static class Proxy extends HandlerProxy<CreditTransferTransaction23> {
		/**
		 * Allocator for CreditTransferTransaction23Handler.
		 */
		private static class Allocator 
				extends HandlerProxy.Allocator<CreditTransferTransaction23> {			
			public XMLFragmentHandler<CreditTransferTransaction23> create(
					XMLReader reader
					, XMLFragmentHandler<?> handler
					, String elementName
					, DataSetter setter
					, boolean doProcess) {
				return new CreditTransferTransaction23Handler(
					reader
					, handler
					, elementName
					, CreditTransferTransaction23.getAllocator()
					, setter
					, doProcess);
			}
		}
		
		/**
		 * Constructor for Proxy.
		 *
		 * @param reader the XML reader
		 * @param parentH the parent XML handler class
		 * @param elementName the name of the XML Element (tag).
		 * @param pSetter the setter for the parent data
		 * @param doProcess indicates whether processing is active for this handler instance
		 */
		public Proxy(XMLReader reader, XMLFragmentHandler<?> parentH, String elementName,
				DataSetter pSetter, boolean doProcess) {
			super(reader, parentH, elementName, pSetter, new Allocator(), doProcess);
		}
	}
	
	/** Data setter class for Cdtr element. */
	private class CdtrSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction23Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public CdtrSetter(CreditTransferTransaction23Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setCdtr((BranchAndFinancialInstitutionIdentification5) data);	
		}
	}	
	/** Data setter class for CdtrAcct element. */
	private class CdtrAcctSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction23Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public CdtrAcctSetter(CreditTransferTransaction23Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setCdtrAcct((CashAccount24) data);	
		}
	}	
	/** Data setter class for CdtrAgt element. */
	private class CdtrAgtSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction23Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public CdtrAgtSetter(CreditTransferTransaction23Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setCdtrAgt((BranchAndFinancialInstitutionIdentification5) data);	
		}
	}	
	/** Data setter class for CdtrAgtAcct element. */
	private class CdtrAgtAcctSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction23Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public CdtrAgtAcctSetter(CreditTransferTransaction23Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setCdtrAgtAcct((CashAccount24) data);	
		}
	}	
	/** Data setter class for Dbtr element. */
	private class DbtrSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction23Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public DbtrSetter(CreditTransferTransaction23Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setDbtr((BranchAndFinancialInstitutionIdentification5) data);	
		}
	}	
	/** Data setter class for DbtrAcct element. */
	private class DbtrAcctSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction23Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public DbtrAcctSetter(CreditTransferTransaction23Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setDbtrAcct((CashAccount24) data);	
		}
	}	
	/** Data setter class for DbtrAgt element. */
	private class DbtrAgtSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction23Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public DbtrAgtSetter(CreditTransferTransaction23Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setDbtrAgt((BranchAndFinancialInstitutionIdentification5) data);	
		}
	}	
	/** Data setter class for DbtrAgtAcct element. */
	private class DbtrAgtAcctSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction23Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public DbtrAgtAcctSetter(CreditTransferTransaction23Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setDbtrAgtAcct((CashAccount24) data);	
		}
	}	
	/** Data setter class for InstdAgt element. */
	private class InstdAgtSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction23Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public InstdAgtSetter(CreditTransferTransaction23Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setInstdAgt((BranchAndFinancialInstitutionIdentification5) data);	
		}
	}	
	/** Data setter class for InstgAgt element. */
	private class InstgAgtSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction23Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public InstgAgtSetter(CreditTransferTransaction23Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setInstgAgt((BranchAndFinancialInstitutionIdentification5) data);	
		}
	}	
	/** Data setter class for InstrForCdtrAgt element. */
	private class InstrForCdtrAgtSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction23Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public InstrForCdtrAgtSetter(CreditTransferTransaction23Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setInstrForCdtrAgt((InstructionForCreditorAgent2) data);	
		}
	}	
	/** Data setter class for InstrForNxtAgt element. */
	private class InstrForNxtAgtSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction23Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public InstrForNxtAgtSetter(CreditTransferTransaction23Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setInstrForNxtAgt((InstructionForNextAgent1) data);	
		}
	}	
	/** Data setter class for IntrBkSttlmAmt element. */
	private class IntrBkSttlmAmtSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction23Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public IntrBkSttlmAmtSetter(CreditTransferTransaction23Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setIntrBkSttlmAmt((ActiveCurrencyAndAmount) data);	
		}
	}	
	/** Data setter class for IntrmyAgt1 element. */
	private class IntrmyAgt1Setter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction23Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public IntrmyAgt1Setter(CreditTransferTransaction23Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setIntrmyAgt1((BranchAndFinancialInstitutionIdentification5) data);	
		}
	}	
	/** Data setter class for IntrmyAgt1Acct element. */
	private class IntrmyAgt1AcctSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction23Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public IntrmyAgt1AcctSetter(CreditTransferTransaction23Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setIntrmyAgt1Acct((CashAccount24) data);	
		}
	}	
	/** Data setter class for IntrmyAgt2 element. */
	private class IntrmyAgt2Setter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction23Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public IntrmyAgt2Setter(CreditTransferTransaction23Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setIntrmyAgt2((BranchAndFinancialInstitutionIdentification5) data);	
		}
	}	
	/** Data setter class for IntrmyAgt2Acct element. */
	private class IntrmyAgt2AcctSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction23Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public IntrmyAgt2AcctSetter(CreditTransferTransaction23Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setIntrmyAgt2Acct((CashAccount24) data);	
		}
	}	
	/** Data setter class for IntrmyAgt3 element. */
	private class IntrmyAgt3Setter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction23Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public IntrmyAgt3Setter(CreditTransferTransaction23Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setIntrmyAgt3((BranchAndFinancialInstitutionIdentification5) data);	
		}
	}	
	/** Data setter class for IntrmyAgt3Acct element. */
	private class IntrmyAgt3AcctSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction23Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public IntrmyAgt3AcctSetter(CreditTransferTransaction23Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setIntrmyAgt3Acct((CashAccount24) data);	
		}
	}	
	/** Data setter class for PmtId element. */
	private class PmtIdSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction23Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public PmtIdSetter(CreditTransferTransaction23Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setPmtId((PaymentIdentification3) data);	
		}
	}	
	/** Data setter class for PmtTpInf element. */
	private class PmtTpInfSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction23Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public PmtTpInfSetter(CreditTransferTransaction23Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setPmtTpInf((PaymentTypeInformation21) data);	
		}
	}	
	/** Data setter class for PrvsInstgAgt element. */
	private class PrvsInstgAgtSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction23Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public PrvsInstgAgtSetter(CreditTransferTransaction23Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setPrvsInstgAgt((BranchAndFinancialInstitutionIdentification5) data);	
		}
	}	
	/** Data setter class for PrvsInstgAgtAcct element. */
	private class PrvsInstgAgtAcctSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction23Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public PrvsInstgAgtAcctSetter(CreditTransferTransaction23Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setPrvsInstgAgtAcct((CashAccount24) data);	
		}
	}	
	/** Data setter class for RmtInf element. */
	private class RmtInfSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction23Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public RmtInfSetter(CreditTransferTransaction23Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setRmtInf((RemittanceInformation2) data);	
		}
	}	
	/** Data setter class for SplmtryData element. */
	private class SplmtryDataSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction23Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public SplmtryDataSetter(CreditTransferTransaction23Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setSplmtryData((SupplementaryData1) data);	
		}
	}	
	/** Data setter class for SttlmTmIndctn element. */
	private class SttlmTmIndctnSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction23Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public SttlmTmIndctnSetter(CreditTransferTransaction23Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setSttlmTmIndctn((SettlementDateTimeIndication1) data);	
		}
	}	
	/** Data setter class for SttlmTmReq element. */
	private class SttlmTmReqSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction23Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public SttlmTmReqSetter(CreditTransferTransaction23Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setSttlmTmReq((SettlementTimeRequest2) data);	
		}
	}	
	/** Data setter class for UltmtCdtr element. */
	private class UltmtCdtrSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction23Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public UltmtCdtrSetter(CreditTransferTransaction23Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setUltmtCdtr((BranchAndFinancialInstitutionIdentification5) data);	
		}
	}	
	/** Data setter class for UltmtDbtr element. */
	private class UltmtDbtrSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction23Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public UltmtDbtrSetter(CreditTransferTransaction23Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setUltmtDbtr((BranchAndFinancialInstitutionIdentification5) data);	
		}
	}	
	/** Data setter class for UndrlygCstmrCdtTrf element. */
	private class UndrlygCstmrCdtTrfSetter implements DataSetter {
		/** data target. */
		private CreditTransferTransaction23Handler pHandler = null;
		
		/**
		 * Constructor.
		 * @param pHandler parent that needs to be updated
		 */
		public UndrlygCstmrCdtTrfSetter(CreditTransferTransaction23Handler pHandler) {
			this.pHandler = pHandler;
		}

		/** {@inheritDoc} */
		public void set(ComplexDataType data) {
			pHandler.getData().setUndrlygCstmrCdtTrf((CreditTransferTransaction24) data);	
		}
	}


	/**
	 * Constructor for handler class.
	 *
	 * @param reader the XML reader
	 * @param parentH the parent XML handler class
	 * @param elementName the name of the XML Element (tag)
	 * @param allocator the allocator for the data type
	 * @param pSetter the setter for the parent data
	 * @param doProcess indicates whether processing is active for this handler instance
	 */
	public CreditTransferTransaction23Handler(
			XMLReader reader
			, XMLFragmentHandler<?> parentH
			, String elementName
			, CreditTransferTransaction23.Allocator allocator
			, DataSetter pSetter
			, boolean doProcess) {
		super(reader, parentH, elementName, allocator, pSetter, doProcess);

		// code for linking children..
		registerHandler(
			new BranchAndFinancialInstitutionIdentification5Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of BranchAndFinancialInstitutionIdentification5Handler
				, "Cdtr" // XML element name
				, doLink("Cdtr") // linking to parent
					? new CdtrSetter(this) // ON
					: null // OFF
				, doProcess("Cdtr")) // processing active or not
				);
  
		registerHandler(
			new CashAccount24Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of CashAccount24Handler
				, "CdtrAcct" // XML element name
				, doLink("CdtrAcct") // linking to parent
					? new CdtrAcctSetter(this) // ON
					: null // OFF
				, doProcess("CdtrAcct")) // processing active or not
				);
  
		registerHandler(
			new BranchAndFinancialInstitutionIdentification5Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of BranchAndFinancialInstitutionIdentification5Handler
				, "CdtrAgt" // XML element name
				, doLink("CdtrAgt") // linking to parent
					? new CdtrAgtSetter(this) // ON
					: null // OFF
				, doProcess("CdtrAgt")) // processing active or not
				);
  
		registerHandler(
			new CashAccount24Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of CashAccount24Handler
				, "CdtrAgtAcct" // XML element name
				, doLink("CdtrAgtAcct") // linking to parent
					? new CdtrAgtAcctSetter(this) // ON
					: null // OFF
				, doProcess("CdtrAgtAcct")) // processing active or not
				);
  
		registerHandler(
			new BranchAndFinancialInstitutionIdentification5Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of BranchAndFinancialInstitutionIdentification5Handler
				, "Dbtr" // XML element name
				, doLink("Dbtr") // linking to parent
					? new DbtrSetter(this) // ON
					: null // OFF
				, doProcess("Dbtr")) // processing active or not
				);
  
		registerHandler(
			new CashAccount24Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of CashAccount24Handler
				, "DbtrAcct" // XML element name
				, doLink("DbtrAcct") // linking to parent
					? new DbtrAcctSetter(this) // ON
					: null // OFF
				, doProcess("DbtrAcct")) // processing active or not
				);
  
		registerHandler(
			new BranchAndFinancialInstitutionIdentification5Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of BranchAndFinancialInstitutionIdentification5Handler
				, "DbtrAgt" // XML element name
				, doLink("DbtrAgt") // linking to parent
					? new DbtrAgtSetter(this) // ON
					: null // OFF
				, doProcess("DbtrAgt")) // processing active or not
				);
  
		registerHandler(
			new CashAccount24Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of CashAccount24Handler
				, "DbtrAgtAcct" // XML element name
				, doLink("DbtrAgtAcct") // linking to parent
					? new DbtrAgtAcctSetter(this) // ON
					: null // OFF
				, doProcess("DbtrAgtAcct")) // processing active or not
				);
  
		registerHandler(
			new BranchAndFinancialInstitutionIdentification5Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of BranchAndFinancialInstitutionIdentification5Handler
				, "InstdAgt" // XML element name
				, doLink("InstdAgt") // linking to parent
					? new InstdAgtSetter(this) // ON
					: null // OFF
				, doProcess("InstdAgt")) // processing active or not
				);
  
		registerHandler(
			new BranchAndFinancialInstitutionIdentification5Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of BranchAndFinancialInstitutionIdentification5Handler
				, "InstgAgt" // XML element name
				, doLink("InstgAgt") // linking to parent
					? new InstgAgtSetter(this) // ON
					: null // OFF
				, doProcess("InstgAgt")) // processing active or not
				);
  
		registerHandler(
			new InstructionForCreditorAgent2Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of InstructionForCreditorAgent2Handler
				, "InstrForCdtrAgt" // XML element name
				, doLink("InstrForCdtrAgt") // linking to parent
					? new InstrForCdtrAgtSetter(this) // ON
					: null // OFF
				, doProcess("InstrForCdtrAgt")) // processing active or not
				);
  
		registerHandler(
			new InstructionForNextAgent1Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of InstructionForNextAgent1Handler
				, "InstrForNxtAgt" // XML element name
				, doLink("InstrForNxtAgt") // linking to parent
					? new InstrForNxtAgtSetter(this) // ON
					: null // OFF
				, doProcess("InstrForNxtAgt")) // processing active or not
				);
  
		registerHandler(
			new ActiveCurrencyAndAmountHandler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of ActiveCurrencyAndAmountHandler
				, "IntrBkSttlmAmt" // XML element name
				, doLink("IntrBkSttlmAmt") // linking to parent
					? new IntrBkSttlmAmtSetter(this) // ON
					: null // OFF
				, doProcess("IntrBkSttlmAmt")) // processing active or not
				);
  
		registerHandler(
			new BranchAndFinancialInstitutionIdentification5Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of BranchAndFinancialInstitutionIdentification5Handler
				, "IntrmyAgt1" // XML element name
				, doLink("IntrmyAgt1") // linking to parent
					? new IntrmyAgt1Setter(this) // ON
					: null // OFF
				, doProcess("IntrmyAgt1")) // processing active or not
				);
  
		registerHandler(
			new CashAccount24Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of CashAccount24Handler
				, "IntrmyAgt1Acct" // XML element name
				, doLink("IntrmyAgt1Acct") // linking to parent
					? new IntrmyAgt1AcctSetter(this) // ON
					: null // OFF
				, doProcess("IntrmyAgt1Acct")) // processing active or not
				);
  
		registerHandler(
			new BranchAndFinancialInstitutionIdentification5Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of BranchAndFinancialInstitutionIdentification5Handler
				, "IntrmyAgt2" // XML element name
				, doLink("IntrmyAgt2") // linking to parent
					? new IntrmyAgt2Setter(this) // ON
					: null // OFF
				, doProcess("IntrmyAgt2")) // processing active or not
				);
  
		registerHandler(
			new CashAccount24Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of CashAccount24Handler
				, "IntrmyAgt2Acct" // XML element name
				, doLink("IntrmyAgt2Acct") // linking to parent
					? new IntrmyAgt2AcctSetter(this) // ON
					: null // OFF
				, doProcess("IntrmyAgt2Acct")) // processing active or not
				);
  
		registerHandler(
			new BranchAndFinancialInstitutionIdentification5Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of BranchAndFinancialInstitutionIdentification5Handler
				, "IntrmyAgt3" // XML element name
				, doLink("IntrmyAgt3") // linking to parent
					? new IntrmyAgt3Setter(this) // ON
					: null // OFF
				, doProcess("IntrmyAgt3")) // processing active or not
				);
  
		registerHandler(
			new CashAccount24Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of CashAccount24Handler
				, "IntrmyAgt3Acct" // XML element name
				, doLink("IntrmyAgt3Acct") // linking to parent
					? new IntrmyAgt3AcctSetter(this) // ON
					: null // OFF
				, doProcess("IntrmyAgt3Acct")) // processing active or not
				);
  
		registerHandler(
			new PaymentIdentification3Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of PaymentIdentification3Handler
				, "PmtId" // XML element name
				, doLink("PmtId") // linking to parent
					? new PmtIdSetter(this) // ON
					: null // OFF
				, doProcess("PmtId")) // processing active or not
				);
  
		registerHandler(
			new PaymentTypeInformation21Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of PaymentTypeInformation21Handler
				, "PmtTpInf" // XML element name
				, doLink("PmtTpInf") // linking to parent
					? new PmtTpInfSetter(this) // ON
					: null // OFF
				, doProcess("PmtTpInf")) // processing active or not
				);
  
		registerHandler(
			new BranchAndFinancialInstitutionIdentification5Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of BranchAndFinancialInstitutionIdentification5Handler
				, "PrvsInstgAgt" // XML element name
				, doLink("PrvsInstgAgt") // linking to parent
					? new PrvsInstgAgtSetter(this) // ON
					: null // OFF
				, doProcess("PrvsInstgAgt")) // processing active or not
				);
  
		registerHandler(
			new CashAccount24Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of CashAccount24Handler
				, "PrvsInstgAgtAcct" // XML element name
				, doLink("PrvsInstgAgtAcct") // linking to parent
					? new PrvsInstgAgtAcctSetter(this) // ON
					: null // OFF
				, doProcess("PrvsInstgAgtAcct")) // processing active or not
				);
  
		registerHandler(
			new RemittanceInformation2Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of RemittanceInformation2Handler
				, "RmtInf" // XML element name
				, doLink("RmtInf") // linking to parent
					? new RmtInfSetter(this) // ON
					: null // OFF
				, doProcess("RmtInf")) // processing active or not
				);
  
		registerHandler(
			new SupplementaryData1Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of SupplementaryData1Handler
				, "SplmtryData" // XML element name
				, doLink("SplmtryData") // linking to parent
					? new SplmtryDataSetter(this) // ON
					: null // OFF
				, doProcess("SplmtryData")) // processing active or not
				);
  
		registerHandler(
			new SettlementDateTimeIndication1Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of SettlementDateTimeIndication1Handler
				, "SttlmTmIndctn" // XML element name
				, doLink("SttlmTmIndctn") // linking to parent
					? new SttlmTmIndctnSetter(this) // ON
					: null // OFF
				, doProcess("SttlmTmIndctn")) // processing active or not
				);
  
		registerHandler(
			new SettlementTimeRequest2Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of SettlementTimeRequest2Handler
				, "SttlmTmReq" // XML element name
				, doLink("SttlmTmReq") // linking to parent
					? new SttlmTmReqSetter(this) // ON
					: null // OFF
				, doProcess("SttlmTmReq")) // processing active or not
				);
  
		registerHandler(
			new BranchAndFinancialInstitutionIdentification5Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of BranchAndFinancialInstitutionIdentification5Handler
				, "UltmtCdtr" // XML element name
				, doLink("UltmtCdtr") // linking to parent
					? new UltmtCdtrSetter(this) // ON
					: null // OFF
				, doProcess("UltmtCdtr")) // processing active or not
				);
  
		registerHandler(
			new BranchAndFinancialInstitutionIdentification5Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of BranchAndFinancialInstitutionIdentification5Handler
				, "UltmtDbtr" // XML element name
				, doLink("UltmtDbtr") // linking to parent
					? new UltmtDbtrSetter(this) // ON
					: null // OFF
				, doProcess("UltmtDbtr")) // processing active or not
				);
  
		registerHandler(
			new CreditTransferTransaction24Handler.Proxy(
				reader	// XML reader
				, this	// 'this' is parent of CreditTransferTransaction24Handler
				, "UndrlygCstmrCdtTrf" // XML element name
				, doLink("UndrlygCstmrCdtTrf") // linking to parent
					? new UndrlygCstmrCdtTrfSetter(this) // ON
					: null // OFF
				, doProcess("UndrlygCstmrCdtTrf")) // processing active or not
				);
  	}


	/**
	 * This method overrides the getData() of the super type, returning the more
	 * specific type.
	 * 
	 */
	@Override 
	public CreditTransferTransaction23 getData() {
		return (CreditTransferTransaction23)super.getData();
	}




	/**
	 * This method is called by the XML parser on an end of element event.
	 *
	 * If the localName == 'the name of the element that this handler handles' control is
	 * passed back to the parent handler (future events go there) and the data of
	 * the children of this handler is attached to the data associated to this handler.	
	 * 
	 * The data that is collected is connected to the parent data only if a parentDataSetter
	 * if found (getParentDataSetter()).
	 *
	 * If processing is enabled for a handler, the processor is called.
	 */
	@Override
	public void endElement(String uri, String localName, String name)
		throws SAXException {
		
		// collect data from children..
		if (localName.equals("IntrBkSttlmDt")) {
			getData().setIntrBkSttlmDt(getValue());
			getContents().reset();
		} else if (localName.equals("SttlmPrty")) {
			getData().setSttlmPrty(getValue());
			getContents().reset();
		} else if (localName.equals(getXMLElementName())) {
				
			// return control to parent handler..
			this.deactivate();
			
			// get content of this item..
			getData().setContent(this.getValue());
			
			// attach data to parent (if parent data setter is found)..
			DataSetter setter = getParentDataSetter();
			if (setter != null) {
				setter.set(getData());
			} 
			
			// process data if required..
			if (doProcess()) {
				process(XMLEvent.END);
			}
		}
		
	}	
}
