package com.ldx.sepa2016.pacs;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: PACS 
  Generation date: Mon Apr 25 12:08:27 CEST 2016 

******************************************************************************/

import java.util.List;
import java.util.ArrayList;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * PaymentTransaction63 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class PaymentTransaction63 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for PaymentTransaction63.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public PaymentTransaction63(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type PaymentTransaction63.
	 */
	static class Allocator implements TypeAllocator<PaymentTransaction63> {
		/**
		 * method for getting a new instance of type PaymentTransaction63.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public PaymentTransaction63 newInstance(String elementName, ComplexDataType parent) {
			return new PaymentTransaction63(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for StsId element. 
	 *  @serial
	 */	
	private String m_stsId = null;
	
	/** element item for OrgnlGrpInf element. 
	 *  @serial
	 */	
	private OriginalGroupInformation3 m_orgnlGrpInf = null;
	
	/** element item for OrgnlInstrId element. 
	 *  @serial
	 */	
	private String m_orgnlInstrId = null;
	
	/** element item for OrgnlEndToEndId element. 
	 *  @serial
	 */	
	private String m_orgnlEndToEndId = null;
	
	/** element item for OrgnlTxId element. 
	 *  @serial
	 */	
	private String m_orgnlTxId = null;
	
	/** element item for TxSts element. 
	 *  @serial
	 */	
	private String m_txSts = null;
	
	/** list of StsRsnInf element. 
	 *  @serial
	 */	
	private List<StatusReasonInformation9> m_stsRsnInfList = new ArrayList<StatusReasonInformation9>();
	
	/** list of ChrgsInf element. 
	 *  @serial
	 */	
	private List<Charges2> m_chrgsInfList = new ArrayList<Charges2>();
	
	/** element item for AccptncDtTm element. 
	 *  @serial
	 */	
	private String m_accptncDtTm = null;
	
	/** element item for AcctSvcrRef element. 
	 *  @serial
	 */	
	private String m_acctSvcrRef = null;
	
	/** element item for ClrSysRef element. 
	 *  @serial
	 */	
	private String m_clrSysRef = null;
	
	/** element item for InstgAgt element. 
	 *  @serial
	 */	
	private BranchAndFinancialInstitutionIdentification5 m_instgAgt = null;
	
	/** element item for InstdAgt element. 
	 *  @serial
	 */	
	private BranchAndFinancialInstitutionIdentification5 m_instdAgt = null;
	
	/** element item for OrgnlTxRef element. 
	 *  @serial
	 */	
	private OriginalTransactionReference22 m_orgnlTxRef = null;
	
	/** list of SplmtryData element. 
	 *  @serial
	 */	
	private List<SupplementaryData1> m_splmtryDataList = new ArrayList<SupplementaryData1>();
	
	/**
	 * Get the embedded StsId element.
	 * @return the item.
	 */
	public String getStsId() {
		return m_stsId;
	}
		
	/**
	 * This method sets (overwrites) the element StsId.
	 * @param data the item that needs to be added.
	 */
	void setStsId(String data) {
		m_stsId = data;
	}
		
	/**
	 * Get the embedded OrgnlGrpInf element.
	 * @return the item.
	 */
	public OriginalGroupInformation3 getOrgnlGrpInf() {
		return m_orgnlGrpInf;
	}
		
	/**
	 * This method sets (overwrites) the element OrgnlGrpInf.
	 * @param data the item that needs to be added.
	 */
	void setOrgnlGrpInf(OriginalGroupInformation3 data) {
		m_orgnlGrpInf = data;
	}
		
	/**
	 * Get the embedded OrgnlInstrId element.
	 * @return the item.
	 */
	public String getOrgnlInstrId() {
		return m_orgnlInstrId;
	}
		
	/**
	 * This method sets (overwrites) the element OrgnlInstrId.
	 * @param data the item that needs to be added.
	 */
	void setOrgnlInstrId(String data) {
		m_orgnlInstrId = data;
	}
		
	/**
	 * Get the embedded OrgnlEndToEndId element.
	 * @return the item.
	 */
	public String getOrgnlEndToEndId() {
		return m_orgnlEndToEndId;
	}
		
	/**
	 * This method sets (overwrites) the element OrgnlEndToEndId.
	 * @param data the item that needs to be added.
	 */
	void setOrgnlEndToEndId(String data) {
		m_orgnlEndToEndId = data;
	}
		
	/**
	 * Get the embedded OrgnlTxId element.
	 * @return the item.
	 */
	public String getOrgnlTxId() {
		return m_orgnlTxId;
	}
		
	/**
	 * This method sets (overwrites) the element OrgnlTxId.
	 * @param data the item that needs to be added.
	 */
	void setOrgnlTxId(String data) {
		m_orgnlTxId = data;
	}
		
	/**
	 * Get the embedded TxSts element.
	 * @return the item.
	 */
	public String getTxSts() {
		return m_txSts;
	}
		
	/**
	 * This method sets (overwrites) the element TxSts.
	 * @param data the item that needs to be added.
	 */
	void setTxSts(String data) {
		m_txSts = data;
	}
		
	/**
	 * Get the embedded list of StsRsnInf elements.
	 * @return list of items.
	 */
	public List<StatusReasonInformation9> getStsRsnInfs() {
		return m_stsRsnInfList;
	}
		
	/**
	 * This method adds data to the list of StsRsnInf.
	 * @param data the item that needs to be added.
	 */
	void setStsRsnInf(StatusReasonInformation9 data) {
		m_stsRsnInfList.add(data);
	}
		
	/**
	 * Get the embedded list of ChrgsInf elements.
	 * @return list of items.
	 */
	public List<Charges2> getChrgsInfs() {
		return m_chrgsInfList;
	}
		
	/**
	 * This method adds data to the list of ChrgsInf.
	 * @param data the item that needs to be added.
	 */
	void setChrgsInf(Charges2 data) {
		m_chrgsInfList.add(data);
	}
		
	/**
	 * Get the embedded AccptncDtTm element.
	 * @return the item.
	 */
	public String getAccptncDtTm() {
		return m_accptncDtTm;
	}
		
	/**
	 * This method sets (overwrites) the element AccptncDtTm.
	 * @param data the item that needs to be added.
	 */
	void setAccptncDtTm(String data) {
		m_accptncDtTm = data;
	}
		
	/**
	 * Get the embedded AcctSvcrRef element.
	 * @return the item.
	 */
	public String getAcctSvcrRef() {
		return m_acctSvcrRef;
	}
		
	/**
	 * This method sets (overwrites) the element AcctSvcrRef.
	 * @param data the item that needs to be added.
	 */
	void setAcctSvcrRef(String data) {
		m_acctSvcrRef = data;
	}
		
	/**
	 * Get the embedded ClrSysRef element.
	 * @return the item.
	 */
	public String getClrSysRef() {
		return m_clrSysRef;
	}
		
	/**
	 * This method sets (overwrites) the element ClrSysRef.
	 * @param data the item that needs to be added.
	 */
	void setClrSysRef(String data) {
		m_clrSysRef = data;
	}
		
	/**
	 * Get the embedded InstgAgt element.
	 * @return the item.
	 */
	public BranchAndFinancialInstitutionIdentification5 getInstgAgt() {
		return m_instgAgt;
	}
		
	/**
	 * This method sets (overwrites) the element InstgAgt.
	 * @param data the item that needs to be added.
	 */
	void setInstgAgt(BranchAndFinancialInstitutionIdentification5 data) {
		m_instgAgt = data;
	}
		
	/**
	 * Get the embedded InstdAgt element.
	 * @return the item.
	 */
	public BranchAndFinancialInstitutionIdentification5 getInstdAgt() {
		return m_instdAgt;
	}
		
	/**
	 * This method sets (overwrites) the element InstdAgt.
	 * @param data the item that needs to be added.
	 */
	void setInstdAgt(BranchAndFinancialInstitutionIdentification5 data) {
		m_instdAgt = data;
	}
		
	/**
	 * Get the embedded OrgnlTxRef element.
	 * @return the item.
	 */
	public OriginalTransactionReference22 getOrgnlTxRef() {
		return m_orgnlTxRef;
	}
		
	/**
	 * This method sets (overwrites) the element OrgnlTxRef.
	 * @param data the item that needs to be added.
	 */
	void setOrgnlTxRef(OriginalTransactionReference22 data) {
		m_orgnlTxRef = data;
	}
		
	/**
	 * Get the embedded list of SplmtryData elements.
	 * @return list of items.
	 */
	public List<SupplementaryData1> getSplmtryDatas() {
		return m_splmtryDataList;
	}
		
	/**
	 * This method adds data to the list of SplmtryData.
	 * @param data the item that needs to be added.
	 */
	void setSplmtryData(SupplementaryData1 data) {
		m_splmtryDataList.add(data);
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_stsId, ((PaymentTransaction63)that).m_stsId))
			return false;
		
		if (!Compare.equals(m_orgnlGrpInf, ((PaymentTransaction63)that).m_orgnlGrpInf))
			return false;
		
		if (!Compare.equals(m_orgnlInstrId, ((PaymentTransaction63)that).m_orgnlInstrId))
			return false;
		
		if (!Compare.equals(m_orgnlEndToEndId, ((PaymentTransaction63)that).m_orgnlEndToEndId))
			return false;
		
		if (!Compare.equals(m_orgnlTxId, ((PaymentTransaction63)that).m_orgnlTxId))
			return false;
		
		if (!Compare.equals(m_txSts, ((PaymentTransaction63)that).m_txSts))
			return false;
		
		if (!Compare.equals(m_stsRsnInfList, ((PaymentTransaction63)that).m_stsRsnInfList))
			return false;
		
		if (!Compare.equals(m_chrgsInfList, ((PaymentTransaction63)that).m_chrgsInfList))
			return false;
		
		if (!Compare.equals(m_accptncDtTm, ((PaymentTransaction63)that).m_accptncDtTm))
			return false;
		
		if (!Compare.equals(m_acctSvcrRef, ((PaymentTransaction63)that).m_acctSvcrRef))
			return false;
		
		if (!Compare.equals(m_clrSysRef, ((PaymentTransaction63)that).m_clrSysRef))
			return false;
		
		if (!Compare.equals(m_instgAgt, ((PaymentTransaction63)that).m_instgAgt))
			return false;
		
		if (!Compare.equals(m_instdAgt, ((PaymentTransaction63)that).m_instdAgt))
			return false;
		
		if (!Compare.equals(m_orgnlTxRef, ((PaymentTransaction63)that).m_orgnlTxRef))
			return false;
		
		if (!Compare.equals(m_splmtryDataList, ((PaymentTransaction63)that).m_splmtryDataList))
			return false;
		
		return true;
	}	

  
  
}
