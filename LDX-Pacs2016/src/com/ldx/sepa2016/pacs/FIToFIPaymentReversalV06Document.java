package com.ldx.sepa2016.pacs;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: PACS 
  Generation date: Mon Apr 25 12:08:29 CEST 2016 

******************************************************************************/

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * FIToFIPaymentReversalV06Document data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class FIToFIPaymentReversalV06Document extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for FIToFIPaymentReversalV06Document.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public FIToFIPaymentReversalV06Document(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type FIToFIPaymentReversalV06Document.
	 */
	static class Allocator implements TypeAllocator<FIToFIPaymentReversalV06Document> {
		/**
		 * method for getting a new instance of type FIToFIPaymentReversalV06Document.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public FIToFIPaymentReversalV06Document newInstance(String elementName, ComplexDataType parent) {
			return new FIToFIPaymentReversalV06Document(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for FIToFIPmtRvsl element. 
	 *  @serial
	 */	
	private FIToFIPaymentReversalV06 m_fIToFIPmtRvsl = null;
	
	/**
	 * Get the embedded FIToFIPmtRvsl element.
	 * @return the item.
	 */
	public FIToFIPaymentReversalV06 getFIToFIPmtRvsl() {
		return m_fIToFIPmtRvsl;
	}
		
	/**
	 * This method sets (overwrites) the element FIToFIPmtRvsl.
	 * @param data the item that needs to be added.
	 */
	void setFIToFIPmtRvsl(FIToFIPaymentReversalV06 data) {
		m_fIToFIPmtRvsl = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_fIToFIPmtRvsl, ((FIToFIPaymentReversalV06Document)that).m_fIToFIPmtRvsl))
			return false;
		
		return true;
	}	

  
  
}
