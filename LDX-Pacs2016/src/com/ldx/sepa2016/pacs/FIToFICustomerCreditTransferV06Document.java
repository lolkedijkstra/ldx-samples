package com.ldx.sepa2016.pacs;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: PACS 
  Generation date: Mon Apr 25 12:08:30 CEST 2016 

******************************************************************************/

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * FIToFICustomerCreditTransferV06Document data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class FIToFICustomerCreditTransferV06Document extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for FIToFICustomerCreditTransferV06Document.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public FIToFICustomerCreditTransferV06Document(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type FIToFICustomerCreditTransferV06Document.
	 */
	static class Allocator implements TypeAllocator<FIToFICustomerCreditTransferV06Document> {
		/**
		 * method for getting a new instance of type FIToFICustomerCreditTransferV06Document.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public FIToFICustomerCreditTransferV06Document newInstance(String elementName, ComplexDataType parent) {
			return new FIToFICustomerCreditTransferV06Document(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for FIToFICstmrCdtTrf element. 
	 *  @serial
	 */	
	private FIToFICustomerCreditTransferV06 m_fIToFICstmrCdtTrf = null;
	
	/**
	 * Get the embedded FIToFICstmrCdtTrf element.
	 * @return the item.
	 */
	public FIToFICustomerCreditTransferV06 getFIToFICstmrCdtTrf() {
		return m_fIToFICstmrCdtTrf;
	}
		
	/**
	 * This method sets (overwrites) the element FIToFICstmrCdtTrf.
	 * @param data the item that needs to be added.
	 */
	void setFIToFICstmrCdtTrf(FIToFICustomerCreditTransferV06 data) {
		m_fIToFICstmrCdtTrf = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_fIToFICstmrCdtTrf, ((FIToFICustomerCreditTransferV06Document)that).m_fIToFICstmrCdtTrf))
			return false;
		
		return true;
	}	

  
  
}
