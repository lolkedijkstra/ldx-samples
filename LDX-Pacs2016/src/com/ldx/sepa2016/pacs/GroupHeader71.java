package com.ldx.sepa2016.pacs;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: PACS 
  Generation date: Mon Apr 25 12:08:29 CEST 2016 

******************************************************************************/

import java.util.List;
import java.util.ArrayList;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * GroupHeader71 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class GroupHeader71 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for GroupHeader71.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public GroupHeader71(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type GroupHeader71.
	 */
	static class Allocator implements TypeAllocator<GroupHeader71> {
		/**
		 * method for getting a new instance of type GroupHeader71.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public GroupHeader71 newInstance(String elementName, ComplexDataType parent) {
			return new GroupHeader71(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for MsgId element. 
	 *  @serial
	 */	
	private String m_msgId = null;
	
	/** element item for CreDtTm element. 
	 *  @serial
	 */	
	private String m_creDtTm = null;
	
	/** list of Authstn element. 
	 *  @serial
	 */	
	private List<Authorisation1Choice> m_authstnList = new ArrayList<Authorisation1Choice>();
	
	/** element item for BtchBookg element. 
	 *  @serial
	 */	
	private String m_btchBookg = null;
	
	/** element item for NbOfTxs element. 
	 *  @serial
	 */	
	private String m_nbOfTxs = null;
	
	/** element item for CtrlSum element. 
	 *  @serial
	 */	
	private String m_ctrlSum = null;
	
	/** element item for GrpRvsl element. 
	 *  @serial
	 */	
	private String m_grpRvsl = null;
	
	/** element item for TtlRvsdIntrBkSttlmAmt element. 
	 *  @serial
	 */	
	private ActiveCurrencyAndAmount m_ttlRvsdIntrBkSttlmAmt = null;
	
	/** element item for IntrBkSttlmDt element. 
	 *  @serial
	 */	
	private String m_intrBkSttlmDt = null;
	
	/** element item for SttlmInf element. 
	 *  @serial
	 */	
	private SettlementInstruction4 m_sttlmInf = null;
	
	/** element item for InstgAgt element. 
	 *  @serial
	 */	
	private BranchAndFinancialInstitutionIdentification5 m_instgAgt = null;
	
	/** element item for InstdAgt element. 
	 *  @serial
	 */	
	private BranchAndFinancialInstitutionIdentification5 m_instdAgt = null;
	
	/**
	 * Get the embedded MsgId element.
	 * @return the item.
	 */
	public String getMsgId() {
		return m_msgId;
	}
		
	/**
	 * This method sets (overwrites) the element MsgId.
	 * @param data the item that needs to be added.
	 */
	void setMsgId(String data) {
		m_msgId = data;
	}
		
	/**
	 * Get the embedded CreDtTm element.
	 * @return the item.
	 */
	public String getCreDtTm() {
		return m_creDtTm;
	}
		
	/**
	 * This method sets (overwrites) the element CreDtTm.
	 * @param data the item that needs to be added.
	 */
	void setCreDtTm(String data) {
		m_creDtTm = data;
	}
		
	/**
	 * Get the embedded list of Authstn elements.
	 * @return list of items.
	 */
	public List<Authorisation1Choice> getAuthstns() {
		return m_authstnList;
	}
		
	/**
	 * This method adds data to the list of Authstn.
	 * @param data the item that needs to be added.
	 */
	void setAuthstn(Authorisation1Choice data) {
		m_authstnList.add(data);
	}
		
	/**
	 * Get the embedded BtchBookg element.
	 * @return the item.
	 */
	public String getBtchBookg() {
		return m_btchBookg;
	}
		
	/**
	 * This method sets (overwrites) the element BtchBookg.
	 * @param data the item that needs to be added.
	 */
	void setBtchBookg(String data) {
		m_btchBookg = data;
	}
		
	/**
	 * Get the embedded NbOfTxs element.
	 * @return the item.
	 */
	public String getNbOfTxs() {
		return m_nbOfTxs;
	}
		
	/**
	 * This method sets (overwrites) the element NbOfTxs.
	 * @param data the item that needs to be added.
	 */
	void setNbOfTxs(String data) {
		m_nbOfTxs = data;
	}
		
	/**
	 * Get the embedded CtrlSum element.
	 * @return the item.
	 */
	public String getCtrlSum() {
		return m_ctrlSum;
	}
		
	/**
	 * This method sets (overwrites) the element CtrlSum.
	 * @param data the item that needs to be added.
	 */
	void setCtrlSum(String data) {
		m_ctrlSum = data;
	}
		
	/**
	 * Get the embedded GrpRvsl element.
	 * @return the item.
	 */
	public String getGrpRvsl() {
		return m_grpRvsl;
	}
		
	/**
	 * This method sets (overwrites) the element GrpRvsl.
	 * @param data the item that needs to be added.
	 */
	void setGrpRvsl(String data) {
		m_grpRvsl = data;
	}
		
	/**
	 * Get the embedded TtlRvsdIntrBkSttlmAmt element.
	 * @return the item.
	 */
	public ActiveCurrencyAndAmount getTtlRvsdIntrBkSttlmAmt() {
		return m_ttlRvsdIntrBkSttlmAmt;
	}
		
	/**
	 * This method sets (overwrites) the element TtlRvsdIntrBkSttlmAmt.
	 * @param data the item that needs to be added.
	 */
	void setTtlRvsdIntrBkSttlmAmt(ActiveCurrencyAndAmount data) {
		m_ttlRvsdIntrBkSttlmAmt = data;
	}
		
	/**
	 * Get the embedded IntrBkSttlmDt element.
	 * @return the item.
	 */
	public String getIntrBkSttlmDt() {
		return m_intrBkSttlmDt;
	}
		
	/**
	 * This method sets (overwrites) the element IntrBkSttlmDt.
	 * @param data the item that needs to be added.
	 */
	void setIntrBkSttlmDt(String data) {
		m_intrBkSttlmDt = data;
	}
		
	/**
	 * Get the embedded SttlmInf element.
	 * @return the item.
	 */
	public SettlementInstruction4 getSttlmInf() {
		return m_sttlmInf;
	}
		
	/**
	 * This method sets (overwrites) the element SttlmInf.
	 * @param data the item that needs to be added.
	 */
	void setSttlmInf(SettlementInstruction4 data) {
		m_sttlmInf = data;
	}
		
	/**
	 * Get the embedded InstgAgt element.
	 * @return the item.
	 */
	public BranchAndFinancialInstitutionIdentification5 getInstgAgt() {
		return m_instgAgt;
	}
		
	/**
	 * This method sets (overwrites) the element InstgAgt.
	 * @param data the item that needs to be added.
	 */
	void setInstgAgt(BranchAndFinancialInstitutionIdentification5 data) {
		m_instgAgt = data;
	}
		
	/**
	 * Get the embedded InstdAgt element.
	 * @return the item.
	 */
	public BranchAndFinancialInstitutionIdentification5 getInstdAgt() {
		return m_instdAgt;
	}
		
	/**
	 * This method sets (overwrites) the element InstdAgt.
	 * @param data the item that needs to be added.
	 */
	void setInstdAgt(BranchAndFinancialInstitutionIdentification5 data) {
		m_instdAgt = data;
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_msgId, ((GroupHeader71)that).m_msgId))
			return false;
		
		if (!Compare.equals(m_creDtTm, ((GroupHeader71)that).m_creDtTm))
			return false;
		
		if (!Compare.equals(m_authstnList, ((GroupHeader71)that).m_authstnList))
			return false;
		
		if (!Compare.equals(m_btchBookg, ((GroupHeader71)that).m_btchBookg))
			return false;
		
		if (!Compare.equals(m_nbOfTxs, ((GroupHeader71)that).m_nbOfTxs))
			return false;
		
		if (!Compare.equals(m_ctrlSum, ((GroupHeader71)that).m_ctrlSum))
			return false;
		
		if (!Compare.equals(m_grpRvsl, ((GroupHeader71)that).m_grpRvsl))
			return false;
		
		if (!Compare.equals(m_ttlRvsdIntrBkSttlmAmt, ((GroupHeader71)that).m_ttlRvsdIntrBkSttlmAmt))
			return false;
		
		if (!Compare.equals(m_intrBkSttlmDt, ((GroupHeader71)that).m_intrBkSttlmDt))
			return false;
		
		if (!Compare.equals(m_sttlmInf, ((GroupHeader71)that).m_sttlmInf))
			return false;
		
		if (!Compare.equals(m_instgAgt, ((GroupHeader71)that).m_instgAgt))
			return false;
		
		if (!Compare.equals(m_instdAgt, ((GroupHeader71)that).m_instdAgt))
			return false;
		
		return true;
	}	

  
  
}
