package com.ldx.sepa2016.pacs;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: PACS 
  Generation date: Mon Apr 25 12:08:28 CEST 2016 

******************************************************************************/

import java.util.List;
import java.util.ArrayList;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * DirectDebitTransactionInformation20 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class DirectDebitTransactionInformation20 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for DirectDebitTransactionInformation20.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public DirectDebitTransactionInformation20(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type DirectDebitTransactionInformation20.
	 */
	static class Allocator implements TypeAllocator<DirectDebitTransactionInformation20> {
		/**
		 * method for getting a new instance of type DirectDebitTransactionInformation20.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public DirectDebitTransactionInformation20 newInstance(String elementName, ComplexDataType parent) {
			return new DirectDebitTransactionInformation20(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for PmtId element. 
	 *  @serial
	 */	
	private PaymentIdentification3 m_pmtId = null;
	
	/** element item for PmtTpInf element. 
	 *  @serial
	 */	
	private PaymentTypeInformation25 m_pmtTpInf = null;
	
	/** element item for IntrBkSttlmAmt element. 
	 *  @serial
	 */	
	private ActiveCurrencyAndAmount m_intrBkSttlmAmt = null;
	
	/** element item for IntrBkSttlmDt element. 
	 *  @serial
	 */	
	private String m_intrBkSttlmDt = null;
	
	/** element item for SttlmPrty element. 
	 *  @serial
	 */	
	private String m_sttlmPrty = null;
	
	/** element item for InstdAmt element. 
	 *  @serial
	 */	
	private ActiveOrHistoricCurrencyAndAmount m_instdAmt = null;
	
	/** element item for XchgRate element. 
	 *  @serial
	 */	
	private String m_xchgRate = null;
	
	/** element item for ChrgBr element. 
	 *  @serial
	 */	
	private String m_chrgBr = null;
	
	/** list of ChrgsInf element. 
	 *  @serial
	 */	
	private List<Charges2> m_chrgsInfList = new ArrayList<Charges2>();
	
	/** element item for ReqdColltnDt element. 
	 *  @serial
	 */	
	private String m_reqdColltnDt = null;
	
	/** element item for DrctDbtTx element. 
	 *  @serial
	 */	
	private DirectDebitTransaction8 m_drctDbtTx = null;
	
	/** element item for Cdtr element. 
	 *  @serial
	 */	
	private PartyIdentification43 m_cdtr = null;
	
	/** element item for CdtrAcct element. 
	 *  @serial
	 */	
	private CashAccount24 m_cdtrAcct = null;
	
	/** element item for CdtrAgt element. 
	 *  @serial
	 */	
	private BranchAndFinancialInstitutionIdentification5 m_cdtrAgt = null;
	
	/** element item for CdtrAgtAcct element. 
	 *  @serial
	 */	
	private CashAccount24 m_cdtrAgtAcct = null;
	
	/** element item for UltmtCdtr element. 
	 *  @serial
	 */	
	private PartyIdentification43 m_ultmtCdtr = null;
	
	/** element item for InitgPty element. 
	 *  @serial
	 */	
	private PartyIdentification43 m_initgPty = null;
	
	/** element item for InstgAgt element. 
	 *  @serial
	 */	
	private BranchAndFinancialInstitutionIdentification5 m_instgAgt = null;
	
	/** element item for InstdAgt element. 
	 *  @serial
	 */	
	private BranchAndFinancialInstitutionIdentification5 m_instdAgt = null;
	
	/** element item for IntrmyAgt1 element. 
	 *  @serial
	 */	
	private BranchAndFinancialInstitutionIdentification5 m_intrmyAgt1 = null;
	
	/** element item for IntrmyAgt1Acct element. 
	 *  @serial
	 */	
	private CashAccount24 m_intrmyAgt1Acct = null;
	
	/** element item for IntrmyAgt2 element. 
	 *  @serial
	 */	
	private BranchAndFinancialInstitutionIdentification5 m_intrmyAgt2 = null;
	
	/** element item for IntrmyAgt2Acct element. 
	 *  @serial
	 */	
	private CashAccount24 m_intrmyAgt2Acct = null;
	
	/** element item for IntrmyAgt3 element. 
	 *  @serial
	 */	
	private BranchAndFinancialInstitutionIdentification5 m_intrmyAgt3 = null;
	
	/** element item for IntrmyAgt3Acct element. 
	 *  @serial
	 */	
	private CashAccount24 m_intrmyAgt3Acct = null;
	
	/** element item for Dbtr element. 
	 *  @serial
	 */	
	private PartyIdentification43 m_dbtr = null;
	
	/** element item for DbtrAcct element. 
	 *  @serial
	 */	
	private CashAccount24 m_dbtrAcct = null;
	
	/** element item for DbtrAgt element. 
	 *  @serial
	 */	
	private BranchAndFinancialInstitutionIdentification5 m_dbtrAgt = null;
	
	/** element item for DbtrAgtAcct element. 
	 *  @serial
	 */	
	private CashAccount24 m_dbtrAgtAcct = null;
	
	/** element item for UltmtDbtr element. 
	 *  @serial
	 */	
	private PartyIdentification43 m_ultmtDbtr = null;
	
	/** element item for Purp element. 
	 *  @serial
	 */	
	private Purpose2Choice m_purp = null;
	
	/** list of RgltryRptg element. 
	 *  @serial
	 */	
	private List<RegulatoryReporting3> m_rgltryRptgList = new ArrayList<RegulatoryReporting3>();
	
	/** list of RltdRmtInf element. 
	 *  @serial
	 */	
	private List<RemittanceLocation4> m_rltdRmtInfList = new ArrayList<RemittanceLocation4>();
	
	/** element item for RmtInf element. 
	 *  @serial
	 */	
	private RemittanceInformation11 m_rmtInf = null;
	
	/** list of SplmtryData element. 
	 *  @serial
	 */	
	private List<SupplementaryData1> m_splmtryDataList = new ArrayList<SupplementaryData1>();
	
	/**
	 * Get the embedded PmtId element.
	 * @return the item.
	 */
	public PaymentIdentification3 getPmtId() {
		return m_pmtId;
	}
		
	/**
	 * This method sets (overwrites) the element PmtId.
	 * @param data the item that needs to be added.
	 */
	void setPmtId(PaymentIdentification3 data) {
		m_pmtId = data;
	}
		
	/**
	 * Get the embedded PmtTpInf element.
	 * @return the item.
	 */
	public PaymentTypeInformation25 getPmtTpInf() {
		return m_pmtTpInf;
	}
		
	/**
	 * This method sets (overwrites) the element PmtTpInf.
	 * @param data the item that needs to be added.
	 */
	void setPmtTpInf(PaymentTypeInformation25 data) {
		m_pmtTpInf = data;
	}
		
	/**
	 * Get the embedded IntrBkSttlmAmt element.
	 * @return the item.
	 */
	public ActiveCurrencyAndAmount getIntrBkSttlmAmt() {
		return m_intrBkSttlmAmt;
	}
		
	/**
	 * This method sets (overwrites) the element IntrBkSttlmAmt.
	 * @param data the item that needs to be added.
	 */
	void setIntrBkSttlmAmt(ActiveCurrencyAndAmount data) {
		m_intrBkSttlmAmt = data;
	}
		
	/**
	 * Get the embedded IntrBkSttlmDt element.
	 * @return the item.
	 */
	public String getIntrBkSttlmDt() {
		return m_intrBkSttlmDt;
	}
		
	/**
	 * This method sets (overwrites) the element IntrBkSttlmDt.
	 * @param data the item that needs to be added.
	 */
	void setIntrBkSttlmDt(String data) {
		m_intrBkSttlmDt = data;
	}
		
	/**
	 * Get the embedded SttlmPrty element.
	 * @return the item.
	 */
	public String getSttlmPrty() {
		return m_sttlmPrty;
	}
		
	/**
	 * This method sets (overwrites) the element SttlmPrty.
	 * @param data the item that needs to be added.
	 */
	void setSttlmPrty(String data) {
		m_sttlmPrty = data;
	}
		
	/**
	 * Get the embedded InstdAmt element.
	 * @return the item.
	 */
	public ActiveOrHistoricCurrencyAndAmount getInstdAmt() {
		return m_instdAmt;
	}
		
	/**
	 * This method sets (overwrites) the element InstdAmt.
	 * @param data the item that needs to be added.
	 */
	void setInstdAmt(ActiveOrHistoricCurrencyAndAmount data) {
		m_instdAmt = data;
	}
		
	/**
	 * Get the embedded XchgRate element.
	 * @return the item.
	 */
	public String getXchgRate() {
		return m_xchgRate;
	}
		
	/**
	 * This method sets (overwrites) the element XchgRate.
	 * @param data the item that needs to be added.
	 */
	void setXchgRate(String data) {
		m_xchgRate = data;
	}
		
	/**
	 * Get the embedded ChrgBr element.
	 * @return the item.
	 */
	public String getChrgBr() {
		return m_chrgBr;
	}
		
	/**
	 * This method sets (overwrites) the element ChrgBr.
	 * @param data the item that needs to be added.
	 */
	void setChrgBr(String data) {
		m_chrgBr = data;
	}
		
	/**
	 * Get the embedded list of ChrgsInf elements.
	 * @return list of items.
	 */
	public List<Charges2> getChrgsInfs() {
		return m_chrgsInfList;
	}
		
	/**
	 * This method adds data to the list of ChrgsInf.
	 * @param data the item that needs to be added.
	 */
	void setChrgsInf(Charges2 data) {
		m_chrgsInfList.add(data);
	}
		
	/**
	 * Get the embedded ReqdColltnDt element.
	 * @return the item.
	 */
	public String getReqdColltnDt() {
		return m_reqdColltnDt;
	}
		
	/**
	 * This method sets (overwrites) the element ReqdColltnDt.
	 * @param data the item that needs to be added.
	 */
	void setReqdColltnDt(String data) {
		m_reqdColltnDt = data;
	}
		
	/**
	 * Get the embedded DrctDbtTx element.
	 * @return the item.
	 */
	public DirectDebitTransaction8 getDrctDbtTx() {
		return m_drctDbtTx;
	}
		
	/**
	 * This method sets (overwrites) the element DrctDbtTx.
	 * @param data the item that needs to be added.
	 */
	void setDrctDbtTx(DirectDebitTransaction8 data) {
		m_drctDbtTx = data;
	}
		
	/**
	 * Get the embedded Cdtr element.
	 * @return the item.
	 */
	public PartyIdentification43 getCdtr() {
		return m_cdtr;
	}
		
	/**
	 * This method sets (overwrites) the element Cdtr.
	 * @param data the item that needs to be added.
	 */
	void setCdtr(PartyIdentification43 data) {
		m_cdtr = data;
	}
		
	/**
	 * Get the embedded CdtrAcct element.
	 * @return the item.
	 */
	public CashAccount24 getCdtrAcct() {
		return m_cdtrAcct;
	}
		
	/**
	 * This method sets (overwrites) the element CdtrAcct.
	 * @param data the item that needs to be added.
	 */
	void setCdtrAcct(CashAccount24 data) {
		m_cdtrAcct = data;
	}
		
	/**
	 * Get the embedded CdtrAgt element.
	 * @return the item.
	 */
	public BranchAndFinancialInstitutionIdentification5 getCdtrAgt() {
		return m_cdtrAgt;
	}
		
	/**
	 * This method sets (overwrites) the element CdtrAgt.
	 * @param data the item that needs to be added.
	 */
	void setCdtrAgt(BranchAndFinancialInstitutionIdentification5 data) {
		m_cdtrAgt = data;
	}
		
	/**
	 * Get the embedded CdtrAgtAcct element.
	 * @return the item.
	 */
	public CashAccount24 getCdtrAgtAcct() {
		return m_cdtrAgtAcct;
	}
		
	/**
	 * This method sets (overwrites) the element CdtrAgtAcct.
	 * @param data the item that needs to be added.
	 */
	void setCdtrAgtAcct(CashAccount24 data) {
		m_cdtrAgtAcct = data;
	}
		
	/**
	 * Get the embedded UltmtCdtr element.
	 * @return the item.
	 */
	public PartyIdentification43 getUltmtCdtr() {
		return m_ultmtCdtr;
	}
		
	/**
	 * This method sets (overwrites) the element UltmtCdtr.
	 * @param data the item that needs to be added.
	 */
	void setUltmtCdtr(PartyIdentification43 data) {
		m_ultmtCdtr = data;
	}
		
	/**
	 * Get the embedded InitgPty element.
	 * @return the item.
	 */
	public PartyIdentification43 getInitgPty() {
		return m_initgPty;
	}
		
	/**
	 * This method sets (overwrites) the element InitgPty.
	 * @param data the item that needs to be added.
	 */
	void setInitgPty(PartyIdentification43 data) {
		m_initgPty = data;
	}
		
	/**
	 * Get the embedded InstgAgt element.
	 * @return the item.
	 */
	public BranchAndFinancialInstitutionIdentification5 getInstgAgt() {
		return m_instgAgt;
	}
		
	/**
	 * This method sets (overwrites) the element InstgAgt.
	 * @param data the item that needs to be added.
	 */
	void setInstgAgt(BranchAndFinancialInstitutionIdentification5 data) {
		m_instgAgt = data;
	}
		
	/**
	 * Get the embedded InstdAgt element.
	 * @return the item.
	 */
	public BranchAndFinancialInstitutionIdentification5 getInstdAgt() {
		return m_instdAgt;
	}
		
	/**
	 * This method sets (overwrites) the element InstdAgt.
	 * @param data the item that needs to be added.
	 */
	void setInstdAgt(BranchAndFinancialInstitutionIdentification5 data) {
		m_instdAgt = data;
	}
		
	/**
	 * Get the embedded IntrmyAgt1 element.
	 * @return the item.
	 */
	public BranchAndFinancialInstitutionIdentification5 getIntrmyAgt1() {
		return m_intrmyAgt1;
	}
		
	/**
	 * This method sets (overwrites) the element IntrmyAgt1.
	 * @param data the item that needs to be added.
	 */
	void setIntrmyAgt1(BranchAndFinancialInstitutionIdentification5 data) {
		m_intrmyAgt1 = data;
	}
		
	/**
	 * Get the embedded IntrmyAgt1Acct element.
	 * @return the item.
	 */
	public CashAccount24 getIntrmyAgt1Acct() {
		return m_intrmyAgt1Acct;
	}
		
	/**
	 * This method sets (overwrites) the element IntrmyAgt1Acct.
	 * @param data the item that needs to be added.
	 */
	void setIntrmyAgt1Acct(CashAccount24 data) {
		m_intrmyAgt1Acct = data;
	}
		
	/**
	 * Get the embedded IntrmyAgt2 element.
	 * @return the item.
	 */
	public BranchAndFinancialInstitutionIdentification5 getIntrmyAgt2() {
		return m_intrmyAgt2;
	}
		
	/**
	 * This method sets (overwrites) the element IntrmyAgt2.
	 * @param data the item that needs to be added.
	 */
	void setIntrmyAgt2(BranchAndFinancialInstitutionIdentification5 data) {
		m_intrmyAgt2 = data;
	}
		
	/**
	 * Get the embedded IntrmyAgt2Acct element.
	 * @return the item.
	 */
	public CashAccount24 getIntrmyAgt2Acct() {
		return m_intrmyAgt2Acct;
	}
		
	/**
	 * This method sets (overwrites) the element IntrmyAgt2Acct.
	 * @param data the item that needs to be added.
	 */
	void setIntrmyAgt2Acct(CashAccount24 data) {
		m_intrmyAgt2Acct = data;
	}
		
	/**
	 * Get the embedded IntrmyAgt3 element.
	 * @return the item.
	 */
	public BranchAndFinancialInstitutionIdentification5 getIntrmyAgt3() {
		return m_intrmyAgt3;
	}
		
	/**
	 * This method sets (overwrites) the element IntrmyAgt3.
	 * @param data the item that needs to be added.
	 */
	void setIntrmyAgt3(BranchAndFinancialInstitutionIdentification5 data) {
		m_intrmyAgt3 = data;
	}
		
	/**
	 * Get the embedded IntrmyAgt3Acct element.
	 * @return the item.
	 */
	public CashAccount24 getIntrmyAgt3Acct() {
		return m_intrmyAgt3Acct;
	}
		
	/**
	 * This method sets (overwrites) the element IntrmyAgt3Acct.
	 * @param data the item that needs to be added.
	 */
	void setIntrmyAgt3Acct(CashAccount24 data) {
		m_intrmyAgt3Acct = data;
	}
		
	/**
	 * Get the embedded Dbtr element.
	 * @return the item.
	 */
	public PartyIdentification43 getDbtr() {
		return m_dbtr;
	}
		
	/**
	 * This method sets (overwrites) the element Dbtr.
	 * @param data the item that needs to be added.
	 */
	void setDbtr(PartyIdentification43 data) {
		m_dbtr = data;
	}
		
	/**
	 * Get the embedded DbtrAcct element.
	 * @return the item.
	 */
	public CashAccount24 getDbtrAcct() {
		return m_dbtrAcct;
	}
		
	/**
	 * This method sets (overwrites) the element DbtrAcct.
	 * @param data the item that needs to be added.
	 */
	void setDbtrAcct(CashAccount24 data) {
		m_dbtrAcct = data;
	}
		
	/**
	 * Get the embedded DbtrAgt element.
	 * @return the item.
	 */
	public BranchAndFinancialInstitutionIdentification5 getDbtrAgt() {
		return m_dbtrAgt;
	}
		
	/**
	 * This method sets (overwrites) the element DbtrAgt.
	 * @param data the item that needs to be added.
	 */
	void setDbtrAgt(BranchAndFinancialInstitutionIdentification5 data) {
		m_dbtrAgt = data;
	}
		
	/**
	 * Get the embedded DbtrAgtAcct element.
	 * @return the item.
	 */
	public CashAccount24 getDbtrAgtAcct() {
		return m_dbtrAgtAcct;
	}
		
	/**
	 * This method sets (overwrites) the element DbtrAgtAcct.
	 * @param data the item that needs to be added.
	 */
	void setDbtrAgtAcct(CashAccount24 data) {
		m_dbtrAgtAcct = data;
	}
		
	/**
	 * Get the embedded UltmtDbtr element.
	 * @return the item.
	 */
	public PartyIdentification43 getUltmtDbtr() {
		return m_ultmtDbtr;
	}
		
	/**
	 * This method sets (overwrites) the element UltmtDbtr.
	 * @param data the item that needs to be added.
	 */
	void setUltmtDbtr(PartyIdentification43 data) {
		m_ultmtDbtr = data;
	}
		
	/**
	 * Get the embedded Purp element.
	 * @return the item.
	 */
	public Purpose2Choice getPurp() {
		return m_purp;
	}
		
	/**
	 * This method sets (overwrites) the element Purp.
	 * @param data the item that needs to be added.
	 */
	void setPurp(Purpose2Choice data) {
		m_purp = data;
	}
		
	/**
	 * Get the embedded list of RgltryRptg elements.
	 * @return list of items.
	 */
	public List<RegulatoryReporting3> getRgltryRptgs() {
		return m_rgltryRptgList;
	}
		
	/**
	 * This method adds data to the list of RgltryRptg.
	 * @param data the item that needs to be added.
	 */
	void setRgltryRptg(RegulatoryReporting3 data) {
		m_rgltryRptgList.add(data);
	}
		
	/**
	 * Get the embedded list of RltdRmtInf elements.
	 * @return list of items.
	 */
	public List<RemittanceLocation4> getRltdRmtInfs() {
		return m_rltdRmtInfList;
	}
		
	/**
	 * This method adds data to the list of RltdRmtInf.
	 * @param data the item that needs to be added.
	 */
	void setRltdRmtInf(RemittanceLocation4 data) {
		m_rltdRmtInfList.add(data);
	}
		
	/**
	 * Get the embedded RmtInf element.
	 * @return the item.
	 */
	public RemittanceInformation11 getRmtInf() {
		return m_rmtInf;
	}
		
	/**
	 * This method sets (overwrites) the element RmtInf.
	 * @param data the item that needs to be added.
	 */
	void setRmtInf(RemittanceInformation11 data) {
		m_rmtInf = data;
	}
		
	/**
	 * Get the embedded list of SplmtryData elements.
	 * @return list of items.
	 */
	public List<SupplementaryData1> getSplmtryDatas() {
		return m_splmtryDataList;
	}
		
	/**
	 * This method adds data to the list of SplmtryData.
	 * @param data the item that needs to be added.
	 */
	void setSplmtryData(SupplementaryData1 data) {
		m_splmtryDataList.add(data);
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_pmtId, ((DirectDebitTransactionInformation20)that).m_pmtId))
			return false;
		
		if (!Compare.equals(m_pmtTpInf, ((DirectDebitTransactionInformation20)that).m_pmtTpInf))
			return false;
		
		if (!Compare.equals(m_intrBkSttlmAmt, ((DirectDebitTransactionInformation20)that).m_intrBkSttlmAmt))
			return false;
		
		if (!Compare.equals(m_intrBkSttlmDt, ((DirectDebitTransactionInformation20)that).m_intrBkSttlmDt))
			return false;
		
		if (!Compare.equals(m_sttlmPrty, ((DirectDebitTransactionInformation20)that).m_sttlmPrty))
			return false;
		
		if (!Compare.equals(m_instdAmt, ((DirectDebitTransactionInformation20)that).m_instdAmt))
			return false;
		
		if (!Compare.equals(m_xchgRate, ((DirectDebitTransactionInformation20)that).m_xchgRate))
			return false;
		
		if (!Compare.equals(m_chrgBr, ((DirectDebitTransactionInformation20)that).m_chrgBr))
			return false;
		
		if (!Compare.equals(m_chrgsInfList, ((DirectDebitTransactionInformation20)that).m_chrgsInfList))
			return false;
		
		if (!Compare.equals(m_reqdColltnDt, ((DirectDebitTransactionInformation20)that).m_reqdColltnDt))
			return false;
		
		if (!Compare.equals(m_drctDbtTx, ((DirectDebitTransactionInformation20)that).m_drctDbtTx))
			return false;
		
		if (!Compare.equals(m_cdtr, ((DirectDebitTransactionInformation20)that).m_cdtr))
			return false;
		
		if (!Compare.equals(m_cdtrAcct, ((DirectDebitTransactionInformation20)that).m_cdtrAcct))
			return false;
		
		if (!Compare.equals(m_cdtrAgt, ((DirectDebitTransactionInformation20)that).m_cdtrAgt))
			return false;
		
		if (!Compare.equals(m_cdtrAgtAcct, ((DirectDebitTransactionInformation20)that).m_cdtrAgtAcct))
			return false;
		
		if (!Compare.equals(m_ultmtCdtr, ((DirectDebitTransactionInformation20)that).m_ultmtCdtr))
			return false;
		
		if (!Compare.equals(m_initgPty, ((DirectDebitTransactionInformation20)that).m_initgPty))
			return false;
		
		if (!Compare.equals(m_instgAgt, ((DirectDebitTransactionInformation20)that).m_instgAgt))
			return false;
		
		if (!Compare.equals(m_instdAgt, ((DirectDebitTransactionInformation20)that).m_instdAgt))
			return false;
		
		if (!Compare.equals(m_intrmyAgt1, ((DirectDebitTransactionInformation20)that).m_intrmyAgt1))
			return false;
		
		if (!Compare.equals(m_intrmyAgt1Acct, ((DirectDebitTransactionInformation20)that).m_intrmyAgt1Acct))
			return false;
		
		if (!Compare.equals(m_intrmyAgt2, ((DirectDebitTransactionInformation20)that).m_intrmyAgt2))
			return false;
		
		if (!Compare.equals(m_intrmyAgt2Acct, ((DirectDebitTransactionInformation20)that).m_intrmyAgt2Acct))
			return false;
		
		if (!Compare.equals(m_intrmyAgt3, ((DirectDebitTransactionInformation20)that).m_intrmyAgt3))
			return false;
		
		if (!Compare.equals(m_intrmyAgt3Acct, ((DirectDebitTransactionInformation20)that).m_intrmyAgt3Acct))
			return false;
		
		if (!Compare.equals(m_dbtr, ((DirectDebitTransactionInformation20)that).m_dbtr))
			return false;
		
		if (!Compare.equals(m_dbtrAcct, ((DirectDebitTransactionInformation20)that).m_dbtrAcct))
			return false;
		
		if (!Compare.equals(m_dbtrAgt, ((DirectDebitTransactionInformation20)that).m_dbtrAgt))
			return false;
		
		if (!Compare.equals(m_dbtrAgtAcct, ((DirectDebitTransactionInformation20)that).m_dbtrAgtAcct))
			return false;
		
		if (!Compare.equals(m_ultmtDbtr, ((DirectDebitTransactionInformation20)that).m_ultmtDbtr))
			return false;
		
		if (!Compare.equals(m_purp, ((DirectDebitTransactionInformation20)that).m_purp))
			return false;
		
		if (!Compare.equals(m_rgltryRptgList, ((DirectDebitTransactionInformation20)that).m_rgltryRptgList))
			return false;
		
		if (!Compare.equals(m_rltdRmtInfList, ((DirectDebitTransactionInformation20)that).m_rltdRmtInfList))
			return false;
		
		if (!Compare.equals(m_rmtInf, ((DirectDebitTransactionInformation20)that).m_rmtInf))
			return false;
		
		if (!Compare.equals(m_splmtryDataList, ((DirectDebitTransactionInformation20)that).m_splmtryDataList))
			return false;
		
		return true;
	}	

  
  
}
