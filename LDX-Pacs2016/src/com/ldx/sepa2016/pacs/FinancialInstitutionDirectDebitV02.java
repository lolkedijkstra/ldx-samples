package com.ldx.sepa2016.pacs;


/******************************************************************************
  -----------------------------------------------------------------------------
  LDX+ XML to Java code generator
  -----------------------------------------------------------------------------
  
  Copyright (c)
	Dijkstra ICT Consulting - 2009-2016

	THIS IS NOT FREE SOFTWARE.
	Rights to commercially use this software are obtained by purchasing a
	license. We provide customization services on request. Please contact
	us for a quote.

	A FREE EVALUATION KIT IS AVAILABLE BY ENTERING AN EVALUATION
	AGREEMENT, WHICH CAN BE OBTAINED BY CONTACTING:

		http://www.xml2java.net
		email: dijkstra@xml2java.net
		support: support@xml2java.net
		phone: +31 6 800 47 205

  -----------------------------------------------------------------------------

  This code was generated using ldxg v. 2.2.0
  Generated code is compatible with ldx-framework v. 2.2
  License: Dijkstra ICT Consulting 
  Module: PACS 
  Generation date: Mon Apr 25 12:08:30 CEST 2016 

******************************************************************************/

import java.util.List;
import java.util.ArrayList;

import com.ldx.util.Compare;

import com.ldx.xml.core.ComplexDataType;

import com.ldx.xml.core.TypeAllocator;



/**
 * FinancialInstitutionDirectDebitV02 data class.
 *
 * This class provides getters and setters for embedded attributes and elements.
 * Any complex data structure can be navigated by using the element getter methods.
 * 
 * 
 */
public class FinancialInstitutionDirectDebitV02 extends ComplexDataType {

	/**
	 * default serial version UID 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for FinancialInstitutionDirectDebitV02.
	 *
	 * @param elementName the name of the originating XML tag
	 * @param parent the parent data
	 */
	public FinancialInstitutionDirectDebitV02(String elementName, ComplexDataType parent) {
		super(elementName, parent);
	}
  
	/**
	 * Allocator class.	
	 *
	 * This class implements the generic allocator interface that is used by the framework.
	 * The allocator is used by the framework to instantiate type FinancialInstitutionDirectDebitV02.
	 */
	static class Allocator implements TypeAllocator<FinancialInstitutionDirectDebitV02> {
		/**
		 * method for getting a new instance of type FinancialInstitutionDirectDebitV02.
		 *
		 * @param elementName the name of the originating XML tag
		 * @param parent the parent data
		 * @return new instance
		 */
		public FinancialInstitutionDirectDebitV02 newInstance(String elementName, ComplexDataType parent) {
			return new FinancialInstitutionDirectDebitV02(elementName, parent);
		}
	}
	
	/** instance of allocator class for this data class. */
	private static Allocator allocator = new Allocator();
	
	/**
	 * Get Allocator for this data class.
	 * This method is used by the handler class.
	 *
	 * @return instance of Allocator
	 */
	static public Allocator getAllocator() {
		return allocator;
	}
	
  	
  
	/** element item for GrpHdr element. 
	 *  @serial
	 */	
	private GroupHeader63 m_grpHdr = null;
	
	/** list of CdtInstr element. 
	 *  @serial
	 */	
	private List<CreditTransferTransaction9> m_cdtInstrList = new ArrayList<CreditTransferTransaction9>();
	
	/** list of SplmtryData element. 
	 *  @serial
	 */	
	private List<SupplementaryData1> m_splmtryDataList = new ArrayList<SupplementaryData1>();
	
	/**
	 * Get the embedded GrpHdr element.
	 * @return the item.
	 */
	public GroupHeader63 getGrpHdr() {
		return m_grpHdr;
	}
		
	/**
	 * This method sets (overwrites) the element GrpHdr.
	 * @param data the item that needs to be added.
	 */
	void setGrpHdr(GroupHeader63 data) {
		m_grpHdr = data;
	}
		
	/**
	 * Get the embedded list of CdtInstr elements.
	 * @return list of items.
	 */
	public List<CreditTransferTransaction9> getCdtInstrs() {
		return m_cdtInstrList;
	}
		
	/**
	 * This method adds data to the list of CdtInstr.
	 * @param data the item that needs to be added.
	 */
	void setCdtInstr(CreditTransferTransaction9 data) {
		m_cdtInstrList.add(data);
	}
		
	/**
	 * Get the embedded list of SplmtryData elements.
	 * @return list of items.
	 */
	public List<SupplementaryData1> getSplmtryDatas() {
		return m_splmtryDataList;
	}
		
	/**
	 * This method adds data to the list of SplmtryData.
	 * @param data the item that needs to be added.
	 */
	void setSplmtryData(SupplementaryData1 data) {
		m_splmtryDataList.add(data);
	}
		
  
	/**
	 * This method compares this and that.
	 * @return true if this and that are the same, false otherwise.	
	 */
	public boolean equals(Object that) {  
		if (!super.equals(that))
			return false;
  
		if (!Compare.equals(m_grpHdr, ((FinancialInstitutionDirectDebitV02)that).m_grpHdr))
			return false;
		
		if (!Compare.equals(m_cdtInstrList, ((FinancialInstitutionDirectDebitV02)that).m_cdtInstrList))
			return false;
		
		if (!Compare.equals(m_splmtryDataList, ((FinancialInstitutionDirectDebitV02)that).m_splmtryDataList))
			return false;
		
		return true;
	}	

  
  
}
