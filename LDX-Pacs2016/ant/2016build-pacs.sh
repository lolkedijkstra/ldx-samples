java -jar "$LDX_HOME/ldxg.jar" -cconf/2016pacscfg.xml -p

echo STARTING BUILD PACS2016

if [ $1 ]
then
	if [ $2 ]
	then
		echo Building $2.jar with main class: com.ldx.sepa2016.pacs.application.$1
		ant runnable-jar -f 2016build-pacs.xml -Dmain-class=com.ldx.sepa2016.pacs.application.$1 -Djar.name=$2.jar -f 2016build-pacs.xml
		mv pain/lib/$2.jar ./$2.jar
	else
		echo Building $1.jar with main class: com.ldx.sepa2016.pacs.application.$1
		ant runnable-jar -f 2016build-pacs.xml -Dmain-class=com.ldx.sepa2016.pacs.application.$1 -Djar.name=$1.jar -f 2016build-pacs.xml
		mv pain/lib/$1.jar ./$1.jar
	fi
else
	echo Building pacs2016.jar..
	ant all -Djar.name=pacs2016.jar -f 2016build-pacs.xml
fi
